//This program demonstrates the declaration and initialization of 
//primitive data types

public class sampleDataTypes
{
	public static void main (String args[])
	{
		//primitive data types
		int wholeNum = 10;		//declare and initialize a variable of type integer
		double realNum = 3.5;		//declare and initialize a variable of type double
		char initial = 'k';		//declare and initialize a variable of type char
		boolean OK = true;		//declare a variable of type boolean

		double answer;			//I'm not going to initialize this

		//variables are initialized

		System.out.println ("The integer is: " + wholeNum);
		System.out.println ("The double is: " + realNum);
		System.out.println ("The char is: " + initial);
		System.out.println ("The bool is: " + OK);
		System.out.println ();
		System.out.println ();
		System.out.println ();

		answer = wholeNum + realNum + 16;

		System.out.println ("The answer is: " + answer);
	}
}
