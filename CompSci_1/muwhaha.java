import javax.swing.JOptionPane;

public class muwhaha
{
	public static void main (String args[])
	{
		final int YEAR = 2004;

		String name;
		String howOld;		//all inputs are strings
		String secondNumber;

		int age, yearBorn;	//so we can do the math

		//get the input
		name = JOptionPane.showInputDialog ("What is your name?" );
		howOld = JOptionPane.showInputDialog ("How old are you?");


		if ((howOld == null) || (name == null))
		{
			JOptionPane.showMessageDialog(null,
					"You did not enter all information",
					"ERROR",
					JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		if ((howOld != null) && (name != null))
		{	
			//convert String to int
			age = Integer.parseInt (howOld);

			//find the answer
			yearBorn = YEAR - age;

			//display the results
			JOptionPane.showMessageDialog(null,
					"You were born in " + yearBorn, name,
					JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}

	}
}
