import javax.swing.JOptionPane;

public class change
{
	public static void main (String args[])
	{
		EasyReader in = new EasyReader();

		String changeS;
		int change, temp, dolOne, dolFive, dolTen, dolTwenty, dolHundred, q, n, d, p;


		changeS = JOptionPane.showInputDialog ("How much change do you need?");
		change = Integer.parseInt (changeS);

		dolHundred = change / 10000;
		temp = change % 10000;
		dolTwenty = temp / 2000;
		temp = change % 2000;
		dolTen = temp / 1000;
		temp = change % 1000;
		dolFive = temp  /500;
		temp = change % 500;
		dolOne = temp / 100;
		temp=change % 100;
		q = temp / 25;
		temp = change % 25;
		d = temp / 10;
		temp = temp % 10;
		n = temp / 5;
		temp = temp % 5;
		p = temp / 1;

		System.out.println ("Dollars:\nOnes: " + dolOne + "\nFives: "  + dolFive + "\nTens: "  + dolTen + "\nTwenties: "  + dolTwenty + "\nHundreds: "  + dolHundred + "\nQuaters: " + q + "\nNickles: " + n + "\nDimes: " + d + "\nPennies: " + p);
	}
}
