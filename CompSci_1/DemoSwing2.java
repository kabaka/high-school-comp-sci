import javax.swing.JOptionPane;

public class DemoSwing2{

	public static void main (String args[])
	{

		String firstNumber;	//all inputs are strings
		String secondNumber;

		int num1, num2, sum;	//so we can do the math


		//get the input
		firstNumber = JOptionPane.showInputDialog("Enter the first value to be added: ");
		secondNumber = JOptionPane.showInputDialog("Enter the second value to be added: ");


		//convert String to int
		num1 = Integer.parseInt(firstNumber);
		num2 = Integer.parseInt(secondNumber);

		//find the answer
		sum = num1 + num2;

		//display the results
		JOptionPane.showMessageDialog(null,
				"The sum is " + sum,
				"See the answer in the box!",
				JOptionPane.PLAIN_MESSAGE);

		System.exit(0);

	}
}
