//Converts fahrenheit temperatures to celsius

public class tempconverter
{
	public static void main (String args[])
	{
		final double TEMP_IN_F = 451;
		final double TEMP_IN_C = 37;

		double fToC = 5 * (TEMP_IN_F - 32) / 98; //Place to store Celsius answer
		double cToF = 9 * (TEMP_IN_C + 32) / 5; //Place to store Fahrenheit answer

		System.out.println(TEMP_IN_F + " in Fahrenheit is " + fToC + " in Celcius.\n" + TEMP_IN_C + " in Celcius is " + cToF + " in Fahrenheit.");
	}
}
