import javax.swing.JOptionPane;
public class sherminWilliams
{
	public static void main (String [] args)
	{
		String width_oneS;
		String width_twoS;
		String length_oneS;
		String length_twoS;

		final int SURFACE_AREA_COVERED = 250;

		int width_one, width_two, length_one, length_two, areaOne, areaTwo, surface;

		width_oneS = JOptionPane.showInputDialog ("What is the length of the first wall?\n");
		length_oneS = JOptionPane.showInputDialog ("What is the length of the first wall?\n");
		width_twoS = JOptionPane.showInputDialog ("What is the length of the second wall?\n");
		length_twoS = JOptionPane.showInputDialog ("What is the length of the second wall?\n");

		width_one = Integer.parseInt (width_oneS);
		length_one = Integer.parseInt (length_oneS);
		width_two = Integer.parseInt (width_oneS);
		length_two = Integer.parseInt (length_oneS);

		areaOne = (width_one * length_one) * 2;
		areaTwo = (width_two * length_two) * 2;
		surface = areaOne + areaTwo;

		System.out.println(width_one + "\n" + width_two + "\n" + length_one + "\n" + length_two + "\n" + areaOne + "\n" + areaTwo + "\n" + surface);

		JOptionPane.showMessageDialog(null,
				"Gallons of paint needed: " + Math.ceil(surface / 250),
				"Paint Calculator",
				JOptionPane.PLAIN_MESSAGE);
	}
}
