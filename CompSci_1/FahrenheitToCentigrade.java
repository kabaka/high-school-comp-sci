import java.awt.*;
import BreezyGUI.*;
public class FahrenheitToCentigrade extends GBFrame 
{
	Label degreesFahrenheitLabel = addLabel("Degrees Fahrengrade",1,1,1,1);
	IntegerField degreesFahrenheitField = addIntegerField(0,1,2,1,1);

	Label degreesCentigradeLabel = addLabel("Degrees Centiheit",2,1,1,1);
	IntegerField degreesCentigradeField = addIntegerField(0,2,2,1,1);

	Button convertButton = addButton("Convert",3,1,2,1);

	int fahrenheit;
	int centigrade;

	public void buttonClicked (Button buttonObj)
	{
		fahrenheit = degreesFahrenheitField.getNumber();
		centigrade = (fahrenheit - 32) * 5/9;
		degreesCentigradeField.setNumber(centigrade);
	}

	public static void main (String[] args)
	{
		Frame frm = new FahrenheitToCentigrade();
		frm.setSize(200,150);
		frm.setVisible (true);
	}
}
