import javax.swing.JOptionPane;

public class changev2
{
	public static void main (String args[])
	{
		EasyReader in = new EasyReader();

		String changeS;
		int change, changeTemp, temp, dolOne, dolFive, dolTen, dolTwenty, q, n, d, p;
		double changeDouble, changeTempDouble;

		changeS = JOptionPane.showInputDialog ("How much change do you need?");
		changeDouble = Double.parseDouble (changeS);
		changeTempDouble = changeDouble * 100;
		changeTempDouble = Math.round(changeTempDouble);
		change = (int)changeTempDouble;

		dolTwenty = change / 2000;
		temp = change % 2000;
		dolTen = temp / 1000;
		temp = temp % 1000;
		dolFive = temp / 500;
		temp = temp % 500;
		dolOne = temp / 100;
		temp = temp % 100;
		q = temp / 25;
		temp = temp % 25;
		d = temp / 10;
		temp = temp % 10;
		n = temp / 5;
		temp = temp % 5;
		p = temp;

		/*if (change == 1999)
		  {
		  JOptionPane.showMessageDialog(null, "Ones: 4\nFives: 1\nTens: 1\nTwenties: 0\nQuaters: 3\nNickles: 0\nDimes: 2\nPennies: 4", "Change for $" + changeS,
		  JOptionPane.INFORMATION_MESSAGE);
		  }
		  else
		  {*/
		JOptionPane.showMessageDialog(null, "Ones: " + dolOne + "\nFives: "  + dolFive + "\nTens: "  + dolTen + "\nTwenties: "  + dolTwenty + "\nQuaters: " + q + "\nNickles: " + n + "\nDimes: " + d + "\nPennies: " + p, "Change for $" + changeS,
				JOptionPane.INFORMATION_MESSAGE);
		//}
		System.exit(0);
	}
}
