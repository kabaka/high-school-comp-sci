import javax.swing.JOptionPane;
public class StringFun_v2
{
	public static void main (String [] args)
	{
		String name;
		String lastName;
		String firstInitial;
		String userName;

		int lastNameIndex;

		name = JOptionPane.showInputDialog ("What is your name?\n"); //get strings

		name = name.toLowerCase();
		lastNameIndex = name.indexOf(" ") + 1;
		lastName = name.substring(lastNameIndex);
		firstInitial = name.substring(0,1);
		userName = firstInitial + lastName;;


		JOptionPane.showMessageDialog(null,
				"User Name: " + userName, "String Fun! Version 2",
				JOptionPane.PLAIN_MESSAGE);
		System.exit(0);
	}
}
