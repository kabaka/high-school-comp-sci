import javax.swing.JOptionPane;

public class DemoSwing3{

	public static void main (String args[])
	{

		String firstNumber;	//all inputs are strings
		String secondNumber;

		int num1, num2, sum;	//so we can do the math


		//get the input
		firstNumber = JOptionPane.showInputDialog("Enter the first value to be added: ");
		secondNumber = JOptionPane.showInputDialog("Enter the second value to be added: ");


		//convert String to int
		num1 = Integer.parseInt(firstNumber);
		num2 = Integer.parseInt(secondNumber);

		//find the answer
		sum = num1 + num2;

		//display the results
		JOptionPane.showMessageDialog(null,
				"The sum is " + sum,
				"See the answer in the box!",
				JOptionPane.PLAIN_MESSAGE);

		JOptionPane.showMessageDialog(null,
				"Notice the stop sign ",
				"See the answer in the box!",
				JOptionPane.ERROR_MESSAGE);

		JOptionPane.showMessageDialog(null,
				"Now it is an  i  for information ",
				"See the answer in the box!",
				JOptionPane.INFORMATION_MESSAGE);

		JOptionPane.showMessageDialog(null,
				"Oh, no! Exclamation point!",
				"See the answer in the box!",
				JOptionPane.WARNING_MESSAGE);

		JOptionPane.showMessageDialog(null,
				"This is getting silly",
				"See the answer in the box!",
				JOptionPane.QUESTION_MESSAGE);

		System.exit(0);

	}
}
