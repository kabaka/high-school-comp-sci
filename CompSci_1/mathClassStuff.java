//import 
public class mathClassStuff
{
	public static void main (String [] args)
	{
		float numberF = 1000000000;
		double numberD = 1000000000;

		System.out.println("Float Number: " + numberF);
		System.out.println(Math.abs(numberF));
		System.out.println(Math.cos(numberF));
		System.out.println(Math.tan(numberF));
		System.out.println(Math.sqrt(numberF));
		System.out.println(Math.floor(numberF));
		System.out.println("Double Number: " + numberD);
		System.out.println(Math.abs(numberD));
		System.out.println(Math.cos(numberD));
		System.out.println(Math.tan(numberD));
		System.out.println(Math.sqrt(numberD));
		System.out.println(Math.floor(numberD));
	}
}
