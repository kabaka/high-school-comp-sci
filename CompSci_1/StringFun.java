import javax.swing.JOptionPane;
public class StringFun
{
	public static void main (String [] args)
	{
		String name;
		String monthBorn;
		String nameLwrCase;
		String concatStrings;

		int nameLength, monthBornLength/*, nameIndex*/;

		name = JOptionPane.showInputDialog("What is your name?\n"); //get strings
		monthBorn = JOptionPane.showInputDialog("What month were you born in?\n");

		nameLength = name.length();
		monthBornLength = monthBorn.length();
		nameLwrCase = name.toLowerCase();
		nameIndex = indexOf(name);

		JOptionPane.showMessageDialog(null,
				"Your name: " + name + "\nLength of your name: " + nameLength + "\n\nMonth you were born in: " + monthBorn + "\nLength of the month you were born in: " + monthBornLength/* + "\nName in lower case: " + nameLwrCase*/, "String Fun!",
				JOptionPane.PLAIN_MESSAGE);

		System.exit(0);
	}
}
