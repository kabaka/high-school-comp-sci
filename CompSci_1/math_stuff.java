public class math_stuff
{
	public static void main (String args[])
	{
		EasyReader in = new EasyReader();

		double width, length, perimeter, area;
		double gasTank, milesOnTank, mpg;
		double tests, quizzes, testA, testB, testC, testD, quizA, quizB, quizC, gpa;

		System.out.println ("What is the width of the room?");
		width = in.readDouble(); // Get the length of the room
		System.out.println ("What is the length of the room?");
		length = in.readDouble(); // Get the width of the room

		perimeter = (2 * width) + (2 * length);
		area = width * length;

		System.out.println ("Perimeter: " + perimeter + "\nArea: " + area);

		System.out.println ("What is the size of your gas tank?");
		gasTank = in.readDouble(); // Get the size of the gas tank
		System.out.println ("How many miles can you go on a tank of gas?");
		milesOnTank = in.readDouble(); // Get the miles can the user go on a tank of gas

		mpg = milesOnTank / gasTank;

		System.out.println ("Miles Per Gallor: " + mpg);

		// GPA: Tests count as 60% and quizzes as 40%

		System.out.println ("What is the grade you got on one test?");
		testA = in.readDouble(); // Get test scores
		System.out.println ("What is the grade you got on another test?");
		testB = in.readDouble();
		System.out.println ("What is the grade you got on another test?");
		testC = in.readDouble();
		System.out.println ("And what is the grade you got on another test?");
		testD = in.readDouble(); // Get quiz scores
		System.out.println ("What is the grade you got on one quiz?");
		quizA = in.readDouble();
		System.out.println ("What is the grade you got on another quiz?");
		quizB = in.readDouble();
		System.out.println ("What is the grade you got on another quiz?");
		quizC = in.readDouble();

		tests = ((testA + testB + testC + testD) /4 / 100) * 60;
		quizzes = ((quizA + quizB + quizC) /3 / 100) * 40;
		gpa = (tests + quizzes) / 25;

		System.out.println ("GPA: " + gpa);


	}
}
