public class favorite_number 
{
	public static void main (String args[])
	{
		EasyReader in = new EasyReader();

		double fav_numb;
		double fav_numb_two;
		double sum;
		double diff;
		double prod;
		double quot;

		System.out.println ("What is your favorite number?");
		fav_numb = in.readDouble(); // Get the user's favorite number
		System.out.println ("What is your second favorite number?");
		fav_numb_two = in.readDouble(); // Get the user's second favorite number

		sum = fav_numb + fav_numb_two;
		diff = fav_numb - fav_numb_two;
		prod = fav_numb * fav_numb_two;
		quot = fav_numb / fav_numb_two;

		System.out.println ("User entered: " + fav_numb + " " + fav_numb_two);
		System.out.println ("Sum: " + sum);
		System.out.println ("Difference: " + diff);
		System.out.println ("Product: " + prod);
		System.out.println ("Quotient:" + quot);
	}
}
