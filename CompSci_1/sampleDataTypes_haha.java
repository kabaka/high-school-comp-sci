//This program demonstrates the declaration and initialization of 
//primitive data types

public class sampleDataTypes_haha
{
	public static void main (String args[])
	{
		//primitive data types
		int answer = 1;
		int runcount = 0;

		System.out.println("==========================================");
		System.out.println("| Number | Value                         |");

		while(answer < 8000000)
		{		
			answer = answer*2;
			runcount = runcount + 1;
			//if ()
			System.out.println ("| " + runcount + " | " + answer + "                         |");
		}
		System.out.println("==========================================");
	}
}
