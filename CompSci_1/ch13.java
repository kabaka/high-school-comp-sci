import java.awt.*;
import BreezyGUI.*;
public class ch13 extends GBFrame 
{

	Label itemLabel = addLabel("Item",1,1,1,1);
	TextField itemField = addTextField("",1,2,1,1);

	Label priceLabel = addLabel("Price",2,2,1,1);
	DoubleField priceField = addDoubleField(0,2,2,1,1);

	//Checkbox overnightBox = addCheckbox ("Overnight", 1, 1, 1, 1);

	Label overnightLabel = addLabel("Overnight (1 for yes)",3,1,1,1);
	DoubleField overnightField = addDoubleField(0,3,2,1,1);

	Button totalButton = addButton("Total",7,8,2,1);

	Label shippingLabel = addLabel("Shipping:   ",5,1,1,1);
	DoubleField shippingField = addDoubleField(0,5,2,1,1);

	Label totalLabel = addLabel("Total:      ",6,1,1,1);
	DoubleField totalField = addDoubleField(0,6,2,1,1);



	int cost, total, shipping, overnight;

	public void buttonClicked (Button buttonObj)
	{
		cost = 0;
		total = 0;
		shipping = 0;
		overnight = 0;

		cost = priceField.getNumber();
		overnight = overnightField.getNumber();
		total = total + cost;

		if (cost < 10)
		{
			total = total + 2;
			shipping = 2;
		}

		if (cost > 10)
		{
			total = total + 3;
			shipping = shipping + 3;
		}

		if (overnight == 1)
		{
			total = total + 5;
			shipping = shipping + 5;
		}

		shippingField.setNumber(shipping);
		totalField.setNumber(total);

	}

	public static void main (String[] args)
	{
		Frame frm = new ch13();
		frm.setSize(200,190);
		frm.setVisible (true);
	}
}
