import javax.swing.JOptionPane;
import BreezySwing.*;
 
public class chTwelveEx
{
	public static void main (String [] args)
	{

		String purchaseString, boltsString, nutsString, washersString, gasCapacityString, gasGaugeString, mpgString, getGas, weightString, canCompete;
		int bolts, nuts, washers, gasCapacity, gasGauge, mpg, weight;
		double purchase, finalCost, gasLeft, milesCanGo;

		Object[] possibleValues = { "Excercise 1",
			"Excercise 2", "Excercise 3", "Excercise 4" };

		Object selectedValue = JOptionPane.showInputDialog(null, 
				"Choose one", "Input",
				JOptionPane.INFORMATION_MESSAGE, null,
				possibleValues, possibleValues[0]);



		purchaseString = JOptionPane.showInputDialog("Enter the cost of the purchase.");

		purchase = Double.parseDouble(purchaseString);

		if (purchase > 10)
		{
			finalCost = purchase - (purchase / 10);
		}
		else
		{
			finalCost = purchase;
		}

		JOptionPane.showMessageDialog(null,
				"Final cost: $" + finalCost,
				"Chapter 12",
				JOptionPane.INFORMATION_MESSAGE);



		boltsString = JOptionPane.showInputDialog("How many bolts are there?");
		nutsString = JOptionPane.showInputDialog("How many nuts are there?");
		washersString = JOptionPane.showInputDialog("How many washers are there?");

		bolts = Integer.parseInt(boltsString);
		nuts = Integer.parseInt(nutsString);
		washers = Integer.parseInt(washersString);

		finalCost = ((bolts * 5) + (nuts * 3) + washers) / 100.0;

		JOptionPane.showMessageDialog(null,
				"Final cost: $" + finalCost,
				"Chapter 12",
				JOptionPane.INFORMATION_MESSAGE);



		gasCapacityString = JOptionPane.showInputDialog("How big is your gas tank?");
		gasGaugeString = JOptionPane.showInputDialog("What does the gauge read? (percent)");
		mpgString = JOptionPane.showInputDialog("How many miles per gallon does your car get?");

		gasCapacity = Integer.parseInt(gasCapacityString);
		gasGauge = Integer.parseInt(gasGaugeString);
		mpg = Integer.parseInt(mpgString);

		gasLeft = gasCapacity * (gasGauge/100.0);
		milesCanGo = gasLeft * mpg;

		if (milesCanGo > 200)
			getGas = "Don't get gas.";
		else
			getGas = "Get gas!";

		JOptionPane.showMessageDialog(null,
				"Tank capacity: " + gasCapacity + "\nGauge reading: " + gasGauge + "\nMiles Per Gallon: " + mpg + "\n" + getGas,
				"Chapter 12",
				JOptionPane.INFORMATION_MESSAGE);


		weightString = JOptionPane.showInputDialog("What is the contestant's weight?");

		weight = Integer.parseInt(weightString);

		if (weight > 220 && weight < 280)
			canCompete = "Can compete.";
		else
			canCompete = "Cannot compete.";

		JOptionPane.showMessageDialog(null, canCompete, "Chapter 12",
				JOptionPane.INFORMATION_MESSAGE);



		System.exit(0);
	}
}
