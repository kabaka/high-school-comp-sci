Imports System.io
Imports System.text
Public Class Form1

    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblWord As System.Windows.Forms.Label
    Friend WithEvents btnPlay As System.Windows.Forms.Button
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblWord = New System.Windows.Forms.Label
        Me.btnPlay = New System.Windows.Forms.Button
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.SuspendLayout()
        '
        'lblWord
        '
        Me.lblWord.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWord.Location = New System.Drawing.Point(8, 24)
        Me.lblWord.Name = "lblWord"
        Me.lblWord.Size = New System.Drawing.Size(248, 64)
        Me.lblWord.TabIndex = 0
        '
        'btnPlay
        '
        Me.btnPlay.Location = New System.Drawing.Point(88, 96)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.TabIndex = 1
        Me.btnPlay.Text = "&Play"
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Add text file to word list"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "E&xit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(264, 134)
        Me.Controls.Add(Me.btnPlay)
        Me.Controls.Add(Me.lblWord)
        Me.Name = "Form1"
        Me.Text = "Word Guessing Game"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim CurrentWord As String
    Dim InputStr, temp, wordlist, wordListArr(1000) As String, i, wordCount As Integer
    Dim ThisOneRight(1000), Done As Boolean
    Dim reader As New StreamReader("wordlist.txt")
    Dim rand As New System.Random(Now.Millisecond)

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPlay.Click
        InputStr = " "
        GetRandomWord()
        While Not InputStr = ""
            CheckAll()
            If Done = True Then Exit While
            InputStr = InputBox("Enter a letter (or type GUESS to guess the word)", "Make a guess!").ToUpper
            If InputStr = "GUESS" Then
                InputStr = InputBox("Guess the entire word", "Make a guess!")
                If InputStr = CurrentWord Then
                    lblWord.Text = CurrentWord
                    Exit While
                End If
            ElseIf InputStr = "" Then
                Exit While
            Else
                For i = 0 To CurrentWord.Length - 1
                    If InputStr = CurrentWord.Chars(i) And ThisOneRight(i) = False Then
                        temp = lblWord.Text
                        lblWord.Text = temp.Substring(0, i) & CurrentWord.Chars(i) & temp.Substring(i + 1)
                        ThisOneRight(i) = True
                    End If
                Next

            End If

        End While
    End Sub

    Sub CheckAll()
        Dim t, doneCheck As Integer, check As Boolean
        doneCheck = 0
        For t = 0 To CurrentWord.Length - 1
            If ThisOneRight(t) = True Then doneCheck += 1
        Next
        If doneCheck = 6 Then Done = True
    End Sub

    Sub GetRandomWord()
        Dim t As Integer
        For t = 0 To 99
            ThisOneRight(t) = False
        Next

        Dim index As Integer
        index = rand.Next(0, wordCount)

        CurrentWord = wordListArr(index)
        lblWord.Text = ""
        For t = 0 To CurrentWord.Length - 1
            lblWord.Text = lblWord.Text & "-"
        Next
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim t As Integer
        For t = 0 To 99
            ThisOneRight(t) = False
        Next

        wordlist = reader.ReadToEnd
        For t = 0 To wordlist.Length - 1
            If wordlist.Chars(t) = ";" Then wordCount += 1
        Next

        wordListArr = Split(wordlist, ";")

    End Sub

    Sub quit()
        reader.Close()
        Application.Exit()
    End Sub

    Private Sub Form1_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        quit()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        quit()
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        Dim filename As String
        OpenFileDialog1.DefaultExt = "txt"
        OpenFileDialog1.InitialDirectory = Environment.SpecialFolder.Desktop


        Dim ExternalReader As New StreamReader(FileName)
    End Sub
End Class
