Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnSolve As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents a8 As System.Windows.Forms.CheckBox
    Friend WithEvents a7 As System.Windows.Forms.CheckBox
    Friend WithEvents a5 As System.Windows.Forms.CheckBox
    Friend WithEvents a2 As System.Windows.Forms.CheckBox
    Friend WithEvents a4 As System.Windows.Forms.CheckBox
    Friend WithEvents a6 As System.Windows.Forms.CheckBox
    Friend WithEvents a3 As System.Windows.Forms.CheckBox
    Friend WithEvents a1 As System.Windows.Forms.CheckBox
    Friend WithEvents b1 As System.Windows.Forms.CheckBox
    Friend WithEvents b3 As System.Windows.Forms.CheckBox
    Friend WithEvents b2 As System.Windows.Forms.CheckBox
    Friend WithEvents b4 As System.Windows.Forms.CheckBox
    Friend WithEvents b6 As System.Windows.Forms.CheckBox
    Friend WithEvents b5 As System.Windows.Forms.CheckBox
    Friend WithEvents b7 As System.Windows.Forms.CheckBox
    Friend WithEvents b8 As System.Windows.Forms.CheckBox
    Friend WithEvents c1 As System.Windows.Forms.CheckBox
    Friend WithEvents c3 As System.Windows.Forms.CheckBox
    Friend WithEvents c2 As System.Windows.Forms.CheckBox
    Friend WithEvents c4 As System.Windows.Forms.CheckBox
    Friend WithEvents c6 As System.Windows.Forms.CheckBox
    Friend WithEvents c5 As System.Windows.Forms.CheckBox
    Friend WithEvents c7 As System.Windows.Forms.CheckBox
    Friend WithEvents c8 As System.Windows.Forms.CheckBox
    Friend WithEvents d1 As System.Windows.Forms.CheckBox
    Friend WithEvents d3 As System.Windows.Forms.CheckBox
    Friend WithEvents d2 As System.Windows.Forms.CheckBox
    Friend WithEvents d4 As System.Windows.Forms.CheckBox
    Friend WithEvents d6 As System.Windows.Forms.CheckBox
    Friend WithEvents d5 As System.Windows.Forms.CheckBox
    Friend WithEvents d7 As System.Windows.Forms.CheckBox
    Friend WithEvents d8 As System.Windows.Forms.CheckBox
    Friend WithEvents h1 As System.Windows.Forms.CheckBox
    Friend WithEvents h3 As System.Windows.Forms.CheckBox
    Friend WithEvents h2 As System.Windows.Forms.CheckBox
    Friend WithEvents h4 As System.Windows.Forms.CheckBox
    Friend WithEvents h6 As System.Windows.Forms.CheckBox
    Friend WithEvents h5 As System.Windows.Forms.CheckBox
    Friend WithEvents h7 As System.Windows.Forms.CheckBox
    Friend WithEvents h8 As System.Windows.Forms.CheckBox
    Friend WithEvents g1 As System.Windows.Forms.CheckBox
    Friend WithEvents g3 As System.Windows.Forms.CheckBox
    Friend WithEvents g2 As System.Windows.Forms.CheckBox
    Friend WithEvents g4 As System.Windows.Forms.CheckBox
    Friend WithEvents g6 As System.Windows.Forms.CheckBox
    Friend WithEvents g5 As System.Windows.Forms.CheckBox
    Friend WithEvents g7 As System.Windows.Forms.CheckBox
    Friend WithEvents g8 As System.Windows.Forms.CheckBox
    Friend WithEvents f1 As System.Windows.Forms.CheckBox
    Friend WithEvents f3 As System.Windows.Forms.CheckBox
    Friend WithEvents f2 As System.Windows.Forms.CheckBox
    Friend WithEvents f4 As System.Windows.Forms.CheckBox
    Friend WithEvents f6 As System.Windows.Forms.CheckBox
    Friend WithEvents f5 As System.Windows.Forms.CheckBox
    Friend WithEvents f7 As System.Windows.Forms.CheckBox
    Friend WithEvents f8 As System.Windows.Forms.CheckBox
    Friend WithEvents e1 As System.Windows.Forms.CheckBox
    Friend WithEvents e3 As System.Windows.Forms.CheckBox
    Friend WithEvents e2 As System.Windows.Forms.CheckBox
    Friend WithEvents e4 As System.Windows.Forms.CheckBox
    Friend WithEvents e6 As System.Windows.Forms.CheckBox
    Friend WithEvents e5 As System.Windows.Forms.CheckBox
    Friend WithEvents e7 As System.Windows.Forms.CheckBox
    Friend WithEvents e8 As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.a8 = New System.Windows.Forms.CheckBox
        Me.a7 = New System.Windows.Forms.CheckBox
        Me.a5 = New System.Windows.Forms.CheckBox
        Me.a2 = New System.Windows.Forms.CheckBox
        Me.a4 = New System.Windows.Forms.CheckBox
        Me.a6 = New System.Windows.Forms.CheckBox
        Me.a3 = New System.Windows.Forms.CheckBox
        Me.a1 = New System.Windows.Forms.CheckBox
        Me.b1 = New System.Windows.Forms.CheckBox
        Me.b3 = New System.Windows.Forms.CheckBox
        Me.b2 = New System.Windows.Forms.CheckBox
        Me.b4 = New System.Windows.Forms.CheckBox
        Me.b6 = New System.Windows.Forms.CheckBox
        Me.b5 = New System.Windows.Forms.CheckBox
        Me.b7 = New System.Windows.Forms.CheckBox
        Me.b8 = New System.Windows.Forms.CheckBox
        Me.c1 = New System.Windows.Forms.CheckBox
        Me.c3 = New System.Windows.Forms.CheckBox
        Me.c2 = New System.Windows.Forms.CheckBox
        Me.c4 = New System.Windows.Forms.CheckBox
        Me.c6 = New System.Windows.Forms.CheckBox
        Me.c5 = New System.Windows.Forms.CheckBox
        Me.c7 = New System.Windows.Forms.CheckBox
        Me.c8 = New System.Windows.Forms.CheckBox
        Me.d1 = New System.Windows.Forms.CheckBox
        Me.d3 = New System.Windows.Forms.CheckBox
        Me.d2 = New System.Windows.Forms.CheckBox
        Me.d4 = New System.Windows.Forms.CheckBox
        Me.d6 = New System.Windows.Forms.CheckBox
        Me.d5 = New System.Windows.Forms.CheckBox
        Me.d7 = New System.Windows.Forms.CheckBox
        Me.d8 = New System.Windows.Forms.CheckBox
        Me.h1 = New System.Windows.Forms.CheckBox
        Me.h3 = New System.Windows.Forms.CheckBox
        Me.h2 = New System.Windows.Forms.CheckBox
        Me.h4 = New System.Windows.Forms.CheckBox
        Me.h6 = New System.Windows.Forms.CheckBox
        Me.h5 = New System.Windows.Forms.CheckBox
        Me.h7 = New System.Windows.Forms.CheckBox
        Me.h8 = New System.Windows.Forms.CheckBox
        Me.g1 = New System.Windows.Forms.CheckBox
        Me.g3 = New System.Windows.Forms.CheckBox
        Me.g2 = New System.Windows.Forms.CheckBox
        Me.g4 = New System.Windows.Forms.CheckBox
        Me.g6 = New System.Windows.Forms.CheckBox
        Me.g5 = New System.Windows.Forms.CheckBox
        Me.g7 = New System.Windows.Forms.CheckBox
        Me.g8 = New System.Windows.Forms.CheckBox
        Me.f1 = New System.Windows.Forms.CheckBox
        Me.f3 = New System.Windows.Forms.CheckBox
        Me.f2 = New System.Windows.Forms.CheckBox
        Me.f4 = New System.Windows.Forms.CheckBox
        Me.f6 = New System.Windows.Forms.CheckBox
        Me.f5 = New System.Windows.Forms.CheckBox
        Me.f7 = New System.Windows.Forms.CheckBox
        Me.f8 = New System.Windows.Forms.CheckBox
        Me.e1 = New System.Windows.Forms.CheckBox
        Me.e3 = New System.Windows.Forms.CheckBox
        Me.e2 = New System.Windows.Forms.CheckBox
        Me.e4 = New System.Windows.Forms.CheckBox
        Me.e6 = New System.Windows.Forms.CheckBox
        Me.e5 = New System.Windows.Forms.CheckBox
        Me.e7 = New System.Windows.Forms.CheckBox
        Me.e8 = New System.Windows.Forms.CheckBox
        Me.btnSolve = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'a8
        '
        Me.a8.Location = New System.Drawing.Point(184, 8)
        Me.a8.Name = "a8"
        Me.a8.Size = New System.Drawing.Size(16, 24)
        Me.a8.TabIndex = 5
        '
        'a7
        '
        Me.a7.Location = New System.Drawing.Point(160, 8)
        Me.a7.Name = "a7"
        Me.a7.Size = New System.Drawing.Size(16, 24)
        Me.a7.TabIndex = 6
        '
        'a5
        '
        Me.a5.Location = New System.Drawing.Point(112, 8)
        Me.a5.Name = "a5"
        Me.a5.Size = New System.Drawing.Size(16, 24)
        Me.a5.TabIndex = 7
        '
        'a2
        '
        Me.a2.Location = New System.Drawing.Point(40, 8)
        Me.a2.Name = "a2"
        Me.a2.Size = New System.Drawing.Size(16, 24)
        Me.a2.TabIndex = 10
        '
        'a4
        '
        Me.a4.Location = New System.Drawing.Point(88, 8)
        Me.a4.Name = "a4"
        Me.a4.Size = New System.Drawing.Size(16, 24)
        Me.a4.TabIndex = 9
        '
        'a6
        '
        Me.a6.Location = New System.Drawing.Point(136, 8)
        Me.a6.Name = "a6"
        Me.a6.Size = New System.Drawing.Size(16, 24)
        Me.a6.TabIndex = 8
        '
        'a3
        '
        Me.a3.Location = New System.Drawing.Point(64, 8)
        Me.a3.Name = "a3"
        Me.a3.Size = New System.Drawing.Size(16, 24)
        Me.a3.TabIndex = 11
        '
        'a1
        '
        Me.a1.Location = New System.Drawing.Point(16, 8)
        Me.a1.Name = "a1"
        Me.a1.Size = New System.Drawing.Size(16, 24)
        Me.a1.TabIndex = 12
        '
        'b1
        '
        Me.b1.Location = New System.Drawing.Point(16, 32)
        Me.b1.Name = "b1"
        Me.b1.Size = New System.Drawing.Size(16, 24)
        Me.b1.TabIndex = 20
        '
        'b3
        '
        Me.b3.Location = New System.Drawing.Point(64, 32)
        Me.b3.Name = "b3"
        Me.b3.Size = New System.Drawing.Size(16, 24)
        Me.b3.TabIndex = 19
        '
        'b2
        '
        Me.b2.Location = New System.Drawing.Point(40, 32)
        Me.b2.Name = "b2"
        Me.b2.Size = New System.Drawing.Size(16, 24)
        Me.b2.TabIndex = 18
        '
        'b4
        '
        Me.b4.Location = New System.Drawing.Point(88, 32)
        Me.b4.Name = "b4"
        Me.b4.Size = New System.Drawing.Size(16, 24)
        Me.b4.TabIndex = 17
        '
        'b6
        '
        Me.b6.Location = New System.Drawing.Point(136, 32)
        Me.b6.Name = "b6"
        Me.b6.Size = New System.Drawing.Size(16, 24)
        Me.b6.TabIndex = 16
        '
        'b5
        '
        Me.b5.Location = New System.Drawing.Point(112, 32)
        Me.b5.Name = "b5"
        Me.b5.Size = New System.Drawing.Size(16, 24)
        Me.b5.TabIndex = 15
        '
        'b7
        '
        Me.b7.Location = New System.Drawing.Point(160, 32)
        Me.b7.Name = "b7"
        Me.b7.Size = New System.Drawing.Size(16, 24)
        Me.b7.TabIndex = 14
        '
        'b8
        '
        Me.b8.Location = New System.Drawing.Point(184, 32)
        Me.b8.Name = "b8"
        Me.b8.Size = New System.Drawing.Size(16, 24)
        Me.b8.TabIndex = 13
        '
        'c1
        '
        Me.c1.Location = New System.Drawing.Point(16, 56)
        Me.c1.Name = "c1"
        Me.c1.Size = New System.Drawing.Size(16, 24)
        Me.c1.TabIndex = 28
        '
        'c3
        '
        Me.c3.Location = New System.Drawing.Point(64, 56)
        Me.c3.Name = "c3"
        Me.c3.Size = New System.Drawing.Size(16, 24)
        Me.c3.TabIndex = 27
        '
        'c2
        '
        Me.c2.Location = New System.Drawing.Point(40, 56)
        Me.c2.Name = "c2"
        Me.c2.Size = New System.Drawing.Size(16, 24)
        Me.c2.TabIndex = 26
        '
        'c4
        '
        Me.c4.Location = New System.Drawing.Point(88, 56)
        Me.c4.Name = "c4"
        Me.c4.Size = New System.Drawing.Size(16, 24)
        Me.c4.TabIndex = 25
        '
        'c6
        '
        Me.c6.Location = New System.Drawing.Point(136, 56)
        Me.c6.Name = "c6"
        Me.c6.Size = New System.Drawing.Size(16, 24)
        Me.c6.TabIndex = 24
        '
        'c5
        '
        Me.c5.Location = New System.Drawing.Point(112, 56)
        Me.c5.Name = "c5"
        Me.c5.Size = New System.Drawing.Size(16, 24)
        Me.c5.TabIndex = 23
        '
        'c7
        '
        Me.c7.Location = New System.Drawing.Point(160, 56)
        Me.c7.Name = "c7"
        Me.c7.Size = New System.Drawing.Size(16, 24)
        Me.c7.TabIndex = 22
        '
        'c8
        '
        Me.c8.Location = New System.Drawing.Point(184, 56)
        Me.c8.Name = "c8"
        Me.c8.Size = New System.Drawing.Size(16, 24)
        Me.c8.TabIndex = 21
        '
        'd1
        '
        Me.d1.Location = New System.Drawing.Point(16, 80)
        Me.d1.Name = "d1"
        Me.d1.Size = New System.Drawing.Size(16, 24)
        Me.d1.TabIndex = 36
        '
        'd3
        '
        Me.d3.Location = New System.Drawing.Point(64, 80)
        Me.d3.Name = "d3"
        Me.d3.Size = New System.Drawing.Size(16, 24)
        Me.d3.TabIndex = 35
        '
        'd2
        '
        Me.d2.Location = New System.Drawing.Point(40, 80)
        Me.d2.Name = "d2"
        Me.d2.Size = New System.Drawing.Size(16, 24)
        Me.d2.TabIndex = 34
        '
        'd4
        '
        Me.d4.Location = New System.Drawing.Point(88, 80)
        Me.d4.Name = "d4"
        Me.d4.Size = New System.Drawing.Size(16, 24)
        Me.d4.TabIndex = 33
        '
        'd6
        '
        Me.d6.Location = New System.Drawing.Point(136, 80)
        Me.d6.Name = "d6"
        Me.d6.Size = New System.Drawing.Size(16, 24)
        Me.d6.TabIndex = 32
        '
        'd5
        '
        Me.d5.Location = New System.Drawing.Point(112, 80)
        Me.d5.Name = "d5"
        Me.d5.Size = New System.Drawing.Size(16, 24)
        Me.d5.TabIndex = 31
        '
        'd7
        '
        Me.d7.Location = New System.Drawing.Point(160, 80)
        Me.d7.Name = "d7"
        Me.d7.Size = New System.Drawing.Size(16, 24)
        Me.d7.TabIndex = 30
        '
        'd8
        '
        Me.d8.Location = New System.Drawing.Point(184, 80)
        Me.d8.Name = "d8"
        Me.d8.Size = New System.Drawing.Size(16, 24)
        Me.d8.TabIndex = 29
        '
        'h1
        '
        Me.h1.Location = New System.Drawing.Point(16, 176)
        Me.h1.Name = "h1"
        Me.h1.Size = New System.Drawing.Size(16, 24)
        Me.h1.TabIndex = 68
        '
        'h3
        '
        Me.h3.Location = New System.Drawing.Point(64, 176)
        Me.h3.Name = "h3"
        Me.h3.Size = New System.Drawing.Size(16, 24)
        Me.h3.TabIndex = 67
        '
        'h2
        '
        Me.h2.Location = New System.Drawing.Point(40, 176)
        Me.h2.Name = "h2"
        Me.h2.Size = New System.Drawing.Size(16, 24)
        Me.h2.TabIndex = 66
        '
        'h4
        '
        Me.h4.Location = New System.Drawing.Point(88, 176)
        Me.h4.Name = "h4"
        Me.h4.Size = New System.Drawing.Size(16, 24)
        Me.h4.TabIndex = 65
        '
        'h6
        '
        Me.h6.Location = New System.Drawing.Point(136, 176)
        Me.h6.Name = "h6"
        Me.h6.Size = New System.Drawing.Size(16, 24)
        Me.h6.TabIndex = 64
        '
        'h5
        '
        Me.h5.Location = New System.Drawing.Point(112, 176)
        Me.h5.Name = "h5"
        Me.h5.Size = New System.Drawing.Size(16, 24)
        Me.h5.TabIndex = 63
        '
        'h7
        '
        Me.h7.Location = New System.Drawing.Point(160, 176)
        Me.h7.Name = "h7"
        Me.h7.Size = New System.Drawing.Size(16, 24)
        Me.h7.TabIndex = 62
        '
        'h8
        '
        Me.h8.Location = New System.Drawing.Point(184, 176)
        Me.h8.Name = "h8"
        Me.h8.Size = New System.Drawing.Size(16, 24)
        Me.h8.TabIndex = 61
        '
        'g1
        '
        Me.g1.Location = New System.Drawing.Point(16, 152)
        Me.g1.Name = "g1"
        Me.g1.Size = New System.Drawing.Size(16, 24)
        Me.g1.TabIndex = 60
        '
        'g3
        '
        Me.g3.Location = New System.Drawing.Point(64, 152)
        Me.g3.Name = "g3"
        Me.g3.Size = New System.Drawing.Size(16, 24)
        Me.g3.TabIndex = 59
        '
        'g2
        '
        Me.g2.Location = New System.Drawing.Point(40, 152)
        Me.g2.Name = "g2"
        Me.g2.Size = New System.Drawing.Size(16, 24)
        Me.g2.TabIndex = 58
        '
        'g4
        '
        Me.g4.Location = New System.Drawing.Point(88, 152)
        Me.g4.Name = "g4"
        Me.g4.Size = New System.Drawing.Size(16, 24)
        Me.g4.TabIndex = 57
        '
        'g6
        '
        Me.g6.Location = New System.Drawing.Point(136, 152)
        Me.g6.Name = "g6"
        Me.g6.Size = New System.Drawing.Size(16, 24)
        Me.g6.TabIndex = 56
        '
        'g5
        '
        Me.g5.Location = New System.Drawing.Point(112, 152)
        Me.g5.Name = "g5"
        Me.g5.Size = New System.Drawing.Size(16, 24)
        Me.g5.TabIndex = 55
        '
        'g7
        '
        Me.g7.Location = New System.Drawing.Point(160, 152)
        Me.g7.Name = "g7"
        Me.g7.Size = New System.Drawing.Size(16, 24)
        Me.g7.TabIndex = 54
        '
        'g8
        '
        Me.g8.Location = New System.Drawing.Point(184, 152)
        Me.g8.Name = "g8"
        Me.g8.Size = New System.Drawing.Size(16, 24)
        Me.g8.TabIndex = 53
        '
        'f1
        '
        Me.f1.Location = New System.Drawing.Point(16, 128)
        Me.f1.Name = "f1"
        Me.f1.Size = New System.Drawing.Size(16, 24)
        Me.f1.TabIndex = 52
        '
        'f3
        '
        Me.f3.Location = New System.Drawing.Point(64, 128)
        Me.f3.Name = "f3"
        Me.f3.Size = New System.Drawing.Size(16, 24)
        Me.f3.TabIndex = 51
        '
        'f2
        '
        Me.f2.Location = New System.Drawing.Point(40, 128)
        Me.f2.Name = "f2"
        Me.f2.Size = New System.Drawing.Size(16, 24)
        Me.f2.TabIndex = 50
        '
        'f4
        '
        Me.f4.Location = New System.Drawing.Point(88, 128)
        Me.f4.Name = "f4"
        Me.f4.Size = New System.Drawing.Size(16, 24)
        Me.f4.TabIndex = 49
        '
        'f6
        '
        Me.f6.Location = New System.Drawing.Point(136, 128)
        Me.f6.Name = "f6"
        Me.f6.Size = New System.Drawing.Size(16, 24)
        Me.f6.TabIndex = 48
        '
        'f5
        '
        Me.f5.Location = New System.Drawing.Point(112, 128)
        Me.f5.Name = "f5"
        Me.f5.Size = New System.Drawing.Size(16, 24)
        Me.f5.TabIndex = 47
        '
        'f7
        '
        Me.f7.Location = New System.Drawing.Point(160, 128)
        Me.f7.Name = "f7"
        Me.f7.Size = New System.Drawing.Size(16, 24)
        Me.f7.TabIndex = 46
        '
        'f8
        '
        Me.f8.Location = New System.Drawing.Point(184, 128)
        Me.f8.Name = "f8"
        Me.f8.Size = New System.Drawing.Size(16, 24)
        Me.f8.TabIndex = 45
        '
        'e1
        '
        Me.e1.Location = New System.Drawing.Point(16, 104)
        Me.e1.Name = "e1"
        Me.e1.Size = New System.Drawing.Size(16, 24)
        Me.e1.TabIndex = 44
        '
        'e3
        '
        Me.e3.Location = New System.Drawing.Point(64, 104)
        Me.e3.Name = "e3"
        Me.e3.Size = New System.Drawing.Size(16, 24)
        Me.e3.TabIndex = 43
        '
        'e2
        '
        Me.e2.Location = New System.Drawing.Point(40, 104)
        Me.e2.Name = "e2"
        Me.e2.Size = New System.Drawing.Size(16, 24)
        Me.e2.TabIndex = 42
        '
        'e4
        '
        Me.e4.Location = New System.Drawing.Point(88, 104)
        Me.e4.Name = "e4"
        Me.e4.Size = New System.Drawing.Size(16, 24)
        Me.e4.TabIndex = 41
        '
        'e6
        '
        Me.e6.Location = New System.Drawing.Point(136, 104)
        Me.e6.Name = "e6"
        Me.e6.Size = New System.Drawing.Size(16, 24)
        Me.e6.TabIndex = 40
        '
        'e5
        '
        Me.e5.Location = New System.Drawing.Point(112, 104)
        Me.e5.Name = "e5"
        Me.e5.Size = New System.Drawing.Size(16, 24)
        Me.e5.TabIndex = 39
        '
        'e7
        '
        Me.e7.Location = New System.Drawing.Point(160, 104)
        Me.e7.Name = "e7"
        Me.e7.Size = New System.Drawing.Size(16, 24)
        Me.e7.TabIndex = 38
        '
        'e8
        '
        Me.e8.Location = New System.Drawing.Point(184, 104)
        Me.e8.Name = "e8"
        Me.e8.Size = New System.Drawing.Size(16, 24)
        Me.e8.TabIndex = 37
        '
        'btnSolve
        '
        Me.btnSolve.Location = New System.Drawing.Point(232, 16)
        Me.btnSolve.Name = "btnSolve"
        Me.btnSolve.TabIndex = 69
        Me.btnSolve.Text = "&Solve"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(232, 160)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 70
        Me.btnExit.Text = "E&xit"
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(232, 48)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.TabIndex = 71
        Me.btnReset.Text = "&Reset"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(328, 206)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSolve)
        Me.Controls.Add(Me.h1)
        Me.Controls.Add(Me.h3)
        Me.Controls.Add(Me.h2)
        Me.Controls.Add(Me.h4)
        Me.Controls.Add(Me.h6)
        Me.Controls.Add(Me.h5)
        Me.Controls.Add(Me.h7)
        Me.Controls.Add(Me.h8)
        Me.Controls.Add(Me.g1)
        Me.Controls.Add(Me.g3)
        Me.Controls.Add(Me.g2)
        Me.Controls.Add(Me.g4)
        Me.Controls.Add(Me.g6)
        Me.Controls.Add(Me.g5)
        Me.Controls.Add(Me.g7)
        Me.Controls.Add(Me.g8)
        Me.Controls.Add(Me.f1)
        Me.Controls.Add(Me.f3)
        Me.Controls.Add(Me.f2)
        Me.Controls.Add(Me.f4)
        Me.Controls.Add(Me.f6)
        Me.Controls.Add(Me.f5)
        Me.Controls.Add(Me.f7)
        Me.Controls.Add(Me.f8)
        Me.Controls.Add(Me.e1)
        Me.Controls.Add(Me.e3)
        Me.Controls.Add(Me.e2)
        Me.Controls.Add(Me.e4)
        Me.Controls.Add(Me.e6)
        Me.Controls.Add(Me.e5)
        Me.Controls.Add(Me.e7)
        Me.Controls.Add(Me.e8)
        Me.Controls.Add(Me.d1)
        Me.Controls.Add(Me.d3)
        Me.Controls.Add(Me.d2)
        Me.Controls.Add(Me.d4)
        Me.Controls.Add(Me.d6)
        Me.Controls.Add(Me.d5)
        Me.Controls.Add(Me.d7)
        Me.Controls.Add(Me.d8)
        Me.Controls.Add(Me.c1)
        Me.Controls.Add(Me.c3)
        Me.Controls.Add(Me.c2)
        Me.Controls.Add(Me.c4)
        Me.Controls.Add(Me.c6)
        Me.Controls.Add(Me.c5)
        Me.Controls.Add(Me.c7)
        Me.Controls.Add(Me.c8)
        Me.Controls.Add(Me.b1)
        Me.Controls.Add(Me.b3)
        Me.Controls.Add(Me.b2)
        Me.Controls.Add(Me.b4)
        Me.Controls.Add(Me.b6)
        Me.Controls.Add(Me.b5)
        Me.Controls.Add(Me.b7)
        Me.Controls.Add(Me.b8)
        Me.Controls.Add(Me.a1)
        Me.Controls.Add(Me.a3)
        Me.Controls.Add(Me.a2)
        Me.Controls.Add(Me.a4)
        Me.Controls.Add(Me.a6)
        Me.Controls.Add(Me.a5)
        Me.Controls.Add(Me.a7)
        Me.Controls.Add(Me.a8)
        Me.Name = "Form1"
        Me.Text = "Visual Queens on Chess Board Solver"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim Queens(7, 7) As Boolean

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub

    Private Sub btnSolve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSolve.Click
        Dim solved As Boolean = False

        Dim x, y, run As Integer
        x = 0
        y = 0
        run = 0

        While solved = False
            y = x + run
            ChangeQueen(x, y)
            
            solved = CheckQueens()
            run += 1
        End While

    End Sub

    Sub ChangeQueen(ByVal x As Integer, ByVal y As Integer)
        If Queens(x, y) = True Then
            Queens(x, y) = False
        Else
            Queens(x, y) = True
        End If
    End Sub

    Function CheckQueens()
        Dim okay As Boolean = True
        Dim x, y, i, xb, yb, ib As Integer
        x = 0
        y = 0

        For i = 0 To 64
            If Queens(x, y) = False Then
            Else
                For ib = 0 To 8 - x
                    yb = y + 1
                    If Queens(x, yb) = True Then okay = False
                Next

                For ib = 0 To 8 - y
                    xb = x + 1
                    If Queens(xb, y) = True Then okay = False
                Next

                For ib = 0 To 8 - y
                    xb = x + 1
                    yb = y + 1
                    If Queens(xb, yb) = True Then okay = False
                Next

                For ib = 0 To 8 - y
                    xb = x + 1
                    yb = y + 1
                    If Queens(xb, yb) = True Then okay = False
                Next
            End If
        Next


        Return okay
    End Function

End Class
