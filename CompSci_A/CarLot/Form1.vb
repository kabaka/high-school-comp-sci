Imports System.io
Public Class Form1
    Inherits System.Windows.Forms.Form
    Structure Automobile
        Public Make As String
        Public Model As String
        Public Price As Decimal
    End Structure
    Const Capacity As Integer = 50
    Public CarLot(Capacity) As Automobile
    Public LotNumber As Integer = 0
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMake As System.Windows.Forms.TextBox
    Friend WithEvents txtModel As System.Windows.Forms.TextBox
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents lstCarLot As System.Windows.Forms.ListBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents miNew As System.Windows.Forms.MenuItem
    Friend WithEvents miOpen As System.Windows.Forms.MenuItem
    Friend WithEvents miSaveAs As System.Windows.Forms.MenuItem
    Friend WithEvents miExit As System.Windows.Forms.MenuItem
    Friend WithEvents miClear As System.Windows.Forms.MenuItem
    Friend WithEvents miAdd As System.Windows.Forms.MenuItem
    Friend WithEvents miDisplay As System.Windows.Forms.MenuItem
    Friend WithEvents Label4 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.miNew = New System.Windows.Forms.MenuItem
        Me.miOpen = New System.Windows.Forms.MenuItem
        Me.miSaveAs = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.miExit = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.miClear = New System.Windows.Forms.MenuItem
        Me.miAdd = New System.Windows.Forms.MenuItem
        Me.miDisplay = New System.Windows.Forms.MenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtMake = New System.Windows.Forms.TextBox
        Me.txtModel = New System.Windows.Forms.TextBox
        Me.txtPrice = New System.Windows.Forms.TextBox
        Me.lstCarLot = New System.Windows.Forms.ListBox
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.Label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem7})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miNew, Me.miOpen, Me.miSaveAs, Me.MenuItem5, Me.miExit})
        Me.MenuItem1.Text = "&File"
        '
        'miNew
        '
        Me.miNew.Index = 0
        Me.miNew.Text = "&New"
        '
        'miOpen
        '
        Me.miOpen.Index = 1
        Me.miOpen.Text = "&Open file"
        '
        'miSaveAs
        '
        Me.miSaveAs.Index = 2
        Me.miSaveAs.Text = "&Save As"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'miExit
        '
        Me.miExit.Index = 4
        Me.miExit.Text = "E&xit"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 1
        Me.MenuItem7.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClear, Me.miAdd, Me.miDisplay})
        Me.MenuItem7.Text = "&Data"
        '
        'miClear
        '
        Me.miClear.Index = 0
        Me.miClear.Text = "&Clear Current Data"
        '
        'miAdd
        '
        Me.miAdd.Index = 1
        Me.miAdd.Text = "&Add Data"
        '
        'miDisplay
        '
        Me.miDisplay.Index = 2
        Me.miDisplay.Text = "&Display"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Make"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Model"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Price"
        '
        'txtMake
        '
        Me.txtMake.Location = New System.Drawing.Point(8, 56)
        Me.txtMake.Name = "txtMake"
        Me.txtMake.TabIndex = 3
        Me.txtMake.Text = ""
        '
        'txtModel
        '
        Me.txtModel.Location = New System.Drawing.Point(8, 104)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.TabIndex = 4
        Me.txtModel.Text = ""
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(8, 152)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.TabIndex = 5
        Me.txtPrice.Text = ""
        '
        'lstCarLot
        '
        Me.lstCarLot.Location = New System.Drawing.Point(128, 16)
        Me.lstCarLot.Name = "lstCarLot"
        Me.lstCarLot.Size = New System.Drawing.Size(192, 199)
        Me.lstCarLot.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(40, 216)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(240, 32)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "                                                "
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(336, 246)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lstCarLot)
        Me.Controls.Add(Me.txtPrice)
        Me.Controls.Add(Me.txtModel)
        Me.Controls.Add(Me.txtMake)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "The Car Lot Problem"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub miExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miExit.Click
        Application.Exit()
    End Sub

    Private Sub miNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miNew.Click
        If LotNumber = 0 Then
            Exit Sub
        End If
        Dim Index As Integer
        For Index = 0 To LotNumber - 1
            With CarLot(Index)
                .Make = ""
                .Model = ""
                .Price = 0
            End With
        Next
        LotNumber = 0
        lstCarLot.Items.Clear()
    End Sub

    Private Sub miClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClear.Click
        txtMake.ResetText()
        txtModel.ResetText()
        txtPrice.ResetText()
        txtMake.Focus()
    End Sub

    Private Sub miAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles miAdd.Click
        If txtMake.Text.ToString = "" Or txtModel.Text.ToString = "" Or _
        CDec(txtPrice.Text) = 0 Then
            MessageBox.Show("Please fill all the fields")
            Exit Sub
        End If
        If LotNumber = Capacity Then
            MessageBox.Show("The lot is full.")
            Exit Sub
        End If
        With CarLot(LotNumber)
            .Make = txtMake.Text.ToString
            .Model = txtModel.Text.ToString
            .Price = CDec(txtPrice.Text)
        End With
        LotNumber += 1
        miClear_Click(sender, e)
    End Sub

    Private Sub miDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles miDisplay.Click
        Dim Index As Integer
        lstCarLot.Items.Clear()
        For Index = 0 To LotNumber - 1
            With CarLot(Index)
                lstCarLot.Items.Add(.Make & "   " & .Model & "    " & _
                .Price.ToString)
            End With
        Next
    End Sub

    Private Sub miOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles miOpen.Click
        Dim PathName As String
        With OpenFileDialog1
            .DefaultExt = "txt"
            .InitialDirectory = "c:\My Documents"
            .ShowDialog()
            PathName = .FileName
        End With

        Dim fs As StreamReader
        fs = File.OpenText(PathName)
        LotNumber = CInt(fs.ReadLine)
        Dim Index As Integer
        For Index = 0 To LotNumber - 1
            With CarLot(Index)
                .Make = fs.ReadLine
                .Model = fs.ReadLine
                .Price = fs.ReadLine
            End With
        Next
        fs.Close()
        miDisplay_Click(sender, e)
    End Sub

    Private Sub miSaveAs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles miSaveAs.Click
        Dim PathName As String
        With SaveFileDialog1
            .DefaultExt = "txt"
            .InitialDirectory = "c:\My Documents"
            .Filter = "Text Files | *.txt"
            .ShowDialog()
            PathName = .FileName
        End With

        Dim fs As StreamWriter
        fs = File.CreateText(PathName)
        Dim strContent As String = LotNumber.ToString
        fs.WriteLine(strContent)
        Dim Index As Integer
        For Index = 0 To LotNumber - 1
            With CarLot(Index)
                fs.WriteLine(.Make)
                fs.WriteLine(.Model)
                fs.WriteLine(.Price)
            End With
        Next
        fs.Close()
        miDisplay_Click(sender, e)
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click

    End Sub
End Class
