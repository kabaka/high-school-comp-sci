Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lstDisplay As System.Windows.Forms.ListBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnProcess As System.Windows.Forms.Button
    Friend WithEvents btnOver As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstDisplay = New System.Windows.Forms.ListBox
        Me.btnProcess = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnOver = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lstDisplay
        '
        Me.lstDisplay.Location = New System.Drawing.Point(16, 16)
        Me.lstDisplay.Name = "lstDisplay"
        Me.lstDisplay.Size = New System.Drawing.Size(104, 225)
        Me.lstDisplay.TabIndex = 0
        '
        'btnProcess
        '
        Me.btnProcess.Location = New System.Drawing.Point(128, 56)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.Size = New System.Drawing.Size(88, 23)
        Me.btnProcess.TabIndex = 1
        Me.btnProcess.Text = "&Enter Value"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(128, 88)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(88, 23)
        Me.btnClear.TabIndex = 2
        Me.btnClear.Text = "&Clear List"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(128, 184)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "E&xit"
        '
        'btnOver
        '
        Me.btnOver.Location = New System.Drawing.Point(128, 120)
        Me.btnOver.Name = "btnOver"
        Me.btnOver.Size = New System.Drawing.Size(88, 23)
        Me.btnOver.TabIndex = 4
        Me.btnOver.Text = "&Another Test"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(240, 266)
        Me.Controls.Add(Me.btnOver)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnProcess)
        Me.Controls.Add(Me.lstDisplay)
        Me.Name = "Form1"
        Me.Text = "The Goldbach Conjecture"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lstDisplay.Items.Clear()
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim x As Integer
        Do
            x = CInt(InputBox("Enter a positive whole number:", "Goldbach", _
            "7"))
        Loop Until x > 0
        lstDisplay.Items.Add(x.ToString)
        Do While x <> 1
            If x Mod 2 = 0 Then
                x = x \ 2
            Else
                x = 3 * x + 1
            End If
            lstDisplay.Items.Add(x.ToString)
        Loop
    End Sub

    Private Sub btnOver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOver.Click
        Dim x, y, Largest As Integer
        x = 1
        Do
            x = x + 1
            Largest = x
            y = x
            lstDisplay.Items.Clear()
            lstDisplay.Items.Add(y)
            Do While y <> 1
                If y Mod 2 = 0 Then
                    y = y / 2
                Else
                    y = 3 * y + 1
                End If
                lstDisplay.Items.Add(y)
                If y > Largest Then
                    Largest = y
                End If
            Loop
        Loop Until Largest > 10000
    End Sub
End Class
