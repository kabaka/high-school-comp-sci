Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtInputOne As System.Windows.Forms.TextBox
    Friend WithEvents txtInputTwo As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents cmbAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblInputOne As System.Windows.Forms.Label
    Friend WithEvents lblInputTwo As System.Windows.Forms.Label
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lbl31 As System.Windows.Forms.Label
    Friend WithEvents lbl32 As System.Windows.Forms.Label
    Friend WithEvents lbl21 As System.Windows.Forms.Label
    Friend WithEvents lbl22 As System.Windows.Forms.Label
    Friend WithEvents lbl41 As System.Windows.Forms.Label
    Friend WithEvents lbl12 As System.Windows.Forms.Label
    Friend WithEvents lbl11 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblInputOne = New System.Windows.Forms.Label
        Me.txtInputOne = New System.Windows.Forms.TextBox
        Me.txtInputTwo = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.lblInputTwo = New System.Windows.Forms.Label
        Me.cmbAction = New System.Windows.Forms.ComboBox
        Me.lblAction = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lbl31 = New System.Windows.Forms.Label
        Me.lbl32 = New System.Windows.Forms.Label
        Me.lbl21 = New System.Windows.Forms.Label
        Me.lbl22 = New System.Windows.Forms.Label
        Me.lbl41 = New System.Windows.Forms.Label
        Me.lbl12 = New System.Windows.Forms.Label
        Me.lbl11 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblInputOne
        '
        Me.lblInputOne.AutoSize = True
        Me.lblInputOne.Location = New System.Drawing.Point(16, 24)
        Me.lblInputOne.Name = "lblInputOne"
        Me.lblInputOne.Size = New System.Drawing.Size(56, 16)
        Me.lblInputOne.TabIndex = 0
        Me.lblInputOne.Text = "Enter Text"
        '
        'txtInputOne
        '
        Me.txtInputOne.Location = New System.Drawing.Point(16, 48)
        Me.txtInputOne.Name = "txtInputOne"
        Me.txtInputOne.Size = New System.Drawing.Size(240, 20)
        Me.txtInputOne.TabIndex = 1
        Me.txtInputOne.Text = ""
        '
        'txtInputTwo
        '
        Me.txtInputTwo.Location = New System.Drawing.Point(16, 104)
        Me.txtInputTwo.Name = "txtInputTwo"
        Me.txtInputTwo.Size = New System.Drawing.Size(240, 20)
        Me.txtInputTwo.TabIndex = 2
        Me.txtInputTwo.Text = ""
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(184, 168)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Go!"
        '
        'lblInputTwo
        '
        Me.lblInputTwo.AutoSize = True
        Me.lblInputTwo.Location = New System.Drawing.Point(16, 80)
        Me.lblInputTwo.Name = "lblInputTwo"
        Me.lblInputTwo.Size = New System.Drawing.Size(144, 16)
        Me.lblInputTwo.TabIndex = 5
        Me.lblInputTwo.Text = "Enter the letter to search for"
        '
        'cmbAction
        '
        Me.cmbAction.Items.AddRange(New Object() {"Letter Count", "String Test", "Find String", "Full Name", "Secret Message Decoder", "Compare Words"})
        Me.cmbAction.Location = New System.Drawing.Point(88, 136)
        Me.cmbAction.Name = "cmbAction"
        Me.cmbAction.Size = New System.Drawing.Size(168, 21)
        Me.cmbAction.TabIndex = 6
        Me.cmbAction.Text = "Letter Count"
        '
        'lblAction
        '
        Me.lblAction.Location = New System.Drawing.Point(16, 136)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(72, 23)
        Me.lblAction.TabIndex = 7
        Me.lblAction.Text = "Action"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtInputTwo)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.lblInputTwo)
        Me.GroupBox1.Controls.Add(Me.cmbAction)
        Me.GroupBox1.Controls.Add(Me.lblAction)
        Me.GroupBox1.Controls.Add(Me.lblInputOne)
        Me.GroupBox1.Controls.Add(Me.txtInputOne)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 208)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Input"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lbl12)
        Me.GroupBox2.Controls.Add(Me.lbl11)
        Me.GroupBox2.Controls.Add(Me.lbl22)
        Me.GroupBox2.Controls.Add(Me.lbl41)
        Me.GroupBox2.Controls.Add(Me.lbl32)
        Me.GroupBox2.Controls.Add(Me.lbl21)
        Me.GroupBox2.Controls.Add(Me.lbl31)
        Me.GroupBox2.Location = New System.Drawing.Point(312, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 208)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Output"
        '
        'lbl31
        '
        Me.lbl31.Location = New System.Drawing.Point(8, 112)
        Me.lbl31.Name = "lbl31"
        Me.lbl31.Size = New System.Drawing.Size(88, 23)
        Me.lbl31.TabIndex = 4
        '
        'lbl32
        '
        Me.lbl32.Location = New System.Drawing.Point(96, 112)
        Me.lbl32.Name = "lbl32"
        Me.lbl32.Size = New System.Drawing.Size(96, 23)
        Me.lbl32.TabIndex = 7
        '
        'lbl21
        '
        Me.lbl21.Location = New System.Drawing.Point(8, 80)
        Me.lbl21.Name = "lbl21"
        Me.lbl21.Size = New System.Drawing.Size(88, 23)
        Me.lbl21.TabIndex = 6
        '
        'lbl22
        '
        Me.lbl22.Location = New System.Drawing.Point(96, 80)
        Me.lbl22.Name = "lbl22"
        Me.lbl22.Size = New System.Drawing.Size(96, 23)
        Me.lbl22.TabIndex = 9
        '
        'lbl41
        '
        Me.lbl41.Location = New System.Drawing.Point(8, 144)
        Me.lbl41.Name = "lbl41"
        Me.lbl41.Size = New System.Drawing.Size(184, 23)
        Me.lbl41.TabIndex = 8
        '
        'lbl12
        '
        Me.lbl12.Location = New System.Drawing.Point(96, 48)
        Me.lbl12.Name = "lbl12"
        Me.lbl12.Size = New System.Drawing.Size(96, 23)
        Me.lbl12.TabIndex = 11
        '
        'lbl11
        '
        Me.lbl11.Location = New System.Drawing.Point(8, 48)
        Me.lbl11.Name = "lbl11"
        Me.lbl11.Size = New System.Drawing.Size(88, 23)
        Me.lbl11.TabIndex = 10
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(520, 238)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "Form1"
        Me.Text = "Stringy Stuff"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAction.SelectedIndexChanged
        Select Case cmbAction.SelectedIndex
            '0-Letter Count
            '1-String Test
            '2-Find String
            '3-Full Name
            '4-Secret Message Decoder
            '5-Compare Words
        Case 0
                ResetAll()
                lblInputTwo.Text = "Enter letter to search for (one)"
                txtInputTwo.MaxLength = 1
            Case 1
                ResetAll()
                lblInputTwo.Text = ""
                txtInputTwo.Enabled = False
            Case 2
                ResetAll()
                lblInputTwo.Text = "Enter search text"
            Case 3
                ResetAll()
                lblInputOne.Text = ""
                lblInputOne.Enabled = False
                lblInputTwo.Text = ""
            Case 4
                ResetAll()
                lblInputOne.Text = ""
                lblInputOne.Enabled = False
                lblInputTwo.Text = ""
                txtInputTwo.Enabled = False
            Case 5
                ResetAll()
                lblInputTwo.Text = "Enter other text"
        End Select
    End Sub

    Private Sub ResetAll()
        ResetOutput()
        ResetInput()
    End Sub

    Private Sub ResetOutput()
        lbl11.Text = ""
        lbl21.Text = ""
        lbl31.Text = ""
        lbl41.Text = ""
        lbl12.Text = ""
        lbl22.Text = ""
        lbl32.Text = ""
    End Sub

    Private Sub ResetInput()
        txtInputOne.ResetText()
        txtInputOne.MaxLength = 32767
        txtInputOne.Enabled = True
        txtInputTwo.ResetText()
        txtInputTwo.MaxLength = 32767
        txtInputTwo.Enabled = True
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Select Case cmbAction.SelectedIndex
            '0-Letter Count
            '1-String Test
            '2-Find String
            '3-Full Name
            '4-Secret Message Decoder
            '5-Compare Words
            Case 0
                Dim InputOneStr, InputTwoStr, Output As String
                Dim i, NumberOfOccurances As Integer
                Dim temp As Char
                InputOneStr = txtInputOne.Text
                InputTwoStr = txtInputTwo.Text
                For i = 0 To InputOneStr.Length - 1
                    temp = InputOneStr.Chars(i)
                    If temp = InputTwoStr Then
                        NumberOfOccurances += 1
                    End If
                Next
                lbl11.Text = "Occurances:"
                lbl12.Text = NumberOfOccurances
            Case 1
                Dim InputOneStr, Output1, Output2, Output3 As String
                InputOneStr = txtInputOne.Text
                Output1 = InputOneStr.Substring(0, 1)
                Output2 = InputOneStr.Substring(InputOneStr.Length / 2, 1)
                Output3 = InputOneStr.Substring(InputOneStr.Length - 1, 1)
                lbl11.Text = "First Letter:"
                lbl12.Text = Output1
                lbl21.Text = "Middle Letter:"
                lbl22.Text = Output2
                lbl31.Text = "Last Letter:"
                lbl32.Text = Output3
            Case 2
                Dim InputOneStr, InputTwoStr As String
                Dim Location As Integer
                InputOneStr = txtInputOne.Text
                InputTwoStr = txtInputTwo.Text
                Location = InputOneStr.IndexOf(InputTwoStr)
                On Error Resume Next
                lbl11.Text = "Location:"
                lbl12.Text = Location
                On Error Resume Next
            Case 3
                lbl41.Text = txtInputOne.Text & " " & txtInputTwo.Text
            Case 4
                Dim InputOneStr, InputTwoStr, Output, temp As String
                Dim i As Integer
                InputOneStr = txtInputOne.Text
                InputTwoStr = txtInputTwo.Text
                For i = 0 To 5
                    temp = InputBox("Enter a character")
                    Output = Output & AscW(temp.ToLower())
                Next
                lbl31.Text = "Encoded:"
                lbl41.Text = Output
            Case 5
                Dim InputOneStr, InputTwoStr, Output As String
                InputOneStr = txtInputOne.Text
                InputTwoStr = txtInputTwo.Text
                Dim i As Integer, match As Boolean = True
                For i = 0 To InputOneStr.Length - 1
                    If Not Char.ToLower(InputOneStr.Chars(i)) = Char.ToLower(InputTwoStr.Chars(i)) Then
                        match = False
                        Exit For
                    End If
                Next

                If match = True Then
                    output = InputOneStr & " is the same as " & InputTwoStr
                Else
                    output = InputOneStr & " is not the same as " & InputTwoStr
                End If

                lbl41.Text = Output
        End Select
    End Sub
End Class
