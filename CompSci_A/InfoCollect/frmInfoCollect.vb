Public Class frmInfoCollect
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents rdbMr As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMs As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMrs As System.Windows.Forms.RadioButton
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtPhoneNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblPhoneNumber As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.rdbMr = New System.Windows.Forms.RadioButton
        Me.rdbMs = New System.Windows.Forms.RadioButton
        Me.rdbMrs = New System.Windows.Forms.RadioButton
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtAddress = New System.Windows.Forms.TextBox
        Me.txtPhoneNumber = New System.Windows.Forms.TextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lblPhoneNumber = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'rdbMr
        '
        Me.rdbMr.Location = New System.Drawing.Point(8, 8)
        Me.rdbMr.Name = "rdbMr"
        Me.rdbMr.Size = New System.Drawing.Size(48, 24)
        Me.rdbMr.TabIndex = 0
        Me.rdbMr.Text = "Mr."
        '
        'rdbMs
        '
        Me.rdbMs.Location = New System.Drawing.Point(56, 8)
        Me.rdbMs.Name = "rdbMs"
        Me.rdbMs.Size = New System.Drawing.Size(48, 24)
        Me.rdbMs.TabIndex = 1
        Me.rdbMs.Text = "Ms."
        '
        'rdbMrs
        '
        Me.rdbMrs.Location = New System.Drawing.Point(104, 8)
        Me.rdbMrs.Name = "rdbMrs"
        Me.rdbMrs.Size = New System.Drawing.Size(48, 24)
        Me.rdbMrs.TabIndex = 2
        Me.rdbMrs.Text = "Mrs."
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(96, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(160, 20)
        Me.txtName.TabIndex = 3
        Me.txtName.Text = ""
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(96, 64)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(160, 20)
        Me.txtAddress.TabIndex = 4
        Me.txtAddress.Text = ""
        '
        'txtPhoneNumber
        '
        Me.txtPhoneNumber.Location = New System.Drawing.Point(96, 96)
        Me.txtPhoneNumber.Name = "txtPhoneNumber"
        Me.txtPhoneNumber.Size = New System.Drawing.Size(160, 20)
        Me.txtPhoneNumber.TabIndex = 5
        Me.txtPhoneNumber.Text = ""
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(8, 32)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(88, 16)
        Me.lblName.TabIndex = 6
        Me.lblName.Text = "Name"
        '
        'lblAddress
        '
        Me.lblAddress.Location = New System.Drawing.Point(8, 64)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(88, 16)
        Me.lblAddress.TabIndex = 7
        Me.lblAddress.Text = "Address"
        '
        'lblPhoneNumber
        '
        Me.lblPhoneNumber.Location = New System.Drawing.Point(8, 96)
        Me.lblPhoneNumber.Name = "lblPhoneNumber"
        Me.lblPhoneNumber.Size = New System.Drawing.Size(88, 16)
        Me.lblPhoneNumber.TabIndex = 8
        Me.lblPhoneNumber.Text = "Phone Number"
        '
        'frmInfoCollect
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(264, 126)
        Me.Controls.Add(Me.lblPhoneNumber)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtPhoneNumber)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.rdbMrs)
        Me.Controls.Add(Me.rdbMs)
        Me.Controls.Add(Me.rdbMr)
        Me.Name = "frmInfoCollect"
        Me.Text = "Information Collection"
        Me.ResumeLayout(False)

    End Sub

#End Region

End Class
