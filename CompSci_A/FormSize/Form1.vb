Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnShrink As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnDouble As System.Windows.Forms.Button
    Friend WithEvents btnHalf As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnShrink = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnDouble = New System.Windows.Forms.Button
        Me.btnHalf = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnShrink
        '
        Me.btnShrink.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShrink.Location = New System.Drawing.Point(392, 304)
        Me.btnShrink.Name = "btnShrink"
        Me.btnShrink.TabIndex = 0
        Me.btnShrink.Text = "Shrink"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(392, 336)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit"
        '
        'btnDouble
        '
        Me.btnDouble.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDouble.Location = New System.Drawing.Point(312, 304)
        Me.btnDouble.Name = "btnDouble"
        Me.btnDouble.TabIndex = 2
        Me.btnDouble.Text = "Double"
        '
        'btnHalf
        '
        Me.btnHalf.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnHalf.Location = New System.Drawing.Point(312, 336)
        Me.btnHalf.Name = "btnHalf"
        Me.btnHalf.TabIndex = 3
        Me.btnHalf.Text = "Half"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(472, 366)
        Me.Controls.Add(Me.btnHalf)
        Me.Controls.Add(Me.btnDouble)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnShrink)
        Me.Name = "Form1"
        Me.Text = "Form Size"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnShrink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShrink.Click
        Dim intWidth As Integer = Form1.ActiveForm.Width
        Dim intHeight As Integer = Form1.ActiveForm.Height
        intWidth = intWidth - 100
        intHeight = intHeight - 100
        Form1.ActiveForm.Size = New Size(intWidth, intHeight)
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnDouble_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDouble.Click
        Dim intWidth As Integer = Form1.ActiveForm.Width
        Dim intHeight As Integer = Form1.ActiveForm.Height
        intWidth = intWidth * 2
        intHeight = intHeight * 2
        Form1.ActiveForm.Size = New Size(intWidth, intHeight)
    End Sub

    Private Sub btnHalf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHalf.Click
        Dim intWidth As Integer = Form1.ActiveForm.Width
        Dim intHeight As Integer = Form1.ActiveForm.Height
        intWidth = intWidth / 2
        intHeight = intHeight / 2
        Form1.ActiveForm.Size = New Size(intWidth, intHeight)
    End Sub
End Class
