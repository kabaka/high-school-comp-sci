Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtBudget As System.Windows.Forms.TextBox
    Friend WithEvents txtItemPrice As System.Windows.Forms.TextBox
    Friend WithEvents btnCalc As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtBudget = New System.Windows.Forms.TextBox
        Me.txtItemPrice = New System.Windows.Forms.TextBox
        Me.btnCalc = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Budget"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Price of Item"
        '
        'txtBudget
        '
        Me.txtBudget.Location = New System.Drawing.Point(16, 40)
        Me.txtBudget.Name = "txtBudget"
        Me.txtBudget.Size = New System.Drawing.Size(104, 20)
        Me.txtBudget.TabIndex = 2
        Me.txtBudget.Text = ""
        '
        'txtItemPrice
        '
        Me.txtItemPrice.Location = New System.Drawing.Point(16, 88)
        Me.txtItemPrice.Name = "txtItemPrice"
        Me.txtItemPrice.TabIndex = 3
        Me.txtItemPrice.Text = ""
        '
        'btnCalc
        '
        Me.btnCalc.Location = New System.Drawing.Point(136, 88)
        Me.btnCalc.Name = "btnCalc"
        Me.btnCalc.TabIndex = 4
        Me.btnCalc.Text = "&Add Item"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(48, 128)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "&Clear"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(136, 128)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "E&xit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(232, 174)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCalc)
        Me.Controls.Add(Me.txtItemPrice)
        Me.Controls.Add(Me.txtBudget)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Grocery Shopping"
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub btnCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalc.Click
        Dim Budget, ItemPrice As Integer
        Budget = CInt(txtBudget.Text)
        ItemPrice = CInt(txtItemPrice.Text)
        If ItemPrice <= Budget Then
            Budget -= ItemPrice
        Else
            MessageBox.Show("You have exceeded your budget.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        txtBudget.Text = Budget.ToString
        txtItemPrice.ResetText()
        txtItemPrice.Focus()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtItemPrice.ResetText()
        txtBudget.ResetText()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub
End Class
