Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblOperand1 As System.Windows.Forms.Label
    Friend WithEvents lblOperand2 As System.Windows.Forms.Label
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents lblExpressionValue As System.Windows.Forms.Label
    Friend WithEvents grpOperators As System.Windows.Forms.GroupBox
    Friend WithEvents txtOperand2 As System.Windows.Forms.TextBox
    Friend WithEvents txtOperand1 As System.Windows.Forms.TextBox
    Friend WithEvents radSubtraction As System.Windows.Forms.RadioButton
    Friend WithEvents radAddition As System.Windows.Forms.RadioButton
    Friend WithEvents radModDivision As System.Windows.Forms.RadioButton
    Friend WithEvents radIntDivision As System.Windows.Forms.RadioButton
    Friend WithEvents radDivision As System.Windows.Forms.RadioButton
    Friend WithEvents radMultiplcation As System.Windows.Forms.RadioButton
    Friend WithEvents radExponentiation As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblOperand1 = New System.Windows.Forms.Label
        Me.lblOperand2 = New System.Windows.Forms.Label
        Me.lblResult = New System.Windows.Forms.Label
        Me.lblExpressionValue = New System.Windows.Forms.Label
        Me.grpOperators = New System.Windows.Forms.GroupBox
        Me.radSubtraction = New System.Windows.Forms.RadioButton
        Me.radAddition = New System.Windows.Forms.RadioButton
        Me.radModDivision = New System.Windows.Forms.RadioButton
        Me.radIntDivision = New System.Windows.Forms.RadioButton
        Me.radDivision = New System.Windows.Forms.RadioButton
        Me.radMultiplcation = New System.Windows.Forms.RadioButton
        Me.radExponentiation = New System.Windows.Forms.RadioButton
        Me.txtOperand2 = New System.Windows.Forms.TextBox
        Me.txtOperand1 = New System.Windows.Forms.TextBox
        Me.grpOperators.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOperand1
        '
        Me.lblOperand1.Location = New System.Drawing.Point(32, 24)
        Me.lblOperand1.Name = "lblOperand1"
        Me.lblOperand1.Size = New System.Drawing.Size(80, 23)
        Me.lblOperand1.TabIndex = 0
        Me.lblOperand1.Text = "First Operand:"
        '
        'lblOperand2
        '
        Me.lblOperand2.Location = New System.Drawing.Point(168, 24)
        Me.lblOperand2.Name = "lblOperand2"
        Me.lblOperand2.Size = New System.Drawing.Size(96, 23)
        Me.lblOperand2.TabIndex = 1
        Me.lblOperand2.Text = "Second Operand:"
        '
        'lblResult
        '
        Me.lblResult.Location = New System.Drawing.Point(16, 144)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(40, 23)
        Me.lblResult.TabIndex = 2
        Me.lblResult.Text = "Result:"
        '
        'lblExpressionValue
        '
        Me.lblExpressionValue.Location = New System.Drawing.Point(64, 144)
        Me.lblExpressionValue.Name = "lblExpressionValue"
        Me.lblExpressionValue.Size = New System.Drawing.Size(128, 23)
        Me.lblExpressionValue.TabIndex = 3
        '
        'grpOperators
        '
        Me.grpOperators.Controls.Add(Me.radSubtraction)
        Me.grpOperators.Controls.Add(Me.radAddition)
        Me.grpOperators.Controls.Add(Me.radModDivision)
        Me.grpOperators.Controls.Add(Me.radIntDivision)
        Me.grpOperators.Controls.Add(Me.radDivision)
        Me.grpOperators.Controls.Add(Me.radMultiplcation)
        Me.grpOperators.Controls.Add(Me.radExponentiation)
        Me.grpOperators.Location = New System.Drawing.Point(16, 64)
        Me.grpOperators.Name = "grpOperators"
        Me.grpOperators.Size = New System.Drawing.Size(344, 64)
        Me.grpOperators.TabIndex = 4
        Me.grpOperators.TabStop = False
        Me.grpOperators.Text = "Select an Operator"
        '
        'radSubtraction
        '
        Me.radSubtraction.Location = New System.Drawing.Point(296, 24)
        Me.radSubtraction.Name = "radSubtraction"
        Me.radSubtraction.Size = New System.Drawing.Size(40, 24)
        Me.radSubtraction.TabIndex = 6
        Me.radSubtraction.Text = "-"
        '
        'radAddition
        '
        Me.radAddition.Location = New System.Drawing.Point(248, 24)
        Me.radAddition.Name = "radAddition"
        Me.radAddition.Size = New System.Drawing.Size(40, 24)
        Me.radAddition.TabIndex = 5
        Me.radAddition.Text = "+"
        '
        'radModDivision
        '
        Me.radModDivision.Location = New System.Drawing.Point(192, 24)
        Me.radModDivision.Name = "radModDivision"
        Me.radModDivision.Size = New System.Drawing.Size(48, 24)
        Me.radModDivision.TabIndex = 4
        Me.radModDivision.Text = "Mod"
        '
        'radIntDivision
        '
        Me.radIntDivision.Location = New System.Drawing.Point(152, 24)
        Me.radIntDivision.Name = "radIntDivision"
        Me.radIntDivision.Size = New System.Drawing.Size(40, 24)
        Me.radIntDivision.TabIndex = 3
        Me.radIntDivision.Text = "\"
        '
        'radDivision
        '
        Me.radDivision.Location = New System.Drawing.Point(104, 24)
        Me.radDivision.Name = "radDivision"
        Me.radDivision.Size = New System.Drawing.Size(40, 24)
        Me.radDivision.TabIndex = 2
        Me.radDivision.Text = "/"
        '
        'radMultiplcation
        '
        Me.radMultiplcation.Location = New System.Drawing.Point(64, 24)
        Me.radMultiplcation.Name = "radMultiplcation"
        Me.radMultiplcation.Size = New System.Drawing.Size(40, 24)
        Me.radMultiplcation.TabIndex = 1
        Me.radMultiplcation.Text = "*"
        '
        'radExponentiation
        '
        Me.radExponentiation.Location = New System.Drawing.Point(16, 24)
        Me.radExponentiation.Name = "radExponentiation"
        Me.radExponentiation.Size = New System.Drawing.Size(40, 24)
        Me.radExponentiation.TabIndex = 0
        Me.radExponentiation.Text = "^"
        '
        'txtOperand2
        '
        Me.txtOperand2.Location = New System.Drawing.Point(264, 24)
        Me.txtOperand2.Name = "txtOperand2"
        Me.txtOperand2.Size = New System.Drawing.Size(32, 20)
        Me.txtOperand2.TabIndex = 2
        Me.txtOperand2.Text = ""
        '
        'txtOperand1
        '
        Me.txtOperand1.Location = New System.Drawing.Point(112, 24)
        Me.txtOperand1.Name = "txtOperand1"
        Me.txtOperand1.Size = New System.Drawing.Size(32, 20)
        Me.txtOperand1.TabIndex = 1
        Me.txtOperand1.Text = ""
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(384, 182)
        Me.Controls.Add(Me.grpOperators)
        Me.Controls.Add(Me.lblExpressionValue)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.lblOperand2)
        Me.Controls.Add(Me.lblOperand1)
        Me.Controls.Add(Me.txtOperand1)
        Me.Controls.Add(Me.txtOperand2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Calculator"
        Me.grpOperators.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Sub UncheckAll()
        radAddition.Checked = False
        radDivision.Checked = False
        radExponentiation.Checked = False
        radIntDivision.Checked = False
        radModDivision.Checked = False
        radMultiplcation.Checked = False
        radSubtraction.Checked = False
    End Sub

    Private Sub txtOperand1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOperand1.TextChanged
        UncheckAll()
    End Sub

    Private Sub txtOperand2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOperand2.TextChanged
        UncheckAll()
    End Sub

    Private Sub radExponentiation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radExponentiation.Click
        UncheckAll()
        radExponentiation.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) ^ Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub

    Private Sub radSubtraction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSubtraction.Click
        UncheckAll()
        radSubtraction.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) - Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub

    Private Sub radAddition_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAddition.Click
        UncheckAll()
        radAddition.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) + Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub

    Private Sub radMutliplcation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radMultiplcation.Click
        UncheckAll()
        radMultiplcation.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) * Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub

    Private Sub radModDivision_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radModDivision.Click
        UncheckAll()
        radModDivision.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) Mod Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub

    Private Sub radIntDivision_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radIntDivision.Click
        UncheckAll()
        radIntDivision.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) \ Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub

    Private Sub radDivision_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radDivision.Click
        UncheckAll()
        radDivision.Checked = True
        Dim answer As Single
        answer = Val(txtOperand1.Text) / Val(txtOperand2.Text)
        lblExpressionValue.Text = answer
    End Sub
End Class
