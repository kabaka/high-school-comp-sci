Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim OrderNumber As Integer = 0

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents gboToppings As System.Windows.Forms.GroupBox
    Friend WithEvents cboPep As System.Windows.Forms.CheckBox
    Friend WithEvents cboOnion As System.Windows.Forms.CheckBox
    Friend WithEvents cboMush As System.Windows.Forms.CheckBox
    Friend WithEvents cboHotPep As System.Windows.Forms.CheckBox
    Friend WithEvents gopSize As System.Windows.Forms.GroupBox
    Friend WithEvents rdbReg As System.Windows.Forms.RadioButton
    Friend WithEvents rdbLarge As System.Windows.Forms.RadioButton
    Friend WithEvents btnOrder As System.Windows.Forms.Button
    Friend WithEvents btnNewOrd As System.Windows.Forms.Button
    Friend WithEvents lblNumber As System.Windows.Forms.Label
    Friend WithEvents lblNumberOut As System.Windows.Forms.Label
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents lblPriceOut As System.Windows.Forms.Label
    Friend WithEvents gboOrderType As System.Windows.Forms.GroupBox
    Friend WithEvents rdbPickup As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDelivery As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.gboToppings = New System.Windows.Forms.GroupBox
        Me.cboPep = New System.Windows.Forms.CheckBox
        Me.cboOnion = New System.Windows.Forms.CheckBox
        Me.cboMush = New System.Windows.Forms.CheckBox
        Me.cboHotPep = New System.Windows.Forms.CheckBox
        Me.gopSize = New System.Windows.Forms.GroupBox
        Me.rdbReg = New System.Windows.Forms.RadioButton
        Me.rdbLarge = New System.Windows.Forms.RadioButton
        Me.btnOrder = New System.Windows.Forms.Button
        Me.btnNewOrd = New System.Windows.Forms.Button
        Me.lblNumber = New System.Windows.Forms.Label
        Me.lblNumberOut = New System.Windows.Forms.Label
        Me.lblPrice = New System.Windows.Forms.Label
        Me.lblPriceOut = New System.Windows.Forms.Label
        Me.gboOrderType = New System.Windows.Forms.GroupBox
        Me.rdbDelivery = New System.Windows.Forms.RadioButton
        Me.rdbPickup = New System.Windows.Forms.RadioButton
        Me.gboToppings.SuspendLayout()
        Me.gopSize.SuspendLayout()
        Me.gboOrderType.SuspendLayout()
        Me.SuspendLayout()
        '
        'gboToppings
        '
        Me.gboToppings.Controls.Add(Me.cboMush)
        Me.gboToppings.Controls.Add(Me.cboOnion)
        Me.gboToppings.Controls.Add(Me.cboPep)
        Me.gboToppings.Controls.Add(Me.cboHotPep)
        Me.gboToppings.Location = New System.Drawing.Point(24, 24)
        Me.gboToppings.Name = "gboToppings"
        Me.gboToppings.Size = New System.Drawing.Size(296, 80)
        Me.gboToppings.TabIndex = 0
        Me.gboToppings.TabStop = False
        Me.gboToppings.Text = "Select Toppings"
        '
        'cboPep
        '
        Me.cboPep.Location = New System.Drawing.Point(24, 16)
        Me.cboPep.Name = "cboPep"
        Me.cboPep.TabIndex = 0
        Me.cboPep.Text = "Pepperoni"
        '
        'cboOnion
        '
        Me.cboOnion.Location = New System.Drawing.Point(168, 16)
        Me.cboOnion.Name = "cboOnion"
        Me.cboOnion.TabIndex = 1
        Me.cboOnion.Text = "Onions"
        '
        'cboMush
        '
        Me.cboMush.Location = New System.Drawing.Point(24, 48)
        Me.cboMush.Name = "cboMush"
        Me.cboMush.TabIndex = 2
        Me.cboMush.Text = "Mushrooms"
        '
        'cboHotPep
        '
        Me.cboHotPep.Location = New System.Drawing.Point(168, 48)
        Me.cboHotPep.Name = "cboHotPep"
        Me.cboHotPep.TabIndex = 3
        Me.cboHotPep.Text = "Hot Peppers"
        '
        'gopSize
        '
        Me.gopSize.Controls.Add(Me.rdbLarge)
        Me.gopSize.Controls.Add(Me.rdbReg)
        Me.gopSize.Location = New System.Drawing.Point(24, 112)
        Me.gopSize.Name = "gopSize"
        Me.gopSize.Size = New System.Drawing.Size(296, 64)
        Me.gopSize.TabIndex = 1
        Me.gopSize.TabStop = False
        Me.gopSize.Text = "Select Pizza Size"
        '
        'rdbReg
        '
        Me.rdbReg.Location = New System.Drawing.Point(16, 24)
        Me.rdbReg.Name = "rdbReg"
        Me.rdbReg.TabIndex = 0
        Me.rdbReg.Text = "Regular"
        '
        'rdbLarge
        '
        Me.rdbLarge.Location = New System.Drawing.Point(168, 24)
        Me.rdbLarge.Name = "rdbLarge"
        Me.rdbLarge.TabIndex = 1
        Me.rdbLarge.Text = "Large"
        '
        'btnOrder
        '
        Me.btnOrder.Location = New System.Drawing.Point(24, 256)
        Me.btnOrder.Name = "btnOrder"
        Me.btnOrder.Size = New System.Drawing.Size(80, 23)
        Me.btnOrder.TabIndex = 2
        Me.btnOrder.Text = "&Place Order"
        '
        'btnNewOrd
        '
        Me.btnNewOrd.Location = New System.Drawing.Point(24, 288)
        Me.btnNewOrd.Name = "btnNewOrd"
        Me.btnNewOrd.Size = New System.Drawing.Size(80, 23)
        Me.btnNewOrd.TabIndex = 3
        Me.btnNewOrd.Text = "&New Order"
        '
        'lblNumber
        '
        Me.lblNumber.Location = New System.Drawing.Point(128, 256)
        Me.lblNumber.Name = "lblNumber"
        Me.lblNumber.Size = New System.Drawing.Size(80, 23)
        Me.lblNumber.TabIndex = 4
        Me.lblNumber.Text = "Order Number:"
        '
        'lblNumberOut
        '
        Me.lblNumberOut.Location = New System.Drawing.Point(208, 256)
        Me.lblNumberOut.Name = "lblNumberOut"
        Me.lblNumberOut.Size = New System.Drawing.Size(120, 23)
        Me.lblNumberOut.TabIndex = 5
        '
        'lblPrice
        '
        Me.lblPrice.Location = New System.Drawing.Point(128, 288)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(48, 23)
        Me.lblPrice.TabIndex = 6
        Me.lblPrice.Text = "Price: $"
        '
        'lblPriceOut
        '
        Me.lblPriceOut.Location = New System.Drawing.Point(176, 288)
        Me.lblPriceOut.Name = "lblPriceOut"
        Me.lblPriceOut.Size = New System.Drawing.Size(152, 23)
        Me.lblPriceOut.TabIndex = 7
        '
        'gboOrderType
        '
        Me.gboOrderType.Controls.Add(Me.rdbDelivery)
        Me.gboOrderType.Controls.Add(Me.rdbPickup)
        Me.gboOrderType.Location = New System.Drawing.Point(24, 184)
        Me.gboOrderType.Name = "gboOrderType"
        Me.gboOrderType.Size = New System.Drawing.Size(296, 64)
        Me.gboOrderType.TabIndex = 2
        Me.gboOrderType.TabStop = False
        Me.gboOrderType.Text = "Select Order Type"
        '
        'rdbDelivery
        '
        Me.rdbDelivery.Location = New System.Drawing.Point(168, 24)
        Me.rdbDelivery.Name = "rdbDelivery"
        Me.rdbDelivery.TabIndex = 1
        Me.rdbDelivery.Text = "Delivery"
        '
        'rdbPickup
        '
        Me.rdbPickup.Location = New System.Drawing.Point(16, 24)
        Me.rdbPickup.Name = "rdbPickup"
        Me.rdbPickup.TabIndex = 0
        Me.rdbPickup.Text = "Pickup"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(344, 326)
        Me.Controls.Add(Me.lblPriceOut)
        Me.Controls.Add(Me.lblPrice)
        Me.Controls.Add(Me.lblNumberOut)
        Me.Controls.Add(Me.lblNumber)
        Me.Controls.Add(Me.btnNewOrd)
        Me.Controls.Add(Me.btnOrder)
        Me.Controls.Add(Me.gopSize)
        Me.Controls.Add(Me.gboToppings)
        Me.Controls.Add(Me.gboOrderType)
        Me.Name = "Form1"
        Me.Text = "Pizza Order"
        Me.gboToppings.ResumeLayout(False)
        Me.gopSize.ResumeLayout(False)
        Me.gboOrderType.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub ResetAll()
        cboPep.Checked = False
        cboMush.Checked = False
        cboOnion.Checked = False
        cboHotPep.Checked = False
        rdbReg.Checked = True
        rdbLarge.Checked = False

        rdbPickup.Checked = True

        lblPriceOut.Text = ""
        lblNumberOut.Text = ""
    End Sub

    Private Sub btnOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOrder.Click
        Dim NumToppings As Integer
        Dim Total As Double
        OrderNumber += 1

        'toppings
        If cboPep.Checked Then NumToppings += 1
        If cboMush.Checked Then NumToppings += 1
        If cboOnion.Checked Then NumToppings += 1
        If cboHotPep.Checked Then NumToppings += 1

        If NumToppings = 1 Then Total += 1
        If NumToppings = 2 Then Total += 1.75
        If NumToppings = 3 Then Total += 2.5
        If NumToppings = 4 Then Total += 3.25

        'size
        If rdbReg.Checked Then Total += 6
        If rdbLarge.Checked Then Total += 10

        'type
        If rdbDelivery.Checked Then Total += 1.5

        lblNumberOut.Text = OrderNumber
        lblPriceOut.Text = Total
    End Sub

    Private Sub btnNewOrd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewOrd.Click
        ResetAll()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ResetAll()
    End Sub
End Class
