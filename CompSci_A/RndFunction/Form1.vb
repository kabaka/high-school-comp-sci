Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim Numbers(100) As Decimal

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lblNumbers As System.Windows.Forms.Label
    Friend WithEvents lstNumbersDisplay As System.Windows.Forms.ListBox
    Friend WithEvents btnTest As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstNumbersDisplay = New System.Windows.Forms.ListBox
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.lblNumbers = New System.Windows.Forms.Label
        Me.btnTest = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lstNumbersDisplay
        '
        Me.lstNumbersDisplay.Location = New System.Drawing.Point(16, 40)
        Me.lstNumbersDisplay.Name = "lstNumbersDisplay"
        Me.lstNumbersDisplay.Size = New System.Drawing.Size(504, 186)
        Me.lstNumbersDisplay.TabIndex = 0
        '
        'btnGenerate
        '
        Me.btnGenerate.Location = New System.Drawing.Point(528, 32)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.TabIndex = 1
        Me.btnGenerate.Text = "&Generate"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(528, 64)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.TabIndex = 2
        Me.btnClear.Text = "&Clear"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(528, 192)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "E&xit"
        '
        'lblNumbers
        '
        Me.lblNumbers.Location = New System.Drawing.Point(16, 16)
        Me.lblNumbers.Name = "lblNumbers"
        Me.lblNumbers.TabIndex = 4
        Me.lblNumbers.Text = "Numbers"
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(528, 96)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.TabIndex = 6
        Me.btnTest.Text = "&Test"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(616, 266)
        Me.Controls.Add(Me.btnTest)
        Me.Controls.Add(Me.lblNumbers)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.lstNumbersDisplay)
        Me.Name = "Form1"
        Me.Text = "Random Numbers"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lstNumbersDisplay.Items.Clear()
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        lstNumbersDisplay.Items.Clear()
        Dim i As Integer
        For i = 0 To 99
            Dim x As Double
            Randomize()
            x = CInt(100 * Rnd()) - 1
            Numbers(i) = x
            lstNumbersDisplay.Items.Add(x)
        Next i
    End Sub

    Private Sub ButtonTesting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        lstNumbersDisplay.Items.Clear()
        Dim i As Integer
        Dim x As Decimal = 1
        For i = 0 To 94
            x = x * 2
            Numbers(i) = x
            On Error Resume Next
            lstNumbersDisplay.Items.Add(x)
        Next i
    End Sub
End Class
