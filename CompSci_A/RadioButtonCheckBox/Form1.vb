Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblIWant As System.Windows.Forms.Label
    Friend WithEvents lblIWantWith As System.Windows.Forms.Label
    Friend WithEvents chkPickles As System.Windows.Forms.CheckBox
    Friend WithEvents chkOnions As System.Windows.Forms.CheckBox
    Friend WithEvents chkTomatoes As System.Windows.Forms.CheckBox
    Friend WithEvents rdbSaladBar As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSoup As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDessert As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblIWant = New System.Windows.Forms.Label
        Me.lblIWantWith = New System.Windows.Forms.Label
        Me.chkPickles = New System.Windows.Forms.CheckBox
        Me.chkOnions = New System.Windows.Forms.CheckBox
        Me.chkTomatoes = New System.Windows.Forms.CheckBox
        Me.rdbSaladBar = New System.Windows.Forms.RadioButton
        Me.rdbSoup = New System.Windows.Forms.RadioButton
        Me.rdbDessert = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'lblIWant
        '
        Me.lblIWant.Location = New System.Drawing.Point(20, 16)
        Me.lblIWant.Name = "lblIWant"
        Me.lblIWant.Size = New System.Drawing.Size(112, 48)
        Me.lblIWant.TabIndex = 0
        Me.lblIWant.Text = "I want the following on my sandwich"
        '
        'lblIWantWith
        '
        Me.lblIWantWith.Location = New System.Drawing.Point(172, 16)
        Me.lblIWantWith.Name = "lblIWantWith"
        Me.lblIWantWith.Size = New System.Drawing.Size(136, 48)
        Me.lblIWantWith.TabIndex = 1
        Me.lblIWantWith.Text = "I want it with"
        '
        'chkPickles
        '
        Me.chkPickles.Location = New System.Drawing.Point(28, 93)
        Me.chkPickles.Name = "chkPickles"
        Me.chkPickles.TabIndex = 2
        Me.chkPickles.Text = "Pickles"
        '
        'chkOnions
        '
        Me.chkOnions.Location = New System.Drawing.Point(28, 146)
        Me.chkOnions.Name = "chkOnions"
        Me.chkOnions.TabIndex = 3
        Me.chkOnions.Text = "Onions"
        '
        'chkTomatoes
        '
        Me.chkTomatoes.Location = New System.Drawing.Point(28, 199)
        Me.chkTomatoes.Name = "chkTomatoes"
        Me.chkTomatoes.TabIndex = 4
        Me.chkTomatoes.Text = "Tomatoes"
        '
        'rdbSaladBar
        '
        Me.rdbSaladBar.Location = New System.Drawing.Point(172, 93)
        Me.rdbSaladBar.Name = "rdbSaladBar"
        Me.rdbSaladBar.TabIndex = 5
        Me.rdbSaladBar.Text = "Salad Bar"
        '
        'rdbSoup
        '
        Me.rdbSoup.Location = New System.Drawing.Point(172, 146)
        Me.rdbSoup.Name = "rdbSoup"
        Me.rdbSoup.TabIndex = 6
        Me.rdbSoup.Text = "Soup"
        '
        'rdbDessert
        '
        Me.rdbDessert.Location = New System.Drawing.Point(172, 199)
        Me.rdbDessert.Name = "rdbDessert"
        Me.rdbDessert.TabIndex = 7
        Me.rdbDessert.Text = "Dessert"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(328, 238)
        Me.Controls.Add(Me.rdbDessert)
        Me.Controls.Add(Me.rdbSoup)
        Me.Controls.Add(Me.rdbSaladBar)
        Me.Controls.Add(Me.chkTomatoes)
        Me.Controls.Add(Me.chkOnions)
        Me.Controls.Add(Me.chkPickles)
        Me.Controls.Add(Me.lblIWantWith)
        Me.Controls.Add(Me.lblIWant)
        Me.Name = "Form1"
        Me.Text = "Using RadioButtons and CheckBoxes"
        Me.ResumeLayout(False)

    End Sub

#End Region

End Class
