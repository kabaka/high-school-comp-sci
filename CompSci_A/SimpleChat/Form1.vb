Imports System.Net.Sockets
Public Class Form1
    Inherits System.Windows.Forms.Form
    Private mobjClient As TcpClient
    Private marData(1024) As Byte
    Private mobjText As New StringBuilder
    Private Connected As Boolean

    Private Sub MarkAsDisconnected()
        Connected = False
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtOutput As System.Windows.Forms.RichTextBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents txtInput As System.Windows.Forms.RichTextBox
    Friend WithEvents btnSend As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtOutput = New System.Windows.Forms.RichTextBox
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.txtInput = New System.Windows.Forms.RichTextBox
        Me.btnSend = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtOutput
        '
        Me.txtOutput.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtOutput.Location = New System.Drawing.Point(0, 0)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Size = New System.Drawing.Size(292, 96)
        Me.txtOutput.TabIndex = 0
        Me.txtOutput.Text = ""
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "File"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Exit"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem4, Me.MenuItem5})
        Me.MenuItem3.Text = "Network"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 0
        Me.MenuItem4.Text = "Connect"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 1
        Me.MenuItem5.Text = "Disconnect"
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(0, 120)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.Size = New System.Drawing.Size(296, 56)
        Me.txtInput.TabIndex = 1
        Me.txtInput.Text = ""
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(216, 184)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(72, 24)
        Me.btnSend.TabIndex = 2
        Me.btnSend.Text = "Send"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 217)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.txtInput)
        Me.Controls.Add(Me.txtOutput)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "SimpleChat"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Disp(ByVal t As String)
        txtOutput.AppendText(t)
    End Sub

    Private Sub Send(ByVal Data As String)
        Dim w As New IO.StreamWriter(mobjClient.GetStream)
        w.Write(Data & vbCr)
        w.Flush()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        End
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        mobjClient = New TcpClient("localhost", 5000)
        Disp("[SystemMessage: Connected to host]" & vbCrLf)
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, _
      ByVal e As System.EventArgs) Handles btnSend.Click
        Send(txtInput.Text)
        txtInput.Text = ""
    End Sub

    Private Sub DoRead(ByVal ar As IAsyncResult)
        Dim intCount As Integer

        Try
            intCount = mobjClient.GetStream.EndRead(ar)
            If intCount < 1 Then
                Disp("[Disconnected]")
                Exit Sub
            End If

            BuildString(marData, 0, intCount)

            mobjClient.GetStream.BeginRead(marData, 0, 1024, _
              AddressOf DoRead, Nothing)
        Catch e As Exception
            MarkAsDisconnected()
        End Try
    End Sub

    Private Sub BuildString(ByVal Bytes() As Byte, _
          ByVal offset As Integer, ByVal count As Integer)
        Dim intIndex As Integer

        For intIndex = offset To offset + count - 1
            If Bytes(intIndex) = 10 Then
                mobjText.Append(vbLf)

                Dim params() As Object = {mobjText.ToString}
                Me.Invoke(New DisplayInvoker(AddressOf Me.DisplayText), params)

                mobjText = New StringBuilder
            Else
                mobjText.Append(ChrW(Bytes(intIndex)))
            End If
        Next
    End Sub

End Class
