Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbInsertCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbUpdateCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbDeleteCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Friend WithEvents DsPortableCD1 As CDPlayers.dsPortableCD
    Friend WithEvents DataGrid2 As System.Windows.Forms.DataGrid
    Friend WithEvents btnFillDataSet As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbDeleteCommand1 = New System.Data.OleDb.OleDbCommand
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection
        Me.OleDbInsertCommand1 = New System.Data.OleDb.OleDbCommand
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand
        Me.OleDbUpdateCommand1 = New System.Data.OleDb.OleDbCommand
        Me.DsPortableCD1 = New CDPlayers.dsPortableCD
        Me.DataGrid2 = New System.Windows.Forms.DataGrid
        Me.btnFillDataSet = New System.Windows.Forms.Button
        CType(Me.DsPortableCD1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.DeleteCommand = Me.OleDbDeleteCommand1
        Me.OleDbDataAdapter1.InsertCommand = Me.OleDbInsertCommand1
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Portable CD Players", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Manufacturer", "Manufacturer"), New System.Data.Common.DataColumnMapping("Model", "Model"), New System.Data.Common.DataColumnMapping("Price", "Price")})})
        Me.OleDbDataAdapter1.UpdateCommand = Me.OleDbUpdateCommand1
        '
        'OleDbDeleteCommand1
        '
        Me.OleDbDeleteCommand1.CommandText = "DELETE FROM [Portable CD Players] WHERE (Model = ?) AND (Manufacturer = ?) AND (P" & _
        "rice = ?)"
        Me.OleDbDeleteCommand1.Connection = Me.OleDbConnection1
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Model", System.Data.OleDb.OleDbType.VarWChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Model", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Manufacturer", System.Data.OleDb.OleDbType.VarWChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Manufacturer", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Price", System.Data.OleDb.OleDbType.Currency, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Price", System.Data.DataRowVersion.Original, Nothing))
        '
        'OleDbConnection1
        '
        Me.OleDbConnection1.ConnectionString = "Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Registry Path=;Jet OLEDB:Database L" & _
        "ocking Mode=0;Data Source=""C:\Documents and Settings\WASStudent\My Documents\tem" & _
        "p\db\portableCD.mdb"";Jet OLEDB:Engine Type=4;Provider=""Microsoft.Jet.OLEDB.4.0"";" & _
        "Jet OLEDB:System database=;Jet OLEDB:SFP=False;persist security info=False;Exten" & _
        "ded Properties=;Mode=Share Deny None;Jet OLEDB:Encrypt Database=False;Jet OLEDB:" & _
        "Create System Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OL" & _
        "EDB:Compact Without Replica Repair=False;User ID=Admin;Jet OLEDB:Global Bulk Tra" & _
        "nsactions=1"
        '
        'OleDbInsertCommand1
        '
        Me.OleDbInsertCommand1.CommandText = "INSERT INTO [Portable CD Players] (Manufacturer, Model, Price) VALUES (?, ?, ?)"
        Me.OleDbInsertCommand1.Connection = Me.OleDbConnection1
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Manufacturer", System.Data.OleDb.OleDbType.VarWChar, 50, "Manufacturer"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Model", System.Data.OleDb.OleDbType.VarWChar, 20, "Model"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Price", System.Data.OleDb.OleDbType.Currency, 0, "Price"))
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT Manufacturer, Model, Price FROM [Portable CD Players]"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        '
        'OleDbUpdateCommand1
        '
        Me.OleDbUpdateCommand1.CommandText = "UPDATE [Portable CD Players] SET Manufacturer = ?, Model = ?, Price = ? WHERE (Mo" & _
        "del = ?) AND (Manufacturer = ?) AND (Price = ?)"
        Me.OleDbUpdateCommand1.Connection = Me.OleDbConnection1
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Manufacturer", System.Data.OleDb.OleDbType.VarWChar, 50, "Manufacturer"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Model", System.Data.OleDb.OleDbType.VarWChar, 20, "Model"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Price", System.Data.OleDb.OleDbType.Currency, 0, "Price"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Model", System.Data.OleDb.OleDbType.VarWChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Model", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Manufacturer", System.Data.OleDb.OleDbType.VarWChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Manufacturer", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Price", System.Data.OleDb.OleDbType.Currency, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Price", System.Data.DataRowVersion.Original, Nothing))
        '
        'DsPortableCD1
        '
        Me.DsPortableCD1.DataSetName = "dsPortableCD"
        Me.DsPortableCD1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'DataGrid2
        '
        Me.DataGrid2.AlternatingBackColor = System.Drawing.Color.BurlyWood
        Me.DataGrid2.BackColor = System.Drawing.Color.Gainsboro
        Me.DataGrid2.CaptionFont = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGrid2.CaptionText = "CDPlayers"
        Me.DataGrid2.DataMember = ""
        Me.DataGrid2.DataSource = Me.DsPortableCD1.Portable_CD_Players
        Me.DataGrid2.Dock = System.Windows.Forms.DockStyle.Top
        Me.DataGrid2.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DataGrid2.HeaderBackColor = System.Drawing.Color.DarkSalmon
        Me.DataGrid2.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid2.Location = New System.Drawing.Point(0, 0)
        Me.DataGrid2.Name = "DataGrid2"
        Me.DataGrid2.Size = New System.Drawing.Size(264, 240)
        Me.DataGrid2.TabIndex = 1
        '
        'btnFillDataSet
        '
        Me.btnFillDataSet.Location = New System.Drawing.Point(0, 240)
        Me.btnFillDataSet.Name = "btnFillDataSet"
        Me.btnFillDataSet.TabIndex = 2
        Me.btnFillDataSet.Text = "&Load"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(264, 262)
        Me.Controls.Add(Me.btnFillDataSet)
        Me.Controls.Add(Me.DataGrid2)
        Me.Name = "Form1"
        Me.Text = "CD Players"
        CType(Me.DsPortableCD1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnFillDataSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFillDataSet.Click
        OleDbDataAdapter1.Fill(DsPortableCD1)
    End Sub

    Protected Overrides Sub OnMouseMove(ByVal e As System.Windows.Forms.MouseEventArgs)
        DataGrid2.Enabled = False
        Dim pnt As Point
        pnt.Y = Me.Cursor.Position.Y()
        pnt.X = Me.Cursor.Position.X()
        Me.Location() = pnt
    End Sub
End Class
