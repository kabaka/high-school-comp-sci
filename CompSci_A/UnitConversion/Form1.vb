Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnConvert As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents cboConversion As System.Windows.Forms.ComboBox
    Friend WithEvents txtInput As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblOutput As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboConversion = New System.Windows.Forms.ComboBox
        Me.txtInput = New System.Windows.Forms.TextBox
        Me.btnConvert = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblOutput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cboConversion
        '
        Me.cboConversion.Items.AddRange(New Object() {"Feet-To-Inches", "Centimeters-To-Inches", "Miles-To-Meters"})
        Me.cboConversion.Location = New System.Drawing.Point(152, 16)
        Me.cboConversion.Name = "cboConversion"
        Me.cboConversion.Size = New System.Drawing.Size(121, 21)
        Me.cboConversion.TabIndex = 0
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(24, 16)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.Size = New System.Drawing.Size(120, 20)
        Me.txtInput.TabIndex = 1
        Me.txtInput.Text = ""
        '
        'btnConvert
        '
        Me.btnConvert.Location = New System.Drawing.Point(112, 48)
        Me.btnConvert.Name = "btnConvert"
        Me.btnConvert.TabIndex = 2
        Me.btnConvert.Text = "&Convert"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(200, 48)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "E&xit"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(288, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 23)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "="
        '
        'lblOutput
        '
        Me.lblOutput.BackColor = System.Drawing.Color.White
        Me.lblOutput.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutput.Location = New System.Drawing.Point(320, 16)
        Me.lblOutput.Name = "lblOutput"
        Me.lblOutput.TabIndex = 5
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(448, 86)
        Me.Controls.Add(Me.lblOutput)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnConvert)
        Me.Controls.Add(Me.txtInput)
        Me.Controls.Add(Me.cboConversion)
        Me.Name = "Form1"
        Me.Text = "Unit Conversion Program"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnConvert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConvert.Click
        Dim Input As Decimal
        Dim Output As Decimal
        Select Case cboConversion.SelectedIndex
            Case 0
                Input = CDec(txtInput.Text)
                Output = Input * 12
                lblOutput.Text = Output
            Case 1
                Input = CDec(txtInput.Text)
                Output = Input * 2.54
                lblOutput.Text = Output
            Case 2
                Input = CDec(txtInput.Text)
                Output = Input * 5280 * 12 * 2.54 / 10
                lblOutput.Text = Output
        End Select
    End Sub
End Class
