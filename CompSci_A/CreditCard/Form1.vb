Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lstCreditList As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.lstCreditList = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(104, 152)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "E&xit"
        '
        'lstCreditList
        '
        Me.lstCreditList.Items.AddRange(New Object() {"Discovery", "Master Card", "VISA", "VISA Gold", "Platinum"})
        Me.lstCreditList.Location = New System.Drawing.Point(8, 8)
        Me.lstCreditList.Name = "lstCreditList"
        Me.lstCreditList.Size = New System.Drawing.Size(168, 134)
        Me.lstCreditList.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(184, 182)
        Me.Controls.Add(Me.lstCreditList)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "Form1"
        Me.Text = "Credit Limits"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub lstCreditList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstCreditList.SelectedIndexChanged
        Dim Index As Integer = lstCreditList.SelectedIndex
        MessageBox.Show("Selected index: " & Index.ToString, "Index")
        Select Case Index
            Case 0 'Discover Card
                MessageBox.Show("$1500", "Credit Limit")
            Case 1 'Master Card
                MessageBox.Show("$2000", "Credit Limit")
            Case 2 'VISA
                MessageBox.Show("$3000", "Credit Limit")
            Case 3 'VISA Gold
                MessageBox.Show("$3500", "Credit Limit")
            Case 4 'Platinum
                MessageBox.Show("$5000", "Credit Limit")
            Case Else
                MessageBox.Show("Illegal Entry")
        End Select
    End Sub
End Class
