Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnShowPrimes As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnShowPrimes = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnShowPrimes
        '
        Me.btnShowPrimes.Location = New System.Drawing.Point(16, 16)
        Me.btnShowPrimes.Name = "btnShowPrimes"
        Me.btnShowPrimes.Size = New System.Drawing.Size(168, 64)
        Me.btnShowPrimes.TabIndex = 0
        Me.btnShowPrimes.Text = "Show primes!"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(104, 96)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "E&xit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(200, 126)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnShowPrimes)
        Me.Name = "Form1"
        Me.Text = "Prime Numbers"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnShowPrimes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowPrimes.Click
        Dim x As Integer
        Dim prime As Integer
        For x = 1 To 41 Step 1
            prime = x ^ 2 - x + 41
            MessageBox.Show("Prime: " & prime & Chr(10) & Chr(13) & x)
        Next x
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub
End Class