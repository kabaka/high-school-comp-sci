Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim logWriter As New StreamWriter("log.txt", True)
    Dim Listener As New Thread(AddressOf ListenerThread)
    Dim SavePath As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtPort As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnStartServer As System.Windows.Forms.Button
    Friend WithEvents txtDestination As System.Windows.Forms.TextBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtPort = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnStartServer = New System.Windows.Forms.Button
        Me.txtDestination = New System.Windows.Forms.TextBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.txtOutput = New System.Windows.Forms.TextBox
        Me.btnClear = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(80, 8)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(120, 20)
        Me.txtPort.TabIndex = 0
        Me.txtPort.Text = "8700"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(32, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Port"
        '
        'btnStartServer
        '
        Me.btnStartServer.Location = New System.Drawing.Point(8, 240)
        Me.btnStartServer.Name = "btnStartServer"
        Me.btnStartServer.Size = New System.Drawing.Size(80, 23)
        Me.btnStartServer.TabIndex = 2
        Me.btnStartServer.Text = "&Start Server"
        '
        'txtDestination
        '
        Me.txtDestination.Location = New System.Drawing.Point(80, 40)
        Me.txtDestination.Name = "txtDestination"
        Me.txtDestination.Size = New System.Drawing.Size(120, 20)
        Me.txtDestination.TabIndex = 3
        Me.txtDestination.Text = ""
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(208, 240)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "E&xit"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 32)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Destination Folder"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(208, 40)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.TabIndex = 6
        Me.btnBrowse.Text = "&Browse"
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(8, 80)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOutput.Size = New System.Drawing.Size(272, 152)
        Me.txtOutput.TabIndex = 7
        Me.txtOutput.Text = ""
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(120, 240)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.TabIndex = 8
        Me.btnClear.Text = "&Clear"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtDestination)
        Me.Controls.Add(Me.btnStartServer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPort)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Log("Starting up...")
        txtDestination.Text = Environment.SpecialFolder.DesktopDirectory
        Log("Server Started!")

    End Sub

    Private Sub Log(ByVal Append As String)
        txtOutput.AppendText(Append & vbCrLf)
        logWriter.WriteLine(Append)
    End Sub

    Sub Quit()
        Log("Exiting...")
        logWriter.Close()
        Application.DoEvents()
        Application.Exit()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Quit()
    End Sub

    Private Sub Form1_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        Quit()
    End Sub

    Private Sub btnStartServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartServer.Click
        Log("Starting Server...")
        txtPort.Enabled = False
        txtDestination.Enabled = False
        SavePath = txtDestination.Text
        Listener.Start()
        If Listener.IsAlive Then
            Log("Server started successfully!")
        Else
            Log("Error starting server thread")
        End If
    End Sub

    'threads

    Sub ListenerThread()
        Dim port As Integer = CInt(txtPort.Text)
        Dim localAddr As IPAddress = IPAddress.Parse("127.0.0.1")
        Dim server As New TcpListener(localAddr, port)
        Dim serverSocket As Socket
        serverSocket = server.AcceptSocket


        While serverSocket.Connected

            Dim bytes As Byte()
            Dim filename As String
            bytes = Encoding.ASCII.GetBytes(0)

            While serverSocket.Available > 0
                serverSocket.Receive(bytes, 1, 0)
                filename = filename & Encoding.ASCII.GetString(bytes)
            End While

            Dim FileWriter As New BinaryWriter(File.OpenWrite(SavePath & filename))

            bytes = Encoding.ASCII.GetBytes("SENDOK")

            serverSocket.Send(bytes, bytes.Length, 0)

            While serverSocket.Available > 0
                FileWriter.Write(serverSocket.Receive(bytes, 1, 0))
            End While

        End While


    End Sub

End Class
