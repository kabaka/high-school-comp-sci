Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        'Initialize the combo boxes
        cboCapacity.Items.Add("30 gallon")
        cboCapacity.Items.Add("40 gallon")
        cboCapacity.Items.Add("50 gallon")
        cboPower.Items.Add("Natural Gas")
        cboPower.Items.Add("Electric")
        cboPower.Items.Add("LP Gas")
        cboWarranty.Items.Add("1 year")
        cboWarranty.Items.Add("4 year")
        cboWarranty.Items.Add("8 year")
        cboWarranty.Items.Add("10 year")
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblCapacity As System.Windows.Forms.Label
    Friend WithEvents lblPower As System.Windows.Forms.Label
    Friend WithEvents lblWarranty As System.Windows.Forms.Label
    Friend WithEvents cboCapacity As System.Windows.Forms.ComboBox
    Friend WithEvents cboPower As System.Windows.Forms.ComboBox
    Friend WithEvents cboWarranty As System.Windows.Forms.ComboBox
    Friend WithEvents lblModels As System.Windows.Forms.Label
    Friend WithEvents lstQualifying As System.Windows.Forms.ListBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblCapacity = New System.Windows.Forms.Label
        Me.lblPower = New System.Windows.Forms.Label
        Me.lblWarranty = New System.Windows.Forms.Label
        Me.cboCapacity = New System.Windows.Forms.ComboBox
        Me.cboPower = New System.Windows.Forms.ComboBox
        Me.cboWarranty = New System.Windows.Forms.ComboBox
        Me.lblModels = New System.Windows.Forms.Label
        Me.lstQualifying = New System.Windows.Forms.ListBox
        Me.btnSearch = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblCapacity
        '
        Me.lblCapacity.Location = New System.Drawing.Point(8, 16)
        Me.lblCapacity.Name = "lblCapacity"
        Me.lblCapacity.TabIndex = 0
        Me.lblCapacity.Text = "Capacity"
        '
        'lblPower
        '
        Me.lblPower.Location = New System.Drawing.Point(8, 80)
        Me.lblPower.Name = "lblPower"
        Me.lblPower.TabIndex = 1
        Me.lblPower.Text = "Power Source"
        '
        'lblWarranty
        '
        Me.lblWarranty.Location = New System.Drawing.Point(8, 144)
        Me.lblWarranty.Name = "lblWarranty"
        Me.lblWarranty.TabIndex = 2
        Me.lblWarranty.Text = "Warranty Period"
        '
        'cboCapacity
        '
        Me.cboCapacity.Location = New System.Drawing.Point(8, 40)
        Me.cboCapacity.Name = "cboCapacity"
        Me.cboCapacity.Size = New System.Drawing.Size(121, 21)
        Me.cboCapacity.TabIndex = 3
        '
        'cboPower
        '
        Me.cboPower.Location = New System.Drawing.Point(8, 104)
        Me.cboPower.Name = "cboPower"
        Me.cboPower.Size = New System.Drawing.Size(121, 21)
        Me.cboPower.TabIndex = 4
        '
        'cboWarranty
        '
        Me.cboWarranty.Location = New System.Drawing.Point(8, 168)
        Me.cboWarranty.Name = "cboWarranty"
        Me.cboWarranty.Size = New System.Drawing.Size(121, 21)
        Me.cboWarranty.TabIndex = 5
        '
        'lblModels
        '
        Me.lblModels.Location = New System.Drawing.Point(144, 16)
        Me.lblModels.Name = "lblModels"
        Me.lblModels.TabIndex = 6
        Me.lblModels.Text = "Qualifying Models"
        '
        'lstQualifying
        '
        Me.lstQualifying.Location = New System.Drawing.Point(144, 40)
        Me.lstQualifying.Name = "lstQualifying"
        Me.lstQualifying.Size = New System.Drawing.Size(120, 147)
        Me.lstQualifying.TabIndex = 7
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(24, 200)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.TabIndex = 8
        Me.btnSearch.Text = "&Search"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(128, 200)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "E&xit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(272, 230)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.lstQualifying)
        Me.Controls.Add(Me.lblModels)
        Me.Controls.Add(Me.cboWarranty)
        Me.Controls.Add(Me.cboPower)
        Me.Controls.Add(Me.cboCapacity)
        Me.Controls.Add(Me.lblWarranty)
        Me.Controls.Add(Me.lblPower)
        Me.Controls.Add(Me.lblCapacity)
        Me.Name = "Form1"
        Me.Text = "Choosing a Water Heater"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lstQualifying.Items.Clear()
        lstQualifying.Items.Add(cboCapacity.Text)
        lstQualifying.Items.Add(cboPower.Text)
        lstQualifying.Items.Add(cboWarranty.Text)
    End Sub
End Class
