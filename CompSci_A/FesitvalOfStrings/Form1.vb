Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents lblNameRev As System.Windows.Forms.Label
    Friend WithEvents lblRevOutput As System.Windows.Forms.Label
    Friend WithEvents blbLastNameFirst As System.Windows.Forms.Label
    Friend WithEvents lblLastFirstOutput As System.Windows.Forms.Label
    Friend WithEvents btnProcess As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtName = New System.Windows.Forms.TextBox
        Me.lblNameRev = New System.Windows.Forms.Label
        Me.lblRevOutput = New System.Windows.Forms.Label
        Me.blbLastNameFirst = New System.Windows.Forms.Label
        Me.lblLastFirstOutput = New System.Windows.Forms.Label
        Me.btnProcess = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(128, 16)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(136, 20)
        Me.txtName.TabIndex = 0
        Me.txtName.Text = ""
        '
        'lblNameRev
        '
        Me.lblNameRev.Location = New System.Drawing.Point(24, 56)
        Me.lblNameRev.Name = "lblNameRev"
        Me.lblNameRev.Size = New System.Drawing.Size(96, 23)
        Me.lblNameRev.TabIndex = 1
        Me.lblNameRev.Text = "Name Reversed:"
        '
        'lblRevOutput
        '
        Me.lblRevOutput.BackColor = System.Drawing.Color.White
        Me.lblRevOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRevOutput.Location = New System.Drawing.Point(128, 56)
        Me.lblRevOutput.Name = "lblRevOutput"
        Me.lblRevOutput.Size = New System.Drawing.Size(136, 23)
        Me.lblRevOutput.TabIndex = 2
        '
        'blbLastNameFirst
        '
        Me.blbLastNameFirst.Location = New System.Drawing.Point(32, 88)
        Me.blbLastNameFirst.Name = "blbLastNameFirst"
        Me.blbLastNameFirst.Size = New System.Drawing.Size(88, 23)
        Me.blbLastNameFirst.TabIndex = 3
        Me.blbLastNameFirst.Text = "Last Name First:"
        '
        'lblLastFirstOutput
        '
        Me.lblLastFirstOutput.BackColor = System.Drawing.Color.White
        Me.lblLastFirstOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastFirstOutput.Location = New System.Drawing.Point(128, 88)
        Me.lblLastFirstOutput.Name = "lblLastFirstOutput"
        Me.lblLastFirstOutput.Size = New System.Drawing.Size(136, 23)
        Me.lblLastFirstOutput.TabIndex = 4
        '
        'btnProcess
        '
        Me.btnProcess.Location = New System.Drawing.Point(8, 128)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.TabIndex = 5
        Me.btnProcess.Text = "&Process"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(96, 128)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.TabIndex = 6
        Me.btnClear.Text = "&Clear"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(208, 128)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "E&xit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 158)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnProcess)
        Me.Controls.Add(Me.lblLastFirstOutput)
        Me.Controls.Add(Me.blbLastNameFirst)
        Me.Controls.Add(Me.lblRevOutput)
        Me.Controls.Add(Me.lblNameRev)
        Me.Controls.Add(Me.txtName)
        Me.Name = "Form1"
        Me.Text = "Festival of Strings"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim Name As String
        Name = Trim(txtName.Text)
        If Name = "" Then
            MessageBox.Show("No name has been entered.")
            Exit Sub
        End If
        lblRevOutput.Text = StrReverse(Name)
        Dim LengthName As Integer
        Dim FirstSpace As Integer, LastSpace As Integer
        Dim FirstName, MiddleName, LastName As String
        Const Blank As String = " "
        LengthName = Len(Name)
        FirstSpace = InStr(Name, Blank)
        On Error Resume Next
        FirstName = Mid(Name, 1, FirstSpace - 1)
        LastSpace = InStrRev(Name, Blank)
        LastName = Mid(Name, LastSpace)
        MiddleName = Mid(Name, FirstSpace + 1, LastSpace - FirstSpace - 1)
        lblLastFirstOutput.Text = UCase(LastName & ", " & FirstName & " " _
            & MiddleName)
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtName.Text = ""
        lblRevOutput.Text = ""
        lblLastFirstOutput.Text = ""
        txtName.Focus()
    End Sub
End Class
