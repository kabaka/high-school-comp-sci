Imports System.IO
Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim PathName As String
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        ReSizeTextBox()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents btnNew As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnOpen As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnSave As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnFont As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnRecent As System.Windows.Forms.ToolBarButton
    Friend WithEvents btnExit As System.Windows.Forms.ToolBarButton
    Friend WithEvents ilsToolbarSupport As System.Windows.Forms.ImageList
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents txtContent As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents FontDialog1 As System.Windows.Forms.FontDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.btnNew = New System.Windows.Forms.ToolBarButton
        Me.btnOpen = New System.Windows.Forms.ToolBarButton
        Me.btnSave = New System.Windows.Forms.ToolBarButton
        Me.btnFont = New System.Windows.Forms.ToolBarButton
        Me.btnRecent = New System.Windows.Forms.ToolBarButton
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.btnExit = New System.Windows.Forms.ToolBarButton
        Me.ilsToolbarSupport = New System.Windows.Forms.ImageList(Me.components)
        Me.txtContent = New System.Windows.Forms.TextBox
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.FontDialog1 = New System.Windows.Forms.FontDialog
        Me.SuspendLayout()
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.btnNew, Me.btnOpen, Me.btnSave, Me.btnFont, Me.btnRecent, Me.btnExit})
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ilsToolbarSupport
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(384, 42)
        Me.ToolBar1.TabIndex = 0
        '
        'btnNew
        '
        Me.btnNew.ImageIndex = 0
        Me.btnNew.Text = "New"
        Me.btnNew.ToolTipText = "Create a New Document"
        '
        'btnOpen
        '
        Me.btnOpen.ImageIndex = 1
        Me.btnOpen.Text = "Open"
        Me.btnOpen.ToolTipText = "Open a File"
        '
        'btnSave
        '
        Me.btnSave.ImageIndex = 2
        Me.btnSave.Text = "Save"
        Me.btnSave.ToolTipText = "Save the File"
        '
        'btnFont
        '
        Me.btnFont.ImageIndex = 3
        Me.btnFont.Text = "Font"
        Me.btnFont.ToolTipText = "Choose a Font"
        '
        'btnRecent
        '
        Me.btnRecent.DropDownMenu = Me.ContextMenu1
        Me.btnRecent.ImageIndex = 2
        Me.btnRecent.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.btnRecent.Text = "Recent"
        Me.btnRecent.ToolTipText = "Edit a Recent File"
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "File1"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "File2"
        '
        'btnExit
        '
        Me.btnExit.ImageIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.ToolTipText = "Exit the Program"
        '
        'ilsToolbarSupport
        '
        Me.ilsToolbarSupport.ImageSize = New System.Drawing.Size(16, 16)
        Me.ilsToolbarSupport.ImageStream = CType(resources.GetObject("ilsToolbarSupport.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilsToolbarSupport.TransparentColor = System.Drawing.Color.Transparent
        '
        'txtContent
        '
        Me.txtContent.Location = New System.Drawing.Point(8, 56)
        Me.txtContent.Multiline = True
        Me.txtContent.Name = "txtContent"
        Me.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtContent.Size = New System.Drawing.Size(368, 288)
        Me.txtContent.TabIndex = 1
        Me.txtContent.Text = ""
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(384, 350)
        Me.Controls.Add(Me.txtContent)
        Me.Controls.Add(Me.ToolBar1)
        Me.Name = "Form1"
        Me.Text = "Toolbars and Dialog Boxes"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case e.Button.Text
            Case "Open"
                'MessageBox.Show("Call the Open File dialog box.")
                With OpenFileDialog1
                    .DefaultExt = "txt"
                    .InitialDirectory = "c:\My Documents"
                    .Filter = "Text Files | *.txt"
                    .ShowDialog()
                    PathName = .FileName
                End With
                MenuItem2.Text = MenuItem1.Text
                MenuItem1.Text = PathName
                LoadFile()
            Case "New"
                'MessageBox.Show("Create a new document")
                txtContent.ResetText()
            Case "Save"
                'MessageBox.Show("Call the Save File dialog box.")
                With SaveFileDialog1
                    .InitialDirectory = "c:\My Documents"
                    .Filter = "Text Files | *.txt"
                    .ShowDialog()
                    PathName = .FileName
                End With
                Dim fs As StreamWriter
                Dim strContent As String = txtContent.Text
                fs = File.CreateText(PathName)
                fs.WriteLine(strContent)
                fs.Close()
            Case "Font"
                'MessageBox.Show("Call the Font dialog box.")
                With FontDialog1
                    .ShowDialog()
                    txtContent.Font = .Font
                End With
            Case "Recent"
                MessageBox.Show("Recent Files")
            Case "Exit"
                Application.Exit()
            Case Else
                MessageBox.Show("Illegal Option", "Error", _
                MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Select
    End Sub

    Sub ReSizeTextBox()
        Dim NewSize As Size
        Dim NewLoc As Point
        NewSize.Width = Me.Size.Width - 8
        NewSize.Height = Me.Size.Height - 68
        NewLoc.X = 0
        NewLoc.Y = 40
        txtContent.Location = NewLoc
    End Sub

    Sub LoadFile()
        Dim fs As StreamReader
        fs = File.OpenText(PathName)
        txtContent.Text = fs.ReadToEnd
        fs.Close()
    End Sub

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        ReSizeTextBox()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        PathName = MenuItem2.Text
        LoadFile()
    End Sub
End Class
