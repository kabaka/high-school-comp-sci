Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim logWriter As New StreamWriter("log.txt", True)

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDestPort As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDestHost As System.Windows.Forms.TextBox
    Friend WithEvents cboPreCompress As System.Windows.Forms.CheckBox
    Friend WithEvents btnDefaultPort As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents lblFile As System.Windows.Forms.Label
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents cboEncrypt As System.Windows.Forms.CheckBox
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtLog As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDestPort = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDestHost = New System.Windows.Forms.TextBox
        Me.cboPreCompress = New System.Windows.Forms.CheckBox
        Me.btnDefaultPort = New System.Windows.Forms.Button
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.lblFile = New System.Windows.Forms.Label
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.cboEncrypt = New System.Windows.Forms.CheckBox
        Me.btnSend = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.txtLog = New System.Windows.Forms.TextBox
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Destination (IP/Hostname)"
        '
        'txtDestPort
        '
        Me.txtDestPort.Location = New System.Drawing.Point(160, 72)
        Me.txtDestPort.Name = "txtDestPort"
        Me.txtDestPort.Size = New System.Drawing.Size(104, 20)
        Me.txtDestPort.TabIndex = 1
        Me.txtDestPort.Text = "8700"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(128, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Port"
        '
        'txtDestHost
        '
        Me.txtDestHost.Location = New System.Drawing.Point(160, 40)
        Me.txtDestHost.Name = "txtDestHost"
        Me.txtDestHost.Size = New System.Drawing.Size(104, 20)
        Me.txtDestHost.TabIndex = 3
        Me.txtDestHost.Text = "localhost"
        '
        'cboPreCompress
        '
        Me.cboPreCompress.Location = New System.Drawing.Point(32, 160)
        Me.cboPreCompress.Name = "cboPreCompress"
        Me.cboPreCompress.Size = New System.Drawing.Size(128, 24)
        Me.cboPreCompress.TabIndex = 4
        Me.cboPreCompress.Text = "&Pre-Compress File"
        '
        'btnDefaultPort
        '
        Me.btnDefaultPort.Location = New System.Drawing.Point(272, 72)
        Me.btnDefaultPort.Name = "btnDefaultPort"
        Me.btnDefaultPort.TabIndex = 5
        Me.btnDefaultPort.Text = "&Default"
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(40, 128)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(224, 20)
        Me.txtFile.TabIndex = 6
        Me.txtFile.Text = "C:\"
        '
        'lblFile
        '
        Me.lblFile.Location = New System.Drawing.Point(16, 128)
        Me.lblFile.Name = "lblFile"
        Me.lblFile.Size = New System.Drawing.Size(24, 23)
        Me.lblFile.TabIndex = 7
        Me.lblFile.Text = "File"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(272, 128)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.TabIndex = 8
        Me.btnBrowse.Text = "&Browse"
        '
        'cboEncrypt
        '
        Me.cboEncrypt.Location = New System.Drawing.Point(32, 184)
        Me.cboEncrypt.Name = "cboEncrypt"
        Me.cboEncrypt.TabIndex = 9
        Me.cboEncrypt.Text = "Use Encryption"
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(264, 200)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.TabIndex = 13
        Me.btnSend.Text = "&Send"
        '
        'txtLog
        '
        Me.txtLog.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtLog.Location = New System.Drawing.Point(0, 432)
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.Size = New System.Drawing.Size(602, 40)
        Me.txtLog.TabIndex = 14
        Me.txtLog.Text = ""
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(602, 432)
        Me.TabControl1.TabIndex = 15
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.btnBrowse)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.btnSend)
        Me.TabPage1.Controls.Add(Me.cboEncrypt)
        Me.TabPage1.Controls.Add(Me.lblFile)
        Me.TabPage1.Controls.Add(Me.txtFile)
        Me.TabPage1.Controls.Add(Me.btnDefaultPort)
        Me.TabPage1.Controls.Add(Me.cboPreCompress)
        Me.TabPage1.Controls.Add(Me.txtDestPort)
        Me.TabPage1.Controls.Add(Me.txtDestHost)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(370, 278)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Direct Connection"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.Button9)
        Me.TabPage2.Controls.Add(Me.Button6)
        Me.TabPage2.Controls.Add(Me.Button7)
        Me.TabPage2.Controls.Add(Me.Button8)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Button5)
        Me.TabPage2.Controls.Add(Me.TextBox3)
        Me.TabPage2.Controls.Add(Me.Button4)
        Me.TabPage2.Controls.Add(Me.Button3)
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Controls.Add(Me.ListBox1)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(594, 406)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "FTP"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Label3"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(104, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(184, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "TextBox1"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Label4"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(104, 48)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(184, 20)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Text = "TextBox2"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(216, 80)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Button1"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(224, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Label5"
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(8, 248)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(576, 121)
        Me.ListBox1.TabIndex = 6
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(416, 376)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "Button2"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(336, 376)
        Me.Button3.Name = "Button3"
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "Button3"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(256, 376)
        Me.Button4.Name = "Button4"
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "Button4"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(224, 216)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(272, 20)
        Me.TextBox3.TabIndex = 10
        Me.TextBox3.Text = "TextBox3"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(504, 216)
        Me.Button5.Name = "Button5"
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "Button5"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 216)
        Me.Label6.Name = "Label6"
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Label6"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(16, 376)
        Me.Button6.Name = "Button6"
        Me.Button6.TabIndex = 15
        Me.Button6.Text = "Button6"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(96, 376)
        Me.Button7.Name = "Button7"
        Me.Button7.TabIndex = 14
        Me.Button7.Text = "Button7"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(176, 376)
        Me.Button8.Name = "Button8"
        Me.Button8.TabIndex = 13
        Me.Button8.Text = "Button8"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(496, 376)
        Me.Button9.Name = "Button9"
        Me.Button9.TabIndex = 16
        Me.Button9.Text = "Button9"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 112)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(602, 472)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtLog)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "File Transfer Utility"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        OpenFileDialog1.ShowDialog()
        If Not OpenFileDialog1.FileName = Nothing Then txtFile.Text = OpenFileDialog1.FileName
    End Sub

    Private Sub btnDefaultPort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefaultPort.Click
        txtDestPort.Text = "8700"
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim filename As String = txtFile.Text
        Dim reader As New BinaryReader(File.OpenRead(Filename))
        'Dim client As TcpClient
        Dim address As IPAddress = IPAddress.Parse("192.168.2.2")
        'Dim endPoint As New IPEndPoint(address, CInt(txtDestPort.Text))
        Dim endPoint As New IPEndPoint(address, 8700)
        Dim clientSocket As New Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp)
        'Dim ClientStream As Stream
        Dim bytes As Byte() = Encoding.ASCII.GetBytes(0)
        Dim rcvdStr As String
        Log("Attempting to connect to " & endPoint.Address.ToString & ":" & endPoint.Port.ToString)
        clientSocket.Connect(endPoint)
        Log("Connected")

        filename = OpenFileDialog1.FileName

        bytes = Encoding.ASCII.GetBytes(filename)
        clientSocket.Send(bytes, bytes.Length, 0)
        'Log("Filename sent, recieving reply")

        'bytes = Encoding.ASCII.GetBytes("ENDOFFILENAME")
        Log("Sleeping for 100 ms")
        Threading.Thread.Sleep(100)

        clientSocket.Receive(bytes, 1, 0)


        'While clientSocket.Available > 0
        '    clientSocket.Receive(bytes, 1, 0)
        '    rcvdStr = rcvdStr & Encoding.ASCII.GetString(bytes)
        'End While

        Log("Sending File...")

        'If rcvdStr = "SENDOK" Then
        While Not reader.PeekChar = -1
            clientSocket.Send(reader.ReadBytes(1), 1, 0)
        End While
        'End If

    End Sub

    Private Sub Log(ByVal Append As String)
        txtLog.AppendText(Now.Date & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & " " & Append & vbCrLf)
        logWriter.WriteLine(Now.Date & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & " " & Append)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
