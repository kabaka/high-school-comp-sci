Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileNew As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWindowCascade As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWindowVertical As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWindowArrange As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWindowHorizontal As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuFileNew = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.mnuWindowCascade = New System.Windows.Forms.MenuItem
        Me.mnuWindowHorizontal = New System.Windows.Forms.MenuItem
        Me.mnuWindowVertical = New System.Windows.Forms.MenuItem
        Me.mnuWindowArrange = New System.Windows.Forms.MenuItem
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem5})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileNew, Me.MenuItem3, Me.MenuItem4})
        Me.MenuItem1.Text = "File"
        '
        'mnuFileNew
        '
        Me.mnuFileNew.Index = 0
        Me.mnuFileNew.Text = "New"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "E&xit"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 1
        Me.MenuItem5.MdiList = True
        Me.MenuItem5.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuWindowCascade, Me.mnuWindowHorizontal, Me.mnuWindowVertical, Me.mnuWindowArrange})
        Me.MenuItem5.Text = "&Window"
        '
        'mnuWindowCascade
        '
        Me.mnuWindowCascade.Index = 0
        Me.mnuWindowCascade.Text = "&Cascade"
        '
        'mnuWindowHorizontal
        '
        Me.mnuWindowHorizontal.Index = 1
        Me.mnuWindowHorizontal.Text = "Tile &Horizontally"
        '
        'mnuWindowVertical
        '
        Me.mnuWindowVertical.Index = 2
        Me.mnuWindowVertical.Text = "Tile &Vertically"
        '
        'mnuWindowArrange
        '
        Me.mnuWindowArrange.Index = 3
        Me.mnuWindowArrange.Text = "&Arrange"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.IsMdiContainer = True
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "Linear Cost Function"

    End Sub

#End Region

    Private Sub mnuFileNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileNew.Click
        Dim LDFForm As New LDFForm
        LDFForm.MdiParent = Me
        LDFForm.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        End
    End Sub

    Private Sub mnuWindowCascade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWindowCascade.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub mnuWindowHorizontal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWindowHorizontal.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub mnuWindowVertical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWindowVertical.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub mnuWindowArrange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWindowArrange.Click
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub
End Class
