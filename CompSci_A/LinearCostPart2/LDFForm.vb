Public Class LDFForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtFixedCost As System.Windows.Forms.TextBox
    Friend WithEvents txtDirectCost As System.Windows.Forms.TextBox
    Friend WithEvents lblDirectCost As System.Windows.Forms.Label
    Friend WithEvents lblFixedCost As System.Windows.Forms.Label
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtFixedCost = New System.Windows.Forms.TextBox
        Me.txtDirectCost = New System.Windows.Forms.TextBox
        Me.lblDirectCost = New System.Windows.Forms.Label
        Me.lblFixedCost = New System.Windows.Forms.Label
        Me.btnCalculate = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtFixedCost
        '
        Me.txtFixedCost.Location = New System.Drawing.Point(120, 48)
        Me.txtFixedCost.Name = "txtFixedCost"
        Me.txtFixedCost.TabIndex = 0
        Me.txtFixedCost.Text = ""
        '
        'txtDirectCost
        '
        Me.txtDirectCost.Location = New System.Drawing.Point(120, 16)
        Me.txtDirectCost.Name = "txtDirectCost"
        Me.txtDirectCost.TabIndex = 1
        Me.txtDirectCost.Text = ""
        '
        'lblDirectCost
        '
        Me.lblDirectCost.Location = New System.Drawing.Point(16, 16)
        Me.lblDirectCost.Name = "lblDirectCost"
        Me.lblDirectCost.TabIndex = 2
        Me.lblDirectCost.Text = "Direct Cost"
        '
        'lblFixedCost
        '
        Me.lblFixedCost.Location = New System.Drawing.Point(16, 48)
        Me.lblFixedCost.Name = "lblFixedCost"
        Me.lblFixedCost.TabIndex = 3
        Me.lblFixedCost.Text = "Fixed Cost"
        '
        'btnCalculate
        '
        Me.btnCalculate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCalculate.Location = New System.Drawing.Point(96, 88)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(88, 23)
        Me.btnCalculate.TabIndex = 4
        Me.btnCalculate.Text = "&Calculate Cost"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(208, 88)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "E&xit"
        '
        'LDFForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 118)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.lblFixedCost)
        Me.Controls.Add(Me.lblDirectCost)
        Me.Controls.Add(Me.txtDirectCost)
        Me.Controls.Add(Me.txtFixedCost)
        Me.Name = "LDFForm"
        Me.Text = "Linear Cost Function"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        'Declare the nescessary variables.
        Dim CrLf As String = Chr(13) & Chr(10)
        Dim StartingValue, UpperLimit, Increment As Integer
        Dim ControlVariable As Integer
        Dim DirectCost, FixedCost, TotalCost As Decimal
        'Collect information from the TextBoxes.
        DirectCost = CDec(txtDirectCost.Text)
        FixedCost = CDec(txtFixedCost.Text)
        'Collect information from the user.
        StartingValue = CInt(InputBox("Enter the starting value:"))
        UpperLimit = CInt(InputBox("Enter the upper limit:"))
        Increment = CInt(InputBox("Enter the increment:"))
        For ControlVariable = StartingValue To UpperLimit Step Increment
            TotalCost = DirectCost * ControlVariable + FixedCost
            MessageBox.Show("The cost at " & ControlVariable.ToString _
            & " items is:" & CrLf & TotalCost.ToString)
        Next ControlVariable
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub
End Class
