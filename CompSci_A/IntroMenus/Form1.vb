Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEdit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditColor As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditColorBlack As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditColorBlue As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditColorRed As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileSep As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents txtMenuTest As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuOpen = New System.Windows.Forms.MenuItem
        Me.mnuFileSep = New System.Windows.Forms.MenuItem
        Me.mnuExit = New System.Windows.Forms.MenuItem
        Me.mnuEdit = New System.Windows.Forms.MenuItem
        Me.mnuEditColor = New System.Windows.Forms.MenuItem
        Me.mnuEditColorBlack = New System.Windows.Forms.MenuItem
        Me.mnuEditColorBlue = New System.Windows.Forms.MenuItem
        Me.mnuEditColorRed = New System.Windows.Forms.MenuItem
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.txtMenuTest = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuEdit})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.mnuFileSep, Me.mnuExit})
        Me.mnuFile.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Text = "&Open"
        '
        'mnuFileSep
        '
        Me.mnuFileSep.Index = 1
        Me.mnuFileSep.Text = "-"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 2
        Me.mnuExit.Text = "E&xit"
        '
        'mnuEdit
        '
        Me.mnuEdit.Index = 1
        Me.mnuEdit.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEditColor})
        Me.mnuEdit.Text = "&Edit"
        '
        'mnuEditColor
        '
        Me.mnuEditColor.Index = 0
        Me.mnuEditColor.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEditColorBlack, Me.mnuEditColorBlue, Me.mnuEditColorRed})
        Me.mnuEditColor.Text = "Color"
        '
        'mnuEditColorBlack
        '
        Me.mnuEditColorBlack.Index = 0
        Me.mnuEditColorBlack.Text = "Black"
        '
        'mnuEditColorBlue
        '
        Me.mnuEditColorBlue.Index = 1
        Me.mnuEditColorBlue.Text = "Blue"
        '
        'mnuEditColorRed
        '
        Me.mnuEditColorRed.Index = 2
        Me.mnuEditColorRed.Text = "Red"
        '
        'txtMenuTest
        '
        Me.txtMenuTest.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMenuTest.Location = New System.Drawing.Point(8, 24)
        Me.txtMenuTest.Name = "txtMenuTest"
        Me.txtMenuTest.Size = New System.Drawing.Size(240, 35)
        Me.txtMenuTest.TabIndex = 0
        Me.txtMenuTest.Text = "Trying out the Menu"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(256, 73)
        Me.Controls.Add(Me.txtMenuTest)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "Introduction to Menus"
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub mnuEditColorBlack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditColorBlack.Click
        'Change the color of the text in the txtMenuTest to Black
        Dim newColor As New Color
        txtMenuTest.ForeColor = newColor.Black()
        'Check the Black submenu control
        mnuEditColorBlack.Checked = True
        'Uncheck the Blue submenu control
        mnuEditColorBlue.Checked = False
        'Uncheck the Red submenu control
        mnuEditColorRed.Checked = False
    End Sub

    Private Sub mnuEditColorBlue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditColorBlue.Click
        'Change the color of the text in the txtMenuTest to Black
        Dim newColor As New Color
        txtMenuTest.ForeColor = newColor.Blue()
        'Check the Black submenu control
        mnuEditColorBlue.Checked = True
        'Uncheck the Blue submenu control
        mnuEditColorBlack.Checked = False
        'Uncheck the Red submenu control
        mnuEditColorRed.Checked = False
    End Sub

    Private Sub mnuEditColorRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditColorRed.Click
        'Change the color of the text in the txtMenuTest to Black
        Dim newColor As New Color
        txtMenuTest.ForeColor = newColor.Red()
        'Check the Black submenu control
        mnuEditColorRed.Checked = True
        'Uncheck the Blue submenu control
        mnuEditColorBlue.Checked = False
        'Uncheck the Red submenu control
        mnuEditColorBlack.Checked = False
    End Sub

    Private Sub mnuExit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        End
    End Sub
End Class
