Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbTurkey As System.Windows.Forms.RadioButton
    Friend WithEvents rdbHam As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRoastBeef As System.Windows.Forms.RadioButton
    Friend WithEvents rdbVeggie As System.Windows.Forms.RadioButton
    Friend WithEvents chbLettuce As System.Windows.Forms.CheckBox
    Friend WithEvents chbTomato As System.Windows.Forms.CheckBox
    Friend WithEvents chbPickles As System.Windows.Forms.CheckBox
    Friend WithEvents chbMustard As System.Windows.Forms.CheckBox
    Friend WithEvents chbMayo As System.Windows.Forms.CheckBox
    Friend WithEvents btnSandwich As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdbVeggie = New System.Windows.Forms.RadioButton
        Me.rdbRoastBeef = New System.Windows.Forms.RadioButton
        Me.rdbHam = New System.Windows.Forms.RadioButton
        Me.rdbTurkey = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chbLettuce = New System.Windows.Forms.CheckBox
        Me.chbTomato = New System.Windows.Forms.CheckBox
        Me.chbPickles = New System.Windows.Forms.CheckBox
        Me.chbMustard = New System.Windows.Forms.CheckBox
        Me.chbMayo = New System.Windows.Forms.CheckBox
        Me.btnSandwich = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbVeggie)
        Me.GroupBox1.Controls.Add(Me.rdbRoastBeef)
        Me.GroupBox1.Controls.Add(Me.rdbHam)
        Me.GroupBox1.Controls.Add(Me.rdbTurkey)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(144, 176)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'rdbVeggie
        '
        Me.rdbVeggie.Location = New System.Drawing.Point(16, 135)
        Me.rdbVeggie.Name = "rdbVeggie"
        Me.rdbVeggie.TabIndex = 3
        Me.rdbVeggie.Text = "Veggie"
        '
        'rdbRoastBeef
        '
        Me.rdbRoastBeef.Location = New System.Drawing.Point(16, 98)
        Me.rdbRoastBeef.Name = "rdbRoastBeef"
        Me.rdbRoastBeef.TabIndex = 2
        Me.rdbRoastBeef.Text = "Roast Beef"
        '
        'rdbHam
        '
        Me.rdbHam.Location = New System.Drawing.Point(16, 61)
        Me.rdbHam.Name = "rdbHam"
        Me.rdbHam.TabIndex = 1
        Me.rdbHam.Text = "Ham"
        '
        'rdbTurkey
        '
        Me.rdbTurkey.Location = New System.Drawing.Point(16, 24)
        Me.rdbTurkey.Name = "rdbTurkey"
        Me.rdbTurkey.TabIndex = 0
        Me.rdbTurkey.Text = "Turkey"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chbMayo)
        Me.GroupBox2.Controls.Add(Me.chbMustard)
        Me.GroupBox2.Controls.Add(Me.chbPickles)
        Me.GroupBox2.Controls.Add(Me.chbTomato)
        Me.GroupBox2.Controls.Add(Me.chbLettuce)
        Me.GroupBox2.Location = New System.Drawing.Point(184, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(152, 176)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "GroupBox2"
        '
        'chbLettuce
        '
        Me.chbLettuce.Location = New System.Drawing.Point(16, 16)
        Me.chbLettuce.Name = "chbLettuce"
        Me.chbLettuce.TabIndex = 0
        Me.chbLettuce.Text = "Lettuce"
        '
        'chbTomato
        '
        Me.chbTomato.Location = New System.Drawing.Point(16, 48)
        Me.chbTomato.Name = "chbTomato"
        Me.chbTomato.TabIndex = 1
        Me.chbTomato.Text = "Tomato"
        '
        'chbPickles
        '
        Me.chbPickles.Location = New System.Drawing.Point(16, 80)
        Me.chbPickles.Name = "chbPickles"
        Me.chbPickles.TabIndex = 2
        Me.chbPickles.Text = "Pickles"
        '
        'chbMustard
        '
        Me.chbMustard.Location = New System.Drawing.Point(16, 112)
        Me.chbMustard.Name = "chbMustard"
        Me.chbMustard.TabIndex = 3
        Me.chbMustard.Text = "Mustard"
        '
        'chbMayo
        '
        Me.chbMayo.Location = New System.Drawing.Point(16, 144)
        Me.chbMayo.Name = "chbMayo"
        Me.chbMayo.TabIndex = 4
        Me.chbMayo.Text = "Mayonnaise"
        '
        'btnSandwich
        '
        Me.btnSandwich.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSandwich.Location = New System.Drawing.Point(160, 232)
        Me.btnSandwich.Name = "btnSandwich"
        Me.btnSandwich.TabIndex = 1
        Me.btnSandwich.Text = "Sandwich!"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(256, 232)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Exit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(344, 270)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSandwich)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "Form1"
        Me.Text = "Sandwich"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnSandwich_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSandwich.Click
        If rdbTurkey.Checked Then
            MessageBox.Show("Turkey")
        End If
        If rdbHam.Checked Then
            MessageBox.Show("Ham")
        End If
        If rdbRoastBeef.Checked Then
            MessageBox.Show("Roast Beef")
        End If
        If rdbVeggie.Checked Then
            MessageBox.Show("Veggie")
        End If
    End Sub
End Class
