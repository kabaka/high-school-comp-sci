Public Class Form1
    Inherits System.Windows.Forms.Form
    Const Capacity = 15
    Dim FirstName(Capacity) As String
    Dim Current As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnEnter As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lstDisplay As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstDisplay = New System.Windows.Forms.ListBox
        Me.btnEnter = New System.Windows.Forms.Button
        Me.btnSearch = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lstDisplay
        '
        Me.lstDisplay.Location = New System.Drawing.Point(24, 24)
        Me.lstDisplay.Name = "lstDisplay"
        Me.lstDisplay.Size = New System.Drawing.Size(120, 212)
        Me.lstDisplay.Sorted = True
        Me.lstDisplay.TabIndex = 0
        '
        'btnEnter
        '
        Me.btnEnter.Location = New System.Drawing.Point(160, 24)
        Me.btnEnter.Name = "btnEnter"
        Me.btnEnter.Size = New System.Drawing.Size(88, 23)
        Me.btnEnter.TabIndex = 1
        Me.btnEnter.Text = "&Enter a Name"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(160, 56)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.TabIndex = 2
        Me.btnSearch.Text = "&Search"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(160, 88)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.TabIndex = 3
        Me.btnClear.Text = "&Clear"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(160, 208)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "E&xit"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnEnter)
        Me.Controls.Add(Me.lstDisplay)
        Me.Name = "Form1"
        Me.Text = "Exploring Arrays"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        If Current < Capacity Then
            FirstName(Current) = InputBox("Enter a first name:")
            lstDisplay.Items.Add(FirstName(Current))
            Current = Current + 1
        Else
            MessageBox.Show("Array Capacity Exceeded!")
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lstDisplay.Items.Clear()
        Dim Index As Integer
        For Index = 0 To 4
            FirstName(Index) = " "
        Next
        Current = 0
        btnEnter.Focus()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim Target As String
        Dim High, Low, Md, Position As Integer
        Do
            Target = InputBox("Enter the name for which to search:")
        Loop Until Target <> ""
        Position = 0
        Low = 0
        High = lstDisplay.Items.Count - 1
        Do While Low <= High
            Md = (Low + High) \ 2
            If Target = lstDisplay.Items(Md) Then
                Position = Md
                Exit Do
            ElseIf Target < lstDisplay.Items(Md).ToString Then
                High = Md - 1
            Else
                Low = Md + 1
            End If
        Loop
        If Low <= High Then
            MessageBox.Show("Found " & Target & " at position:" & Position)
        Else
            MessageBox.Show("The Target has not been found.")
        End If
    End Sub
End Class
