Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblGreeting As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblGreeting = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblGreeting
        '
        Me.lblGreeting.Location = New System.Drawing.Point(8, 8)
        Me.lblGreeting.Name = "lblGreeting"
        Me.lblGreeting.Size = New System.Drawing.Size(280, 48)
        Me.lblGreeting.TabIndex = 0
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 62)
        Me.Controls.Add(Me.lblGreeting)
        Me.Name = "Form1"
        Me.Text = "Using the Dim Statement"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strUserName As String
        strUserName = InputBox("Plase enter your full name.")
        Dim intAge As Integer = 21
        Dim sglGPA As Single = 3.65
        Dim CrLf As String = Chr(13) & Chr(10)
        lblGreeting.Text = "Welcome " & strUserName & "." _
            & CrLf & "Your age = " & intAge _
            & CrLf & "Your GPA = " & sglGPA
    End Sub
End Class
