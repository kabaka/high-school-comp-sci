import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.util.Random;

public class LDEnvTest
{
  public static void main(String[] args)
  {
    Environment env = new BoundedEnv(20, 20);
    LDEnvDisplay display = new LDEnvDisplay(env);

    createFrame(display, 20, 20);
    
    LocDir ld1 = new LocDir(new Location(3, 7), Direction.NORTH);
    env.add(ld1);
    //randomPath(display, env, 5000, 5, 5, 250);
    

  ///////////// remove the sample code below and replace with your code ///////
/*
    env.add(new ColorBlock(new Location(3,3), Color.blue));

    ColorBlock cb = new ColorBlock(new Location(7,7), Color.red);
    env.add(cb);
    env.add(new ColorBlock(new Location(11,11), Color.green));

    display.showEnv();
    System.out.println(env);
    try
    { Thread.sleep(2000);
    }
    catch(InterruptedException ex){}
    LocDir ld1 = new LocDir(new Location(3, 7), Direction.SOUTH);
    env.add(ld1);

    LocDir ld2 = new LocDir(new Location(11, 7), Direction.NORTH);
    env.add(ld2);
*/
    display.showEnv();
    System.out.println(env);
  }

  private static void randomPath(LDEnvDisplay display, Environment env,
	  int count, int x, int y, long interval)
  {
      Location currentLoc = new Location(x, y);
      LocDir ld = new LocDir(currentLoc, Direction.randomDirection());
      env.add(ld);
      sleep(interval);
      display.showEnv();
      
      for(int i = 0; i < count; i++)
      {
	  if(ld.direction().equals(Direction.NORTH))
	    x--;
	  else if(ld.direction().equals(Direction.SOUTH))
	    x++;
	  else if(ld.direction().equals(Direction.EAST))
	    y++;
	  else
	    y--;
	  
	  if(x > env.numRows())
	      x = env.numRows();
	  else if(x < 0)
	      x = 0;
	  else if(y > env.numCols())
	      y = env.numCols();
	  else if(y < 0)
	      y = 0;
	  
	  currentLoc = new Location(x, y);
	  
	  ld = new LocDir(currentLoc, Direction.randomDirection());
	  if(!(env.isEmpty(currentLoc)))
	    env.add(ld);
	  
	  display.showEnv();
	  sleep(interval);
      }
  }
  
  private static void sleep(long time)
  {
      try
      {
	  Thread.sleep(time);
      }
      catch (Exception e)
      {
	  System.err.println("huh...");
      }
  }

  private static void createFrame(JPanel display, int xCoord, int yCoord)
  {
    int width = (int)Math.round((display).getSize().getWidth());
    int height = (int)Math.round((display).getSize().getHeight());

    JFrame frame = new JFrame();
    frame.setVisible(true);

    width = width + frame.getInsets().right + frame.getInsets().left;
    height = width + frame.getInsets().top + frame.getInsets().bottom;

    frame.setSize(width,height);
    frame.setLocation(xCoord, yCoord);

    frame.getContentPane().add(display);
    frame.setVisible(true);
  }

}