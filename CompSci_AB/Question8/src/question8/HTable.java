/*
 * HTable.java
 *
 * Created on February 5, 2007, 8:58 AM
 *
 *
 */

package question8;

/**
 *
 * @author Kyle Johnson
 */
public class HTable
{
    
    static char[] digits;
    String[] data;
    
    int colCount;
    
    /** Creates a new instance of HTable */
    public HTable()
    {
	colCount = 0;
	
	digits = new char[60];
	data = new String[60];
	
	for(int i = 0; i < 60; i++)
	    digits[i] = (char)i;
    }
    
    public void insert(String val)
    {
	int index = (int)(getIndex(val).charAt(0));
	
	if(data[index] != null)
	{
	    System.out.println("*COLLISION* (" + colCount + ")");
	    colCount++;
	}
	
	data[index] = val;
    }
    
    public String get(String val)
    {
	return data[(int)(getIndex(val).charAt(0))];
    }
    
    private String getIndex(String val)
    {
	String index = "";
	int sum = 0;
	
	for(int i = val.length() - 1; i !=0; i--)
	    sum += Character.getNumericValue(val.charAt(i)) -
		    Character.getNumericValue('A');
	
	index = baseConv(sum, digits.length);
	
	System.out.println("index: " + index);
	
	return index;
    }
    
    private static String baseConv(int i, int radix)
    {
	int lenPlusOne = digits.length + 1;

	char buf[] = new char[lenPlusOne];
	boolean negative = (i < 0);
	int charPos = 32;

	if (!negative)
	    i = -i;

	while (i <= -radix)
	{
	    buf[charPos--] = digits[-(i % radix)];
	    i = i / radix;
	}

	buf[charPos] = digits[-i];

	if (negative)
	    buf[--charPos] = '-';

	return new String(buf, charPos, (lenPlusOne - charPos));
    }
    
}
