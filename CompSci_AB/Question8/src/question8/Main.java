/*
 * Main.java
 *
 * Created on February 5, 2007, 8:51 AM
 *
 * 
 */

package question8;

 import java.util.Scanner;
 import java.io.File;
 
/**
 *
 * @author Kyle Johnson
 */
public class Main
{
    
    /** Creates a new instance of Main (THIS IS BAD) */
    private Main()
    {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
	try
	{
	    HTable tbl = new HTable();
	    Scanner fil = new Scanner(new File("C:\\data\\School\\CS_AB\\" +
		    "Question8\\src\\Question8\\words.txt"));
	    
	    System.out.println("Reading data and populating Hash Table...");
	    while(fil.hasNextLine())
		tbl.insert(fil.nextLine());
	    System.out.println("Done! Ready for testing");
	    
	    Scanner kb = new Scanner(System.in);
	    String input;
	    do
	    {
		System.out.print("Test Existence of data > ");
		input = kb.nextLine().trim().toLowerCase();
		System.out.println("Data: " + tbl.get(input));
		
	    } while(!input.equals("exit"));
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	
	
	
    }
    
}
