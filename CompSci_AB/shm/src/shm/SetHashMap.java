/*
 * SetHashMap.java
 *
 * Created on February 19, 2007, 8:54 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package shm;

 import java.util.Set;
 import java.util.LinkedList;
 import java.util.Map;
 import java.util.HashMap;
 import java.util.HashSet;
 import java.util.TreeSet;
 import java.util.Collection;
/**
 *
 * @author Kyle Johnson
 */
 
public class SetHashMap
{
    private Map hMap;
    private Map tMap;
    
    /** Creates a new instance of SetHashMap */
    public SetHashMap()
    {
	hMap = new HashMap();
    }
    
    public Set allHashCodes(Set set)
    {
	HashSet<Integer> list = new HashSet<Integer>();
	for(Object obj : set)
	    list.add(set.hashCode());
	return list;
    }
    
    public Map mapByHashCode(Set testSet)
    {
	HashMap map = new HashMap();
	for(Object obj : testSet)
	    map.put(obj.hashCode(), obj);
	return map;
    }
    
    public Set largestBucket(Set testSet)
    {
	TreeSet largest = new TreeSet();
	
	HashMap sets = new HashMap();
	
	for(Object obj : testSet)
	{
	    int hash = obj.hashCode();
	    if(!sets.containsKey(hash))
		sets.put(hash, new LinkedList());
	    ((LinkedList)sets.get(hash)).add(obj);
	}
	
	Collection col = sets.values();
	int greatest = 0;
	LinkedList largestSet = new LinkedList();
	
	for(Object obj : col)
	{
	    int size = ((LinkedList)obj).size();
	    if(size > greatest)
	    {
		greatest = size;
		largestSet = (LinkedList)obj;
	    }
	}
	
	largest.addAll(largestSet);
	
	return largest;
    }
    
    public long benchmark(Set testSet, Set samples, int numRepetitions)
    {
	long start = System.currentTimeMillis();
	
	for (int i = numRepetitions; i != 0; i--)
	{
	    for(Object obj : samples)
		testSet.remove(obj);
	    
	    for(Object obj : samples)
		testSet.add(obj);
	}
	
	return System.currentTimeMillis() - start;
    }
}
