/*
 * Main.java
 *
 * Created on February 19, 2007, 8:53 AM
 *
 */

package shm;
 import java.util.HashSet;
 import java.util.ArrayList;
/**
 *
 * @author Kyle Johnson
 */
public class Main
{
    
    /** Creates a new instance of Main (but don't...)*/
    private Main(){}
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
	SetHashMap SHM  = new SetHashMap();
	HashSet set = new HashSet();
	HashSet set2 = new HashSet();
	
	/* Generate all possible strings of length 3 in which all the characters 
	 are either upper-case letters A-Z or digits 0-9 and add these strings 
	 to a HashSet. compare the size of this set with the number of different
	 hashCode values for the strings in this set. */
	
	System.out.println(" ** Starting size comparison... **");
	
	ArrayList<Character> strs = new ArrayList<Character>();
	
	System.out.println("Constructing list of characters...");
	
	for(int i = 65; i <= 90; i++)
	    strs.add(Character.valueOf((char)i));
	for(int i = 0; i < 10; i++)
	    strs.add(Character.valueOf((char)(i+48)));
	
	System.out.println("Building Set...");
	
	int size = strs.size();
	int count = 0;
	
	for(int i = 0; i < size; i++)
	    for(int j = 0; j < size; j++)
		for(int k = 0; k < size; k++)
		{
		    set.add("" + strs.get(i) + strs.get(j)+ strs.get(k));
		    count++;
		}
	
	System.out.println("Constructed Set Size: " + count);
	System.out.println("count - allHashCodes() = " + (count - SHM.allHashCodes(set).size()));
	
	System.out.println(" ** Benchmark **");
	
	
	for(int i = 0; i < size; i++)
	    for(int j = 0; j < size; j++)
		for(int k = 0; k < size; k++)
		{
		    if((new java.util.Random()).nextBoolean())
			set2.add("" + strs.get(i) + strs.get(j)+ strs.get(k));
		}
	
	System.out.println("Set Size: " + set.size());
	System.out.println("Samples Size: " + set2.size());
	System.out.println("Time for 5 repetitions (ms): " + SHM.benchmark(set,set2,5));
	
    }
}
