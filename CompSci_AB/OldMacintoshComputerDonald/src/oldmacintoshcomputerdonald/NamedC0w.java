/*
 * NamedC0w.java
 *
 * Created on March 2, 2007, 9:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package oldmacintoshcomputerdonald;

/**
 *
 * @author Kyle Johnson
 */
public class NamedC0w extends Cow
{
    
    String myName;
    
    /** Creates a new instance of NamedC0w */
    public NamedC0w(String name)
    {
	super();
	myName = name;
    }
    public String getName()
    {
	return myName;
    }
    
}
