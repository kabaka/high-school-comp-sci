/**
 * @(#)Recursion_Assignment4_TowersOfHanoi.java
 *
 * Recursion_Assignment4_TowersOfHanoi application
 * Solving the classic puzzle...
 *
 * @author Kyle Johnson
 * @version 1.00 2006/11/21
 */
 import java.util.Scanner;
 import java.util.Stack;
public class Recursion_Assignment4_TowersOfHanoi
{

    static printer prntr;
    static Stack<Integer> tow1, tow2, tow3;
    static Stack<Integer> tow1T, tow2T, tow3T;
    static Scanner kb;

    static final int L = 1, C = 2, R = 3;

    public static void main(String[] args)
    {
		kb = new Scanner(System.in);
		try
		{
			buildTowers(5);
			printTowers();

			String input = kb.nextLine();
			solveTowers(5, L, R, C);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			kb.nextLine();
		}
    }

    private static void buildTowers(int size)
    {
    	tow1 = new Stack<Integer>();
    	tow2 = new Stack<Integer>();
    	tow3 = new Stack<Integer>();

    	tow1T = new Stack<Integer>();
    	tow2T = new Stack<Integer>();
    	tow3T = new Stack<Integer>();

    	for(Integer i = size; i > 0; i--)
    	{
    		tow1.push(i);
    	}
    }

     static void solveTowers(int height, int from, int to, int with)
     {
         if (height > 0)
         {
            solveTowers(height-1, from, with, to);
            move(from, to);
            printTowers();
            kb.nextLine();
            solveTowers(height-1, with, to, from);
         }
     }

    private static void move(int source, int dest)
    {
    	int piece = 0;
    	switch(source)
    	{
    		case 1:
    			piece = tow1.pop();
    			break;
    		case 2:
    			piece = tow2.pop();
    			break;
    		case 3:
    			piece = tow3.pop();
    			break;
    	}

    	switch(dest)
    	{
    		case 1:
    			tow1.push(piece);
    			break;
    		case 2:
    			tow2.push(piece);
    			break;
    		case 3:
    			tow3.push(piece);
    			break;
    	}
    }

    private static void printTowers()
    {
    	printTowers(0, "");
    }

    private static void printTowers(int line, String print)
    {
    	String printMe = "";

    	if(tow1.isEmpty() && tow2.isEmpty() && tow3.isEmpty())
    	{
    		if(line < 5)
    			for(int i = 5; line < i; i--)
    				System.out.println(" |      |      |");

    		System.out.println(print.substring(1));
    		return;
    	}
    	else
    	{
    		if(tow1.isEmpty())
    			printMe = " |     ";
    		else
    		{
    			int tow1Len = tow1.pop();
    			printMe = towerLine(tow1Len);
    			tow1T.push(tow1Len);
    		}
    		if(tow2.isEmpty())
    			printMe += " |     ";
    		else
    		{
    			int tow2Len = tow2.pop();
    			printMe += towerLine(tow2Len);
    			tow2T.push(tow2Len);
    		}
    		if(tow3.isEmpty())
    			printMe += " |     ";
    		else
    		{
    			int tow3Len = tow3.pop();
    			printMe += towerLine(tow3Len);
    			tow3T.push(tow3Len);
    		}
    	}
    	printTowers(line + 1, print + "\n" + printMe);

	   	if(!tow1T.isEmpty())
		{
			tow1.push(tow1T.pop());
		}
		if(!tow2T.isEmpty())
		{
			tow2.push(tow2T.pop());
		}
		if(!tow3T.isEmpty())
		{
			tow3.push(tow3T.pop());
		}
    }

    private static String towerLine(int size)
    {
    	String str = " ";
    	for(int i = 0; i < 6; i++)
    	{
    		if(i < size)
    			str += "-";
    		else
    			str += " ";
    	}
    	return str;
    }
}
