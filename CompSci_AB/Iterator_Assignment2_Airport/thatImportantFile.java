 import java.util.LinkedList;
 import java.util.ListIterator;
 import java.util.ArrayList;
 import java.util.Scanner;
 import java.util.StringTokenizer;
public class thatImportantFile
{
	
	static ArrayList flights;
	static boolean debug = false;
	
	public static void main(String[] args)
	{
		System.out.println("Welcome to the airport management system");
		System.out.println("Initializing database-like-thing, please wait...");
		
		flights = new ArrayList();
		flights.add(new LinkedList<String>());
		flights.add(new LinkedList<String>());
		flights.add(new LinkedList<String>());
		flights.add(new LinkedList<String>());
		
		System.out.println("Wasn't that fast? Good.");
		System.out.println("-----------------------");
		System.out.println("Commands:");
		System.out.println("\tadd flightNumber passengerName");
		System.out.println("\t\tadds passenger to flight");
		System.out.println("\tdelete flightNumber passengerName");
		System.out.println("\t\tdeletes passenger from flight");
		System.out.println("\tlist flightNumber");
		System.out.println("\t\tlists all passengers on flight");
		System.out.println("\texit");
		
		Scanner input = new Scanner(System.in);
		
		while(!input.equals("exit"))
		{
			System.out.print("> ");
			String temp = input.nextLine().trim();
			StringTokenizer tok = new StringTokenizer(temp);
			
			try
			{
				String cmd = tok.nextToken().toLowerCase();
				if(cmd.equals("add"))
				{
					//tok.nextToken();
					int flightNum = Integer.parseInt(tok.nextToken());
					String passengerName = tok.nextToken();
					while(tok.hasMoreTokens())
						passengerName += " " + tok.nextToken();
					addPassenger(flightNum, passengerName);
				}
				else if(cmd.equals("delete"))
				{
					//tok.nextToken();
					int flightNum = Integer.parseInt(tok.nextToken());
					String passengerName = tok.nextToken();
					while(tok.hasMoreTokens())
						passengerName += " " + tok.nextToken();
					deletePassenger(flightNum, passengerName);
				}
				else if(cmd.equals("clear"))
				{
					//tok.nextToken();
					int flightNum = Integer.parseInt(tok.nextToken());
					clearFlight(flightNum);
				}
				else if(cmd.equals("list"))
				{
					//tok.nextToken();
					int flightNum = Integer.parseInt(tok.nextToken());
					listPassengers(flightNum);
				}
				else if(cmd.equals("debug"))
				{
					for(int i = 0; i < 4; i++)
						System.out.println(((LinkedList)flights.get(i)).toString());
					debug = true;
					System.out.println("** DEBUG MODE ENABLED**");
					
				}
				else if(cmd.equals("undebug"))
				{
					for(int i = 0; i < 4; i++)
						System.out.println(((LinkedList)flights.get(i)).toString());
					debug = false;
					System.out.println("** DEBUG MODE DISABLED**");
				}
				else if(cmd.equals("exit"))
				{
					break;
				}
				else if(cmd.equals("help") || cmd.equals("?"))
				{
					System.out.println("Commands:");
					System.out.println("\tadd flightNumber passengerName");
					System.out.println("\t\tadds passenger to flight");
					System.out.println("\tdelete flightNumber passengerName");
					System.out.println("\t\tdeletes passenger from flight");
					System.out.println("\tlist flightNumber");
					System.out.println("\t\tlists all passengers on flight");
					System.out.println("\tclear flightNumber");
					System.out.println("\t\tclears all passengers on flight");
					System.out.println("\texit");
					System.out.println("\t\texits");
				}
				
				else
				{
					System.err.println("ERROR 1A: Incompetent User. Please try again.");
				}
			}
			catch(Exception ex)
			{
				System.err.println("ERROR 1B: Incompetent User. Please try again.");
				System.err.println("  (here's a hint what you did wrong...)");
				ex.printStackTrace();
			}
		}
	}
	
	private static void addPassenger(int flight, String name)
	{
		
		ListIterator iterator = ((LinkedList)flights.get((flight / 1000) - 1)).listIterator();
		int count = 0;
		int addHere = 0;
		boolean added = false;
		
		if(name.length() > 0)
			name = name.substring(0,1).toUpperCase() + name.substring(1);
		
		//find the spot to put the name in
		while(iterator.hasNext() && addHere == 0)
		{
			if(debug) System.out.println("Entering iterator (addPassenger) at " + count + " and " + addHere);
			
			String current = (String)iterator.next();
			if(current.compareTo(name) >= 0)
			{
				if(debug) System.out.println("Compare > 0 (addPassenger) at " + count + " and " + addHere);
				
				addHere = count;
				break;
			}
			count++;
			if(debug) System.out.println("Count incremented  (addPassenger) at " + count + " and " + addHere);
		}
		
		if(addHere == 0 && added == false)
		{
			if(debug) System.out.println("Count is Zero, loop exited  (addPassenger) at " + count + " and " + addHere);
			iterator.add(name);
			added = true;
		}
		count = 0;
		if(debug) System.out.println("Count reset (addPassenger) at " + count + " and " + addHere);
		iterator = ((LinkedList)flights.get((flight / 1000) - 1)).listIterator();
		
		//get to that spot and add the name
		while(iterator.hasNext() && !added)
		{
			if(debug) System.out.println("Entering insertion iterator (addPassenger ins) at " + count + " and " + addHere);
			
			if(count == addHere)
			{
				if(debug) System.out.println("addHere reached (addPassenger ins) at " + count + " and " + addHere);
				
				iterator.add(name);
				added = true;
				break;
			}
			count++;
			iterator.next();
			if(debug) System.out.println("Count incremented  (addPassenger ins) at " + count + " and " + addHere);
		}
		
	}
	
	private static void deletePassenger(int flight, String name)
	{
		
		ListIterator iterator = ((LinkedList)flights.get((flight / 1000) - 1)).listIterator();
		
		while(iterator.hasNext())
		{
			if(iterator.next().equals(name))
			{
				if(debug) System.out.println("Removing Passenger " + name + " from flight " + flight);
				iterator.remove();
				break;
			}
		}
	}
	
	private static void listPassengers(int flight)
	{
		ListIterator iterator = ((LinkedList)flights.get((flight / 1000) - 1)).listIterator();
		System.out.println("Passengers for flight " + flight + ":");
		while(iterator.hasNext())
		{
			System.out.print(iterator.next() + "\t");
		}
		System.out.println();
	}
	
	private static void clearFlight(int flight)
	{
		ListIterator iterator = ((LinkedList)flights.get((flight / 1000) - 1)).listIterator();
		System.out.println("Passengers for flight " + flight + ":");
		while(iterator.hasNext())
		{
			iterator.remove();
			if(debug) System.out.println("Removing Passenger from flight " + flight);
		}
	}
}