/**
   Lab Assignment AB28.1 - Email Directory

   File Name: EMDirectory.java

   The class EMDirectory stores email addresses as strings that are stored
   and accessed by name. The directory is initially setup in the constructor
   that reads  the information from the indicated file and creates the
   email directory. The email directory is then be able to add additonal names
   and email addresses, lookup the email address that goes with a name,
   look up all email addresses that have names with the same last name,
   and display all name - email pairs.

   @author     Your Name Here
   @created    Month ##, ####

   Period ##

   Sources: Chris Nevison
   Modifiied by Jason Quesenberry and Nancy Quesenberry
   February 9, 2006
 */
import java.io.*;
import java.util.*;

public class EMDirectory{
  private Map dir;

  /**
     Constructs an empty email directory map.
   */
  public EMDirectory(){
    dir = new TreeMap();
  }

  /**
     Constructs an email directory map by reading name and email
     information from the indicated file name and adding them to the
     email directory map. The name information serves as the key for
     the email address items

     @param dirFile - file name containing names and email addresses
   */
  public EMDirectory(String dirFile){
    dir = new TreeMap();

    Scanner in;

    try{
    	in = new Scanner(new File(dirFile));
    	while(in.hasNext()){
      		String lastName = "";
      		String firstName = "";
      		String emailAddr = "";
      		if(in.hasNext())
        		firstName = in.next();
      		if(in.hasNext())
        		lastName = in.next();
      		if(in.hasNext())
        		emailAddr = in.next();

      		addEntry(new Name(lastName, firstName), emailAddr);
    	}
    }catch(IOException i){
    	System.out.println("Error: " + i.getMessage());
    }
  }

  /**
     Add email to the map with name as the key

     @param name  Name object containing first and last name to be used as key
     @param emailAddr  Value to be put into map, pointed to by name
   */
  public void addEntry(Name name, String emailAddr){
    dir.put(name, emailAddr);
  }

  /**
     Get the email address associated with the name

     @param name  Name object which should point to a value in the map -- an email address
     @returns  Email address associated with name
   */
  public String lookup(Name name){
    return (String)dir.get(name);
  }

  /**
     Iterate through the map and return all email addresses associated with a name
     that has the last name set to the same value as lastName

     @param lastName  Look for this in the last() spot in the Name keys
     @returns  TreeSet containing the matching email addresses
   */
  public Set lookupLastName(String lastName) {
    Set result = new TreeSet();
    Name key;
    String addr;

    Set keys = dir.keySet();
    Iterator iter = keys.iterator();

    while(iter.hasNext())
    {
    	key = (Name)iter.next();
    	if(key.last().equals(lastName))
    		result.add((String)(dir.get(key)));
    }

    return result;
  }

  /**
     print a list of all names and their associated email addresses
   */
  public void listAll(){
    Name key;
    String addr;

    Set keys = dir.keySet();
    Iterator iter = keys.iterator();

    while(iter.hasNext())
    {
    	key = (Name)iter.next();
    	addr = (String)dir.get(key);
    	System.out.println(key + " - " + addr);
    }
  }
}

