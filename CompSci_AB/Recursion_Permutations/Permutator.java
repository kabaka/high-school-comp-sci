/**
 * @(#)Permutator.java
 *
 *
 * @author Kyle Johnson
 * @version 1.00 2006/11/16
 */
 import java.util.Scanner;
 import java.util.ArrayList;
 import java.util.Stack;
 import java.util.Queue;
 import java.util.LinkedList;
public class Permutator
{

	static long count = 0;
	static ArrayList<String> wordlist;
	static Scanner wl;
	static Scanner kb;

    public static void main(String[] args)
    {

			System.out.println((char)174 + "Permutator!" + (char)175);
			kb = new Scanner(System.in);
			wordlist = new ArrayList<String>();

	    	try
	    	{
	    		System.out.println("Loading list...");
				wl = new Scanner(new java.io.File("wordlist.txt"));
				int line = 0;
				String lastLine = "";

				System.out.print("Words Loaded: ");

				while(wl.hasNextLine())
				{
					wordlist.add(wl.nextLine());
					line++;
					if(line % 111 == 0)
					{
						for(int i = 0; i < lastLine.length(); i++)
							System.out.print(((char)8));
						lastLine = line + "";
						System.out.print(lastLine);
					}
				}
				for(int i = 0; i < lastLine.length(); i++)
					System.out.print(((char)8));
				lastLine = line + "";
				System.out.print(lastLine);

				System.out.println();


    			while(true)
    			{
					System.out.print("Type some STUFF " + ((char)1) + "> ");

					String input = kb.nextLine();
					input.replace((char)7,' ');  // NO BEEPS!! Grrrrr...


					System.out.print("Type 1 for permutations, 2 for possible words > ");

					String choice = kb.nextLine();
					if(wordHasChar(choice,'1'))
						permutate(input);
					else if(wordHasChar(choice,'~'))
						findPalindromes();
					else
						permutationsAreWords(input);
				}

	    	}
	    	catch(Exception ex)
	    	{
	    		ex.printStackTrace();
	    	}
	    	System.out.println("Press any key (actually just return/enter " + ((char)1) + ") to continue...");

    }

	/* precondition: input is not null
	 * postcondition: all words that contain any of the letters in input,
	 *                but no letters not in input, are printed to screen
	 */
    private static void permutationsAreWords(String input)
    {
    	int pos = 0;
    	String modInput = input.replaceAll(" | more", "");
    	boolean enabledMore = input.length() > modInput.length();
    	input = modInput;

    	ArrayList<String> strArry = new ArrayList<String>();
    	String lastLine = "";

    	System.out.print("Progress: ");

		while(pos < wordlist.size())
		{
			String word = wordlist.get(pos);
			pos++;
			if(((pos + 0.0) / wordlist.size() * 100) % 10 == 0)
			{
				for(int i = 0; i < lastLine.length(); i++)
					System.out.print(((char)8));
				lastLine = (((pos + 0.0) / wordlist.size() * 100)) + "%";
				System.out.print(lastLine);
			}

			for(int i = 0; i < input.length(); i++)
				if(i == input.length() - 1 && wordHasChar(word,input.charAt(i)))
					for(int j = 0; j < word.length(); j++)
						if(j == word.length() - 1 && wordHasChar(input,word.charAt(j)))
							strArry.add(word);
						else if(!wordHasChar(input,word.charAt(j)))
							break;
				else if(!wordHasChar(word,input.charAt(i)))
					break;
		}

		System.out.println();

		if(strArry.size() == 0)
		{
			System.out.println("No Matches!");
			return;
		}

		int lines = 0;
		for(String str : strArry)
		{
			if(enabledMore && lines % 23 == 0 && lines > 1)
			{
				System.out.println("More... ( " + (((lines + 0.0) / strArry.size()) * 100) + "% - " + lines + " of " + strArry.size() + ")");
				String moreInput = kb.nextLine().trim().toLowerCase();
				if(wordHasChar(moreInput,'x'))
					return;
			}
			lines++;
			System.out.println(str);
		}
    }

	/* precondition: str contains a string of any length, ch is not null
	 * postcondition: return true if str contains any instances of ch
	 */
    private static boolean wordHasChar(String str, char ch)
    {
    	for(int i = 0; i < str.length(); i++)
    		if(str.charAt(i) == ch)
    			return true;
    	return false;
    }

	// just a caller method
    private static void permutate(String str)
    {
    	String modInput = str.replaceAll(" | more", "");
    	boolean enabledMore = str.length() > modInput.length();

    	permutate(modInput, "", enabledMore);
    }

	/* precondition: str contains a string of any length, pre is empty unless
	 *               it has been called recursively
	 * postcondition: all permutations of str are printed on screen
	 */
    private static void permutate(String str, String pre, boolean enabledMore)
    {

		if(str.length() < 2)
		{
			if(enabledMore && count % 23 == 0 && count > 1)
			{
				System.out.println("More...");
				String moreInput = kb.nextLine().trim().toLowerCase();
				if(wordHasChar(moreInput,'x'))
					return;
			}

			System.out.println(pre + str);
			count++;
		}
		else
		{
			String temp;
			for (int i = 0; i < str.length(); i++)
			{
				temp = str.substring(0, i) + str.substring(i + 1);
				permutate(temp, pre + str.charAt(i), enabledMore);
			}
		}
    }

    private static void findPalindromes()
    {
    	for(int i = 0; i < wordlist.size(); i++)
    	{
    		if(checkIfPalindrome(wordlist.get(i).trim().toLowerCase()))
    			System.out.println(wordlist.get(i));
    	}
    }

    /* precondition: str contains a string
     * postcondition: return true if str is a palindrome, false otherwise
     */
    private static boolean checkIfPalindrome(String str)
	{
		Stack<Character> stk = new Stack<Character>();
		Queue<Character> que = new LinkedList<Character>();

		String test = "";

		for(int i = 0; i < str.length() / 2; i++)
		{
			stk.push(str.charAt(i));
			que.add(str.charAt(i + str.length() / 2));
		}
		for(int i = 0; i < str.length() / 2; i++)
		{
			if(((Character)stk.pop()) != ((Character)que.poll()))
				return false;
		}
		return true;
	}
}
