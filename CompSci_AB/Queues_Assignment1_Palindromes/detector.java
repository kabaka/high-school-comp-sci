import java.util.Scanner;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class detector
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		System.out.print("Type somethin' >");
		String input = in.nextLine().toLowerCase().trim();
		for(int i = 0; i < input.length(); i++)
			if(!(Character.isLetter(input.charAt(i))))
			{
				input = input.substring(0,i) + input.substring(i+1);
				i--;
			}
		
		if(input.length() % 2 != 0)
		{
			input = input.substring(0, (input.length() / 2) + 1) + input.charAt(input.length() / 2) + input.substring(input.length() / 2 + 1);
		}
		
		//System.out.println("Checking your input... Parsed to: " + input);
		
		if(checkIfPalindrome(input))
			System.out.println("Apparently that's a palindrome. Hooray!");
		else 
			System.out.println("That's not a palindrome...");
	}
	
	private static boolean checkIfPalindrome(String str)
	{
		Stack<Character> stk = new Stack<Character>();
		Queue<Character> que = new LinkedList<Character>();
		
		String test = "";
		
		for(int i = 0; i < str.length() / 2; i++)
		{
			stk.push(str.charAt(i));
			que.add(str.charAt(i + str.length() / 2));
		}
		for(int i = 0; i < str.length() / 2; i++)
		{
			if(((Character)stk.pop()) != ((Character)que.poll()))
				return false;
		}
		return true;
	}
	
	private void printBanner()
	{
		System.out.println("*******************");
		System.out.println("*   Palindrome    *");
		System.out.println("*    Detection    *");
		System.out.println("*     System      *");
		System.out.println("*******************");
	}
}