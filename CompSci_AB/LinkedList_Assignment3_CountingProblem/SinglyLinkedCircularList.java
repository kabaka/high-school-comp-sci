// Implements a singly-linked list.

import java.util.Iterator;

public class SinglyLinkedCircularList implements Iterable<Object>
{
  //private ListNode head;
  private ListNode tail;
  private int nodeCount;
  private ListNode currentNode;
  private int currentPosition = 0;

  // Constructor: creates an empty list
  public SinglyLinkedCircularList()
  {
	//head = null;
	tail = null;
	nodeCount = 0;
  }

  // Constructor: creates a list that contains
  // all elements from the array values, in the same order
  public SinglyLinkedCircularList(Object[] values)
  {
	tail = null;
	for (Object value : values) // for each value to insert
	{
	  ListNode node = new ListNode(value, null);
	  /*if (head == null)
		head = node;
	  else*/
		tail.setNext(node);
	  tail = node;    // update tail
	  //tail.setNext(head);
	}

	nodeCount = values.length;
	currentNode = tail.getNext();
  }

  // Returns true if this list is empty; otherwise returns false.
  public boolean isEmpty()
  {
	return nodeCount == 0;
  }

  // Returns the number of elements in this list.
  public int size()
  {
	return nodeCount;
  }

  // Returns true if this list contains an element equal to obj;
  // otherwise returns false.
  public boolean contains(Object obj)
  {
  	//ListNode myNode = head;
	for(Object ob : this)
	{
		if(obj.equals(ob)) return true;
		//myNode = myNode.getNext();
	}
	return false;
  }

  // Returns the index of the first element in equal to obj;
  // if not found, retunrs -1.
  public int indexOf(Object obj)
  {
  	ListNode myNode = tail.getNext();
	for(int i = 0; i < nodeCount; i++)
	{
		if(obj.equals(myNode.getValue())) return i;
		myNode = myNode.getNext();
	}
	return -1;
  }

  // Adds obj to this collection.  Returns true if successful;
  // otherwise returns false.
  public boolean add(Object obj)
  {
	if(tail == null)
	{
  		//head = new ListNode(obj, null);
  		ListNode newNode = new ListNode(obj, tail);
  		tail = newNode;  //update tail
  		tail.setNext(newNode);
  		currentNode = tail.getNext();
	}
	else
	{
		ListNode newNode = new ListNode(obj, tail.getNext());
		tail.setNext(newNode);
		tail = newNode;
		//tail.setNext(head);
	}

	nodeCount++;

	return true;
  }

  // Removes the first element that is equal to obj, if any.
  // Returns true if successful; otherwise returns false.
  public boolean remove(Object obj)
  {
	if(tail == null)
		return false;
	else
	{
		ListNode node = tail.getNext();
		ListNode last = tail;

		while(node.getNext() != null)
		{
			if(node.getValue().equals(obj))
				break;
			last = node;
			node = node.getNext();
		}

		if(!node.getValue().equals(obj)) return false;

		ListNode temp = new ListNode(null, null);
		temp.setNext(node.getNext());

		last.setNext(temp.getNext());

		temp.setNext(null);
		nodeCount--;
		return true;
	}
  }

  public Object next()
  {
  	currentNode = currentNode.getNext();
  	if(currentNode == tail.getNext())
  		currentPosition = 0;
  	else
  		currentPosition++;
  	return currentNode.getValue();
  }

  public void goToNext()
  {
  	currentNode = currentNode.getNext();
  	if(currentNode == tail.getNext())
  		currentPosition = 0;
  	else
  		currentPosition++;
  }

  public Object currentValue()
  {
  	return currentNode.getValue();
  }


  // Returns the i-th element.
  public Object get(int i)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount) throw new IndexOutOfBoundsException();

  	ListNode myNode = tail.getNext();
	for(int j = 0; j < i; j++)
		myNode = myNode.getNext();

	return myNode.getValue();
  }

  // Replaces the i-th element with obj and returns the old value.
  public Object set(int i, Object obj)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount) throw new IndexOutOfBoundsException();

  	ListNode myNode = tail.getNext();
	for(int j = 0; j < i; j++)
		myNode = myNode.getNext();

	myNode.setValue(obj);

	//tail = myNode.getNext();
	//tail.setNext(head);
	return true;
  }

  // Inserts obj to become the i-th element. Increments the size
  // of the list by one.
  public void add(int i, Object obj)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount || i < 0) throw new IndexOutOfBoundsException();

	moveTo(i);
	//System.out.println("debug[1]: CN: " + currentNode.getValue() + " CP: " + currentPosition);
  	ListNode temp = new ListNode(obj, currentNode);

  	if(i != 0)
	{
		moveTo(i - 1);
		//System.out.println("debug[2]: CN: " + currentNode.getValue() + " CP: " + currentPosition);
		currentNode.setNext(temp);
	}
	else
	{
		tail.setNext(temp);
		//head = temp;
	}
  	nodeCount++;

  	moveTo(nodeCount / 2); // dirty fix - list gets confused
  }

  // Removes the i-th element and returns its value.
  // Decrements the size of the list by one.
  public Object remove(int i)
  {
	if(tail == null)
		return false;
	else if(i == nodeCount - 1)
	{
		moveTo(i);
		currentNode.setNext(tail.getNext());
		tail = currentNode;
		nodeCount--;
		currentPosition--;
		return true;
	}
	else
	{
		ListNode node = tail.getNext();
		ListNode last = tail;

		for(int j = 0; j < i; j++)
		{
			last = node;
			node = node.getNext();
		}

		//if(!node.getValue().equals(obj)) return false;

		ListNode temp = new ListNode(null, null);
		temp.setNext(node.getNext());

		last.setNext(temp.getNext());

		temp.setNext(null);
		nodeCount--;
		currentPosition--;
		if(currentPosition < 0)
			currentPosition = nodeCount - 1;
		else if(currentPosition == 0)
			currentNode = tail.getNext();
		return true;
	}
  }

  // Move current to the specified index
  private void moveTo(int index)
  {
  	//make sure the node we're going after is in the range of the list
  	if(index < 0 || index >= nodeCount || isEmpty())
  		throw new IndexOutOfBoundsException();

  	//if we're somewhere that is nowhere, go home
  	if(currentNode == null)
  		currentNode = tail.getNext();


  	if(index < currentPosition)
  	{
  		currentNode = tail.getNext();
  		currentPosition = 0;
  	}

  	while(currentPosition < index)
	{
		//System.out.println("Iterating to " + index);
		currentNode = currentNode.getNext();
		currentPosition++;
	}

  }

  // Returns a string representation of this list.
  public String toString()
  {
	String asString = "[";
	ListNode myNode = tail.getNext();

	for(int i = 0; i < nodeCount; i++)
	{
		if(i > 0)
			asString += ", ";
		//System.out.println("i: " + i + "; nodeCount: " + nodeCount + "; asString: " + asString);
		asString += myNode.getValue().toString();
		if(i == currentPosition)
			asString += "*";
		myNode = myNode.getNext();
	}
	asString += "]";

	return asString;
  }

  // Returns an iterator for this collection.
  public Iterator<Object> iterator()
  {
	return new SinglyLinkedCircularListIterator(tail.getNext());
  }

  //Print the list backwards
  public void printBackwards()
  {
  	if(nodeCount == 0)
  		throw new IndexOutOfBoundsException();

  	if(tail != null)
  		printBackwards(tail.getNext().getNext());
  	System.out.println(tail.getNext().getValue());
  }

  //The recursive element of the above method
  //This should never be called outside that method
  private void printBackwards(ListNode node)
  {
  	if(node.getNext() != null)
  		printBackwards(node.getNext());
  	System.out.println(node.getValue());
  }

  //print the list
  public void printList()
  {
  	/*for(int i = 0; i < nodeCount; i++)
  		System.out.println(this.get(i));*/
	System.out.println(this);
  }

  public ListNode find(Object obj)
  {
  	ListNode node = tail.getNext();
  	while(node.getNext() != null)
		if(node.getValue().equals(obj))
			return node;
	return null;
  }

  public void clear()
  {
  	//head = new ListNode(null, null);
  	tail = new ListNode(null,null);
  }


  //debugging method
  public void testPrint(int iterations)
  {
  	ListNode node = tail.getNext();
  	for(int i = 0; i < iterations; i++)
  	{
  		System.out.println(node.getValue());
  		node = node.getNext();
  	}
  }

  public int getCurrentPosition()
  {
  	return currentPosition;
  }

}
