public class printer
{
        public static void printLine(String line)
        {
                fillLine(3);
                System.out.print((char)219);
                fillLine(3);
                System.out.print(line);
                fillLine(72 - (3 + line.length()));
                System.out.print((char)219);
                System.out.print(" ");
                fillLine(2);
        }

        public static void printLine(String line, boolean noReturn)
        {
                fillLine(3);
                System.out.print((char)219);
                fillLine(3);
                System.out.print(line);
        }

        public static void printLine(String line, int distToEnd)
        {
                System.out.print(line);
                fillLine(69 - distToEnd - line.length());
                System.out.print((char)219 + " ");
                fillLine(2);
        }

        public static void fillLine()
        {
                for(int i = 0; i < 80 ; i++)
                        System.out.print((char)177);
        }

        public static void fillLine(int chars)
        {
                for(int i = 0; i < chars ; i++)
                        System.out.print((char)177);
        }

        public static void Clear()
        {
                System.out.println();
                for(int i = 0; i < 25; i++)
                        fillLine();
        }

        public static void fillLineEdge()
        {
                fillLine(3);
                for(int i = 0; i < 74; i++)
                        System.out.print((char)219);
                fillLine(3);
        }

        public static void fillLineEdgeBottom()
        {
                fillLine(3);
                for(int i = 0; i < 74; i++)
                        System.out.print((char)219);
                System.out.print(" ");
                fillLine(2);
                fillLine(5);
                for(int i = 0; i < 73; i++)
                        System.out.print(" ");
                fillLine(2);
        }

        public static void showMenu(String[] items)
        {
                Clear();
                fillLineEdge();
                printLine("");
                for(int i = 0; i < items.length; i++)
                        printLine(items[i]);
                printLine("");
                fillLineEdgeBottom();
                fillLine();
                fillLine();
        }
}


/*
 *
 *
                prntr.Clear();
                prntr.fillLineEdge();
                prntr.printLine("");
                sort.displayTehBirds();
                prntr.printLine("");
                prntr.fillLineEdgeBottom();
                prntr.fillLine();
                prntr.fillLine();
                prntr.fillLine(8);

 *
 *
 *
 */