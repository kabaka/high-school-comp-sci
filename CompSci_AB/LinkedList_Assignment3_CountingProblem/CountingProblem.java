/**
 * @(#)CountingProblem.java
 * Solve the counting problem
 *
 * @author Kyle Johnson
 * @version 1.00
 */
 import java.util.Scanner;
public class CountingProblem
{
	private static int lastInterval = 1;
	private static SinglyLinkedCircularList list;
	private static SinglyLinkedCircularList list2;

	/**
	 * Method main
	 * @param args
	 */
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		list = new SinglyLinkedCircularList();

		System.out.println("The Terrible Counting Problem: Solution Finder!");
		System.out.println();

		System.out.print("Input File [terriblecount.dat]> ");

		//String input = in.nextLine();
		String input = "";

		String filename = "terriblecount.dat";
		//Scanner kb = new Scanner(System.in);

		try
		{
			if(!input.equals(""))
				filename = input;

			Scanner fileIn = new Scanner(new java.io.File(filename));

			while(fileIn.hasNextLine())
				list.add(fileIn.nextLine());
			fileIn.close();

			while(!runAttempt(lastInterval))
				lastInterval++;
			System.out.println("Solution Found: " + lastInterval);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			in.nextLine();

		}
		in.nextLine();
	}

	/**
	 * Method runAttempt
	 * precondition: list is populated with x's and o's and is length 30
	 * postcondition: returns true if x's occur in list at interval, false
	 *                otherwise
	 * @param interval - rate of X's in list
	 *
	 * @return true if solution found
	 *
	 */
	private static boolean runAttempt(int interval)
	{
		backupList();
		System.out.println(list + "\nCurrentValue: " + ((String)list.currentValue()) + "\nCurrentPosition: " + list.getCurrentPosition());

		Scanner kb = new Scanner(System.in);
		for(int k = 0; k < 15; k++)
		{
			for(int i = 0; i < interval+1; i++)
			{
				list.goToNext();
			}
			System.out.println(list + "\nCurrentValue: " + ((String)list.currentValue()) + "\nCurrentPosition: " + list.getCurrentPosition());

			kb.nextLine();

			if(((String)(list.currentValue())).equals("x"))
			{
				System.out.println("FALSE AT " + interval);
				restoreList();
				return false;
			}
			else
				list.remove(list.getCurrentPosition());
		}

		return true;
	}

	/**
	 * Method backupList
	 * precondition: list contains data
	 * postcondition: list2 contains a duplicate of list in the same order
	 */
	private static void backupList()
	{
		list2 = new SinglyLinkedCircularList();

		for(int i = 0; i < list.size(); i++)
			list2.add(list.get(i));
	}

	/**
	 * Method restoreList
	 * precondition: list2 contains data
	 * postcondition: list contains the data from list2 in the same order
	 */
	private static void restoreList()
	{
		list = new SinglyLinkedCircularList();

		for(int i = 0; i < list2.size(); i++)
			list.add(list2.get(i));
	}
}
