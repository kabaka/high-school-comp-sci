
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.GridLayout;
import java.awt.Dimension;

public class LDEnvDisplay extends JPanel
{
  private Environment myEnv;

  private Icon northIcon;
  private Icon eastIcon;
  private Icon southIcon;
  private Icon westIcon;
  private JButton[][] cellGrid;
  private static final int MAXDIM = 50;

  public LDEnvDisplay(Environment env)
  { myEnv = env;
    setBackground(Color.white);

    int nr = env.numRows();
    int nc = env.numCols();
    int cellSize = cellDim();

    setSize(nc * cellSize, nr * cellSize);
    setLayout(new GridLayout(nr, nc));

    cellGrid = new JButton[nr][nc];
    for(int row = 0; row < nr; row++){
      for(int col = 0; col < nc; col++){
        cellGrid[row][col] = new JButton();
        cellGrid[row][col].setBackground(Color.white);
        cellGrid[row][col].setBorder(BorderFactory.createLineBorder(Color.black));
        cellGrid[row][col].setPreferredSize(new Dimension(cellSize, cellSize));
        add(cellGrid[row][col]);
      }
    }

    northIcon= new ImageIcon("C:\\data\\school\\CS_AB\\f1sh_LocDir\\src\\north.gif");
    eastIcon= new ImageIcon("C:\\data\\school\\CS_AB\\f1sh_LocDir\\src\\east.gif");
    southIcon= new ImageIcon("C:\\data\\school\\CS_AB\\f1sh_LocDir\\src\\south.gif");
    westIcon= new ImageIcon("C:\\data\\school\\CS_AB\\f1sh_LocDir\\src\\west.gif");
  }

  private int cellDim()
  {
    int numCells;
    if(myEnv.numRows() < myEnv.numCols())
      numCells = myEnv.numCols();
    else
      numCells = myEnv.numRows();

    if(numCells > MAXDIM)
      throw new IllegalArgumentException("grid too large");

    return 2 * (250/numCells);
  }

  private Icon icon(Direction dir)
  { if(dir.equals(Direction.NORTH))
      return northIcon;
    else if(dir.equals(Direction.EAST))
      return eastIcon;
    else if(dir.equals(Direction.SOUTH))
      return southIcon;
    else if(dir.equals(Direction.WEST))
      return westIcon;
    else
      return null;
  }

  public void showEnv()
  {
    clearAll();

    Locatable[] itemList = myEnv.allObjects();
    for(int k = 0; k < itemList.length; k++)
    { if(itemList[k] instanceof LocDir)
      { LocDir item = (LocDir)itemList[k];
        cellGrid[item.location().row()][item.location().col()].setIcon(icon(item.direction()));
      }
      else if(itemList[k] instanceof ColorBlock)
      {
        ColorBlock cb = (ColorBlock)itemList[k];
        cellGrid[cb.location().row()][cb.location().col()].setBackground(cb.color());
      }
    }
  }

  private void clearAll()
  {
    for(int row = 0; row < myEnv.numRows(); row++)
      for(int col = 0; col < myEnv.numCols(); col++)
      {
        cellGrid[row][col].setIcon(null);
        cellGrid[row][col].setBackground(Color.white);
      }
  }
}