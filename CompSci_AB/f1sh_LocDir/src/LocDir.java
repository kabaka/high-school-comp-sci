
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class LocDir implements Locatable
{
  private Location myLoc;
  private Direction myDir;


  public LocDir(Location loc, Direction dir)
  { myLoc = loc;
    myDir = dir;
  }

  public Location location()
  { return myLoc;
  }

  public Direction direction()
  { return myDir;
  }

  public String toString()
  {
    return "Mover " + location()  + " " + direction();
  }
}


