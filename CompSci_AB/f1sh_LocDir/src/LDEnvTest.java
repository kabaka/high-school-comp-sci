

import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.util.Random;

public class LDEnvTest
{
  public static void main(String[] args)
  {
    Environment env = new BoundedEnv(20, 20);
    LDEnvDisplay display = new LDEnvDisplay(env);

    createFrame(display, 20, 20);
    
    cbTriangle(display, env, 0, 5, 6); // ex 11
    /*
    LocDir ld1 = new LocDir(new Location(4, 4), Direction.EAST); //ex 12
    env.add(ld1);*/
    
    randomPath(display, env, 10, 8, 8, 250); // ex 13
    

  ///////////// remove the sample code below and replace with your code ///////
/*
    env.add(new ColorBlock(new Location(3,3), Color.blue));

    ColorBlock cb = new ColorBlock(new Location(7,7), Color.red);
    env.add(cb);
    env.add(new ColorBlock(new Location(11,11), Color.green));

    display.showEnv();
    System.out.println(env);
    try
    { Thread.sleep(2000);
    }
    catch(InterruptedException ex){}
    LocDir ld1 = new LocDir(new Location(3, 7), Direction.SOUTH);
    env.add(ld1);

    LocDir ld2 = new LocDir(new Location(11, 7), Direction.NORTH);
    env.add(ld2);
*/
    display.showEnv();
    System.out.println(env);
  }
  
  private static void cbTriangle(LDEnvDisplay display, Environment env, int topx, int topy, int width)
  {
	/*ColorBlock cb1 = new ColorBlock(new Location(topx,topy), Color.blue);
	env.add(cb1);/*/
      
      for(int i = topx; i < width; i++)
      {
	  for(int j = topy - i; j < width; j++)
	  {
	      ColorBlock cb = new ColorBlock(new Location(i, j), Color.blue);
	      env.add(cb);
	      display.showEnv();
	  }   
      }
      
  }

  private static void randomPath(LDEnvDisplay display, Environment env,
	  int count, int x, int y, long interval)
  {
      Location currentLoc = new Location(x, y);
      LocDir ld = new LocDir(currentLoc, randomDir());
      env.add(ld);
      sleep(interval);
      display.showEnv();
      
      for(int i = 0; i < count; i++)
      {
	  if(ld.direction().equals(Direction.NORTH))
	    x--;
	  else if(ld.direction().equals(Direction.SOUTH))
	    x++;
	  else if(ld.direction().equals(Direction.EAST))
	    y++;
	  else
	    y--;
	  
	  if(x > env.numRows())
	      x = env.numRows() - 1;
	  else if(x < 0)
	      x = 0;
	  else if(y > env.numCols())
	      y = env.numCols() - 1;
	  else if(y < 0)
	      y = 0;
	  
	  currentLoc = new Location(x, y);
	  
	  ld = new LocDir(currentLoc,randomDir(ld.direction()));
	  if((env.isEmpty(currentLoc)))
	    env.add(ld);
	  
	  display.showEnv();
	  System.out.println(i + "--" + env);
	  sleep(interval);
      }
  }
  
  private static Direction randomDir()
  {
      Random ran = new Random();
      
      int num = ran.nextInt(3);
      
      if(num == 0)
	  return Direction.NORTH;
      else if(num == 1)
	  return Direction.SOUTH;
      else if(num == 2)
	  return Direction.EAST;
      else
	  return Direction.WEST;
  }
  
  private static Direction randomDir(Direction dir)
  {
      Random ran = new Random();
      
      int num = ran.nextInt(3);
      
      if(num == 0 && !(dir.equals(Direction.SOUTH)))
	  return Direction.NORTH;
      else if(num == 0)
	  return dir;
      
      if(num == 1 && !(dir.equals(Direction.NORTH)))
	  return Direction.SOUTH;
      else if(num == 1)
	  return dir;
      
      if(num == 2 && !(dir.equals(Direction.WEST)))
	  return Direction.EAST;
      else if(num == 2)
	  return dir;
      
      if(num == 3  && !(dir.equals(Direction.EAST)))
	  return Direction.WEST;
      else
	  return dir;
  }
  
  private static void sleep(long time)
  {
      try
      {
	  Thread.sleep(time);
      }
      catch (Exception e)
      {
	  System.err.println("huh...");
      }
  }

  private static void createFrame(JPanel display, int xCoord, int yCoord)
  {
    int width = (int)Math.round((display).getSize().getWidth());
    int height = (int)Math.round((display).getSize().getHeight());

    JFrame frame = new JFrame();
    frame.setVisible(true);

    width = width + frame.getInsets().right + frame.getInsets().left;
    height = width + frame.getInsets().top + frame.getInsets().bottom;

    frame.setSize(width,height);
    frame.setLocation(xCoord, yCoord);

    frame.getContentPane().add(display);
    frame.setVisible(true);
  }

}