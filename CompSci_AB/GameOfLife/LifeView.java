import java.awt.*;
import javax.swing.*;

/*
  this is the View component
*/

class LifeView extends JPanel
{
    private int SIZE_X = 60;
    private int SIZE_Y = 110;
    private LifeCell[][] myGrid;

    public LifeView()
    {
        myGrid = new LifeCell[SIZE_X][SIZE_Y];
    }
    
    public LifeView(int sizeX, int sizeY)
    {
    	SIZE_X = sizeX;
    	SIZE_Y = sizeY;
        myGrid = new LifeCell[SIZE_X][SIZE_Y];
    }

    // entry point from model, requests grid be redisplayed
    public void updateView( LifeCell[][] mg )
    {
        myGrid = mg;
        repaint();
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        int testWidth = getWidth() / (SIZE_X+1);
        int testHeight = getHeight() / (SIZE_Y+1);
        // keep each life cell square
        int boxSize = Math.min(testHeight, testWidth);

        for ( int r = 0; r < SIZE_X; r++ )
        {
            for (int c = 0; c < SIZE_Y; c++ )
            {
                if (myGrid[r][c] != null)
                {
                    if ( myGrid[r][c].isAliveNow() )
                        g.setColor( Color.red );
                    else
                        g.setColor( new Color(235,235,255) );
                        //g.setColor( Color.gray );
                    g.fillRect( (c+1)*boxSize, (r+1)* boxSize, 
                                                boxSize-2, boxSize-2);
                }
            }
        }
    }
    
    public int getSizeX()
    {
    	return SIZE_X;
    }
    
    public int getSizeY()
    {
    	return SIZE_Y;
    }
}
