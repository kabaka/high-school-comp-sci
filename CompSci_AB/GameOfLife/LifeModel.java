import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LifeModel implements ActionListener
{

    /*
     *  This is the Model component.
     */

    private int SIZE_X = 0;
    private int SIZE_Y = 0;
    private LifeCell[][] myGrid;
    LifeView myView;
    Timer timer;

    // initial population from file or random if no file available
    public LifeModel(LifeView view)
    {
    	SIZE_X = view.getSizeX();
    	SIZE_Y = view.getSizeY();
        int r, c;
        myGrid = new LifeCell[SIZE_X][SIZE_Y];
        for ( r=0; r < SIZE_X; r++ )
            for ( c = 0; c < SIZE_Y; c++ )
                myGrid[r][c] = new LifeCell();

        EasyReader infile = new EasyReader("lifegrid.dat");
        if ( infile.bad() )
        // use random population
        {                                           
            for ( r=0; r < SIZE_X; r++ )
            {
                for ( c = 0; c < SIZE_Y; c++ )
                {
                    if ( Math.random() > 0.75)
                        myGrid[r][c].setAliveNow(true);
                }
            }
        }
        else
        // use file input from inFile
        {                                    
            int numInitialCells = infile.readInt();
            for (int count=0; count<numInitialCells; count++)
            {
                r = infile.readInt();
                c = infile.readInt();
                myGrid[r][c].setAliveNow(true);
            }
            infile.close();
        }

        myView = view;
        myView.updateView(myGrid);
    }

    public void pause()
    {
        timer.stop();
    }
    public void resume()
    {
        timer.restart();
    }
    public void run()
    {
        timer = new Timer(50, this);
        timer.setCoalesce(true);
        timer.start();
    }

    // get here each time timer fires
    public void actionPerformed(ActionEvent e)
    {
        oneGeneration();
        myView.updateView(myGrid);
    }
    
    private int numNeighbors(int x, int y)
    {
    	int count = 0;
    	for(int i = x - 1; i <= x + 1; i++)
    		for(int j = y - 1; j <= y + 1; j++)
    			if((i > 0) && (j > 0) && (i < myGrid.length) && (j < myGrid[0].length) && !(i == x && j == y))
    				if(myGrid[i][j].isAliveNow())
    					count++;
    	return count;
    }

    private void oneGeneration()
    {
        for(int i = 0; i < myGrid.length; i++)
        {
        	for(int j = 0; j < myGrid[0].length; j++)
        	{
        		LifeCell currentCell = myGrid[i][j];
        		
        		int neighbors = numNeighbors(i,j);
        		
        		if(currentCell.isAliveNow())
        			currentCell.setAliveNext(neighbors == 3 || neighbors == 2);
        		else
        			currentCell.setAliveNext(neighbors == 3);
        	}
        }
		for(int i = 0; i < myGrid.length; i++)
        	for(int j = 0; j < myGrid[0].length; j++)
        		myGrid[i][j].setAliveNow(myGrid[i][j].isAliveNext());
    }
}

