
/**
 * @(#)Enigma.java
 *
 * Enigma is the hash table and hint generator for the Cryptogram program.
 * This program has been created to illustrate the use and basic manipulation
 * of hash tables.
 *
 * @author Kyle Johnson
 * @version 1.00 2007/1/24
 */

 package cryptogram;

public class Enigma
{
    private char[] lookupTable;
    private int length = 0;

    /**
     * Create an instance of Enigma<p>
     *
     * alphabetLength defines the maximum number of letters that will be
     * allowed to be associated with another letter.
     *
     */
    public Enigma(int alphabetLength)
    {
    	lookupTable = new char[alphabetLength];
    	length = alphabetLength;
    	
    	for(int i = 0; i < alphabetLength; i++)
    		lookupTable[i] = '-';
    }
    
    /**
     * Associate a letter of the alphabet at position i with character ch
     *
     */
    public void setSubstitution(int i, char ch)
    {
    	lookupTable[i] = Character.toUpperCase(ch);
    }
    
    /**
     * Using values set by setSubstitution, decrypt String text and return
     * the result
     *
     */
    public String decode(String text)
    {
    	String decodedText = "";
    	for(int i = 0; i < text.length(); i++)
    		decodedText += getAssociatedChar(text.charAt(i));
    		
    	return decodedText;
    }
    
    /**
     * Returns a string containing a list of likely solutions for each character
     * orders ascending according to how often a character occurs in 'text'.<p>
     *
     * For example, if <i>text</i> contains the letter B only 1 time, while all
     * other letters occur at least 2 times, and the second letter of
     * <i>lettersByFrequency</i> is Q (which, in this case, it always will be),
     * then the first letter in the returned String will be Q.<p>
     *
     * Furthermore, if the letter Z occurs in <i>text</i>more than any other
     * letter, the 26th letter in <i>lettersByFrequency</i>' (once again, this
     * is hard-coded and will be E), then the last letter returned will also be
     * E.<p>
     *
     */    
    public String getHints(String text, String lettersByFrequency)
    {
    	String hints = "";
    	int count[] = countLetters(text);
    	
    	for(int i = 0; i < length; i++)
    	{
	    int pos = 0;
    		for(int k = 0; k < length; k++)
    		{
    			if(count[k] < count[i] || (count[k] == count[i] && k < i))
    				pos++;
			
    		}
    		
    		hints += lettersByFrequency.charAt(pos);
		
    	}
    	
    	
    	return hints;
    }
    
    /**
     * Look up the associated value of a character. Return spaces and
     * punctuation unmodified.
     *
     */
    private char getAssociatedChar(char ch)
    {
    	if(!Character.isLetter(ch))
    		return ch;
    	
    	return lookupTable[(Character.getNumericValue(ch) - Character.getNumericValue('A'))];
    }
    
    /**
     * Count the occurances of each letter and return them in an int array
     * sorted by A-Z (first position is the number of A's, second is B's, etc.).
     */
    private int[] countLetters(String text)
    {
    	int[] count = new int[length];
    	for(int i = 0; i < text.length(); i++)
    	{
    		char current = text.charAt(i);
    		if(Character.isLetter(current))
    		{
    			count[(Character.getNumericValue(current) -
    				Character.getNumericValue('A'))]++;
    		}
    	}
    	
    	return count;
    }
    
}