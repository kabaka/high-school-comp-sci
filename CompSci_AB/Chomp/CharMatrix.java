/**
 *  Implements a 2-D array of characters
 */

public class CharMatrix
{
  // fields:
	char[][] grid;

  /**
   *  Constructor:
   *  Creates a grid with dimensions <code>rows</code>, <code>cols</code>,
   *  and fills it with the <code>fill</code> character
   */
  public CharMatrix(int rows, int cols, char fill)
  {
    grid = new char[rows][cols];
    fillRect(0,0,numRows()-1,numCols()-1,fill);
  }

  /**
   *  Fills the given rectangle with <code>fill</code> characters.
   *  row0, col0 is the upper left corner and row1, col1 is the
   *  lower right corner of the rectangle.
   */
  public void fillRect(int row0, int col0, int row1, int col1, char fill)
  {
	for(int i = row0; i <= row1; i++)
		for(int j = col0; j <= col1; j++)
			grid[i][j] = fill;
  }

  /**
   *  Fills the given rectangle with SPACE characters.
   *  row0, col0 is the upper left corner and row1, col1 is the
   *  lower right corner of the rectangle.
   */
  public void clearRect(int row0, int col0, int row1, int col1)
  {
	fillRect(row0, col0, row1, col1, ' ');
  }

  /**
   *  Returns the number of rows in grid
   */
  public int numRows()
  {
    return grid.length;
  }

  /**
   *  Returns the number of columns in grid
   */
  public int numCols()
  {
	return grid[0].length;
  }

  /**
   *  Returns the character at row, col position
   */
  public char charAt(int row, int col)
  {
    return (char)(grid[row][col]);
  }

  /**
   *  Returns true if the character at row, col is a space,
   *  false otherwise
   */
  public boolean isEmpty(int row, int col)
  {
	return (charAt(row, col) == ' ');
  }

  /**
   *  Returns the count of all non-space characters in <code>row</code>
   */
  public int countInRow(int row)
  {
	int count = 0;
	for(int i = 0; i < numCols(); i++)
		if(!isEmpty(row,i)) count++;
	return count;
  }

  /**
   *  Returns the count of all non-space characters in <code>col</code>
   */
  public int countInCol(int col)
  {
  	int count = 0;
	for(int i = 0; i < numRows(); i++)
		if(!isEmpty(i,col)) count++;
	return count;
  }
}
