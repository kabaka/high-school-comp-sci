import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.util.Random;

public class CBTest
{
  public static void main(String[] args)
  {
    Environment env = new BoundedEnv(20, 20);
    CBEnvDisplay display = new CBEnvDisplay(env);

    createFrame(display, 20, 20);

    createSquare(display, 0, 0, 20, randomColor());
    
    randomSquares(display, env, 50000, 10);
    //randomBlocks(display, env, 500000, 10);
    
  }
  
  private static void randomSquares(CBEnvDisplay disp, Environment envr,
	  int count, long interval)
  {
      for(int i = 0; i < count; i++)
      {
	  Random rand = new Random();
	  int x = rand.nextInt(envr.numRows()-1);
	  int y = rand.nextInt(envr.numRows()-1);
	  int length = rand.nextInt(envr.numRows());
	  if(length > envr.numRows() || length > envr.numCols())
	      length = 1;
	  createSquare(disp, x, y, length, randomColor());
      }
  }
  
  private static void createSquare(CBEnvDisplay disp, int x, int y, int length,
	  Color color)
  {
    length--;
    Location loc;
    for(int i = x; i <= x + length; i++)
	for(int j = y; j <= y + length; j++)
	{
	    if(i == x || i == x + length || j == y || j == y + length)
	    {
		loc = new Location(i,j);
		disp.setCellColor(loc, color);
		System.out.println(loc);
	    }
	}
  }

  private static void randomBlocks(CBEnvDisplay disp, Environment envr,
	  int count, long interval)
  {
      for(int i = 0; i < count; i++)
      {
	  disp.setCellColor(randomLocation(envr), randomColor());
	  sleep(interval);
      }
  }
  
  private static void sleep(long time) // no see (hah!)
  {
      try
      {
	  Thread.sleep(time);
      }
      catch (Exception e)
      {
	  System.err.println("huh...");
      }
  }

  private static Color randomColor()
  {
      Random rand = new Random();
      
      return new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
  }
  
  private static Location randomLocation(Environment envr)
  {
      Random rand = new Random();
      return new Location(rand.nextInt(envr.numRows()), rand.nextInt(envr.numCols()));
  }
  
  private static void createFrame(JPanel display, int xCoord, int yCoord)
  {
    int width = (int)Math.round((display).getSize().getWidth());
    int height = (int)Math.round((display).getSize().getHeight());

    JFrame frame = new JFrame();
    frame.setVisible(true);

    width = width + frame.getInsets().right + frame.getInsets().left;
    height = width + frame.getInsets().top + frame.getInsets().bottom;

    frame.setSize(width,height);
    frame.setLocation(xCoord, yCoord);

    frame.getContentPane().add(display);
    frame.setVisible(true);
  }

}