// AP(r) Computer Science Marine Biology Simulation:
// The DarterFish class is copyright(c) 2002 College Entrance
// Examination Board (www.collegeboard.com).
//
// This class is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation.
//
// This class is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

import java.awt.Color;
import java.util.ArrayList;

/**
 *  AP&reg; Computer Science Marine Biology Simulation:<br>
 *  The <code>DarterFish</code> class represents a fish in the Marine
 *  Biology Simulation that darts forward two spaces if it can, moves
 *  forward one space if it  can't move two, and reverses direction
 *  (without moving) if it cannot  move forward.  It can only "see" an
 *  empty location two cells away if the cell in between is empty also.
 *  In other words, if both the cell in front of the darter and the cell
 *  in front of that cell are empty, the darter fish will move forward
 *  two spaces.  If only the cell in front of the darter is empty, it
 *  will move there.  If neither forward cell is empty, the fish will turn
 *  around, changing its direction but not its location.
 *
 *  <p>
 *  <code>DarterFish</code> objects inherit instance variables and much
 *  of their behavior from the <code>Fish</code> class.
 *
 *  <p>
 *  The <code>DarterFish</code> class is
 *  copyright&copy; 2002 College Entrance Examination Board
 *  (www.collegeboard.com).
 *
 *  @author APCS Development Committee
 *  @author Alyce Brady
 *  @version 1 July 2002
 **/

public class HunterF1sh extends GoalS33ker
{
    
    private Locatable target;

  // constructors

    /** Constructs a hunter fish at the specified location in a
     *  given environment.   This darter is colored yellow.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public HunterF1sh(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.yellow);
	nextTarget();
    }

    /** Constructs a hunter fish at the specified location and direction in a
     *  given environment.   This darter is colored yellow.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public HunterF1sh(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.RED);
	nextTarget();
    }
        
    /** Constructs a hunter fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public HunterF1sh(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
	nextTarget();
    }
    
    
    public HunterF1sh(Environment env, Location loc, Direction dir, Color col,
	    Locatable target)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col, target);
    }

    /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
            move();
	}
    }
    
    
  // redefined methods

    /** Creates a new hunter fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        HunterF1sh child = new HunterF1sh(environment(), loc,
                                          environment().randomDirection(),
                                          color());
        Debug.println("  New HunterF1sh created: " + child.toString());
    }

    /** Moves this fish in its environment.
     *  An hunter fish darts forward (as specified in
     *  <code>nextLocation</code>) if possible, or reverses direction
     *  (without moving) if it cannot move forward.
     **/
    protected void move()
    {
        // Find a location to move to.
        Debug.print("Fish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) && environment().isEmpty(nextLoc))
        {
            // Move to new location.
            Location oldLoc = location();
            changeLocation(nextLoc);

            // Update direction in case fish had to turn to move.
            Direction newDir = environment().getDirection(oldLoc, nextLoc);
            changeDirection(newDir);
            Debug.println("  Moves to " + location() + direction());
        }
        else
            Debug.println("  Does not move.");
    }
    
    public Location goal()
    {
	if(target == null)
	    setTarget(nextTarget());
	if(target == null)
	    return null;
	return target.location();
    }
    
    public void setTarget(Locatable target)
    {
	this.target = target;
    }
    
    protected Locatable nextTarget()
    {
	Locatable[] stuff = environment().allObjects();
	for(int i = 0; i < stuff.length - 1; i++)
	{
	    Locatable selected = stuff[i];
	    if(selected instanceof GoalSeekerF1sh)
		return selected;
	}
	return this;
    }

}
