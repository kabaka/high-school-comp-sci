import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class SharkFish extends Fish
{

  // constructors

    /** Constructs a shark fish at the specified location in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public SharkFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.gray);
    }

    /** Constructs a shark fish at the specified location and direction in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public SharkFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.gray);
    }
        
    /** Constructs a shark fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public SharkFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
    }

     /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
	    fireLazers();
            move();
	}
    }
    
    private void fireLazers()
    {
	Location loc = location();
	Location target; 
	if(direction().equals(Direction.NORTH))
	    for(int i = loc.row() - 1; i >= 0; i--)
	    {
		target = new Location(i, loc.col());
		if(!environment().isEmpty(target))
		    ((Fish)environment().objectAt(target)).die();
		new TrailerFish(environment(), target, environment().getDirection(location(), target));
	    }	
	else if(direction().equals(Direction.SOUTH))
	    for(int i = loc.row() + 1; i < environment().numRows(); i++)
	    {
		target = new Location(i, loc.col());
		if(!environment().isEmpty(target))
		    ((Fish)environment().objectAt(target)).die();
		new TrailerFish(environment(), target, environment().getDirection(location(), target));
	    }
	else if(direction().equals(Direction.EAST))
	    for(int i = loc.col() + 1; i < environment().numCols(); i++)
	    {
		target = new Location(loc.row(), i);
		if(!environment().isEmpty(target))
		    ((Fish)environment().objectAt(target)).die();
		new TrailerFish(environment(), target, environment().getDirection(location(), target));
	    }
	else
	    for(int i = loc.col() - 1; i >= 0; i--)
	    {
		target = new Location(loc.row(), i);
		if(!environment().isEmpty(target))
		    ((Fish)environment().objectAt(target)).die();
		new TrailerFish(environment(), target, environment().getDirection(location(), target));
	    }
    }

  // redefined methods

    /** Creates a new shark fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        SharkFish child = new SharkFish(environment(), loc,
                                          environment().randomDirection(),
                                          color());
        Debug.println("  New SharkFish created: " + child.toString());
    }

    protected void move()
    {
        // Find a location to move to.
        Debug.print("SharkFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) )
        {
	    changeDirection(environment().getDirection(location(), nextLoc));
            changeLocation(nextLoc);
            Debug.println("  Moves to " + location());
        }
        else
        {
            Debug.println("  No move ");
        }
    }
    
    
    

    /*protected Location nextLocation()
    {
        // Get list of neighboring empty locations.
        ArrayList emptyNbrs = environment().neighborsOf(location());

        // Remove the location behind, since fish do not move backwards.
        Direction oppositeDir = direction().reverse();
        Location locationBehind = environment().getNeighbor(location(),
                                                            oppositeDir);
        emptyNbrs.remove(locationBehind);
        Debug.print("Possible new locations are: " + emptyNbrs.toString());

        // If there are no valid empty neighboring locations, then we're done.
        if ( emptyNbrs.size() == 0 )
            return location();

        // Return a randomly chosen neighboring empty location.
        Random randNumGen = RandNumGenerator.getInstance();
        int randNum = randNumGen.nextInt(emptyNbrs.size());
	    return (Location) emptyNbrs.get(randNum);
    }*/

}
