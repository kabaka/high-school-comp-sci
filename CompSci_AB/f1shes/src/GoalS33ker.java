import java.awt.Color;
public abstract class GoalS33ker extends Fish
{
    
    public GoalS33ker(Environment env, Location loc)
    {
        super(env, loc, env.randomDirection(), Color.yellow);
	//nextTarget();
    }

    public GoalS33ker(Environment env, Location loc, Direction dir)
    {
        super(env, loc, dir, Color.RED);
	//nextTarget();
    }
        
    public GoalS33ker(Environment env, Location loc, Direction dir, Color col)
    {
        super(env, loc, dir, col);
	//nextTarget();
    }
    
    public GoalS33ker(Environment env, Location loc, Direction dir, Color col,
	    Locatable target)
    {
        super(env, loc, dir, col);
	//setTarget(target);
    }
    
    
    
    
    public GoalS33ker(Environment env, Location loc, int team)
    {
        super(env, loc, env.randomDirection(), Color.yellow, team);
	//nextTarget();
    }

    public GoalS33ker(Environment env, Location loc, Direction dir, int team)
    {
        super(env, loc, dir, Color.RED, team);
	//nextTarget();
    }
        
    public GoalS33ker(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        super(env, loc, dir, col, team);
	//nextTarget();
    }
    
    public GoalS33ker(Environment env, Location loc, Direction dir, Color col,
	    Locatable target, int team)
    {
        super(env, loc, dir, col, team);
	//setTarget(target);
    }
    
    protected Location nextLocation()
    {
	return environment().getNeighbor(location(),
		environment().getDirection(location(),goal()));
    }
    
    abstract public Location goal();
    
    //abstract public void setTarget(Locatable target);
    
    //abstract protected Locatable nextTarget();
    

}