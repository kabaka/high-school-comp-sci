
import java.awt.Color;

public class TrailerFish extends Fish
{
    private int age;
    private int lifespan;

    public TrailerFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.red);
	age = 0;
	lifespan = 1;
    }

    public TrailerFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red);
	age = 0;
	lifespan = 1;
    }
    
    public TrailerFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
	age = 0;
	lifespan = 1;
    }
    
    public TrailerFish(Environment env, Location loc, int lifespan)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.red);
	age = 0;
	this.lifespan = lifespan;
    }

    public TrailerFish(Environment env, Location loc, Direction dir, int lifespan)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red);
	age = 0;
	this.lifespan = lifespan;
    }
    
    public TrailerFish(Environment env, Location loc, Direction dir, Color col, int lifespan)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
	age = 0;
	this.lifespan = lifespan;
    }
    
    
    public void act()
    {
	if(age >= lifespan)
	    die();
	age++;
	return;
    }
}
