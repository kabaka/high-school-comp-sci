import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class CommanderFishT2 extends CommanderFish
{
    
    private int minimumMineLayers = 10;
    private boolean moreMineLayers = false;
    private ArrayList mineLayersNeedingTargets = new ArrayList();
    private Location enemyLoc;
    private int rank = 3;

  // constructors

    /** Constructs a shark fish at the specified location in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public CommanderFishT2(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.black, 1);
    }

    /** Constructs a shark fish at the specified location and direction in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public CommanderFishT2(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.black, 1);
    }
        
    /** Constructs a shark fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public CommanderFishT2(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.black, 1);
    }
    
    
    public CommanderFishT2(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.black, team);
    }

}