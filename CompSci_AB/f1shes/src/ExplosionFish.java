import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class ExplosionFish extends Fish
{
    
    int radius = 2;	// size of the explosion

  // constructors

    /** Constructs a shark fish at the specified location in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public ExplosionFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.red);
    }

    /** Constructs a shark fish at the specified location and direction in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public ExplosionFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red);
    }
        
    /** Constructs a shark fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public ExplosionFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
    }
    
    
    
    public ExplosionFish(Environment env, Location loc, Direction dir, Color col,
	    int radius)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
	this.radius = radius;
    }

     /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv()) 
	{
	    if(radius > 0)
		beginExplosion();
	    else
		die();
	}
    }
    
    public void beginExplosion()
    {
	ArrayList neighbors = environment().neighborsOf(location());
	
	for(int i = 0; i < neighbors.size(); i++)
	{
	    Location current = (Location)neighbors.get(i);
	    if(!(environment().isEmpty(current)))
		environment().remove(environment().objectAt(current));
	    generateChild(current);
	}
	die();
    }
    
    public void propagateRecursive()
    {
	ArrayList neighbors = environment().neighborsOf(location());
	
	for(int i = 0; i < neighbors.size(); i++)
	{
	    Location current = (Location)neighbors.get(i);
	    if(!environment().isEmpty(current))
		propagateRecursive(current);
	}
    }
    
    private void propagateRecursive(Location loc)
    {
	if(!(environment().isEmpty(loc)))
	    environment().remove(environment().objectAt(loc));
	generateChild(loc);
	((ExplosionFish)environment().objectAt(loc)).beginExplosion();
	((ExplosionFish)environment().objectAt(loc)).propagateRecursive();
    }

  // redefined methods

    /** Creates a new shark fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        ExplosionFish child = new ExplosionFish(environment(), loc,
                                          environment().randomDirection(),
                                          color(), radius - 1);
        Debug.println("  New ExplosionFish created: " + child.toString());
    }

    protected void move()
    {
        // Find a location to move to.
        Debug.print("ExplosionFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) )
        {
	    changeDirection(environment().getDirection(location(), nextLoc));
            changeLocation(nextLoc);
            Debug.println("  Moves to " + location());
        }
        else
        {
            Debug.println("  No move ");
        }
    }
    
    
    

    /*protected Location nextLocation()
    {
        // Get list of neighboring empty locations.
        ArrayList emptyNbrs = environment().neighborsOf(location());

        // Remove the location behind, since fish do not move backwards.
        Direction oppositeDir = direction().reverse();
        Location locationBehind = environment().getNeighbor(location(),
                                                            oppositeDir);
        emptyNbrs.remove(locationBehind);
        Debug.print("Possible new locations are: " + emptyNbrs.toString());

        // If there are no valid empty neighboring locations, then we're done.
        if ( emptyNbrs.size() == 0 )
            return location();

        // Return a randomly chosen neighboring empty location.
        Random randNumGen = RandNumGenerator.getInstance();
        int randNum = randNumGen.nextInt(emptyNbrs.size());
	    return (Location) emptyNbrs.get(randNum);
    }*/

}
