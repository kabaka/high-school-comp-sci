import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class WallFish extends Fish
{
    
    private int rank = 2;
    private Location start;
    private Location end;
    private boolean startReached = false;

  // constructors

    /** Constructs a shark fish at the specified location in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public WallFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.white, 0);
    }

    /** Constructs a shark fish at the specified location and direction in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public WallFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.white, 0);
    }
        
    /** Constructs a shark fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public WallFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.white, 0);
    }
    
    
    public WallFish(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.white, team);
    }
    
    
    public WallFish(Environment env, Location loc, Direction dir, Color col,
	    int team, Location start, Location end)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.white, team);
	this.start = start;
	this.end = end;
    }

     /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
	    if(!(location().equals(end)))
		move();
	    else
	    {
		die();
		new Block(environment(), location());
	    }
	    
	    if(location().equals(start))
		startReached = true;
		
	}
    }
    
  // redefined methods

    /** Creates a new shark fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        WallFish child = new WallFish(environment(), loc,
                                          environment().randomDirection(),
                                          color(), getTeam());
        Debug.println("  New WallFish created: " + child.toString());
    }

    protected void move()
    {
	Location oldLoc = location();
	
        // Find a location to move to.
        Debug.print("WallFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location())  && environment().isEmpty(nextLoc))
        {
	    changeDirection(environment().getDirection(location(), nextLoc));
            changeLocation(nextLoc);
	    if(startReached)
		new Block(environment(), oldLoc, getTeam());
            Debug.println("  Moves to " + location());
        }
        else
        {
            Debug.println("  No move ");
        }
    }
    
    protected Location nextLocation()
    {
	
        // Get list of neighboring empty locations.
        ArrayList emptyNbrs = environment().neighborsOf(location());

        Debug.print("Possible new locations are: " + emptyNbrs.toString());

        // If there are no valid empty neighboring locations, then we're done.
        if ( emptyNbrs.size() == 0 )
            return location();
	
	Location loc;

	if(!startReached)
	{
	    loc = environment().getNeighbor(location(),
		    environment().getDirection(location(), start));
	    if(environment().isEmpty(loc))
		return loc;
	    
	    Random randNumGen = RandNumGenerator.getInstance();
	    int randNum = randNumGen.nextInt(emptyNbrs.size());
	    return (Location) emptyNbrs.get(randNum);
	}
	
	loc = environment().getNeighbor(location(),
		    environment().getDirection(location(), end));
	if(environment().isEmpty(loc))
	    return loc;
	return location();
    }

}
