import java.awt.Color;
import java.util.Random;

public class PopulatorFish extends Fish
{

  // constructors

    /** Constructs a shark fish at the specified location in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public PopulatorFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.gray);
    }

    /** Constructs a shark fish at the specified location and direction in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public PopulatorFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.gray);
    }
        
    /** Constructs a shark fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public PopulatorFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
    }

     /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
	    populate();
            die();
	}
    }
    
    private void populate()
    {
	Random random = new Random();
	for(int i = 0; i < environment().numRows(); i++)
	    for(int j = 0; j < environment().numCols(); j++)
	    {
		Location loc = new Location(i, j);
		if(random.nextBoolean() && environment().isEmpty(loc))
		{
		    Fish fish = new Fish(environment(), loc, super.randomDirection(),
			    color());
		}
		    
	    }
    }

  // redefined methods

    /** Creates a new shark fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        SharkFish child = new SharkFish(environment(), loc,
                                          environment().randomDirection(),
                                          color());
        Debug.println("  New PopulatorFish created: " + child.toString());
    }

    protected void move()
    {
        // Find a location to move to.
        Debug.print("SharkFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) )
        {
            changeLocation(nextLoc);
            Debug.println("  Moves to " + location());
        }
        else
        {
            // Otherwise, reverse direction.
            changeDirection(direction().reverse());
            Debug.println("  Now facing " + direction());
        }
    }

/*    protected Location nextLocation()
    {
        Environment env = environment();
        Location oneInFront = env.getNeighbor(location(), direction());
        Debug.println("  Location in front is empty? " + 
                        env.isEmpty(oneInFront));
        if ( env.isEmpty(oneInFront) )
        {
                return oneInFront;
        }

        // Only get here if there isn't a valid location to move to.
        Debug.println("  Shark is blocked.");
        return location();
    }*/

}
