import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class SwanFish extends Fish
{
    
    private int age;
    private SwanFish child;
    private SwanFish parent;
    private boolean isLeader = true;
    private Color CHILDCOL = Color.WHITE;
    private Color LEADERCOL = Color.RED;

  // constructors

    public SwanFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.gray);
    }
    
    public SwanFish(Environment env, Location loc, boolean isChild,
	    SwanFish parent)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.gray);
	isLeader = isChild;
	this.parent = parent;
    }

    public SwanFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.gray);
    }
        
    public SwanFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
    }
    public SwanFish(Environment env, Location loc, Direction dir, Color col,
	    boolean isChild, SwanFish parent)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
	isLeader = isChild;
	this.parent = parent;
	if(isLeader)
	    setColor(LEADERCOL);
	else
	    setColor(CHILDCOL);
    }

     /** Acts for one step in the simulation.
      *
      *  The Swan should move itself (one step in any random location that is 
      *    empty)
      *  Increment its age
      *  If there are any swans following it, it should call the move method for 
      *   the next swan, passing the location of the current swan. 
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv()) 
	{
	    if(isLeader)
	    {
		setColor(Color.RED);
		
		move();

		Random rand = new Random();
		if(rand.nextInt(10) > 8)
		    breed();
	    }
	    else
		setColor(Color.WHITE);
	    
	    if(child != null && child.isInEnv())
		child.move(location());
	    
	    age++;
	    if(age == 3 && !isLeader)
	    {
		isLeader = true;
		if(parent != null)
		    parent.abandonChild();
	    }
	}
    }
    
    public void abandonChild()
    {
	child = null;
    }
    
    public void breed()
    {
	if(child != null && child.isInEnv())
	    child.breed();
	else
	    generateChild(environment().getNeighbor(location(),
		    direction().reverse()));
    }
    
    

  // redefined methods

    /** Creates a new swan fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
	if(!(environment().isEmpty(loc))) return;
        SwanFish child = new SwanFish(environment(), loc, direction(), color(),
		false, this);
	this.child = child;
        Debug.println("  New SwanFish created: " + child.toString());
    }

    public void move()
    {
        // Find a location to move to.
        Debug.print("SwanFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) )
        {
	    changeDirection(environment().getDirection(location(), nextLoc));
            changeLocation(nextLoc);
            Debug.println("  Moves to " + location());
        }
        else
        {
            Debug.println("  No move ");
        }
    }
    
    public void move(Location parentLocation)
    {
	// Don't listen to another fish if this fish is a leader
	if(isLeader)
	    return;
        // Find a location to move to.
        Debug.print("SwanFish " + toString() + " attempting to move.  ");
        Location nextLoc = environment().getNeighbor(location(),
		environment().getDirection(location(), parentLocation));

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) && environment().isEmpty(nextLoc))
        {
	    changeDirection(environment().getDirection(location(), nextLoc));
            changeLocation(nextLoc);
            Debug.println("  Moves to " + location());
        }
        else
        {
            Debug.println("  No move ");
        }
    }
    
    public int getAge()
    {
	return age;
    }
}
