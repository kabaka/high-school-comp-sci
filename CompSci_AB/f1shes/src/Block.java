
import java.awt.Color;

public class Block extends Fish
{

    public Block(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.black);
    }

    public Block(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.black);
    }
    
    public Block(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col);
    }
    public Block(Environment env, Location loc, int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.black, team);
    }

    public Block(Environment env, Location loc, Direction dir, int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.black, team);
    }
    
    public Block(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col, team);
    }
    
    
    public void act()
    {
	return;
    }
}
