// AP(r) Computer Science Marine Biology Simulation:
// The DarterFish class is copyright(c) 2002 College Entrance
// Examination Board (www.collegeboard.com).
//
// This class is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation.
//
// This class is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

/**
 *  AP&reg; Computer Science Marine Biology Simulation:<br>
 *  The <code>DarterFish</code> class represents a fish in the Marine
 *  Biology Simulation that darts forward two spaces if it can, moves
 *  forward one space if it  can't move two, and reverses direction
 *  (without moving) if it cannot  move forward.  It can only "see" an
 *  empty location two cells away if the cell in between is empty also.
 *  In other words, if both the cell in front of the darter and the cell
 *  in front of that cell are empty, the darter fish will move forward
 *  two spaces.  If only the cell in front of the darter is empty, it
 *  will move there.  If neither forward cell is empty, the fish will turn
 *  around, changing its direction but not its location.
 *
 *  <p>
 *  <code>DarterFish</code> objects inherit instance variables and much
 *  of their behavior from the <code>Fish</code> class.
 *
 *  <p>
 *  The <code>DarterFish</code> class is
 *  copyright&copy; 2002 College Entrance Examination Board
 *  (www.collegeboard.com).
 *
 *  @author APCS Development Committee
 *  @author Alyce Brady
 *  @version 1 July 2002
 **/

public class HQFish extends GoalS33ker
{

    private Location target;
    private int noMove = 0;
    private boolean needNewTarget = false;
    private int rank = 10;
    private int walls = 0;
    private int ageAfterWalls = 0;
    private int age = 0;
    
    private Color[] blink = { Color.white, Color.black };
    
  // constructors

    /** Constructs a darter fish at the specified location in a
     *  given environment.   This darter is colored yellow.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public HQFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.red);
	target = nextTarget();
    }

    /** Constructs a darter fish at the specified location and direction in a
     *  given environment.   This darter is colored yellow.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public HQFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red);
	target = nextTarget();
    }
        
    /** Constructs a darter fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public HQFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red);
	target = nextTarget();
    }
    
    public HQFish(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red, team);
	target = nextTarget();
    }
    
    
    public HQFish(Environment env, Location loc, Direction dir, Color col,
	    Locatable target)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red, target);
    }
    
    public HQFish(Environment env, Location loc, Direction dir, Color col,
	    Locatable target, int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.red, target, team);
    }

    /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
	    if(!arrived())
	    {
		move();
		
		if(target == null)
		    target = nextTarget();

		if(target.row() > environment().numRows() - 3 ||
			target.col() > environment().numCols() - 3 ||
			target.col() < 3 || target.row() < 3)
		    target = nextTarget();
	    }
	    
	    blink();
	    wallCheck();
	    
	    if(ageAfterWalls >= 10)
	    {
		checkEnvStatus();
		internalShield();
	    }
	    
	    age++;
	}
    }
    
    private void blink()
    {
	if(age % 15 == 0 && getTeam() >= 0 && getTeam() < blink.length)
	    setColor(blink[getTeam()]);
	else
	    setColor(Color.red);
    }
    
    private void internalShield()
    {
	ArrayList nbrs = environment().neighborsOf(location());
	nbrs.add(new Location(((Location)nbrs.get(0)).row(),
		((Location)nbrs.get(0)).col() - 1));
	nbrs.add(new Location(((Location)nbrs.get(0)).row(),
		((Location)nbrs.get(0)).col() + 1));
	nbrs.add(new Location(((Location)nbrs.get(5)).row(),
		((Location)nbrs.get(5)).col() - 1));
	nbrs.add(new Location(((Location)nbrs.get(6)).row(),
		((Location)nbrs.get(6)).col() + 1));
	
	for(int i = 0; i < nbrs.size(); i++)
	{
	    if(!(environment().isEmpty((Location)nbrs.get(i))))
	    {
		Locatable current = environment().objectAt((Location)nbrs.get(i));
		if(!(current instanceof WallFish) && 
		    !(current instanceof CommanderFish) &&
		    !(current instanceof CommanderFishT2))
		    environment().remove(current);
	    }
	}
    }
    
    private void constructWall(int wall)
    {
	switch(wall)
	{
	    case 1:
		if(environment().isValid(new Location(location().row() - 2,
			location().col() - 2)) && walls < 1)
		{
		    int row;
		    for(row = location().row() - 1;
			environment().isValid(
			    new Location(row, location().col() - 2))
			    && row <= location().row() + 2; row++);
		    row--;
		    generateWall(new Location(location().row() - 2,
			    location().col() - 2), new Location(row,
			    location().col() - 2));
		}
		break;
		
	    case 2:
		if(environment().isValid(new Location(location().row() - 2,
		location().col() + 2)) && walls < 2)
		{
		    int row;
		    for(row = location().row() - 1;
			environment().isValid(
			    new Location(row, location().col() - 1))
			    && row <= location().row() + 2; row++);
		    row--;
		    generateWall(new Location(location().row() - 2,
			    location().col() + 2), new Location(row,
			    location().col() + 2));
		}
		break;
		
	    case 3:
		if(environment().isValid(new Location(location().row() - 2,
		location().col() - 1)) && walls < 4)
		{
		    int col;
		    for(col = location().col() - 1;
			environment().isValid(
			    new Location(location().row() - 2, col))
			    && col <= location().col() + 1; col++);
		    col--;
		    generateWall(new Location(location().row() - 2,
			    location().col() - 1), new Location(location().row()
			    - 2, col));
		}
		break;
		
	    case 4:
		if(environment().isValid(new Location(location().row() + 2,
		location().col() - 1)) && walls < 5)
		{
		    int col;
		    for(col = location().col() - 1;
			environment().isValid(
			    new Location(location().row() + 2, col))
			    && col <= location().col() + 1; col++);
		    col--;
		    generateWall(new Location(location().row() + 2,
			    location().col() - 1), new Location(location().row()
			    + 2, col));
		}
		break;
		
	}
    }
    
    private ArrayList<Location> getWallHoles()
    {
	ArrayList<Location> holes = new ArrayList();
	
	for(int row = -2; row <= 2; row++)
	{
	    for(int col = -2; col <= 2; col++)
	    {
		if(!(environment().isEmpty(new Location(location().row() + row,
			    location().col() + col))))
		{
		    Locatable current = environment().
			    objectAt(new Location(location().row() + row,
			    location().col() + col));
		    if(row == -2 || row == 2 || col == -2 || col == 2)
		    {
			if(!(current instanceof WallFish) ||
			    !(current instanceof Block) ||
			    !(current instanceof CommanderFish) ||
			    !(current instanceof CommanderFishT2))
			    holes.add(new Location(location().row() + row,
			    location().col() + col));
		    }
		}
		else if(row == -2 || row == 2 || col == -2 || col == 2)
		    holes.add(new Location(location().row() + row,
			    location().col() + col));
	    }
	}
	
	return holes;
    }
    
    private void wallCheck()
    {
	if(arrived() && walls < 4)
	{
	    for(int i = 1; i < 5; i++)
		constructWall(i);

	    if(arrived() && walls >= 2)
	    {
		if(arrived() && ageAfterWalls < 6)
		    clearSurroundings();
	    }	
	}
	
	if(arrived())
	   ageAfterWalls++;
	
	if(arrived() && ageAfterWalls > 10)
	{
	    ArrayList<Location> holes = getWallHoles();
	    for(Location hole : holes)
	    {
		//if(!(environment().isEmpty(hole)))
		 //   environment().remove(environment().objectAt(hole));
		if(environment().isEmpty(hole))
		    new Block(environment(), hole);
	    }
	}
    }
    
    private void clearSurroundings()
    {
	ArrayList nbrs = environment().neighborsOf(location());
	nbrs.add(new Location(((Location)nbrs.get(0)).row(),
		((Location)nbrs.get(0)).col() - 1));
	nbrs.add(new Location(((Location)nbrs.get(0)).row(),
		((Location)nbrs.get(0)).col() + 1));
	nbrs.add(new Location(((Location)nbrs.get(5)).row(),
		((Location)nbrs.get(5)).col() - 1));
	nbrs.add(new Location(((Location)nbrs.get(6)).row(),
		((Location)nbrs.get(6)).col() + 1));
	
	for(int i = 0; i < nbrs.size(); i++)
	{
	    if(!(environment().isEmpty((Location)nbrs.get(i))))
	    {
		Locatable current = environment().objectAt((Location)nbrs.get(i));
		if(!(current instanceof WallFish))
		    environment().remove(current);
	    }
	}
	
    }
    
    
    private void checkEnvStatus()
    {
	int count = 0;
	boolean commanderPresent = false;
	
	Locatable[] objects = environment().allObjects();
	int highestRank = 0;
	
	for(int i  = 0; i < objects.length; i++)
	{
	    Locatable current = (Locatable)objects[i];
	    
	    if(current instanceof CommanderFish &&
		((Fish)current).getTeam() == getTeam())
		commanderPresent = true;
	    /*else if(getTeam() == 1 && current instanceof CommanderFishT2)
		commanderPresent = true;*/
	}
	
	if(!commanderPresent)
	{
	    sendCommanderFish();
	}
    }
    
    private void sendCommanderFish()
    {
	/*if(!(environment().isEmpty(new Location(0,0))))
	    environment().remove(
		    environment().objectAt(new Location(0,0)));
	new CommanderFish(environment(), new Location(0,0), direction(),
			color(), getTeam());*/
	ArrayList empty = emptyNeighbors();
	    if(empty.size() > 0)
	    {
		if(getTeam() == 0)
		    new CommanderFish(environment(), (Location)empty.get(0),
			Direction.NORTH, color(), getTeam());
		else
		    new CommanderFishT2(environment(), (Location)empty.get(0),
			Direction.NORTH, color(), getTeam());
	    }
    }
    
    private boolean arrived()
    {
	return location().equals(target);
	//return true;
    }
    
    private void generateWall(Location start, Location end)
    {
	Location spawn = environment().getNeighbor(location(),
		environment().getDirection(location(), start));
	if(environment().isEmpty(spawn))
	{
	    new WallFish(environment(), spawn, Direction.SOUTH, Color.gray,
		    getTeam(), start, end);
	    walls++;
	}
    }
    
    public boolean needTarget()
    {
	return needNewTarget;
    }
    
    
  // redefined methods

    /** Creates a new internet predator fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        HQFish child = new HQFish(environment(), loc,
                                          environment().randomDirection(),
                                          color(), getTeam());
        Debug.println("  New HQFish created: " + child.toString());
	
    }

    /** Moves this fish in its environment.
     *  An internet predator fish darts forward (as specified in
     *  <code>nextLocation</code>) if possible, or reverses direction
     *  (without moving) if it cannot move forward.
     **/
    protected void move()
    {
        // Find a location to move to.
        Debug.print("HQFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) && environment().isEmpty(nextLoc))
        {
            // Move to new location.
            Location oldLoc = location();
            changeLocation(nextLoc);

            // Update direction in case fish had to turn to move.
            Direction newDir = environment().getDirection(oldLoc, nextLoc);
            changeDirection(newDir);
            Debug.println("  Moves to " + location() + direction());
        }
        else
	{
            Debug.println("  Does not move.");
	    noMove++;
	    if(noMove > 2)
	    {
		Debug.println("  Cannot move after " + noMove + " steps." +
			" Moving randomly if possible.");
		
		ArrayList emptyNbrs = emptyNeighbors();
		
		if(emptyNbrs.size() == 0)
		{
		    noMove++;
		    return;
		}
		
		Random randNumGen = RandNumGenerator.getInstance();
		int randNum = randNumGen.nextInt(emptyNbrs.size());
		 changeLocation((Location) emptyNbrs.get(randNum));
		
		noMove = 0;
		
	    }
	}
    }
    
    public Location goal()
    {
	if(target == null)
	    setTarget(nextTarget());
	if(target == null)
	    return null;
	return target;
    }
    
    public void setTarget(Location target)
    {
	if(target == null)
	{
	    setRandomTarget();
	}
	else
	{
	    this.target = target;
	    needNewTarget = false;
	}
    }
    
    public void setRandomTarget()
    {
	target = nextTarget();
	needNewTarget = false;
    }
    
    protected Location nextTarget()
    {
	Random rand = new Random();
	//return new Location(rand.nextInt(environment().numRows() - 6) + 3,
	//	rand.nextInt(environment().numCols() - 6) + 3);
	return new Location(rand.nextInt(environment().numRows()),
		rand.nextInt(environment().numCols()));
    }
    
    public void die()
    {
	environment().remove(this);
	//if(!(environment().isEmpty(target)))
	//   ((Fish)environment().objectAt(target)).die();
    }

}
