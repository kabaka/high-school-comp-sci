import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class CommanderFish extends Fish
{
    
    private int minimumMineLayers = 5;
    private boolean moreMineLayers = false;
    private ArrayList mineLayersNeedingTargets = new ArrayList();
    private Location enemyLoc;
    private int rank = 3;
    private int age = 0;
    

  // constructors

    /** Constructs a shark fish at the specified location in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     **/
    public CommanderFish(Environment env, Location loc)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, env.randomDirection(), Color.white, 0);
    }

    /** Constructs a shark fish at the specified location and direction in a
     *  given environment.   This shark is colored gray.
     *  (Precondition: parameters are non-null; <code>loc</code>
     *  is valid for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     **/
    public CommanderFish(Environment env, Location loc, Direction dir)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, Color.white, 0);
    }
        
    /** Constructs a shark fish of the specified color at the specified
     *  location and direction.
     *  (Precondition: parameters are non-null; <code>loc</code> is valid
     *  for <code>env</code>.)
     *  @param env    environment in which fish will live
     *  @param loc    location of the new fish in <code>env</code>
     *  @param dir    direction the new fish is facing
     *  @param col    color of the new fish
     **/
    public CommanderFish(Environment env, Location loc, Direction dir, Color col)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col, 0);
    }
    
    
    public CommanderFish(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        // Construct and initialize the attributes inherited from Fish.
        super(env, loc, dir, col, team);
    }

     /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
	    /*if(age == 0 && emptyNeighbors().size() > 0)
		new WallFish(environment(), (Location)emptyNeighbors().get(0),
			direction(), color(), getTeam(), new Location(0,0), 
			new Location(environment().numCols() - 1,
			environment().numRows() - 1));*/
	    
	    if(getTeam() == 0)
		setColor(Color.white);
	    else
		setColor(Color.black);
	    
            move();
	    
	    //if(!checkMineLayers())
		//addMineLayer();
	    
	    checkEnvStatus();
	    
	    if(moreMineLayers)
	    {
		addMineLayer();
		moreMineLayers = false;
	    }
	    
	    age++;
		
	}
    }
    
    private void addMineLayer()
    {
	ArrayList emptyNbrs = emptyNeighbors();
	
	if(emptyNbrs.size() > 0)
	new MineLayerFish(environment(), (Location)emptyNbrs.get(0), direction(),
		color().green, getTeam());
	
    }
    
    public int getMinMineLayers()
    {
	return minimumMineLayers;
    }
    
    private void checkEnvStatus()
    {
	int count = 0;
	boolean HQPresent = false;
	
	Locatable[] objects = environment().allObjects();
	int highestRank = 0;
	
	for(int i  = 0; i < objects.length; i++)
	{
	    Fish current = (Fish)objects[i];
	    
	    /*if(objects[i] instanceof CommanderFish && current.getTeam()
		== getTeam())
		count -= ((CommanderFish)objects[i]).getMinMineLayers();
	   */
	    if(objects[i] instanceof MineLayerFish &&
		((MineLayerFish)objects[i]).getTeam() == getTeam())
	    {
		count++;
		if(((MineLayerFish)objects[i]).needTarget())
		{
		    ((MineLayerFish)objects[i]).setTarget(enemyLocation());
		}
	    }
	    
	    if(objects[i] instanceof HQFish &&
		((HQFish)objects[i]).getTeam() == getTeam())
	    {
		HQPresent = true;
	    }
	    
	    if(current.getTeam() != getTeam() && current.getRank() > highestRank)
	    {
		highestRank = current.getRank();
		enemyLoc = current.location();
	    }
	}
	
	moreMineLayers = count < minimumMineLayers;
	
	if(!HQPresent)
	{
	    ArrayList empty = emptyNeighbors();
	    if(empty.size() > 0)
		new HQFish(environment(), (Location)empty.get(0), direction(),
			color(), getTeam());
	}
    }
    
    private Location randomLoc()
    {
		Random rand = new Random();
	return new Location(rand.nextInt(environment().numRows()),
		rand.nextInt(environment().numCols()));
    }
    
    private Location enemyLocation()
    {
	if(enemyLoc == null)
	    return null;
	return enemyLoc;
    }
    
    private boolean hasHQNeighbor()
    {
	ArrayList neighbors = environment().neighborsOf(location());
	for(Object ob : neighbors)
	    if(ob instanceof HQFish)
		return true;
	return false;
    }
    
  // redefined methods

    /** Creates a new shark fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        CommanderFish child = new CommanderFish(environment(), loc,
                                          environment().randomDirection(),
                                          color(), getTeam());
        Debug.println("  New CommanderFish created: " + child.toString());
    }

    protected void move()
    {
        // Find a location to move to.
        Debug.print("CommanderFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) )
        {
	    changeDirection(environment().getDirection(location(), nextLoc));
            changeLocation(nextLoc);
            Debug.println("  Moves to " + location());
        }
        else
        {
            Debug.println("  No move ");
        }
    }
    
    protected Location nextLocation()
    {
        // Get list of neighboring empty locations.
        //ArrayList emptyNbrs = emptyNeighbors();
	ArrayList nbrs = environment().neighborsOf(location());
	
	for(int i = 0; i < nbrs.size(); i++)
	{
	    Locatable current = environment().objectAt((Location)nbrs.get(i));
	    /*if(current instanceof Fish)
	    {*/
		if(current instanceof Block &&
		    ((Block)current).getTeam() == getTeam())
		{
		    environment().remove(current);
		    return (Location)nbrs.get(i);
		}
		else if(current instanceof Fish)
		{
		    nbrs.remove(i);
		    i--;
		}
	   // }
	}
	if(nbrs.size() == 0)
	    return location();
	// Return a randomly chosen neighboring empty location.
        Random randNumGen = RandNumGenerator.getInstance();
	
        int randNum = randNumGen.nextInt(nbrs.size());
	return (Location) nbrs.get(randNum);
    }
}
