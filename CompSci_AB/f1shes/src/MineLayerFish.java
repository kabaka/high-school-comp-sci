import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;


public class MineLayerFish extends GoalS33ker
{

    private Location target;
    private int noMove = 0;
    private boolean needNewTarget = false;
    private int rank = 2;
    private int movesWithoutTarget = 0;
    
  // constructors
    
    public MineLayerFish(Environment env, Location loc)
    {
        super(env, loc, env.randomDirection(), Color.green);
	target = nextTarget();
    }

    public MineLayerFish(Environment env, Location loc, Direction dir)
    {
        super(env, loc, dir, Color.green);
	target = nextTarget();
    }
    
    public MineLayerFish(Environment env, Location loc, Direction dir, Color col)
    {
        super(env, loc, dir, Color.green);
	target = nextTarget();
    }
    
    public MineLayerFish(Environment env, Location loc, Direction dir, Color col,
	    int team)
    {
        super(env, loc, dir, Color.green, team);
	target = nextTarget();
    }
    
    
    public MineLayerFish(Environment env, Location loc, Direction dir, Color col,
	    Locatable target)
    {
        super(env, loc, dir, Color.green, target);
    }
    
    public MineLayerFish(Environment env, Location loc, Direction dir, Color col,
	    Locatable target, int team)
    {
        super(env, loc, dir, Color.green, target, team);
    }

    
    
    /** Acts for one step in the simulation.
     **/
    public void act()
    {
        // Make sure fish is alive and well in the environment -- fish
        // that have been removed from the environment shouldn't act.
        if ( isInEnv() ) 
	{
            move();
	    
	    if(target == null)
		needNewTarget = true;
	    
	    if(nextLocation().equals(target) || enemyNearby())
	    {
		setMine(target);
	    }
	    
	    if(needNewTarget)
		movesWithoutTarget++;
	    
	    if(movesWithoutTarget > 10)
	    {
		this.die();
		new ExplosionFish(environment(), location());
	    }
	}
    }
    
    private boolean enemyNearby()
    {
	ArrayList neighbors = environment().neighborsOf(location());
	
	for(int i = 0; i < neighbors.size(); i++)
	{
	    Location current = (Location)neighbors.get(i);
	    if(!(environment().isEmpty(current)))
		if(((Fish)environment().objectAt(current)).getTeam() != getTeam())
		    return true;
	}
	return false;
    }
    
    private void setMine(Location loc)
    {
	if(environment().isEmpty(loc))
	    new MineFish(environment(), loc, Direction.SOUTH, Color.gray, 4,
		    getTeam());
	else
	{
	    ArrayList emptyNbrs = emptyNeighbors();
	    if(emptyNbrs.size() > 0)
		new MineFish(environment(), (Location)emptyNbrs.get(0),
			Direction.SOUTH, Color.gray, 4, getTeam());
	}
	needNewTarget = true;
    }
    
    private void setMine()
    {
	ArrayList emptyNbrs = emptyNeighbors();
	    if(emptyNbrs.size() > 0)
		new MineFish(environment(), (Location)emptyNbrs.get(0),
			Direction.SOUTH, Color.gray, 4, getTeam());
	needNewTarget = true;
    }
    
    public boolean needTarget()
    {
	return needNewTarget;
    }
    
    

    /** Creates a fish.
     *  @param loc    location of the new fish
     **/
    protected void generateChild(Location loc)
    {
        // Create new fish, which adds itself to the environment.
        MineLayerFish child = new MineLayerFish(environment(), loc,
                                          environment().randomDirection(),
                                          color(), getTeam());
        Debug.println("  New MineLayerFish created: " + child.toString());
	
    }

    
    protected void move()
    {
        // Find a location to move to.
        Debug.print("MineLayerFish " + toString() + " attempting to move.  ");
        Location nextLoc = nextLocation();

        // If the next location is different, move there.
        if ( ! nextLoc.equals(location()) && environment().isEmpty(nextLoc))
        {
            // Move to new location.
            Location oldLoc = location();
            changeLocation(nextLoc);

            // Update direction in case fish had to turn to move.
            Direction newDir = environment().getDirection(oldLoc, nextLoc);
            changeDirection(newDir);
            Debug.println("  Moves to " + location() + direction());
        }
        else
	{
            Debug.println("  Does not move.");
	    noMove++;
	    if(noMove > 2)
	    {
		Debug.println("  Cannot move after " + noMove + " steps." +
			" Requesting new target.");
		
		needNewTarget = true;
		noMove = 0;
	    }
	}
    }
    
    public Location goal()
    {
	if(target == null)
	    setTarget(nextTarget());
	if(target == null)
	    return null;
	return target;
    }
    
    public void setTarget(Location target)
    {
	if(target == null)
	{
	    setRandomTarget();
	}
	else
	{
	    this.target = target;
	    needNewTarget = false;
	}
    }
    
    public void setRandomTarget()
    {
	target = nextTarget();
	needNewTarget = false;
    }
    
    protected Location nextTarget()
    {
	Random rand = new Random();
	return new Location(rand.nextInt(environment().numRows()),
		rand.nextInt(environment().numCols()));
    }
    
    public void die()
    {
	environment().remove(this);
    }

}
