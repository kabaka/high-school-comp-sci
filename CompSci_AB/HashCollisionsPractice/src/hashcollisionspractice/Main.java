/*
 * Main.java
 *
 * Created on January 30, 2007, 9:00 
AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package hashcollisionspractice;

 import com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl;
import java.util.Scanner;
/**
 *
 * @author Kyle Johnson
 */
public class Main
{
    static ListNode[] nodes;
    
    /** Creates a new instance of Main - don't do this!*/
    private Main(){}
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
	System.out.println("Hash Table Collision-Handling Practice" +
		"Application");
	
	int length = 26;
	
	nodes = new ListNode[length];
	for(int i  = length - 1; i >= 0; i--)
	    nodes[i] = new ListNode();
	
	Scanner hWrds = new Scanner("hashWords.txt");
	
	while(hWrds.hasNextLine())
	{
	    String temp = hWrds.nextLine();
	    add(temp);
	}


    }
    
    private static void add(String data)
    {
	
    }
    
    private static int getIndex(String data)
    {
	return Character.getNumericValue(data.toUpperCase().charAt(0)) -
		Character.getNumericValue('A');
    }
    
}
