/*
 * ListNode.java
 *
 * Created on January 30, 2007, 9:10 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package hashcollisionspractice;

/**
 *
 * @author Kyle Johnson
 */
public class ListNode<E>
{
    private E val;
    private ListNode next;
    /** Creates a new instance of ListNode */
    public ListNode()
    {
	setVal(null);
	setNext(null);
    }
    
    public ListNode(E nodeValue)
    {
	setVal(nodeValue);
	setNext(null);
    }
    
    public ListNode(E nodeValue, ListNode nextNode)
    {
	setVal(nodeValue);
	setNext(nextNode);
    }

	public E getVal()
	{
		return val;
	}

	public void setVal(E val)
	{
		this.val = val;
	}

	public ListNode getNext()
	{
		return next;
	}

	public void setNext(ListNode next)
	{
		this.next = next;
	}
    
    
}
