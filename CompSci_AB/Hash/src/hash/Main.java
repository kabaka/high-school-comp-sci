/*
 * Main.java
 *
 * Created on January 23, 2007, 8:54 AM
 *
 * The main class for the hash package.
 * This package is to illustrate the use of a hash table that stores state names
 * according to a hash generated with the two-letter abbreviation of the name.
 */

package hash;

 import java.util.Scanner;

/**
 *
 * @author Kyle Johnson
 */
public class Main
{
    
    /** Creates a new instance of Main 
     * Private -- don't instantiate this class!
     */
    private Main(){}
    
    public static void main(String[] args)
    {
	System.out.println("Hash Table Demonstration Application");
	System.out.println("  Type exit to quit.");
	LookupState states = new LookupState();
	
	//states.debug_printAll();
	
	Scanner kb = new Scanner(System.in);
	
	while(true)
	{
	    System.out.print("HTDA: Input State Abbreviation > ");
	    
	    String input = kb.nextLine().trim().toLowerCase();
	    if(input.equals("exit"))
		break;
	    
	    if(input.length() > 2)
		System.out.println("Abbreviations must be in standard" +
			" two-letter format");
	    
	    else
	    {
		try
		{
		    System.out.println(states.find(input.toUpperCase()));
		}
		catch (Exception e)
		{
		    System.out.println("* Input must be a pair of characters! *");
		}
		
	    }
	}
    }
}
