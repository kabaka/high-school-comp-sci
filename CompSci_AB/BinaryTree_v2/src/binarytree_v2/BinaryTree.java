/**
 * @(#)BinaryTree.java
 *
 * BinaryTree Class
 * Class for the data structure of a Binary Tree
 *
 * @author Kyle Johnson
 * @version 2.00 2007/2/26
 */
 package binarytree_v2;
 
public class BinaryTree<E extends Comparable>
{
	private btNode<E> rootNode = null;
	private int nodeCount;


	/**
	 * Method BinaryTree
	 *
	 *
	 */
	public BinaryTree()
	{
		rootNode = new btNode<E>();
		nodeCount = 0;
	}

	/**
	 * Method BinaryTree
	 *
	 *
	 */
	public BinaryTree(btNode newRoot)
	{
		rootNode = newRoot;
		nodeCount = 0;
	}

	/**
	 * Method retrieve
	 *
	 *
	 * @return value at specified ...?
	 *
	 */
	public E retrieve(E obj)
	{
		return rootNode.findNode(obj);
	}

	/**
	 * Method insert
	 *
	 * Insert a given value into the tree
	 */
	public void insert(E obj)
	{
		if(rootNode.insertNode(obj))
			nodeCount++;
	}

	/**
	 * Method printPreOrder
	 *
	 *
	 */
	public void printPreOrder()
	{
		rootNode.printPreOrder();
	}

	/**
	 * Method printPostOrder
	 *
	 *
	 */
	public void printPostOrder()
	{
		rootNode.printPostOrder();
	}

	/**
	 * Method printInorder
	 *
	 *
	 */
	public void printInorder()
	{
		rootNode.printInorder();
	}

	public boolean isLeaf(E value)
	{
		return rootNode.isLeaf(value);
	}

	public int leafCount()
	{
		return rootNode.leafCount();
	}

	public double sumNodes()
	{
		return rootNode.sumNodes();
	}

	public int nodeCount()
	{
		return nodeCount;
	}

	public E findMax()
	{
		return rootNode.findMax();
	}

	public E findMin()
	{
		return rootNode.findMin();
	}

	public boolean delete(E value)
	{
		btNode node = rootNode.delete(value);
		if(node == null)
			return false;
		if(value.equals(rootNode.getValue()))
			rootNode = node;
		nodeCount--;
		return true;
	}
	
	public String toString()
	{
	    return toString(rootNode, 0);
	}
	
	private String toString(btNode<E> tree, int level)
	{
	    String str = "";
	    if(tree != null)
	    {
		str += toString(tree.getRight(), level + 1);
		for (int i = 0; i <= level; i++)
		    str = str + "| ";
		str += tree.getValue().toString() + "\n";
		str += toString(tree.getLeft(), level + 1);
	    }
	    return str;
	}
		
}
