package binarytree_v2;

 import java.util.Scanner;
 import java.io.*;
 
public class Main
{

	/**
	 * Method main
	 * @param args
	 */
	public static void main(String[] args)
	{
		Scanner kb = new Scanner(System.in);
		try
		{

			System.setErr(new PrintStream("error.log"));

			BinaryTree tree = new BinaryTree();

			Scanner fi = new Scanner(new java.io.File("treeNums.txt"));

			while(fi.hasNextLine())
			{
				String line = fi.nextLine();
				Double numForm = new Double(0);
				try
				{
					numForm = Double.valueOf(line);
					tree.insert(numForm);
				}
				catch(NumberFormatException numEx)
				{
					tree.insert(line);
				}
			}
			fi.close();

			System.out.println("INORDER:");
			tree.printInorder();

			System.out.print("\nfindMax: ");
			System.out.println(tree.findMax());
			System.out.print("\nfindMin: ");
			System.out.println(tree.findMin());
			System.out.print("\nnodeCount: ");
			System.out.println(tree.nodeCount());
			System.out.print("\nsumNodes: ");
			System.out.println(tree.sumNodes());
			System.out.print("\nleafCount: ");
			System.out.println(tree.leafCount());

			/*int del = 50;

			System.out.println("Attempting to delete a value... (" + del + ")");
			tree.delete(new Double(del));
			System.out.println("\nINORDER:");
			tree.printInorder();

			del = 80;

			System.out.println("Attempting to delete a value... (" + del + ")");
			tree.delete(new Double(del));
			System.out.println("\nINORDER:");
			tree.printInorder();*/

			int del = 0;
			while(del != -1)
			{
				del = kb.nextInt();

				System.out.println("Attempting to delete a value... (" + del + ")");
				tree.delete(new Double(del));

				System.out.println("\nINORDER:");
				tree.printInorder();
			}
			
			System.out.println(tree);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		System.out.println("Press the \"ANY\" key to continue...");
		kb.nextLine();
	}
}
