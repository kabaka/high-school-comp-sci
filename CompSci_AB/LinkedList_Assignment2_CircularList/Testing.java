 import java.util.Scanner;
public class Testing
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		try
		{

			SinglyLinkedCircularList list = new SinglyLinkedCircularList();

			list.add("A");
			list.add("B");
			list.add("C");
			list.add("D");
			list.add("E");

			//list.testPrint(15);	//prove circularity

			list.printList();

			//System.out.println(list);

			list.add(0,"1");
			System.out.println(list);
			list.add(0,"2");
			System.out.println(list);
			list.add(1,"3");
			System.out.println(list+"\n");

			list.remove(list.size() - 1);
			System.out.println(list);
			list.remove("D");
			System.out.println(list);

			System.out.print("Press Enter to exit...");
			in.nextLine();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			in.nextLine();
		}
	}
}