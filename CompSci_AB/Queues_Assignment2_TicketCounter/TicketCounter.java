 import java.util.Queue;
 import java.util.LinkedList;
 import java.util.Stack;

public class TicketCounter
{
	
	static final int PROCESS = 15;
	static final int MAX_CASHIERS = 5;
	static final int NUM_CUSTOMERS = 100;
	//static boolean[] cashiers = new boolean[MAX_CASHIERS];
	static int cashiersTaken = 0;
	
	public static void main(String[] args)
	{
		System.out.println("Ticket Counter \"Simulation\" (except not) (revised)");
		
		Queue<Customer> customers = new LinkedList<Customer>();
		Stack<Customer> processing = new Stack<Customer>();
		
		int count = 0;
		int totalTime = 0;
		Customer current;
		
		//populate queue, set arrival times to PROCESS interval
		for(int i = 1; i <= NUM_CUSTOMERS; i++)
		{
			customers.add(new Customer(PROCESS * i));
		}
		
		do
		{
			System.out.println("CT: " + cashiersTaken + "; C: " + count + "; TT: " + totalTime);
			while(cashiersTaken < MAX_CASHIERS)
			{
				current = (Customer)customers.poll();
				current.setDepartureTime(current.getArrivalTime() + count + 120);
				processing.push(current);
				cashiersTaken++;
				System.out.println("*");
			}
			
			boolean done = false;
			while(!done)
			{
				current = (Customer)processing.peek();
				
				if(current.getDepartureTime() == count)
				{
					totalTime += current.totalTime();
					processing.pop();
					cashiersTaken--;
				}
				else
					done = true;
			}
			
			count++;
		}while(!customers.isEmpty());
		
		System.out.println("Total Time Taken: " + totalTime);
		System.out.println("Average Time Taken: " + (totalTime / NUM_CUSTOMERS));
	}
}