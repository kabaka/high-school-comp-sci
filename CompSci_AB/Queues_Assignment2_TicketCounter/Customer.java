public class Customer
{
	private int arrivalTime;
	private int departureTime;
	
	public Customer(int Arrives)
	{
		arrivalTime = Arrives;
	}
	
	public int getArrivalTime()
	{
		return arrivalTime;
	}
	public void setDepartureTime(int departs)
	{
		departureTime = departs;
	}
	public int getDepartureTime()
	{
		return departureTime;
	}
	
	public int totalTime()
	{
		return (departureTime - arrivalTime);
	}
}