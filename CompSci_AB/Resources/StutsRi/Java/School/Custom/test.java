import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.File;
import java.io.IOException;
import java.util.ListIterator;

public class test
{
	public static void main(String[] args)
	{
		DoubleLinkedList<String> test = new DoubleLinkedList<String>();
		testDList(test);
		ListIterator it = test.listIterator();
		for(double i=0;it.hasNext();i+=.5)
			if(((int)i)==i) 
				System.out.print(((int)(i+1))+"\t"+it.next()+"\t");
			else System.out.println(it.next());
		System.out.println("\n");
		while(it.hasPrevious()) System.out.println(it.previous());
	}
	
	public static void testDList(List l)
	{
		System.out.println("Enter the file name:");
		String fname = (new Scanner(System.in)).nextLine();
		Scanner in;
		try
		{
			in = new Scanner(new File(fname));
		} catch(IOException ex) {
			System.out.println("No such file");
			return;
		}
		String dump="";
		while(in.hasNext()) dump+=in.nextLine();
		
		StringTokenizer data = new StringTokenizer(dump);
		
		while(data.hasMoreTokens()) l.add(data.nextToken());
	}
	
	public static void doSomething(List l)
	{
		//DoubleLinkedList<String> l = new DoubleLinkedList<String>();
		//DSortedList<String> l = new DSortedList<String>();
		l.add("hello");
		System.out.println(l);
		System.out.println(l.add("thar"));
		System.out.println(l);
		l.add(0,"why");
		System.out.println(l);
		l.add("?");
		System.out.println(l);
		System.out.println(l.size());
		System.out.println(l.remove("thar"));
		System.out.println(l);
		l.add(2,"why");
		l.add(3,"hello");
		l.add(4,"thar");
		for(Object a:l)
		{
			System.out.println(a);
		}
		System.out.println(l);
		l.set(3,"goodbye");
		System.out.println(l);
		System.out.println(l.indexOf("hello"));
		System.out.println(l.contains("ifjeiaffea"));
		System.out.println(""+l.get(2)+l.get(3)+l.get(4));
		System.out.println(l);
		System.out.println(l.remove(2));
		System.out.println(l);
		System.out.println(l.remove(2));
		System.out.println(l);
		System.out.println(l.remove(2));
		System.out.println(l);
		System.out.println(l.remove(2));
		System.out.println(l.add("thar"));
		System.out.println(l);
	}
	
	
}