public class DNode<E>
{
	E obj;
	DNode<E> next;
	DNode<E> prev;
	
	public DNode(DNode<E> p,E o, DNode<E> n)
	{
		obj = o;
		next = n;
		prev = p;
	}
	
	public E value()
	{
		return obj;
	}
	
	public DNode<E> next()
	{
		return next;
	}
	
	public DNode<E> prev()
	{
		return prev;
	}
	
	public void setVal(E o)
	{
		obj = o;
	}
	
	public void setNext(DNode<E> n)
	{
		next = n;
	}
	
	public void setPrev(DNode<E> p)
	{
		prev = p;
	}
}