import java.util.Iterator;
import java.util.ListIterator;

public class DSortedList<E extends Comparable<E>> extends DoubleLinkedList<E>
{
	public DSortedList()
	{
		super();
	}
	
	protected class SDLIterator extends DLIterator
	{
		public SDLIterator()
		{
			super();
		}
		
		public SDLIterator(int i)
		{
			super(i);
		}
		
		public void add(E o)
		{
			if(current.value().compareTo(o)<=0 &&
				current.next().value().compareTo(o)>=0)
				super.add(o);
			throw new IllegalArgumentException();
		}
		
		public void set(E o)
		{
			if(current.prev().value().compareTo(o)<=0 &&
				current.next().value().compareTo(o)>=0)
				super.set(o);
			throw new IllegalArgumentException();
		}
	}
	
	public boolean add(E o)
	{
		if(head==null)
		{
			head = new DNode<E>(null,o,null);
			length++;
			return true;
		}
		
		DNode<E> cur = head;
		
		while(true)
		{
			if(cur.value().compareTo(o)>=0)
			{
				cur.setPrev(new DNode<E>(cur.prev(),o,cur));
				length++;
				return true;
			}
			if(cur.next()==null)
			{
				cur.setNext(new DNode<E>(cur,o,null));
				return true;
			}
			cur = cur.next();
		}
	}
	
	public void add(int j, E o)
	{
		if(j>length||j<0) throw new IndexOutOfBoundsException();
		
		if(head==null)
		{
			head = new DNode<E>(null,o,null);
			length++;
			return;
		}
		
		DNode<E> cur = head;
		
		for(int i=0;i<j;i++) cur = cur.next();
		
		if((cur.prev()==null||(o.compareTo(cur.prev().value())<=0)) &&
			(cur.next()==null||(o.compareTo(cur.value())>=0)))
			{
				System.out.println(cur==head);
				length++;
				cur.setPrev(new DNode<E>(cur.prev(),o,cur));
				if(cur==head) head = cur.prev();
				return;
			}
		throw new IllegalArgumentException();
	}
	
	public E set(int j, E o)
	{
		if(j>=length||j<0) throw new IndexOutOfBoundsException();
		
		DNode<E> cur = head;
		
		for(int i=0;i<j;i++) cur = cur.next();
		
		if((cur.prev()==null||(o.compareTo(cur.prev().value())<=0)) &&
			(cur.next()==null||(o.compareTo(cur.next().value())>=0)))
			{
				E temp = cur.value();
				cur.setVal(o);
				return temp;
			}
		throw new IllegalArgumentException();
	}
	
	public void reverse()
	{
		if(head==null) return;
		
		DNode<E> cur = head;
		
		while(cur.next()!=null)
		{
			DNode<E> sn = cur.prev();
			DNode<E> sp = cur.next();
			cur.setNext(sn);
			cur.setPrev(sp);
			cur = sp;
		}
		cur.setNext(cur.prev());
		head = cur;
	}
	
	public Iterator<E> iterator()
	{
		return new SDLIterator();
	}
	
	public ListIterator<E> listIterator()
	{
		return new SDLIterator();
	}
	
	public ListIterator<E> listIterator(int j)
	{
		return new SDLIterator(j);
	}
}