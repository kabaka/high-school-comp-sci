import java.util.Scanner;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class Palindromes
{
	public static void main(String[] args)
	{
		Scanner text = new Scanner(System.in);
		
		while(true)
		{
			String temp = text.nextLine();
			if(temp.equals("exit")) System.exit(0);
			System.out.println("The string is a palindrome: "+isPalin(temp));
			
		}
	}
	
	private static boolean isPalin(String x)
	{
		Queue<Character> f = new LinkedList<Character>();
		Stack<Character> l = new Stack<Character>();
		
		x = x.toLowerCase();
		for(int i=0;i<x.length();i++)
			if(!(x.charAt(i)>='a'&&x.charAt(i)<='z'))
				x = x.substring(0,i) + x.substring(i+1);
		
		for(int i=0;i<x.length()/2;i++)
			f.add(x.charAt(i));
			
		for(int i=(int)(x.length()/2.0);i<x.length();i++)
			l.push(x.charAt(i));
			
		while(!f.isEmpty()&&!l.isEmpty())
		{
			if(f.remove()!=l.pop()) return false;
			if(f.isEmpty()^l.isEmpty()) return false;
		}
		
		return true;
	}
}