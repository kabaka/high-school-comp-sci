public class Customer
{
	private static int setId = 1;
	
	private int arrivalTime, departureTime, id;
	
	public Customer(int Arrives)
	{
		arrivalTime = Arrives;
		departureTime = -1;
		id = setId;
		setId++;
	}
	
	public int getArrivalTime()
	{
		return arrivalTime;
	}
	
	public void setDepartureTime(int dt)
	{
		departureTime = dt;
	}
	
	public int getDepartureTime()
	{
		return departureTime;
	}
	
	public int totalTime()
	{
		return departureTime - arrivalTime;
	}
	
	public int getId()
	{
		return id;
	}
}