import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class newTicketCounter
{
	public static final int PROCESS=15, MAX_CASHIERS=10, NUM_CUSTOMERS=100;
	
	private static Queue<Customer> line = new LinkedList<Customer>();
	
	public static void main(String[] args)
	{
		int time =0;
		int customNum=NUM_CUSTOMERS;
		int sum = 0;
		int[] cashiers = new int[MAX_CASHIERS];
		
		
		while(customNum>0||!line.isEmpty())
		{
			if(customNum>0&&time%PROCESS==0)
			{
				line.add(new Customer(time));
				customNum--; 
			}
			
			
			
			time++;
		}
		
		System.out.println("Total time: "+time);
		System.out.println("Average time: "+sum/(NUM_CUSTOMERS/1.0));
	}
}