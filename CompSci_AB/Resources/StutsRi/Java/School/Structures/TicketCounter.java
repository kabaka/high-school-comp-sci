import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class TicketCounter
{
	public static final int PROCESS=15, MAX_CASHIERS=10, NUM_CUSTOMERS=100;
	
	private static Queue<Customer> line = new LinkedList<Customer>();
	
	public static void main(String[] args)
	{
		int time =0;
		int customNum=NUM_CUSTOMERS;
		int sum = 0;
		Customer[] working = new Customer[MAX_CASHIERS];
		
		while(customNum>0||!line.isEmpty())
		{
			if(customNum>0&&time%PROCESS==0&&time!=0)
			{
				line.add(new Customer(time));
				customNum--; 
			}
			
			for(int i=0;i<MAX_CASHIERS&&!line.isEmpty();i++)
			{
				
				if(working[i]==null)
				{
					working[i] = line.poll();
					working[i].setDepartureTime(time+120);
				} else if(working[i].getDepartureTime()==time) {
					System.out.println(working[i].totalTime());
					sum += working[i].totalTime();
					working[i] = null;
					i--;
				}
			}
			time++;
		}
		
		for(int i=0;i<MAX_CASHIERS;i++)
			if(working[i]!=null)
			{
				System.out.println(working[i].totalTime());
				sum += working[i].totalTime();
			}
		
		System.out.println("Total time: "+time);
		System.out.println("Average time: "+sum/(NUM_CUSTOMERS/1.0));
	}
}