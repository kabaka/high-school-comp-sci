import java.io.PrintStream;
import java.io.File;
import java.io.IOException;

public class sim
{
	public static void main(String[] args)
	{
		/*try
		{
			System.setOut(new PrintStream(new File("data.txt")));
		} catch(IOException ex) {
			System.exit(0);
		}*/
		
		int unknown = 24, people = 24, tempc=0, temps=0, spreaders=1;
		
		while(spreaders>0)
		{
			System.out.println("Chance: "+unknown+"/"+people);
			System.out.println("Spreaders: "+spreaders+"\n");
			
			tempc = unknown;
			temps = spreaders;
			for(int i=0;i<spreaders;i++)
			{
				double x,y;
				if((x=Math.random()) < (y=unknown/(people/1.0)))
				{
					System.out.println("Random: "+x+" true");
					temps++;
					tempc--;
				} else {
					temps--;
				}
			}
			unknown = tempc;
			spreaders = temps;
			System.out.println("\n");
		}
		
		System.out.println("Done");
		
	}
	
	public static void printAr(boolean[] x)
	{
		for(boolean a:x) System.out.print(a+" ");
		System.out.println("");
	}
}