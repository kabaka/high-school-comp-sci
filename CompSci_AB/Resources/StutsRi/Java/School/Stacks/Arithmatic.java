import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Arithmatic
{
	public static void main(String[] args)
	{
		Scanner text = new Scanner(System.in);
		while(true)
		{
			String ex = text.nextLine();
			if(ex.equals("exit")) System.exit(0);
			if(matched(ex))
				System.out.println("Good expression");
			else
				System.out.println("Bad expression");
		}
	}
	
	private static boolean matched(String ex)
	{
		StringTokenizer tok = new StringTokenizer(ex," ");
		Stack st = new Stack();
		int dump=0;
		
		for(int i=0;i<ex.length();i++)
		{
			st.push(ex.charAt(i));
			char t = ex.charAt(i);
			
			if(t>='0'&&t<='9')
			{
				dump = dump*10 + ((int)t-(int)'0');
			} else if(dump!=0) {
				st.push(dump);
				st.push(t);
			} else {
				st.push(t);
			}
		}
		
		int countL=0,countR=0;
		
		while(!st.empty())
		{
			char temp = (st.peek() instanceof Character)?st.pop():' ';
			if(temp=='(') countL++;
			else if(temp==')') countR++;
		}
		
		return countR==countL;
	}
}