import java.util.*;

public class Airlines
{
	ArrayList<LinkedList<String>> plane = new ArrayList<LinkedList<String>>();
	
	public Airlines()
	{
		for(int i=0;i<4;i++)
		{
			plane.add(new LinkedList<String>());
		}
	}
	
	public void add(int fl, String name)
	{
		try
		{
			ListIterator it = plane.get(fl/1000-1).listIterator();
			
			while(it.hasNext())
			{
				if(((String)plane.get(fl/1000-1).get(it.nextIndex())).compareTo(name)>=0)
				{
					it.add(name);
					return;
				} else {
					it.next();
				}
			}
			it.add(name);
		} catch(ArrayIndexOutOfBoundsException ex) {
			throw new IllegalArgumentException("No such plane");
		}
	}
	
	public void delete(int fl, String name)
	{
		try
		{
			ListIterator it = plane.get(fl/1000-1).listIterator();
			
			while(it.hasNext())
				if(it.next().equals(name))
				{
					it.remove();
					return;
				}
			throw new IllegalArgumentException("No such passenger");
		} catch(ArrayIndexOutOfBoundsException ex) {
			throw new IllegalArgumentException("No such plane");
		}
	}
	
	public String list(int fl)
	{
		try
		{
			ListIterator it = plane.get(fl/1000-1).listIterator();
			String ret = "";
			
			while(it.hasNext())
				ret += it.next()+"\n";
			return ret;
			
		} catch(ArrayIndexOutOfBoundsException ex) {
			throw new IllegalArgumentException("No such plane");
		}
	}
	
	public static void main(String[] args)
	{
		Airlines air = new Airlines();
		Scanner in = new Scanner(System.in);
		StringTokenizer tok;
		
		while(true)
		{
			String temp = in.nextLine();
			tok = new StringTokenizer(temp);
			String tempi = tok.nextToken();
			
			try
			{
				if(tempi.equals("add"))
				{
					air.add(Integer.parseInt(tok.nextToken()),tok.nextToken());
				} else if(tempi.equals("exit")) {
					System.exit(0);
				} else if(tempi.equals("delete")) {
					air.delete(
						Integer.parseInt(tok.nextToken()),tok.nextToken());
				} else if(tempi.equals("list")) {
					System.out.println(
						air.list(Integer.parseInt(tok.nextToken())));
				} else {
					System.out.println("Invalid command");
				}
				System.out.println("\n");
			} catch(RuntimeException ex) {
				System.out.println("Invalid command\n");
			}
		}
	}
}