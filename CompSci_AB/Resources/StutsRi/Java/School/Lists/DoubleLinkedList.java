import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;

public class DoubleLinkedList<E> implements List<E>
{
	private int length;
	private DNode<E> head;
	
	public DoubleLinkedList()
	{
		length = 0;
		head = null;
	}
	
	private class DLIterator implements ListIterator<E>
	{
		DNode<E> current;
		int ind;
		
		public DLIterator()
		{
			current = head;
			ind=0;
		}
		
		public DLIterator(int i)
		{
			current = head;
			ind=i;
			for(int j=0;j<i;j++) current = current.next();
		}
		
		public void add(E o)
		{
			current.setNext(new DNode<E>(current,o,current.next()));
			length++;
		}
		
		public boolean hasNext()
		{
			return !(current.next()==null);
		}
		
		public boolean hasPrevious()
		{
			return !(current.prev()==null);
		}
		
		public E next()
		{
			ind++;
			return (current=current.next()).value();
		}
		
		public int nextIndex()
		{
			return ind+1;
		}
		
		public E previous()
		{
			ind--;
			return (current=current.prev()).value(); 
		}
		
		public int previousIndex()
		{
			return ind-1;
		}
		
		public void remove()
		{
			if(current.prev().next()!=current)throw new IllegalStateException();
			length--;
			current.prev().setNext(current.next());
		}
		
		public void set(E o)
		{
			current.setVal(o);
		}
	}
	
	public boolean add(E o)
	{
		if(head==null)
		{
			head = new DNode<E>(null,o,null);
			length++;
			return true;
		}
		
		DNode<E> thenode = head;
		
		for(int i=0;i<length;i++) thenode = thenode.next();
		
		thenode.setNext(new DNode<E>(thenode,o,null));
		length++;
		return true;
	}
	
	public void add(int j, E o)
	{
		if(j>=length||j<0) throw new IndexOutOfBoundsException();
		
		if(j==0)
		{
			head = new DNode<E>(null,o,head);
			length++;
			return;
		}
		
		DNode<E> thenode = head;
		
		for(int i=0;i<j;i++) thenode = thenode.next();
		
		thenode.setNext(new DNode<E>(thenode,o,thenode.next()));
		length++;
	}
	
	public boolean addAll(Collection<? extends E> c)
	{
		throw new UnsupportedOperationException();
	}
	
	public boolean addAll(int i, Collection<? extends E> c)
	{
		throw new UnsupportedOperationException();
	}
	
	public void clear()
	{
		head = null;
		length = 0;
	}
	
	public boolean contains(Object o)
	{
		for(DNode<E> thenode = head; thenode!=null; thenode=thenode.next())
			if(thenode.value().equals(o))
				return true;
		return false;
	}
	
	public boolean containsAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof List)
		{
			List test = (List) o;
			DNode<E> cur = head;
			
			for(Object t:test)
			{
				if(!t.equals(cur.value())) return false;
				cur = cur.next();
			}
			
			return true;
		}
		
		return false;
	}
	
	public E get(int j)
	{
		if(j>=length||j<0) throw new IndexOutOfBoundsException();
		
		DNode<E> thenode = head;
		for(int i=0;i<j;i++)
			thenode = thenode.next();
		return thenode.value();
	}
	
	public int hashCode()
	{
		int h = 1;
		
		for(DNode<E> cur = head; cur!=null; cur = cur.next())
			h = 31*h + (cur.value()==null? 0 : cur.value().hashCode());
			
		return h;
	}
	
	public int indexOf(Object o)
	{
		DNode<E> cur = head;
		
		for(int i=0;i<length;i++)
		{
			if(cur.value().equals(o)) return i;
			cur = cur.next();
		}
		
		return -1;
	}
	
	public boolean isEmpty()
	{
		return length==0;
	}
	
	public Iterator<E> iterator()
	{
		return new DLIterator();
	}
	
	public int lastIndexOf(Object o)
	{
		DNode<E> cur = head;
		int ret = -1;
		
		for(int i=0;i<length;i++)
		{
			if(cur.value().equals(o)) ret = i;
			cur = cur.next();
		}
		
		return ret;
	}
	
	public ListIterator<E> listIterator()
	{
		return new DLIterator();
	}
	
	public ListIterator<E> listIterator(int j)
	{
		return new DLIterator(j);
	}
	
	public E remove (int j)
	{
		if(j>=length||j<0) throw new IndexOutOfBoundsException();
		
		DNode<E> thenode = head;
		
		for(int i=0;i<j-1;i++) thenode = thenode.next();
		
		E temp = thenode.next().value();
		thenode.setNext(thenode.next().next());
		return temp;
	}
	
	public boolean remove(Object o)
	{
		if(head==null) return false;
		
		DNode<E> thenode = head;
		
		for(int i=0;i<length;i++)
		{
			if(thenode.value().equals(o))
			{
				thenode.prev().setNext(thenode.next());
				return true;
			}
			thenode = thenode.next();
		}
		return false;
	}
	
	public boolean removeAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}
	
	public boolean retainAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}
	
	public E set(int j, E obj)
	{
		if(j>=length||j<0) throw new IndexOutOfBoundsException();
		
		DNode<E> thenode = head;
		for(int i=0;i<j;i++)
			thenode = thenode.next();
			
		E temp = thenode.value();
		thenode.setVal(obj);
		return temp;
	}
	
	public int size()
	{
		return length;
	}
	
	public List<E> subList(int a, int b)
	{
		if(a<0||b>length||a>b) throw new IndexOutOfBoundsException();
		
		DNode<E> start = head;
		List<E> ret = new DoubleLinkedList<E>();
		
		for(int i=0;i<b;i++)
		{
			if(i>=a) ret.add(start.value());
			start = start.next();
		}
		
		return ret;
	}
	
	public Object[] toArray()
	{
		DNode<E> cur = head;
		Object[] ret = new Object[length];
		
		for(int i=0;i<length;i++)
		{
			ret[i] = cur.value();
			cur = cur.next();
		}
		
		return ret;
	}
	
	public<T> T[] toArray(T[] a)
	{		
		DNode<E> cur = head;
		for(int i=0;i<length;i++)
		{
			try
			{
				a[i] = (T)cur.value();
			} catch (ClassCastException ex) {
				throw new ArrayStoreException();
			}
			cur = cur.next();
		}
		return a;
	}
}

class DNode<E>
{
	E obj;
	DNode<E> next;
	DNode<E> prev;
	
	public DNode(DNode<E> p,E o, DNode<E> n)
	{
		obj = o;
		next = n;
		prev = p;
	}
	
	public E value()
	{
		return obj;
	}
	
	public DNode<E> next()
	{
		return next;
	}
	
	public DNode<E> prev()
	{
		return prev;
	}
	
	public void setVal(E o)
	{
		obj = o;
	}
	
	public void setNext(DNode<E> n)
	{
		next = n;
	}
	
	public void setPrev(DNode<E> p)
	{
		prev = p;
	}
}