/**
 *  Implements a 2-D array of characters
 */

public class CharMatrix
{
  // fields:
  private char[][] grid;

  /**
   *  Constructor:
   *  Creates a grid with dimensions <code>rows</code>, <code>cols</code>,
   *  and fills it with the <code>fill</code> character
   */
  public CharMatrix(int rows, int cols, char fill)
  {
    grid = new char[rows][cols];
    for(int i=0;i<rows;i++)
    	for(int j=0;j<cols;j++)
    		grid[i][j] = fill;
  }

  /**
   *  Fills the given rectangle with <code>fill</code> characters.
   *  row0, col0 is the upper left corner and row1, col1 is the
   *  lower right corner of the rectangle.
   */
  public void fillRect(int row0, int col0, int row1, int col1, char fill)
  {
  	for(int i=row0;i<=row1;i++)
  		for(int j=col0;j<=col1;j++)
  			grid[i][j] = fill;
  }

  /**
   *  Fills the given rectangle with SPACE characters.
   *  row0, col0 is the upper left corner and row1, col1 is the
   *  lower right corner of the rectangle.
   */
  public void clearRect(int row0, int col0, int row1, int col1)
  {
    for(int i=row0;i<=row1;i++) for(int j=col0;j<=col1;j++) grid[i][j] = ' ';
  }

  /**
   *  Returns the number of rows in grid
   */
  public int numRows()
  {
    return grid.length;
  }

  /**
   *  Returns the number of columns in grid
   */
  public int numCols()
  {
    return grid[0].length;
  }

  /**
   *  Returns the character at row, col position
   */
  public char charAt(int row, int col)
  {
    return grid[row][col];
  }

  /**
   *  Returns true if the character at row, col is a space,
   *  false otherwise
   */
  public boolean isEmpty(int row, int col)
  {
    return (grid[row][col]==' ');
  }

  /**
   *  Returns the count of all non-space characters in <code>row</code>
   */
  public int countInRow(int row)
  {
  	int ret=0;
    for(int i=0;i<grid[row].length;i++) if(grid[row][i]!=' ') ret++;
    return ret;
  }

  /**
   *  Returns the count of all non-space characters in <code>col</code>
   */
  public int countInCol(int col)
  {
    int ret=0;
    for(int i=0;i<grid.length;i++) if(grid[i][col]!=' ') ret++;
    return ret;
  }
  
  public String toString()
  {
  	String ret = "";
  	for(int i=0;i<grid.length;i++)
  	{
  		for(int j=0;j<grid[0].length;j++)
  			ret += grid[i][j];
  		ret += '\n';
  	}
  	return ret;
  }
  
  public static void main(String[] args)
  {
  	CharMatrix x = new CharMatrix(10,10,'x');
  	System.out.println(x);
  	x.fillRect(0,0,4,3,'y');
  	System.out.println(x);
  	x.clearRect(0,0,4,3);
  	System.out.println(x);
  	System.out.println(x.countInCol(0));
  }
}
