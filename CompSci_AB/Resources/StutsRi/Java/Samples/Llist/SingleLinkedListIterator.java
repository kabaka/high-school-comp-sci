// Implements an iterator for a SinglyLinkedList.

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SingleLinkedListIterator<E> implements Iterator<E>
{
  private ListNode nextNode;

  // Constructor
  public SingleLinkedListIterator(ListNode head)
  {
    nextNode = head;
  }

  public boolean hasNext()
  {
    return nextNode != null;
  }

  public E next()
  {
    if (nextNode == null)
      throw new NoSuchElementException();

    E value = (E) nextNode.getValue();
    nextNode = nextNode.getNext();
    return value;
  }

  // Not implemented.
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}
