// Implements a singly-linked list.

import java.util.Iterator;
import java.util.ListIterator;

public class SinglyLinkedList<E> implements Iterable<E>
{
  private ListNode head;
  private int nodeCount;

  // Constructor: creates an empty list
  public SinglyLinkedList()
  {
	head = null;
	nodeCount = 0;
  }

  // Constructor: creates a list that contains
  // all elements from the array values, in the same order
  public SinglyLinkedList(E[] values)
  {
	ListNode tail = null;
	for (E value : values) // for each value to insert
	{
	  ListNode node = new ListNode(value, null);
	  if (head == null)
		head = node;
	  else
		tail.setNext(node);
	  tail = node;    // update tail
	}

	nodeCount = values.length;
  }

  // Returns true if this list is empty; otherwise returns false.
  public boolean isEmpty()
  {
	return nodeCount==0;
  }

  // Returns the number of elements in this list.
  public int size()
  {
	return nodeCount;
  }

  // Returns true if this list contains an element equal to obj;
  // otherwise returns false.
  public boolean contains(E obj)
  {
  	ListNode thenode = head;
	for(int i=0;i<nodeCount;i++)
	{
		if(obj.equals(thenode.getValue())) return true;
		thenode = thenode.getNext();
	}
	return false;
  }

  // Returns the index of the first element in equal to obj;
  // if not found, retunrs -1.
  public int indexOf(E obj)
  {  	
	ListNode thenode = head;
	for(int i=0;i<nodeCount;i++)
	{
		if(obj.equals(thenode.getValue())) return i;
		thenode = thenode.getNext();
	}
	return -1;
  }

  // Adds obj to this collection.  Returns true if successful;
  // otherwise returns false.
  public boolean add(E obj)
  {
  	if(head==null)
  	{
  		head = new ListNode(obj,null);
  		nodeCount++;
  		return true;
  	}
  	
	ListNode thenode = head;
	
	for(int j=0;j<nodeCount-1;j++)
		thenode = thenode.getNext();
		
	thenode.setNext(new ListNode(obj,null));
	nodeCount++;
	return true;
  }

  // Removes the first element that is equal to obj, if any.
  // Returns true if successful; otherwise returns false.
  public boolean remove(E obj)
  {
  	if(head==null) return false;
  	
	ListNode prev=head,thenode = prev.getNext();
	
	if(obj.equals(head.getValue()))
	{
		head = head.getNext();
		nodeCount--;
		return true;
	}
	
	for(int i=0;i<nodeCount-1;i++)
	{
		if(obj.equals(thenode.getValue()))
		{
			prev.setNext(thenode.getNext());
			nodeCount--;
			return true;
		}
		prev=prev.getNext();
		thenode = thenode.getNext();
	}
	return false;
  }

  // Returns the i-th element.               
  public E get(int i)
  {
	if(i>=nodeCount||i<0) throw new IndexOutOfBoundsException();
	
	ListNode thenode = head;
	
	for(int j=0;j<i;j++)
		thenode = thenode.getNext();
		
	return (E)thenode.getValue();
	
	
  }

  // Replaces the i-th element with obj and returns the old value.
  public E set(int i, E obj)
  {
	if(i>=nodeCount||i<0) throw new IndexOutOfBoundsException();
	
	ListNode thenode = head;
	
	for(int j=0;j<i;j++)
		thenode = thenode.getNext();
		
	Object temp = thenode.getValue();
	thenode.setValue(obj);
	return (E)temp;
  }

  // Inserts obj to become the i-th element. Increments the size
  // of the list by one.
  public void add(int i, E obj)
  {
	if(i>nodeCount||i<0) throw new IndexOutOfBoundsException();
	
	if(i==0)
	{
		head = new ListNode(obj,head);
		nodeCount++;
		return;
	}
	
	ListNode prev=head,thenode = prev.getNext();
	
	for(int j=0;j<i-1;j++)
	{
		thenode = thenode.getNext();
		prev = prev.getNext();
   	}
	prev.setNext(new ListNode(obj,thenode));
	nodeCount++;
  }

  // Removes the i-th element and returns its value.
  // Decrements the size of the list by one.
  public E remove(int i)
  {
  	if(i>=nodeCount||i<0) throw new IndexOutOfBoundsException();
  	
  	if(i==0)
  	{
  		E asdf = (E) head.getValue();
  		head = head.getNext();
  		nodeCount--;
  		return asdf;
  	}
  	
	ListNode prev=head,thenode = prev.getNext();
	
	for(int j=0;j<i-1;j++)
	{
		prev=prev.getNext();
		thenode = thenode.getNext();
	}
	
	prev.setNext(thenode.getNext());
	nodeCount--;
	return (E) thenode.getValue();
  }

  // Returns a string representation of this list.
  public String toString()
  {
	String ret="";
	ListNode temp = head;
	for(int i=0;i<nodeCount;i++)
	{
		ret += temp.getValue()+" ";
		temp = temp.getNext();
	}
	
	return ret;
  }

  // Returns an iterator for this collection.
  public Iterator<E> iterator()
  {
	return new SingleLinkedListIterator<E>(head);
  }
}
