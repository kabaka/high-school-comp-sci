import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LifeModel implements ActionListener
{

    /*
     *  This is the Model component.
     */

    private static int SIZE = 60;
    private LifeCell[][] myGrid;
    LifeView myView;
    Timer timer;

    // initial population from file or random if no file available
    public LifeModel(LifeView view)
    {
        int r, c;
        myGrid = new LifeCell[SIZE][SIZE];
        for ( r=0; r < SIZE; r++ )
            for ( c = 0; c < SIZE; c++ )
                myGrid[r][c] = new LifeCell();

        EasyReader infile = new EasyReader("lifegrid.dat");
        if ( infile.bad() )
        // use random population
        {                                           
            for ( r=0; r < SIZE; r++ )
            {
                for ( c = 0; c < SIZE; c++ )
                {
                    if ( Math.random() > 0.85)
                        myGrid[r][c].setAliveNow(true);
                }
            }
        }
        else
        // use file input from inFile
        {                                    
            int numInitialCells = infile.readInt();
            for (int count=0; count<numInitialCells; count++)
            {
                r = infile.readInt();
                c = infile.readInt();
                myGrid[r][c].setAliveNow(true);
            }
            infile.close();
        }

        myView = view;
        myView.updateView(myGrid);
    }

    public void pause()
    {
        timer.stop();
    }
    public void resume()
    {
        timer.restart();
    }
    public void run()
    {
        timer = new Timer(50, this);
        timer.setCoalesce(true);
        timer.start();
    }

    // get here each time timer fires
    public void actionPerformed(ActionEvent e)
    {
        oneGeneration();
        myView.updateView(myGrid);
    }
    
    private void oneGeneration()
    {
    	//int a=0,b=1,c=2;
        for(int i=0;i<myGrid.length;i++)
        {
        	for(int j=0;j<myGrid[0].length;j++)
        	{
        		int nc=0;
        		if(myGrid[i][j].isAliveNow())
        		{
        			for(int k=0;k<9;k++)
        			{
        				if(k==4) continue;
        				
        				try
        				{
        					if(myGrid[i-1+k/3][j-1+k%3].isAliveNow()) nc++;
        				} catch(ArrayIndexOutOfBoundsException ex) {
        					continue;
        				}
        			}
        			myGrid[i][j].setAliveNext(nc==2||nc==3);
        		} else {
        			for(int k=0;k<9;k++)
        			{
        				if(k==4) continue;
        				
        				try
        				{
        					if(myGrid[i-1+k/3][j-1+k%3].isAliveNow()) nc++;
        				} catch(ArrayIndexOutOfBoundsException ex) {
        					continue;
        				}
        			}
        			myGrid[i][j].setAliveNext(nc==3);
        		}
        	}
        }
        for(int i=0;i<myGrid.length;i++)
        {
        	for(int j=0;j<myGrid[0].length;j++)
        	{
        		myGrid[i][j].setAliveNow(myGrid[i][j].isAliveNext());
        	}
        }
        return;
    }
}

