import java.awt.*;
import javax.swing.*;

/*
  this is the View component
*/

public class LifeView extends Board
{
    private static int SIZE = 60;
    private LifeCell[][] myGrid;

    public LifeView()
    {
    	super(SIZE,SIZE,Color.white,Color.white);
        myGrid = new LifeCell[SIZE][SIZE];
    }

    // entry point from model, requests grid be redisplayed
    public void updateView( LifeCell[][] mg )
    {
        myGrid = mg;
        for(int i=0;i<SIZE;i++)
        {
        	for(int j=0;j<SIZE;j++)
        	{
        		super.getCellButton(i,j).addActionListener(
        			new ClickListener(myGrid[i][j]));
        	}
        }
        repaint();
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        /*int testWidth = getWidth() / (SIZE+1);
        int testHeight = getHeight() / (SIZE+1);
        // keep each life cell square
        int boxSize = Math.min(testHeight, testWidth);

        for ( int r = 0; r < SIZE; r++ )
        {
            for (int c = 0; c < SIZE; c++ )
            {
                if (myGrid[r][c] != null)
                {
                    if ( myGrid[r][c].isAliveNow() )
                        g.setColor( Color.blue );
                    else

                        g.setColor( new Color(235,235,255) );
                    g.fillRect( (c+1)*boxSize, (r+1)* boxSize, 
                                                boxSize-2, boxSize-2);
                }
            }
        }*/
        for(int i=0;i<SIZE;i++)
        {
        	for(int j=0;j<SIZE;j++)
        	{
        		if(myGrid[i][j]!=null)
        		{
        			if(myGrid[i][j].isAliveNow())
        				super.setCellColor(i,j,Color.BLUE);
        			else
        				super.setCellColor(i,j,Color.WHITE);
        		}
        	}
        }
    }
}
