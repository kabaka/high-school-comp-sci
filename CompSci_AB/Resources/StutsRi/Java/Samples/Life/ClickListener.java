/*
 *@author Richard Stutsman
 */
 
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClickListener implements ActionListener
{
	LifeCell cell;
	
	public ClickListener(LifeCell c)
	{
		cell = c;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		cell.setAliveNow(!cell.isAliveNow());
	}
	
}