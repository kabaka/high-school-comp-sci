import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Stack;

public class MazeModel {

    /*
     *  This is the Model component.
     */

    private static int SIZE = 50;
    private MazeCell[][] myGrid;
    private MazeView myView;
    private ListStack solution = new ListStack();
    private int cRow=0, cCol=0, nRow=0, nCol=0;     // current,new row,col
    private Point temp;
    private boolean keepGoing;
    
    public MazeModel(MazeView view)
    {
        myGrid = new MazeCell[SIZE][SIZE];
        int setNum = 0;
        for (int r=0; r<SIZE; r++)
        {
            for (int c=0; c<SIZE; c++) 
            {
                myGrid[r][c] = new MazeCell( setNum );
                setNum++;
            }
        }
        int numSets = setNum;
        
        while ( numSets > 1 ) 
        {
            int aCol = (int)(Math.random() * SIZE);            // choose random cell
            int aRow = (int)(Math.random() * SIZE);
            int bCol = aCol;
            int bRow = aRow;
            int testdir = 0;
            while (aCol == bCol && aRow == bRow)              // and random neighbor
            {
                testdir = (int)(Math.random() * 4);
                switch (testdir) 
                {
                    case 0:
                      if (aCol > 0) bCol = aCol-1;  break;     // left cell candidate
                    case 1: 
                      if (aCol < SIZE-1) bCol = aCol+1; break; // right cell candidate
                    case 2:
                      if (aRow > 0) bRow = aRow-1; break;      // top cell candidate
                    case 3: 
                      if (aRow < SIZE-1) bRow = aRow+1; break; // bottom cell candidate
                }
            }
            
            // we have two adjacent cells both in the maze grid
            if (myGrid[aRow][aCol].getSetNumber() != myGrid[bRow][bCol].getSetNumber() ) 
            {
                // join them by going through all cells and sharing set number
                int changeToSet = myGrid[aRow][aCol].getSetNumber();
                int changeFromSet = myGrid[bRow][bCol].getSetNumber();
                for (int col=0; col<SIZE; col++) 
                {
                    for (int row=0; row<SIZE; row++) 
                    {
                        if ( myGrid[col][row].getSetNumber() == changeFromSet )
                            myGrid[col][row].setSetNumber(changeToSet);
                    }
                }
              
                // now remove common hedge
                switch (testdir) 
                {
                    case 0: myGrid[aRow][aCol].setLeftHedge(false);
                            myGrid[bRow][bCol].setRightHedge(false);
                            break;
                    case 1: myGrid[aRow][aCol].setRightHedge(false);
                            myGrid[bRow][bCol].setLeftHedge(false);
                            break;  
                    case 2: myGrid[aRow][aCol].setTopHedge(false);
                            myGrid[bRow][bCol].setBottomHedge(false);
                            break;  
                    case 3: myGrid[aRow][aCol].setBottomHedge(false);
                            myGrid[bRow][bCol].setTopHedge(false);
                            break;  
                }  // end of switch
                numSets--;
            }
        }
    
        // make an entry and exit point to the maze
        myGrid[0][0].setLeftHedge(false);
        myGrid[0][0].setStatus(1);
        myGrid[SIZE-1][SIZE-1].setRightHedge(false);
        
        myView = view;
        myView.updateView(myGrid);            
        
        solution.push(new Point(cRow,cCol));
    }
                  
    public void stepMaze()
    {
    	/*if(cRow==SIZE-1&&cCol==SIZE-1) throw new MazeFinishedException();
    	
    	for(int i=0;i<4;i++)
    	{
    		
    		if(!myGrid[cRow][cCol].getHedge(i)&&
    			statusAt(cRow,cCol,i)==0)
    		{
    			solution.push(cCol);
    			solution.push(cRow);
    			
    			switch(i)
    			{
	    			case 0:
	    				myGrid[cRow-1][cCol].setStatus(1);
	    				cRow -= 1;
	    				break;
	    			case 1:
	    				myGrid[cRow][cCol+1].setStatus(1);
	    				cCol += 1;
	    				break;
	    			case 2:
	    				myGrid[cRow+1][cCol].setStatus(1);
	    				cRow += 1;
	    				break;
	    			case 3:
	    				myGrid[cRow][cCol-1].setStatus(1);
	    				cCol -= 1;
	    				break;
	    		}
	    		myView.updateView(myGrid);
	    		return;
    		}
    	}
    	myGrid[cRow][cCol].setStatus(2);
    	cRow = (Integer)solution.pop();
    	cCol = (Integer)solution.pop();
        myView.updateView(myGrid);*/
        if (nRow != (SIZE-1) || nCol != (SIZE-1))
        {
        	temp=(Point)solution.peekTop();
        	cCol=(int)temp.getX();
        	cRow=(int)temp.getY();
        	keepGoing=false;
        }
        if (myGrid[cRow][cCol].getTopHedge() == false && cRow!=0 && myGrid[cRow-1][cCol].getStatus() == 0)
        {
        	nRow=cRow-1;
        	nCol=cCol;
        	keepGoing=true;
        }
        else if (myGrid[cRow][cCol].getRightHedge() == false && cCol!=SIZE-1 &&myGrid[cRow][cCol+1].getStatus() == 0)
        {
        	nRow=cRow;
        	nCol=cCol+1;
        	keepGoing=true;
        }
        else if (myGrid[cRow][cCol].getBottomHedge() == false && cRow!=SIZE-1 && myGrid[cRow+1][cCol].getStatus() == 0)
        {
        	nRow=cRow+1;
        	nCol=cCol;
        	keepGoing=true;
        }
        else if (myGrid[cRow][cCol].getLeftHedge() == false && cCol!=0 && myGrid[cRow][cCol-1].getStatus() == 0)
        {
        	nRow=cRow;
        	nCol=cCol-1;
        	keepGoing=true;
        }
        else
        {
        	temp=(Point)solution.pop();
        	nCol=(int)temp.getX();
        	nRow=(int)temp.getY();
        	myGrid[cRow][cCol].setStatus(2);
        	myView.updateView(myGrid);
        }
        if (keepGoing)
        {
        	solution.push(new Point(nRow,nCol));
        	myGrid[nRow][nCol].setStatus(1);
        	myView.updateView(myGrid);
        }
        
    }
    
    class MazeFinishedException extends RuntimeException{}
    
    private int statusAt(int r,int c,int dir)
    {
    	switch(dir)
    	{
    		case 0: return myGrid[r-1][c].getStatus();
    		case 1: return myGrid[r][c+1].getStatus();
    		case 2: return myGrid[r+1][c].getStatus();
    		case 3: return myGrid[r][c-1].getStatus();
    		default: throw new IllegalArgumentException();
    	}
    }
}

