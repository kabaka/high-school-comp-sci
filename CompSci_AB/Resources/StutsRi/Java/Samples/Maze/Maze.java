import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/*
    this is the Controller component
*/

class Maze extends JFrame implements ActionListener
{

    private MazeView view;
    private MazeModel model;
    private JButton solveButton,stopButton,stepButton,finButton;
    private Timer timer;

    Maze()
    {
        super("Maze");

        // build the buttons
        JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        solveButton = new JButton("Solve");
        solveButton.addActionListener(this);
        controlPanel.add(solveButton);
        
        stopButton = new JButton("Stop");
        stopButton.setEnabled(false);
        stopButton.addActionListener(new ActionListener()
        							{
        								public void actionPerformed
        								(ActionEvent e)
        								{
        								solveButton.setEnabled(true);
        								timer.stop();
        								stopButton.setEnabled(false);}
        							});
        controlPanel.add(stopButton);
        
        stepButton = new JButton("Step");
        stepButton.addActionListener(new ActionListener()
        							{
        								public void actionPerformed
        								(ActionEvent e){
        								model.stepMaze();}
        							});
        controlPanel.add(stepButton);
        
        finButton = new JButton("Finish maze");
        finButton.addActionListener(new ActionListener()
        							{
        								public void actionPerformed(
        														ActionEvent e)
        								{
        								timer.stop();
        								solveButton.setEnabled(false);
        								stopButton.setEnabled(false);
        								stepButton.setEnabled(false);
        								finButton.setEnabled(false);
        								while(true) model.stepMaze();
        								}
        							});
        controlPanel.add(finButton);

        // build the view
        view = new MazeView();
        view.setBackground(Color.black);

        // put buttons, view together
        Container c = getContentPane();
        c.add(controlPanel, BorderLayout.NORTH);
        c.add(view, BorderLayout.CENTER);

        // construct the timer and the listener to step the maze
        timer = new Timer(1, new ActionListener()
                          {
                              public void actionPerformed(ActionEvent e)
                              {
                                  model.stepMaze();
                              }
                          }
                         );

        // build the model
        model = new MazeModel(view);
    }

    public void run()
    {
        timer.setCoalesce(true);
        timer.start();
    }

    // when user clicks on solve, the timer is started
    // each tick advances the maze solution.
    public void actionPerformed(ActionEvent e)
    {
        JButton b = (JButton)e.getSource();
        if ( b == solveButton )
        {
            this.run();
            //while(true) model.stepMaze();
            solveButton.setEnabled(false);
            stopButton.setEnabled(true);
        }
    }

    public static void main(String[] args)
    {
        Maze conway = new Maze();
        conway.addWindowListener(new WindowAdapter()
                                 {
                                     public void windowClosing(WindowEvent e)
                                     {
                                         System.exit(0);
                                     }
                                 }
                                );
        conway.setSize(570, 640);
        conway.show();
    }
}
