import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Board extends JPanel
{
	private JButton[][] cellGrid;
	private Color[][] gridTest;
	private static final int MAXDIM = 50;
	private int myNumRows;
	private int myNumCols;
	private Color backGround;
	private Icon[] ics ={new ImageIcon("x.gif"),new ImageIcon("o.gif")};
	private char[][] chars;
	
	public Board(int nr, int nc,Color back, Color bord)
	{
		setBackground(back);
		backGround=back;
		myNumRows = nr;
		myNumCols = nc;
		int cellSize = cellDim();
		setSize(nc * cellSize, nr * cellSize);
		setLayout(new GridLayout(nr, nc));
		
		cellGrid = new JButton[nr][nc];
		gridTest = new Color[nr][nc];
		chars = new char[nr][nc];
		
		for(int row = 0; row < nr; row++)
		{
			for(int col = 0; col < nc; col++)
			{
				cellGrid[row][col] = new JButton();
				cellGrid[row][col].setBackground(back);
				gridTest[row][col] = back;
				cellGrid[row][col].setBorder(BorderFactory.createLineBorder(bord));
				cellGrid[row][col].setPreferredSize(new Dimension(cellSize, cellSize));
				cellGrid[row][col].addActionListener(new function(row,col));
				add(cellGrid[row][col]);
			}
		}
	}
	
	public int numRows()
	{
		return myNumRows;
	}
	
	public int numCols()
	{
		return myNumCols;
	}
	
	private int cellDim()
	{
		int numCells;
		if(numRows() < numCols())
			numCells = numCols();
		else
			numCells = numRows();
		
		if(numCells > MAXDIM)
			throw new IllegalArgumentException("grid too large");
			
		return 2 * (250/numCells);
	}
	
	public void setCellColor(int r,int c, Color color)
	{
		cellGrid[r][c].setBackground(color);
		gridTest[r][c]=color;
	}
	
	public void clearCellColor(int r,int c)
	{
		cellGrid[r][c].setBackground(backGround);
		gridTest[r][c]=backGround;
	}
	
	public void setCellIcon(int r,int c,char a)
	{
		cellGrid[r][c].setIcon(ics[a=='x'?0:1]);
		chars[r][c] = a;
	}
	
	public char getCellChar(int r,int c)
	{
		return chars[r][c];
	}
	
	public boolean isEmpty(int r,int c)
	{
		if(gridTest[r][c].equals(backGround))
		{
			return true;
		} else {
			return false;
		}
	}
	
	public Color getColor(int r,int c)
	{
		return gridTest[r][c];
	}
	
	
	public void flash(int[] theAR)
	{
		if(theAR.length%2!=0) return;
		
		Color[] temp = new Color[theAR.length/2];
		
		for(int i=0;i<temp.length;i++)
		{
			temp[i] = getColor(theAR[2*i],theAR[2*i+1]);
		}
		
		while(true)
		{
			for(int i=0;i<theAR.length/2;i++)
			{
				clearCellColor(theAR[2*i],theAR[2*i+1]);
			}
			
			main.wait(75);
			
			for(int i=0;i<theAR.length/2;i++)
			{
				setCellColor(theAR[2*i],theAR[2*i+1],temp[i]);
			}
			
			main.wait(75);
		}
	}
}