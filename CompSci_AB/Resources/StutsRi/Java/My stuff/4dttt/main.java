import java.awt.*;
import javax.swing.*;
import java.util.Scanner;

public class main
{
	static fdttt game = new fdttt();
	static Board board; //= new Board(4,4,Color.white,Color.black);
	static JFrame frame;
	
	public static void main(String[] args)
	{
		Scanner text = new Scanner(System.in);
		int size = text.nextInt();
		board = new Board(size,size,Color.white,Color.black);
		setUp(board,size,size);
	}
	
	public static fdttt getGame()
	{
		return game;
	}
	
	public static Board getBoard()
	{
		return board;
	}
	
	public static void wait(int x)
	{
		try
		{
			Thread.sleep(x);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void setUp(JPanel display, int xCoord, int yCoord)
	{
		int width = (int)Math.round((display).getSize().getWidth());
		int height = (int)Math.round((display).getSize().getHeight());
		frame = new JFrame("Connect Four");
		frame.show();
		width = width + frame.getInsets().right + frame.getInsets().left;
		height = width + frame.getInsets().top + frame.getInsets().bottom;
		frame.setSize(width,height);
		frame.setLocation(xCoord, yCoord);
		frame.getContentPane().add(display);
		frame.show();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}