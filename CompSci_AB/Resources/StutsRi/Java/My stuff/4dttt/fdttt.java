public class fdttt
{
	boolean turn=true;
	boolean active = true;
	Board temp;
	
	public void act(int r,int c)
	{
		if(!active) return;
		
		temp = main.getBoard();
		
		if(temp.getCellChar(r,c)!=(char)0)
		{
			boolean test=true;
			for(int i=0;i<temp.numRows();i++)
			{
				for(int j=0;j<temp.numCols();j++)
				{
					if(temp.getCellChar(i,j)==(char)0)
					{
						test = false;
						break;
					}
				}
				if(!test) break;
			}
			if(test)
			{
				System.out.println("Cats game");
				System.exit(0);
			}
			System.out.println("Invalid move");
			return;
		}
		
		temp.setCellIcon(r,c,turn?'x':'o');
		checkWin();
		
		turn = !turn;
	}
	
	public void checkWin()
	{
		char t = turn?'x':'o';
		int count=0;
		
		for(int i=0;i<temp.numRows();i++)
		{
			for(int j=0;j<temp.numCols();j++)
			{
				if(temp.getCellChar(i,j)==t)
					count++;
				else
					count = 0;
				
				if(count==temp.numRows())
				{
					win();
					return;
				}
				
			}
			count = 0;
		}
		
		count = 0;
		
		for(int i=0;i<temp.numCols();i++)
		{
			for(int j=0;j<temp.numRows();j++)
			{
				if(temp.getCellChar(j,i)==t)
					count++;
				else
					count = 0;
				
				if(count==temp.numRows())
				{
					win();
					return;
				}
				
			}
			count = 0;
		}
		
		count =0;
		
		for(int i=0;i<temp.numCols();i++)
		{
			if(temp.getCellChar(i,i)==t)
				count++;
			else
				count=0;
			
			if(count==temp.numRows())
			{
				win();
				return;
			}
		}
		
		count =0;
		for(int i=0,j=temp.numCols()-1;i<temp.numCols();i++)
		{
			if(temp.getCellChar(i,j)==t)
				count++;
			else
				count=0;
			
			if(count==temp.numRows())
			{
				win();
				return;
			}
			j--;
		}
	}
	
	public void win()
	{
		System.out.println((turn?"Player 1":"Player 2")+" wins!");
		active = false;
	}
}