import java.awt.event.*;

public class function implements ActionListener
{
  	int col,row;
  	
  	public function(int r, int c)
  	{
  		col = c;
  		row = r;
  	}
  	
  	public void actionPerformed(ActionEvent x)
  	{
  		main.getGame().act(row,col);
  	}
}