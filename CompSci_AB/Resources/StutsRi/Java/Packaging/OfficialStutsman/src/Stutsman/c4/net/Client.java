/*
 *@author Richard Stutsman
 */

package Stutsman.C4.net;

import java.net.Socket;
import java.net.UnknownHostException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.awt.Color;
import Stutsman.C4.GridAct;
import Stutsman.C4.net.ChatBoard;
import Stutsman.GUI.ChatInput;

//The class that does the actual work of the client program.
//It doesn't actually know how to play connect four; it relies on the server
//for that. It takes the place of the Connect4 class in the offline version,
//as you can see by the fact that it implements GridAct. Rather, this class
//converts the user input into messages and sends over a network as well as
//reads them from the network.
public class Client implements GridAct
{
	Socket soc;
	InputStreamReader reader;
	BufferedReader read;
	PrintWriter writer;
	ChatBoard display;
	ChatInput chatArea;
	boolean attempt=false,active=true,fakeBoard=true,chatOpen=true;
	char nextAction=(char)0;
	String name;
	
	public Client(String n,String ip,int p,ChatInput boarded) 
		throws UnknownHostException,IOException
	{
		//Gets the host to connect to and tries to connect.
		soc = new Socket(ip,p);
		writer = new PrintWriter(soc.getOutputStream());
		
		//Sends the use name as fast as it can. I think I did this for some
		//verification system I had earlier that sucked.
		writer.println(C4Protocol.NAME+" "+n);
		writer.flush();
		
		//Gets the input stream
		reader = new InputStreamReader(soc.getInputStream());
		read = new BufferedReader(reader);
		
		//Gets the chat area. The actual GUI will be created when it gets the
		//information from the server
		chatArea=boarded;
		name = n;
		//Creates a fake board until it gets the information from the server
		//This won't show up normally
		display = new ChatBoard(1,1,Color.black,Color.black,boarded);
		
		//The thread that constantly waits for input from the server
		Thread x = new Thread(new runner());
		x.start();
	}
	
	public ChatBoard getBoard()
	{
		//Ugly method I use to delay returning the GUI until the client
		//Gets the information from the server. Added the active variable
		//To prevent an infinite loop should the connection go out
		while(fakeBoard&&active){}
		return display;
	}
	
	public void notifyEnd()
	{
		//Infinite loop until the client stops working.
		//I'm sure there's a better way to do this, but it works fine for me.
		while(active){}
	}
	
	public boolean act(int r,int c)
	{
		//Calls the most important method. Called by the GUI when the user
		//clicks on the board. This method prevents the func method from being
		//called in a situtation it shouldn't.
		
		//If the user clicks REALLY fast, I don't want it to send more than
		//one move to the server, so I block off the program from processing
		//more than one click at a time.
		if(attempt)
		{
			display.alert("Moving","You have already chosen your move");
			return false;
		}
		
		//Game over man, game over
		if(!active)
		{
			display.alert("Game over","The game is over.");
			return false;
		}
		
		attempt = true; //Start blocking off
		func(r,c); //Actually do the work
		attempt = false; //Stop blocking off
		
		return true; //It worked!
	}
	
	public void send(String mes)
	{
		//Called to send a message to the server for chat.
		writer.println(C4Protocol.sendChat(mes));
		writer.flush();
	}
	
	private void func(int r,int c)
	{
		//Checks a variable set by the thread getting input from the server
		//in order to determine the status of the game and make an action
		//based on the statuts.
		
		switch(nextAction)
		{
			case C4Protocol.WAIT:
				//The game is waiting on an opponent. Do nothing.
				display.alert("Waiting","Waiting on opponent");
				return;
			case C4Protocol.STOP:
				//It's not your turn. Do nothing.
				display.alert("Turn","It's not your turn");
				return;
			case C4Protocol.GO: 
				//It is your turn, send the move to the server.
				writer.println(C4Protocol.sendMove(r,c));
				writer.flush();
				//Another safety measure against multiple actions sent to the
				//server
				nextAction='m';
				return;
			case 'm':
				//You just moved, do nothing
				display.alert("Move","You just moved");
				return;
			case C4Protocol.ERROR:
				//Error!!! System.headashplode();
				display.alert("Error","There was an error.");
				active = false;//Blow up.
				chatOpen=false;//Continue exploding
				return;
			case C4Protocol.BEGIN:
				//>_>
				//Forgot what I used this one for
				display.alert("Start","The game has already begun");
				return;
			case C4Protocol.SUCC:
				//<_<
				display.alert("Server","You have connected to the server");
				return;
			default: 
				//Useful in debugging. Shouldn't show up otherwise.
				display.alert("?","Something happened. Try again");
				return;
		}
		
	}
	
	private void go()
	{
		//Called by the other thread. Gets input from the server.
		//Basically manages the whole thing. Uses the nextAction variable
		//to update stuff.
		try
		{
			boolean test = true;
			String act="";
			char tempread = (char)0;
			
			while(chatOpen) //Keeps chat open even when the game is over
			{
				while(true) //Reads until it gets a message
				{
					act=read.readLine();
					if(act.length()>0)
					{
						tempread = act.charAt(0);
						break;
					}
				}
				switch(tempread) //Analyzes the message
				{
					case C4Protocol.WAIT:
						//Shows up if you don't have an opponent yet
						display.alert("Waiting","Waiting on opponent");
						nextAction = tempread;
						break;
					case C4Protocol.GO:
						//It's your turn
						display.alert("Turn","It's your turn");
						nextAction = tempread;
						break;
					case C4Protocol.WIN:
						//You win; end the game
						display.alert("Winner","You win!");
						active=false;
						break;
					case C4Protocol.DRAW:
						//Draw; end the game
						display.alert("Draw","The game is a draw!");
						active=false;
						break;
					case C4Protocol.ILLEGAL:
						//Illegal move. I think it's if the columns blocked
						//Allows you to move again
						display.alert("Move","Illegal move. Move again.");
						nextAction = C4Protocol.GO;
						break;
					case C4Protocol.LOSE:
						//Lose; end the game
						display.alert("Lose","You lose.");
						active=false;
						break;
					case C4Protocol.FLASH: 
						//Tells the GUI which spaces to flash
						int[] temp = C4Protocol.parseFlash(act);
						display.flash(temp);
						break;
					case C4Protocol.DROP:
						//Tells the GUI where to animate a drop
						int[] obs = C4Protocol.parseDrop(act);
						int x = obs[0];
						int y = obs[1];
						Color z = new Color(obs[2],obs[3],obs[4]);
						display.animateDD(x,y,z);
						break;
					case C4Protocol.ALERT:
						//Display some other message as dictated by the server
						String[] massage = C4Protocol.parseAlert(act);
						display.alert(massage[0],massage[1]);
						break;
					case C4Protocol.ERROR:
						//ERROR!!! System.headashplode();
						display.alert("Error","There was an error");
						nextAction = tempread; //Blow up
						active = false; //Continue exploding
						chatOpen=false; //Finish exploding
						return; //Have a nice day :)
					case C4Protocol.SETTINGS:
						//This actually gets the settings
						Object[] haha = C4Protocol.parseSettings(act);
						display=new ChatBoard(
							(Integer) haha[0],(Integer) haha[1],(Color) haha[2],
							(Color) haha[3],chatArea);
						fakeBoard = false;
						break;
					case C4Protocol.FAIL:
						active = false;
						chatOpen=false;
						return;
					case C4Protocol.SUCC:
						nextAction = tempread;
						break;
					case C4Protocol.QUIT:
						display.alert("Quit","Your opponent has quit");
						active = false;
						chatOpen=false;
						return;
					case C4Protocol.BEGIN:
						display.alert("Start","The game has begun");
						nextAction = tempread;
						break;
					case C4Protocol.CHAT:
						display.alert("usermessage",C4Protocol.parseChat(act));
						break;
					case C4Protocol.STOP:
						display.alert("Move","It is your opponent's turn");
						nextAction = tempread;
						break;
					case C4Protocol.OVER:
						display.alert("Chat","Chat closed");
						chatOpen=false;
						break;
				}
			}
		} catch(Exception ex) {
			active = false;
			chatOpen=false;
			ex.printStackTrace();
			display.alert("Error","There was an error");
			display.alert("Game","The game is over");
			return;
		}
	}
	
	private class runner implements Runnable
	{
		public void run()
		{
			go();
		}
	}
}