/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4.net;

import java.awt.Color;
import Stutsman.C4.ConnectBoard;
import Stutsman.GUI.ChatInput;

//Simply the board with the function of alert overwritten to make it compatable
//with chat.
public class ChatBoard extends ConnectBoard
{
	private ChatInput theChat;
	
	public ChatBoard(int a,int b,Color c,Color d,ChatInput chat)
	{
		super(a,b,c,d);
		theChat = chat;
	}
	
	public void alert(String a,String b)
	{
		if(a.equals("usermessage"))
		{
			theChat.addText(b+"\n");
		} else {
			theChat.addText("Server\t"+b+"\n");
		}
	}
}