/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4.net;

import java.awt.Color;

//I forgot exactly what this class did. I think it was put in to offset
//a circular dependancy in classes.
public class Caller
{
	private Updater service=null;
	
	public void alert(String a,String b)
	{
		service.alert(a,b);
	}
	
	public void animateDD(int r,int c,Color col)
	{
		service.animateDD(r,c,col);
	}
	
	public void flash(int[] locs)
	{
		service.flash(locs);
	}
	
	public void setServer(Updater s)
	{
		service = s;
	}
	
	public void win(int con)
	{
		service.win(con);
	}
	
	public void illegalMove(boolean turn)
	{
		service.illegalMove(turn);
	}
}