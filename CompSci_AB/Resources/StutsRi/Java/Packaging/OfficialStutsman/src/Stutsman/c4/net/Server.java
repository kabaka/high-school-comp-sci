/*
 *@author Richard Stutsman
 */

package Stutsman.C4.net;

import java.awt.Color;
import java.net.Socket;
import java.net.ServerSocket;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Random;
import Stutsman.C4.Connect4;
import Stutsman.C4.GridAct;
import Stutsman.C4.ConnectBoard;

//The server. Handles connections and initiates games.
public class Server
{
	private ServerSocket theserve;
	private int rows, cols;
	private Color back,bord,pl1,pl2;
	
	public Server(int port,int r,int c,Color ba,Color bo,Color p1,Color p2) 
		throws IOException
	{
		theserve = new ServerSocket(port);
		rows = r;
		cols = c;
		back = ba;
		bord = bo;
		pl1 = p1;
		pl2 = p2;
		if(back.equals(pl1)||back.equals(pl2)||pl1.equals(pl2))
		{
			throw new IllegalArgumentException(
				"Player color can't be the background or other player color");
		}
	}
	
	public Server(int port,Color p1,Color p2) throws IOException
	{
		theserve = new ServerSocket(port);
		rows = 6;
		cols = 8;
		back = Color.WHITE;
		bord = Color.BLACK;
		pl1 = p1;
		pl2 = p2;
		if(back.equals(pl1)||back.equals(pl2)||pl1.equals(pl2))
		{
			throw new IllegalArgumentException(
				"Player color can't be the background or other player color");
		}
	}
	
	public Server(int port,int r,int c,Color ba,Color bo) throws IOException
	{
		theserve = new ServerSocket(port);
		rows = r;
		cols = c;
		back = ba;
		bord = bo;
		pl1 = Color.BLUE;
		pl2 = Color.RED;
		if(back.equals(pl1)||back.equals(pl2))
		{
			throw new IllegalArgumentException(
				"Player color can't be the background color");
		}
	}
	
	public Server(int port) throws IOException
	{
		theserve = new ServerSocket(port);
		rows = 6;
		cols = 8;
		back = Color.WHITE;
		bord = Color.BLACK;
		pl1 = Color.BLUE;
		pl2 = Color.RED;
	}
	
	//Woah. This one was even bigger when I had the encryption codes >_>
	//Basically an infinite loop to handle connections and tie them to games.
	public void run()
	{
		Socket cl1,cl2;
		PrintWriter[] write;
		InputStreamReader[] reader;
		BufferedReader[] read;
		String[] names;
		int[] encoded;
		int gCounter=0;
		GameHandler temp;
		Caller call;
		ConnectBoard display;
		GridAct game;
		
		while(true)
		{
			write = new PrintWriter[2];
			reader = new InputStreamReader[2];
			read = new BufferedReader[2];
			names = new String[2];
			call = new Caller();
			display = new ServerBoard(rows,cols,back,bord,call);
			
			try
			{
				cl1 = theserve.accept();
				reader[0] = new InputStreamReader(cl1.getInputStream());
				read[0] = new BufferedReader(reader[0]);
				write[0] = new PrintWriter(cl1.getOutputStream());
				System.out.println(
					"Connection: "+cl1.getRemoteSocketAddress());
				
				write[0].println(C4Protocol.SUCC);
				write[0].flush();
				Thread.sleep(100);
				names[0]=read[0].ready()
					?C4Protocol.parseName(read[0].readLine()):"Player 1";
				System.out.println(names[0]);
				write[0].println(C4Protocol.sendSettings(rows,cols,back,bord));
				write[0].println(C4Protocol.WAIT);
				write[0].flush();
				
				cl2 = theserve.accept();
				reader[1] = new InputStreamReader(cl2.getInputStream());
				read[1] = new BufferedReader(reader[1]);
				write[1] = new PrintWriter(cl2.getOutputStream());
				System.out.println(
					"Connection: "+cl2.getRemoteSocketAddress());
				
				write[1].println(C4Protocol.SUCC);
				write[1].flush();
				Thread.sleep(100);
				names[1] = read[1].ready()
					?C4Protocol.parseName(read[1].readLine()):"Player 2";
				System.out.println(names[1]);
				write[1].println(C4Protocol.sendSettings(rows,cols,back,bord));
				write[1].flush();
				
				game = new Connect4(pl1,pl2,names[0],names[1],display);
				temp = new GameHandler(cl1,cl2,names[0],names[1],game);
				call.setServer(temp);
				Thread nextcon = new Thread(new runner(temp,gCounter));
				System.out.println(
					"Game "+gCounter+" start: "+cl1.getRemoteSocketAddress()
					+" "+cl1.getRemoteSocketAddress());
				nextcon.start();
				gCounter++;
			} catch(IOException ex) {
				try
				{
					write[0].println(C4Protocol.ERROR);
					write[0].flush();
					write[1].println(C4Protocol.ERROR);
					write[1].flush();
				} catch(Exception ex2) {
					ex.printStackTrace();
					continue;
				}
				System.out.println("\nError:");
				ex.printStackTrace();
			} catch(InterruptedException ex) {
				ex.printStackTrace();
			}
			System.out.println("Waiting on next connection...\n");
		}
	}
	
	//Creates a handler for each two connections and lets it run on its own.
	private class runner implements Runnable
	{
		private GameHandler han;
		private int gameNum;
		
		public runner(GameHandler x,int n)
		{
			han = x;
			gameNum = n;
		}
		
		public void run()
		{
			try
			{
				han.start();
				System.out.println("Game "+gameNum+" ended succesfully");
			} catch(Exception ex) {
				han.printER();
				System.out.println("\nError in game "+gameNum+"\n");
				ex.printStackTrace();
			}
		}
	}
}