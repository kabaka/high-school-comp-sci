/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4.net;

import java.net.Socket;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.awt.Color;
import Stutsman.C4.GridAct;

//Server class that is created for every two connections (each game). This
//class handles the game between two clients by sending and recieving their
//messages.
public class GameHandler implements Updater
{
	private Socket client1,client2;
	private PrintWriter[] write = new PrintWriter[2];
	private InputStreamReader[] reader = new InputStreamReader[2];
	private BufferedReader[] read = new BufferedReader[2];
	private String name1,name2,tempin1="",tempin2="";
	private GridAct game;
	private boolean active = true,turn=true,connected=true;
	
	public GameHandler(Socket c1,Socket c2,String n1,String n2,GridAct up) 
		throws IOException
	{
		client1 = c1;
		client2 = c2;
		name1 = n1;
		name2 = n2;
		game = up;
		reader[0] = new InputStreamReader(client1.getInputStream());
		read[0] = new BufferedReader(reader[0]);
		write[0] = new PrintWriter(client1.getOutputStream());
		reader[1] = new InputStreamReader(client2.getInputStream());
		read[1] = new BufferedReader(reader[1]);
		write[1] = new PrintWriter(client2.getOutputStream());
	}
	
	public void start() throws IOException
	{
		Thread a,b,c;
		a = new Thread(new reading1());
		b = new Thread(new reading2());
		c = new Thread(new testing());
		a.start();
		b.start();
		c.start();
		
		while(active)
		{
			write[0].println(C4Protocol.GO+"");
			write[1].println(C4Protocol.STOP+"");
			write[0].flush();
			write[1].flush();
			while(true)
			{
				if(tempin1.length()!=0)
				{
					if(C4Protocol.matches(tempin1,C4Protocol.MOVE))
					{
						int[] temp = C4Protocol.parseMove(tempin1);
						tempin1="";
						if(game.act(temp[0],temp[1])) break;
					}
				}
			}
			if(!active) break;
			turn = !turn;
			write[0].println(C4Protocol.STOP+"");
			write[1].println(C4Protocol.GO+"");
			write[0].flush();
			write[1].flush();
			while(true)
			{
				if(tempin2.length()!=0)
				{
					if(C4Protocol.matches(tempin2,C4Protocol.MOVE))
					{
						int[] temp = C4Protocol.parseMove(tempin2);
						tempin2 = "";
						if(game.act(temp[0],temp[1])) break;
					}
				}
			}
			turn = !turn;
		}		
	}
	
	public void printER()
	{
		write[0].println(C4Protocol.ERROR);
		write[1].println(C4Protocol.ERROR);
	}
	
	public void alert(String a,String b)
	{
		write[0].println(C4Protocol.sendAlert(a,b));
		write[1].println(C4Protocol.sendAlert(a,b));
		write[0].flush();
		write[1].flush();
	}
	
	public void animateDD(int r,int c,Color col)
	{
		write[0].println(C4Protocol.sendDrop(r,c,col));
		write[1].println(C4Protocol.sendDrop(r,c,col));
		write[0].flush();
		write[1].flush();
	}
	
	public void flash(int[] locs)
	{
		if(locs==null) return;
		write[0].println(C4Protocol.sendFlash(locs));
		write[1].println(C4Protocol.sendFlash(locs));
		write[0].flush();
		write[1].flush();
	}
	
	public void win(int con)
	{
		if(con==0)
		{
			write[0].println(C4Protocol.DRAW);
			write[1].println(C4Protocol.DRAW);
			write[0].flush();
			write[1].flush();
			active=false;
		} else {
			write[0].println(con==1?C4Protocol.WIN:C4Protocol.LOSE);
			write[1].println(con==2?C4Protocol.WIN:C4Protocol.LOSE);
			write[0].flush();
			write[1].flush();
			active = false;
		}
	}
	
	public void illegalMove(boolean turn)
	{
		write[turn?0:1].println(C4Protocol.ILLEGAL);
		write[turn?0:1].flush();
	}
	
	private class testing implements Runnable
	{
		public void run()
		{
			while(true)
			{
				if(!client1.isConnected())
				{
					try
					{
						write[1].println(C4Protocol.QUIT);
						write[1].flush();
					} catch (Exception ex) {
					}
					connected = false;
					active = false;
					return;
				}
				if(!client2.isConnected())
				{
					try
					{
						write[0].println(C4Protocol.QUIT);
						write[0].flush();
					} catch (Exception ex) {
					}
					connected = false;
					active = false;
					return;
				}
			}
		}
	}
	private class reading1 implements Runnable
	{
		public void run()
		{
			String input1;
			while(connected)
			{
				try
				{
					input1 = read[0].readLine();
					tempin1 = new String(input1);
				} catch(IOException ex) {
					continue;
				}
				if(input1.length()!=0)
				{
					if(C4Protocol.matches(input1,C4Protocol.CHAT))
					{
						input1 = C4Protocol.parseChat(input1);
						input1 = name1+": "+input1;
						input1 = C4Protocol.sendChat(input1);
						write[0].println(input1);
						write[1].println(input1);
						write[0].flush();
						write[1].flush();
					}
				}
			}
		}
	}
	private class reading2 implements Runnable
	{
		public void run()
		{
			String input2;
			while(connected)
			{
				try
				{
					input2 = read[1].readLine();
					tempin2 = new String(input2);
				} catch(IOException ex) {
					continue;
				}
				if(input2.length()!=0)
				{
					if(C4Protocol.matches(input2,C4Protocol.CHAT))
					{
						input2 = C4Protocol.parseChat(input2);
						input2 = name2+": "+input2;
						input2 = C4Protocol.sendChat(input2);
						write[0].println(input2);
						write[1].println(input2);
						write[0].flush();
						write[1].flush();
					}
				}
			}
		}
	}
}