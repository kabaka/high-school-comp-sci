/*
 *@author Richard Stutsman
 */

package Stutsman.C4.net;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Stutsman.C4.net.Client;
import Stutsman.GUI.ChatInput;

//The action listener for the send button. Necessary for the chat feature
public class ChatListener implements ActionListener
{
	private ChatInput chat;
	private Client send;
	
	public ChatListener(Client cl,ChatInput x)
	{
		send = cl;
		chat = x;
	}
	
	public void actionPerformed(ActionEvent x)
	{
		//Sends the text in the box and checks that it's not empty.
		//If you needed a censoring program or one that would format the
		//chat messages, it would go here with temp as a argument.
		String temp = chat.getInput();
		if(temp.equals("")) return;
		chat.setInput("");
		send.send(temp);
	}
}