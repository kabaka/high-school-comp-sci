/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4;

public interface GridAct //Interface to declare that a class is capable
//of handling an action that occurs on the board, specifically when a move
//is specified at that point
{
	public boolean act(int r,int c);
}