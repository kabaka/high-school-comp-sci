/*
 *@author Richard Stutsman
 */
 
package Stutsman.cryptography;

import java.util.Random;

//Encryption stuff that got removed due to lag.
public class randomCode
{	
	public static int[] encode(String x,int set1,int set2,int set3,int set4)
	{
		Random r = new Random(set1);
		Random s = new Random(set2);
		Random t = new Random(set3);
		
		int num,ran1,ran2,ran3,a,b;
		
		int[] ret = new int[x.length()+4];
		ret[0] = set1;
		ret[1] = set2;
		ret[2] = set3;
		ret[3] = set4;
		
		for (int i=0;i<ret.length-4;i++)
		{
			num = (int)x.charAt(i);
			ran1 = r.nextInt(set4);
			ran2 = s.nextInt(set4);
			ran3 = t.nextInt(set4);
			a = num^ran1;
			b = a^ran2;
			ret[i+4] = b^ran3;
		}
		return ret;
	}
	
	public static String decode(int[] nums)
	{
		Random r = new Random(nums[0]);
		Random s = new Random(nums[1]);
		Random t = new Random(nums[2]);
		String ret="";
		int ran1,ran2,ran3,a,b,c;
		
		for(int i=4;i<nums.length;i++)
		{
			c = nums[i];
			ran1 = r.nextInt(nums[3]);
			ran2 = s.nextInt(nums[3]);
			ran3 = t.nextInt(nums[3]);
			b = c^ran3;
			a = b^ran2;
			ret += ""+(char)(a^ran1);
		}
		
		return ret;
	}
} 
