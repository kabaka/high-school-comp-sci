/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4.net;

import java.awt.Color;
import Stutsman.C4.ConnectBoard;

//The server's board for each game. It could be displayed, but it probably
//wouldn't be pretty (in terms of memory usage and effectiveness). The only
//difference between this and its super class is that it calls a "Caller" to
//tell the server that a certain event has happened on its board, and as such
//must update the clients' boards.
public class ServerBoard extends ConnectBoard
{
	private Caller update;
	
	public ServerBoard(int a,int b,Color c,Color d,Caller x)
	{
		super(a,b,c,d);
		update = x;
	}
	
	public void alert(String a,String b)
	{
		update.alert(a,b);
	}
	
	public void animateDD(int r,int c,Color col)
	{
		super.animateDD(r,c,col);
		update.animateDD(r,c,col);
	}
	
	public void flash(int[] locs)
	{
		update.flash(locs);
		super.flash(locs);
	}
	
	public void win(int code)
	{
		update.win(code);
	}
	
	public void illegalMove(boolean turn)
	{
		update.illegalMove(turn);
	}
}