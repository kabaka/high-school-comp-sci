/*
 *@author Richard Stutsman
 */

package Stutsman.GUI;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Dimension;

public class Board extends JPanel //Board consisting of a grid of buttons
{
	private JButton[][] cellGrid; //The matrix of buttons
	private int numRows; //Rows
	private int numCols; //Columns
	private Color backGround; //Background color
	
	public Board(int nr, int nc,Color back, Color bord) //New board with
	//Specified number of rows and columns, with the specified background
	//and border color.
	{
		setBackground(back); //Sets background
		backGround=back; //Updates the variable storing the background color
		numRows = nr; //Updates rows
		numCols = nc; //Updates columns
		int cellSize = cellDim(); //Finds each cell size
		setSize(nc * cellSize, nr * cellSize); //Sets the panel's size
		setLayout(new GridLayout(nr, nc)); //New layout
		
		cellGrid = new JButton[nr][nc]; //Creates the matrix
		
		for(int i=0;i<nr;i++) //Loop to set up each button
		{
			for(int j=0;j<nc;j++)
			{
				cellGrid[i][j] = new JButton();
				cellGrid[i][j].setBackground(back);
				cellGrid[i][j].setBorder(BorderFactory.createLineBorder(bord));
				cellGrid[i][j].setPreferredSize(
					new Dimension(cellSize, cellSize));
				add(cellGrid[i][j]);
			}
		}
	}
	
	public int getRows() //returns number of rows
	{
		return numRows;
	}
	
	public int getCols() //returns number of columns
	{
		return numCols;
	}
	
	public JButton getCellButton(int r, int c) //Returns button at (row,column)
	{
		return cellGrid[r][c];
	}
	
	public Color getBackground() //Returns the background
	{
		return backGround;
	}
	
	private int cellDim() //Returns the size of every button
	{
		int numCells;
		
		if(getRows() < getCols())
			numCells = getCols();
		else
			numCells = getRows();
			
		return 2 * (250/numCells);
	}
	
	public void setCellColor(int r,int c, Color color) //Sets color at
	//(row,column) to specified color
	{
		cellGrid[r][c].setBackground(color);
	}
	
	public void clearCellColor(int r,int c) //Sets color at (row,column) to
	//the background color
	{
		cellGrid[r][c].setBackground(backGround);
	}
	
	public void alert(String title,String message) //Displays a message
	{
		JOptionPane.showMessageDialog(null,
        message, title, JOptionPane.ERROR_MESSAGE);
	}
}