/*
 *@author Richard Stutsman
 */

package Stutsman.GUI;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;

//Panel designed for chats or general text input and display.
//I'm way too lazy to actually comment this code really well.
public class ChatInput extends JPanel
{
	JTextArea field;
	JScrollPane thescroll;
	userInput input;
	
	//Sets up the area, with specified heigth, width, and character limit.
	public ChatInput(int h,int w,int c)
	{
		input = new userInput(c);
		field = new JTextArea(h,w);
		field.setEditable(false);
		field.setLineWrap(true);
		JScrollPane thescroll = new JScrollPane(field);
		thescroll.setHorizontalScrollBarPolicy(
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		thescroll.setVerticalScrollBarPolicy(
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		setLayout(new BorderLayout());
        add(thescroll, BorderLayout.CENTER);
        add(input,BorderLayout.SOUTH);
	}
	
	//Adds text to the main text box. Also contains code so that if the scroll
	//bar is on the bottom before the text is added, move it to the bottom of
	//the area after the text is added.
	public void addText(String x)
	{
		try
		{
			boolean temp;
			temp = thescroll.getVerticalScrollBar().getValue()==
				thescroll.getVerticalScrollBar().getMaximum();
			
			field.append(x);
			
			if(temp) thescroll.getVerticalScrollBar().setValue(
				thescroll.getVerticalScrollBar().getMaximum());
		
		} catch(NullPointerException ex) {
			field.append(x);
		}
	}
	
	public void addClickListener(ActionListener x)
	{
		input.addClickListener(x);
	}
	
	public String getInput()
	{
		return input.getInput();
	}
	
	public void setInput(String x)
	{
		input.setInput(x);
	}
	
	//The input portion of the chat area. Contains the button and form.
	private class userInput extends JPanel
	{
		JTextField theInput;
		JButton click;
		
		public userInput(int c)
		{
			theInput = new JTextField(c);
			click = new JButton("Send");
			
			add(theInput);
			add(click);
		}
		
		public void addClickListener(ActionListener x)
		{
			click.addActionListener(x);
			theInput.addActionListener(x);
		}
		
		public String getInput()
		{
			return theInput.getText();
		}
		
		public void setInput(String x)
		{
			theInput.setText(x);
		}
	}
}