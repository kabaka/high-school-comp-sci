/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4;

import java.awt.Color;

public class Connect4 implements GridAct
{
	private Color player1,player2; //Color of each player
	private String p1name,p2name; //Each players' names
	private boolean turn =true;  // Which turn it is
	private boolean active = true; //Whether the game is still going or not
	private ConnectBoard display; //The board to display events on
	
	public Connect4(Color p1,Color p2,ConnectBoard d)
	{
		player1 = p1;
		p1name = "Player 1";
		player2 = p2;
		p2name = "Player 2";
		display = d;
	}
	
	public Connect4(Color p1,Color p2,String a,String b,ConnectBoard d)
	{
		player1 = p1;
		p1name = a;
		player2 = p2;
		p2name = b;
		display = d;
	}
	
	public boolean act(int r, int c) //Called when someone moves
	//Returns true if move is succesful, false if not
	{
		if(!active) return false; //Failed because game is over
		
		int i;
		
		if(!display.isEmpty(0,c)) //Empty at the top row, initiate check
		{
			boolean test=true;
			
			//For each column, check if top row is full
			for(int j=0;j<display.getCols();j++)
			{
				if(display.isEmpty(0,j))
				{
					test=false; //There is at least one move left
					break;
				}
			}
			
			if(test) //There are no moves left: end the game
			{
				win((int[])null,0);
				active=false;
				return false;
			}
			
			display.illegalMove(turn); //Indicate an illegal move
			return false; //You can't move into a full column: move failed
		}
		
		//Locates lowest empty row
		for(i=0;i<display.getRows()-1;i++)
		{
			if(!display.isEmpty(i+1,c))	
				break;
		}
		
		display.animateDD(i,c,turn?player1:player2); //Displays move
		
		int[] thAR=null; //Empty array for the winning four pieces
		
		thAR = checkForWin(i,c,turn?player1:player2); //Finds which four pieces
		//win the game
		
		if(thAR!=null) //If the array is not empty, someone has won.
		{
			win(thAR,turn?1:2); //Win the game
			return true; //Move valid
		}
		
		turn = !turn; //Finished moving, switch turns.
		autoMove(turn);
		return true; //Move valid
	}
	
	protected void autoMove(boolean s)
	{
	}
	
	private int[] checkForWin(int r,int c,Color play) //Returns the locations
	//of the pieces that win
	{
		int[] theAR = new int[8]; //Stores four locations
		int count=0; //Counting variable
		
		for(int i=r-3;i<=r+3;i++) //Checks vertically
		{
			try
			{
				if(display.getColor(i,c).equals(play))
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
			
			if(count>=4) //Four in a row, proceed to locate winning locations
			{
				for(int ret=3;ret>=0;ret--) //Populate array
				{
					theAR[ret*2+1] = c;
					theAR[ret*2]   = i;
					i--;
					
				}
				return theAR; //Return locations
			}
		}
		
		count=0; //Resets variable
		
		for(int j=c-3;j<=c+3;j++) //Checks horizontally
		{
			try
			{
				if(display.getColor(r,j).equals(play))
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
				
			if(count==4)
			{
				for(int ret=3;ret>=0;ret--)
				{
					theAR[ret*2+1] = j;
					theAR[ret*2]   = r;
					j--;
				}
				return theAR;
			}
		}
		
		count=0;
		
		for(int i=r-3,j=c-3;(i<=r+3)&&(j<=c+3);i++) //Checks diagonally
		//Starts at four left and four up, ends four right and four down
		{
			try
			{
				if(display.getColor(i,j).equals(play))
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
				
			if(count==4)
			{
				for(int ret=3;ret>=0;ret--)
				{
					theAR[ret*2+1] = j;
					theAR[ret*2]   = i;
					j--;
					i--;
				}
				return theAR;
			}
				
			j++;
		}
		
		count=0;
		
		for(int i=r-3,j=c+3;(i<=r+3)&&(j>=c-3);i++) //Checks diagonally
		//Starts four right and four up, ends four left and four down
		{
			try
			{
				if(display.getColor(i,j).equals(play))
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
				
			if(count==4)
			{
				for(int ret=3;ret>=0;ret--)
				{
					theAR[ret*2+1] = j;
					theAR[ret*2]   = i;
					j++;
					i--;
				}
				return theAR;
			}
				
			j--;
		}
		
		return null; //No wins found for specified color, array null
	}
	
	private void win(int[] winAR,int code) //Someone has won
	//code is to tell who has won: 0 is a draw, 1 is player one, and 2 is player
	//two
	{
		active = false; //Game over
		
		Thread x = new Thread(new winDisp(winAR));
		x.start();
		
		display.win(code); //Displays the win
	}
	
	private class winDisp implements Runnable //Code for thread to flash
	//the four winning locations, needed due to infinite loop
	{
		private int[] winners; //Winning locations
		
		public winDisp(int[] myAR)
		{
			winners = myAR;
		}
		
		public void run()
		{
			display.flash(winners);
		}
	}
}