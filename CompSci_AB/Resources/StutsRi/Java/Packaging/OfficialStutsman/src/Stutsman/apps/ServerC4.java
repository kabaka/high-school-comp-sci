/*
 *@author Richard Stutsman
 */
 
package Stutsman.apps.ServerC4;

import java.io.PrintStream;
import java.io.File;
import java.io.FileOutputStream;
import java.awt.Color;
import javax.swing.JOptionPane;
import Stutsman.C4.net.Server;

//The main of the server. Just ties it together.
public class ServerC4
{
	
	public static void main(String[] args)
	{
		try
		{
			Server theserve;
			boolean v=false;
			int port;
			port = Integer.parseInt(input("8080","Enter port number"));
			if(ask("Logging","Enable logging?")==0)
			{
				File logfile = new File("ServerLog.txt");
				FileOutputStream fiout = new FileOutputStream(logfile,true);
				PrintStream sout = new PrintStream(fiout,true);
				System.setOut(sout);
			}
			if(ask("Default","Use default settings?")==0)
			{
				theserve = new Server(port);
			} else {
				int r,c;
				r = Integer.parseInt(input("6","Enter the number of rows"));
				c = Integer.parseInt(input("8","Enter the number of columns"));
				if(ask("Colors","Enter color settings?")==0)
				{
					Color[] cs = new Color[4];
					String type,temp;
					String[] rgb;
					
					for(int i=0;i<4;i++)
					{
						switch(i)
						{
							case 0: type = " the background "; break;
							case 1: type = " the borders "; break;
							case 2: type = " player 1 "; break;
							case 3: type = " player 2 "; break;
							default: type = " ? "; break;
						}
						temp = input("0 0 0",
								"Enter the RGB value (separated by spaces) for"+
								type);
						rgb = temp.split(" ");
						cs[i] = new Color(Integer.parseInt(rgb[0]),
												Integer.parseInt(rgb[1]),
												Integer.parseInt(rgb[2]));
						
					}
					
					theserve = new Server(port,r,c,cs[0],cs[1],cs[2],cs[3]);
				} else {
					theserve = new Server(port,r,c,Color.white,Color.black);
				}
			}
			theserve.run();
		} catch(IllegalArgumentException ex) {
			ex.printStackTrace();
			alert("Error","You have entered illegal settings, probably colors");
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Server closed");
		alert("Done","The server has closed (for some reason).");
	}
	
	private static int ask(String title,String message)
	{
		return JOptionPane.showConfirmDialog(null,message,title,
		JOptionPane.YES_NO_OPTION);
	}
	
	private static String input(String title,String message)
	{
		return JOptionPane.showInputDialog(null,message,title);
	}
	
	private static void alert(String title,String message)
	{
		JOptionPane.showMessageDialog(null,
        message, title, JOptionPane.ERROR_MESSAGE);
	}
}