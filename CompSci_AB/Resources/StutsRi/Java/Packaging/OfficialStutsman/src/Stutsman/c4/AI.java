/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4;

import java.awt.Color;
import Stutsman.C4.ConnectBoard;
import Stutsman.C4.GridAct;

public class AI
{
	private ConnectBoard gameB;
	private Color col,bcol,ocol;
	private byte[][] board;
	private int retRow=0,retCol;
	private boolean flag=true;
	
	public AI(byte[][] b)
	{
		flag = false;
		board = b;
	}
	
	public AI(ConnectBoard gameBoard, Color myColor, Color opCol, Color backCol)
	{
		gameB = gameBoard;
		col = myColor;
		ocol = opCol;
		bcol = backCol;
	}
	
	public void move(GridAct game)
	{
		if(flag) updateBoard();
		decide();
		game.act(retRow,retCol);
	}
	
	public void changeBoard(byte[][] b)
	{
		board = b;
		flag = false;
	}
	
	public void changeBoard(ConnectBoard gameBoard, Color myColor,
							Color opCol, Color backCol)
	{
		flag = true;
		gameB = gameBoard;
		col = myColor;
		bcol = opCol;
		ocol = backCol;
	}
	
	private void updateBoard()
	{
		board = new byte[gameB.getRows()][gameB.getCols()];
		
		for(int i=0;i<gameB.getRows();i++)
		{
			for(int j=0;j<gameB.getCols();j++)
			{
				if(col.equals(gameB.getColor(i,j)))
					board[i][j] = 1;
				else if (ocol.equals(gameB.getColor(i,j)))
					board[i][j] = 2;
				else
					board[i][j] = 0;
			}
		}
	}
	
	private void decide()
	{
		int maxorz = 0;
		double maxchance=0,temp;
		for(int i=0;i<board[0].length;i++)
		{
			temp = predict(i,(byte)1,board);
			if(temp>maxchance)
			{
				maxchance = temp;
				maxorz = i;
			}
			
		}
		retCol = maxorz;
	}
	
	private double predict(int c,byte m,byte[][] temp)
	{
		byte[][] bboard = cloneAr(temp);
		int r = move(c,m,bboard);
		double ret = 0;
		if(r==-1) return 0;
		boolean test = checkForWin(r,c,m,bboard);
		if(test&&m==1) return 1;
		else if(test&&m==2) return 0;
		for(int i=0;i<bboard[0].length;i++)
		{
			if(m==1)
			{
				ret = Math.max(ret,predict(i,(byte)2,bboard));
			} else {
				ret += predict(i,(byte)1,bboard);
			}
		}
		return m==1?ret:(ret/bboard[0].length);
		
	}

	private boolean checkForWin(int r,int c,byte p,byte[][] bboard)
	{
		int[] theAR = new int[8]; //Stores four locations
		int count=0; //Counting variable
		
		for(int i=r-3;i<=r+3;i++) //Checks vertically
		{
			try
			{
				if(bboard[i][c]==p)
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
			
			if(count>=4) //Four in a row, proceed to locate winning locations
			{
				return true;
				/*for(int ret=3;ret>=0;ret--) //Populate array
				{
					theAR[ret*2+1] = c;
					theAR[ret*2]   = i;
					i--;
					
				}*/
				//return theAR; //Return locations
			}
		}
		
		count=0; //Resets variable
		
		for(int j=c-3;j<=c+3;j++) //Checks horizontally
		{
			try
			{
				if(bboard[r][j]==p)
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
				
			if(count==4)
			{
				return true;
				/*
				for(int ret=3;ret>=0;ret--)
				{
					theAR[ret*2+1] = j;
					theAR[ret*2]   = r;
					j--;
				}*/
				//return theAR;
			}
		}
		
		count=0;
		
		for(int i=r-3,j=c-3;(i<=r+3)&&(j<=c+3);i++) //Checks diagonally
		//Starts at four left and four up, ends four right and four down
		{
			try
			{
				if(bboard[i][j]==p)
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
				
			if(count==4)
			{
				return true;
				/*
				for(int ret=3;ret>=0;ret--)
				{
					theAR[ret*2+1] = j;
					theAR[ret*2]   = i;
					j--;
					i--;
				}*/
				//return theAR;
			}
				
			j++;
		}
		
		count=0;
		
		for(int i=r-3,j=c+3;(i<=r+3)&&(j>=c-3);i++) //Checks diagonally
		//Starts four right and four up, ends four left and four down
		{
			try
			{
				if(bboard[i][j]==p)
					count++;
				else
					count=0;
			} catch(ArrayIndexOutOfBoundsException ex) {
				count=0;
			}
				
			if(count==4)
			{
				return true;
				/*
				for(int ret=3;ret>=0;ret--)
				{
					theAR[ret*2+1] = j;
					theAR[ret*2]   = i;
					j++;
					i--;
				}*/
				//return theAR;
			}
				
			j--;
		}
		
		return false;
	}

	private int move(int c,byte p,byte[][] bboard)
	{
		if(bboard[0][c]!=0) return -1;
		for(int i=1;i<bboard.length;i++)
		{
			if(bboard[i][c]!=0)
			{
				bboard[i-1][c] = p;
				return i-1;
			}
		}
		bboard[bboard.length-1][c] = p;
		return bboard.length-1;
	}
	
	private byte[][] cloneAr(byte[][] bboard)
	{
		byte[][] ret = new byte[bboard.length][bboard[0].length];
		for(int i=0;i<bboard.length;i++)
		{
			for(int j=0;j<bboard[0].length;j++)
			{
				ret[i][j] = bboard[i][j];
			}
		}
		return ret;
	}
}