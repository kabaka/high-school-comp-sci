/*
 *@author Richard Stutsman
 */
 
package Stutsman.apps;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import Stutsman.C4.ConnectBoard;
import Stutsman.C4.Connect4;
import Stutsman.C4.GridFunction;
import Stutsman.GUI.ChatInput;

//??
public class testC4
{
	private static ConnectBoard theBoard = new ConnectBoard(6, 8,Color.white,
															Color.black);
	private static JFrame frame;
	private static Connect4 game= new Connect4(Color.blue,Color.red,theBoard);
	private static ChatInput chat = new ChatInput(4,10,30);
	
	public static void main(String[] args)
	{
		setUp(theBoard,theBoard.getRows(),theBoard.getCols());
		for(int i=0;i<theBoard.getRows();i++)
		{
			for(int j=0;j<theBoard.getCols();j++)
			{
				JButton temp =theBoard.getCellButton(i,j);
				temp.addActionListener(new GridFunction(i,j,game));
			}
		}
	}
	
	private static void setUp(JPanel display, int xCoord, int yCoord)
	{
		int width = (int)Math.round((display).getSize().getWidth());
		int height = (int)Math.round((display).getSize().getHeight());
		frame = new JFrame("Connect Four");
		frame.show();
		width += frame.getInsets().right + frame.getInsets().left;
		height += frame.getInsets().top + frame.getInsets().bottom;
		height += 105;
		frame.setSize(width,height);
		frame.setLocation(xCoord, yCoord);
		frame.getContentPane().add(display,BorderLayout.NORTH);
		frame.getContentPane().add(chat,BorderLayout.SOUTH);
		frame.show();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}