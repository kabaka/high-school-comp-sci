/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GridFunction implements ActionListener //Listener tied to the
//buttons on the board. Calls the act method of the necessary GridAct to
//carry out the action needed when said button is pushed
{
  	private int col,row;
  	private GridAct performer;
  	
  	public GridFunction(int r, int c,GridAct ac)
  	{
  		col = c;
  		row = r;
  		performer = ac;
  	}
  	
  	public void actionPerformed(ActionEvent x)
  	{
  		performer.act(row,col);
  	}
}