/*
 *@author Richard Stutsman
 *@version 1.67 5/25/06
 */
 
package Stutsman.C4;

import java.awt.Color;
import Stutsman.GUI.Board;

//Specialized version of the standard board meant for playing connect four.
public class ConnectBoard extends Stutsman.GUI.Board
{
	private Color[][] gridTest;
	
	public ConnectBoard(int a, int b, Color c, Color d)
	{
		super(a,b,c,d);
		
		gridTest = new Color[a][b];
		
		for(int i=0;i<a;i++)
		{
			for(int j=0;j<b;j++)
			{
				gridTest[i][j] = getBackground();
			}
		}
	}
	
	public void setCellColor(int r, int c,Color col)
	{
		super.setCellColor(r,c,col);
		gridTest[r][c] = col;
	}
	
	public void clearCellColor(int r,int c)
	{
		super.clearCellColor(r,c);
		gridTest[r][c] = getBackground();
	}
	
	public boolean isEmpty(int r,int c)
	{
		if(gridTest[r][c].equals(getBackground()))
		{
			return true;
		} else {
			return false;
		}
	}
	
	public Color getColor(int r,int c)
	{
		return gridTest[r][c];
	}
	
	public void animateDD(int r, int c, Color paint)
	{
		
		for(int i=0;i<r+1;i++)
		{
			
			setCellColor(i,c,paint);
			paintImmediately(getVisibleRect());
			
			wait(5);
			
			if (i==r)
				break;
			clearCellColor(i,c);
		}
	}
	
	public void flash(int[] theAR)
	{
		if(theAR==null) return;
		if(theAR.length%2!=0) return;
		
		Color[] temp = new Color[theAR.length/2];
		
		for(int i=0;i<temp.length;i++)
		{
			temp[i] = getColor(theAR[2*i],theAR[2*i+1]);
		}
		
		while(true)
		{
			for(int i=0;i<theAR.length/2;i++)
			{
				clearCellColor(theAR[2*i],theAR[2*i+1]);
			}
			
			wait(75);
			
			for(int i=0;i<theAR.length/2;i++)
			{
				setCellColor(theAR[2*i],theAR[2*i+1],temp[i]);
			}
			
			wait(75);
		}
	}
	
	private void wait(int x)
	{
		try
		{
			Thread.sleep(x);
		} catch (InterruptedException ex){
			ex.printStackTrace();
		}
	}
	
	public void win(int code)
	{
		String mess;
		switch(code)
		{
			case 0: mess="No one wins.";break;
			case 1: mess="Player 1 wins!";break;
			case 2: mess="Player 2 wins!";break;
			default:mess="Serious error with code";
		}
		alert("Game over!",mess);
	}
	
	public void illegalMove(boolean player)
	{
		alert("Illegal move","That was an illegal move! Move again.");
	}
}