/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4.net;

import java.awt.Color;

public interface Updater //Interface to declare that a class can update a
//display for a game
{
	public void alert(String a,String b);
	public void animateDD(int r,int c,Color col);
	public void flash(int[] locs);
	public void win(int condition);
	public void illegalMove(boolean turn);
}