/*
 *@author Richard Stutsman
 */

package	Stutsman.C4;

import java.awt.Color;
import Stutsman.C4.Connect4;
import Stutsman.C4.AI;

public class Connect4AI	extends	Connect4
{
	private AI comp;
	public Connect4AI(Color p1,Color p2,ConnectBoard d)
	{
		super(p1,p2,d);
		comp = new AI(d,p1,p2,d.getBackground());
	}
	
	public Connect4AI(Color p1,Color p2,String a,String b,ConnectBoard d)
	{
		super(p1,p2,a,b,d);
		comp = new AI(d,p1,p2,d.getBackground());
	}
	
	protected void autoMove(boolean s)
	{
		if(s) return;
		else {
			comp.move(this);
		}
	}
}