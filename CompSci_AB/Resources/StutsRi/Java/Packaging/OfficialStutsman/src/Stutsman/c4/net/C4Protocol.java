/*
 *@author Richard Stutsman
 */
 
package Stutsman.C4.net;

import java.awt.Color;

public final class C4Protocol //Class to standardize what is sent over the
//internet by the game. Insures that the clients and servers can understand
//the data being recieved and sent
{
	//Data. I regret not using an enumeration. At the time I didn't realize
	//That enums had associated values that would be sent when they were
	//written to a printstream or read from an inputstream
	public static final char WAIT = 'w';
	public static final char GO = 'g';
	public static final char STOP = 's';
	public static final char DROP = 'd';
	public static final char FLASH = 'f';
	public static final char ALERT = 'a';
	public static final char WIN = 'v';
	public static final char LOSE = 'l';
	public static final char MOVE = 'm';
	public static final char ERROR = 'e';
	public static final char SETTINGS = 'z';
	public static final char CHAT = 'c';
	public static final char QUIT = 'q';
	public static final char CODE = 'C';
	public static final char NAME = 'n';
	public static final char FAIL = 'F';
	public static final char SUCC = 'S';
	public static final char BEGIN = 'b';
	public static final char CHECK = 'V';
	public static final char OVER = 'o';
	public static final char DRAW = 'D';
	public static final char ILLEGAL = 'i';
	
	private C4Protocol(){} //Class is not meant to be instantiated: everything
	//is static
	
	//All methods are simply ways of sending the more complex data and parsing
	//said data. The parsing methods use techniques that could throw exceptions,
	//but if the user only passes data to them that is the result of a send
	//method, they will not break. This is not a flaw: the user of this
	//class is expected to be aware of that.
	public static String sendName(String n)
	{
		return NAME+" "+n;
	}
	
	public static String parseName(String n)
	{
		if(n.charAt(0)!=NAME)
		{
			return null;
		} else {
			return n.substring(2);
		}
	}
	
	public static String sendCode(int[] codes)
	{
		String ret = ""+CODE;
		for(int i=0;i<codes.length;i++)
		{
			ret+=" "+codes[i];
		}
		return ret;
	}
	
	public static int[] parseCode(String m)
	{
		if(m.charAt(0)!=CODE)
		{
			return null;
		} else {
			String[] temp = m.split(" ");
			int[] ret = new int[temp.length-1];
			
			for(int i=1;i<temp.length;i++)
			{
				ret[i-1] = Integer.parseInt(temp[i]);
			}
			
			return ret;
		}
	}
	
	public static String sendChat(String m)
	{
		m = CHAT+" "+m;
		return m;
	}
	
	public static String parseChat(String m)
	{
		if(m.charAt(0)!=CHAT&&m.charAt(1)!=' ')
		{
			return null;
		} else {
			return m.substring(2);
		}
	}
	
	public static String sendFlash(int[] locs)
	{
		String x = ""+FLASH;
		for(int a:locs)
		{
			x+=" "+a;
		}
		return x;
	}
	
	public static int[] parseFlash(String x)
	{
		if(x.charAt(0)!=FLASH)
		{
			return null;
		} else {
			String[] temp = x.split(" ");
			int[] ret = new int[temp.length-1];
			for(int i=0;i<ret.length;i++)
			{
				ret[i] = Integer.parseInt(temp[i+1]);
			}
			return ret;
		}
	}
	
	public static String sendAlert(String a,String b)
	{
		String ret = ""+ALERT;
		ret+="\799"+a+"\799"+b;
		return ret;
	}
	
	public static String[] parseAlert(String x)
	{
		if(x.charAt(0)!=ALERT)
		{
			return null;
		} else {
			String[] temp = x.split("\799");
			String[] ret = new String[temp.length-1];
			for(int i=0;i<ret.length;i++)
			{
				ret[i]=temp[i+1];
			}
			return ret;
		}
	}
	
	public static String sendMove(int r,int c)
	{
		String temp =  ""+MOVE;
		temp+= " "+r+" "+c;
		return temp;
	}
	
	public static int[] parseMove(String m)
	{
		if(m.charAt(0)!=MOVE)
		{
			return null;
		} else {
			String[] temp = m.split(" ");
			int[] ret = new int[2];
			ret[0] = Integer.parseInt(temp[1]);
			ret[1] = Integer.parseInt(temp[2]);
			return ret;
		}
	}
	
	public static String sendDrop(int r,int c,Color col)
	{
		String ret = ""+DROP;
		ret += " "+r+" "+c+" "+col.getRed()+" "+col.getGreen();
		ret += " "+col.getBlue();
		return ret;
	}
	
	public static int[] parseDrop(String d)
	{
		if(d.charAt(0)!=DROP)
		{
			return null;
		} else {
			String[] temp = d.split(" ");
			int[] ret = new int[5];
			ret[0] = Integer.parseInt(temp[1]);
			ret[1] = Integer.parseInt(temp[2]);
			ret[2] = Integer.parseInt(temp[3]);
			ret[3] = Integer.parseInt(temp[4]);
			ret[4] = Integer.parseInt(temp[5]);
			return ret;
		}
	}
	
	public static String sendSettings(int r,int c,Color back,Color bord)
	{
		String ret = ""+SETTINGS;
		ret += " "+r+" "+c+" "+back.getRed()+" "+back.getGreen();
		ret += " "+back.getBlue()+" "+bord.getRed()+" "+bord.getGreen();
		ret += " "+bord.getRed();
		return ret;
	}
	
	public static Object[] parseSettings(String x)
	{
		if(x.charAt(0)!=SETTINGS)
		{
			return null;
		} else {
			Object[] temp = new Object[4];
			String[] sets = x.split(" ");
			temp[0] = Integer.parseInt(sets[1]);
			temp[1] = Integer.parseInt(sets[2]);
			temp[2] = new Color(Integer.parseInt(sets[3]),
								Integer.parseInt(sets[4]),
								Integer.parseInt(sets[5]));
			temp[3] = new Color(Integer.parseInt(sets[6]),
								Integer.parseInt(sets[7]),
								Integer.parseInt(sets[8]));
			return temp;
		}
	}
	
	public static boolean matches(String x,char p)
	{
		char temp = x.charAt(0);
		switch(temp)
		{
			case WAIT:if(p==WAIT) return true; break;
			case GO: if(p==GO) return true; break;
			case STOP: if(p==STOP) return true; break;
			case DROP: if(p==DROP) return true; break;
			case FLASH:if(p==FLASH) return true; break;
			case ALERT:if(p==ALERT) return true; break;
			case WIN: if(p==WIN) return true; break;
			case LOSE:if(p==LOSE) return true; break;
			case MOVE:if(p==MOVE) return true; break;
			case ERROR:if(p==ERROR) return true;break;
			case SETTINGS:if(p==SETTINGS) return true; break;
			case QUIT:if(p==QUIT) return true; break;
			case CHAT:if(p==CHAT) return true; break;
			case CODE:if(p==CODE) return true; break;
			case NAME:if(p==NAME) return true; break;
			case FAIL:if(p==FAIL) return true; break;
			case SUCC:if(p==SUCC) return true; break;
			case BEGIN:if(p==BEGIN) return true; break;
			case CHECK:if(p==CHECK) return true; break;
		}
		return false;
	}
}