/*
 *@author Richard Stutsman
 */
 
package Stutsman.apps;

import java.net.UnknownHostException;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import Stutsman.C4.ConnectBoard;
import Stutsman.C4.net.Client;
import Stutsman.C4.net.ChatListener;
import Stutsman.C4.GridFunction;
import Stutsman.GUI.ChatInput;

//The main of the client version of connect four. Just ties all the classes
//together and lets it run.
public class ClientC4
{
	private static JFrame frame;
	private static ChatInput chat = new ChatInput(4,30,30);
	
	public static void main(String[] args)
	{
		Client service;
		ConnectBoard theBoard;
		String a,b,d;
		int c;
		a=input("127.0.0.1","Enter the IP of the server you will connect to");
		b=input("8080","Enter the port number that you will connect to");
		c=Integer.parseInt(b);
		d=input("Player","Enter the name you want to go by on the server");
		
		while(true)
		{
			try
			{
				service = new Client(d,a,c,chat);
			} catch(UnknownHostException ex) {
				alert("Bad host","Trying to connect to unknown host.");
				return;
			} catch(IOException ex) {
				alert("Error","There has been an error");
				return;
			} catch(Exception ex) {
				alert("Error","Unknown error");
				return;
			}
			
			theBoard = service.getBoard();
			if(theBoard==null)
			{
				alert("Error","Server connection error");
				return;
			}
			
			chat.addClickListener(new ChatListener(service,chat));
			for(int i=0;i<theBoard.getRows();i++)
			{
				for(int j=0;j<theBoard.getCols();j++)
				{
					JButton temp =theBoard.getCellButton(i,j);
					temp.addActionListener(new GridFunction(i,j,service));
				}
			}
			
			setUp(theBoard,theBoard.getRows(),theBoard.getCols());
			
			service.notifyEnd();
			
			if(ask("Again?","Play again?")==1) break;
		}
	}
	
	private static int ask(String title,String message)
	{
		return JOptionPane.showConfirmDialog(null,message,title,
		JOptionPane.YES_NO_OPTION);
	}
	
	private static String input(String title,String message)
	{
		return JOptionPane.showInputDialog(null,message,title);
	}
	
	private static void alert(String title,String message)
	{
		JOptionPane.showMessageDialog(null,
        message, title, JOptionPane.ERROR_MESSAGE);
	}
	
	private static void setUp(JPanel display, int xCoord, int yCoord)
	{
		int width = (int)Math.round((display).getSize().getWidth());
		int height = (int)Math.round((display).getSize().getHeight());
		frame = new JFrame("Connect Four");
		frame.show();
		width += frame.getInsets().right + frame.getInsets().left;
		height += frame.getInsets().top + frame.getInsets().bottom;
		height += 105;
		frame.setSize(width,height);
		frame.setLocation(xCoord, yCoord);
		frame.getContentPane().add(display,BorderLayout.NORTH);
		frame.getContentPane().add(chat,BorderLayout.SOUTH);
		frame.show();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}