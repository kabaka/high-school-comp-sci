/**
 *  Implements a board game applet with someone
 *  playing against the computer
 */
import java.awt.*;
import javax.swing.*;

public class BoardGameApplet extends JApplet
{
  private ChompGame game;
  private JTextField display;
  private Player players[];
  private int currentPlayer;

  public void init()
  {
    Container c = getContentPane();

    display = new JTextField(20);
    display.setBackground(Color.yellow);
    display.setEditable(false);
    c.add(display, BorderLayout.NORTH);

    ChompBoard board = new ChompBoard();
    c.add(board, BorderLayout.CENTER);

    game = new ChompGame(board);

    players = new Player[2];
    players[0] = new HumanPlayer(this, game, board);
    players[1] = new ComputerPlayer(this, game, board);
    currentPlayer = 0;                   // optional -- default

    display.setText(" You go first...");
    players[currentPlayer].makeMove();
  }

  /**
   *  Called by the player when its move is completed.
   */
  public void hasMoved()
  {
    currentPlayer = (currentPlayer + 1) % 2;
    Player p = players[currentPlayer];
    ...
  }
}

