/**
 *  Implements a 2-D array of characters
 */

public class CharMatrix
{
  // fields:
    ...

  /**
   *  Constructor:
   *  Creates a grid with dimensions <code>rows</code>, <code>cols</code>,
   *  and fills it with the <code>fill</code> character
   */
  public CharMatrix(int rows, int cols, char fill)
  {
    ...
  }

  /**
   *  Fills the given rectangle with <code>fill</code> characters.
   *  row0, col0 is the upper left corner and row1, col1 is the
   *  lower right corner of the rectangle.
   */
  public void fillRect(int row0, int col0, int row1, int col1, char fill)
  {
    ...
  }

  /**
   *  Fills the given rectangle with SPACE characters.
   *  row0, col0 is the upper left corner and row1, col1 is the
   *  lower right corner of the rectangle.
   */
  public void clearRect(int row0, int col0, int row1, int col1)
  {
    ...
  }

  /**
   *  Returns the number of rows in grid
   */
  public int numRows()
  {
    ...
  }

  /**
   *  Returns the number of columns in grid
   */
  public int numCols()
  {
    ...
  }

  /**
   *  Returns the character at row, col position
   */
  public char charAt(int row, int col)
  {
    ...
  }

  /**
   *  Returns true if the character at row, col is a space,
   *  false otherwise
   */
  public boolean isEmpty(int row, int col)
  {
    ...
  }

  /**
   *  Returns the count of all non-space characters in <code>row</code>
   */
  public int countInRow(int row)
  {
    ...
  }

  /**
   *  Returns the count of all non-space characters in <code>col</code>
   */
  public int countInCol(int col)
  {
    ...
  }
}
