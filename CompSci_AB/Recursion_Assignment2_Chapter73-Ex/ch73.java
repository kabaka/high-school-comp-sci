/**
 * @(#)SomeStuff.java
 * BLAH
 *
 * @author Kyle Johnson
 * @version 1.00.00.00.00.00.1-0a release 1 beta 2006/11/9
 */
 import java.util.Scanner;
 import java.util.StringTokenizer;
 import java.util.ArrayList;
public class ch73 {


    /**
     * @param args the command line arguments
     */

    static Scanner in;
    static String mode = "main menu";

    public static void main(String[] args)
    {
    	try
    	{
	    	in = new Scanner(System.in);
	    	String inputText = "";
			boolean exit = false;

			while(!exit)
			{
				for(int i = 30; i > 0; i--)
					System.out.println();
		        System.out.println("Chapter 73!");
		        System.out.println("Choose:");
		        System.out.println("1 - Triangle      5 - Array Sumation");
		        System.out.println("2 - Prime Number  6 - Max Array Element");
		        System.out.println("3 - Cube          7 - Palindrome Detector");
		        System.out.println("4 - Square        Q - Quit");

		        inputText = prompt("Choice");

		        if(inputText.toLowerCase().equals("q"))
		        	exit = true;
		        else
		        {
			        Integer choice = Integer.valueOf(inputText);
			        if(choice == 1)
			        {
			        	mode = "triangle";
				        System.out.println("Answer: " + triangle(Double.valueOf(prompt("Find Triangle of"))));
			        }
			        else if(choice == 2)
			        {
			        	mode = "prime numbers";
						System.out.println("Answer: " + primeNumber(Double.valueOf(prompt("Test for Prime"))));
			        }
			        else if(choice == 3)
			        {
			        	mode = "cube (^3)";
						System.out.println("Answer: " + cube(Double.valueOf(prompt("x^3, x="))));
			        }
			        else if(choice == 4)
			        {
			        	mode = "square (^2)";
						System.out.println("Answer: " + square(Double.valueOf(prompt("x^2, x="))));
			        }
			        else if(choice == 5)
			        {
			        	mode = "array sumation";
			        	System.out.println("List format: 1,2,3,4,5,6");
						System.out.println("Answer: " + arraySum(prompt("enter int list")));
			        }
			        else if(choice == 6)
			        {
			        	mode = "max element";
			        	System.out.println("List format: 1,2,3,4,5,6");
						System.out.println("Answer: " + maxElement(prompt("enter int list")));
			        }
			        else if(choice == 7)
			        {
			        	mode = "palindrome";
						System.out.println("Answer: " + palindrome(prompt("enter text")));
			        }
		        prompt("Press Enter to Continue");
		        }
			}
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    		prompt("Press Enter to Continue...");
    	}
    }

    /* get input from user with a formatted prompt
     *
     * text: text of the prompt shown to user
     */
    private static String prompt(String text)
    {
    	System.out.print("[RecursiveStuff(" + mode + "): " + text + "]: ");
    	return in.nextLine();
    }

    /* triangle
     */
     private static double triangle(double number)
     {
     	if(number == 1)
     		return 1;
     	return number + triangle(number - 1);
     }

     /* prime numbers
      */
	private static boolean primeNumber(double number)
	{
		if(number == 1)
			return true;
		return primeNumber(number, number - 1);
	}

	private static boolean primeNumber(double number, double number2)
	{
		if(number2 == 1)
			return true;
		else if(number % number2 == 0)
			return false;
		return primeNumber(number, number2 - 1);
	}


	/*cube
	 */
	private static double cube(double number)
	{
		return(power(number, 3));
	}

	private static double power(double number, int mult)
	{
		if(mult == 0)
			return 1;
		else
			return number * power(number, mult - 1);
	}


	/*square
	 */
	private static double square(double number)
	{
		return(power(number, 2));
	}

	/*array sum
	 */
	private static int arraySum(String input)
	{
		return sum(stringToIntArrayList(input), 0);
	}

	private static ArrayList<Integer> stringToIntArrayList(String str)
	{
		StringTokenizer tok = new StringTokenizer(str, ",");
		ArrayList<Integer> arry = new ArrayList<Integer>();

		while(tok.hasMoreTokens())
		{
			arry.add(Integer.valueOf(tok.nextToken()));
		}
		return arry;
	}

	private static int sum(ArrayList<Integer> arry, int index)
	{
		if(index == arry.size())
			return 0;
		return arry.get(index) + sum(arry, index + 1);
	}

	/* max element
	 */
	private static int maxElement(String input)
	{
		return maxElement(stringToIntArrayList(input), 0, 0);
	}

	private static int maxElement(ArrayList<Integer> arry, int index, int max)
	{
		if(index == arry.size())
			return max;
		if(arry.get(index) > max)
			max = arry.get(index);
		return maxElement(arry, index + 1, max);
	}

	private static boolean palindrome(String str)
	{
		if(str.length() < 2)
			return true;
		else if(str.charAt(0) != str.charAt(str.length() - 1))
			return false;
		return palindrome(str.substring(1,str.length() - 1));
	}
}

