 import java.util.LinkedList;
 import java.util.ListIterator;
public class theStuff
{
	public static void main(String [] args)
	{
		LinkedList<Integer> numbers = new LinkedList<Integer>();
		
		for(int i = 1; i <= 10; i++)
			numbers.addLast(i);
		
		ListIterator iter = numbers.listIterator();
		ListIterator iterTwo = numbers.listIterator();
		
		while(iter.hasNext())
			System.out.println(iter.next());
		System.out.println("\n\n");
		
		iter = numbers.listIterator();
		
		iter.next();
		iter.next(); // just passed 2
		
		iter.add(5000);
		
		iter.next();
		iter.next();
		iter.next();
		iter.next(); // just passed 6
		
		iter.remove();
		
		iter = numbers.listIterator();
		
		while(iter.hasNext())
			System.out.println(iter.next());
		
		
		//Start simultaneous access test
		
		iter = numbers.listIterator();
		
		iterTwo.next();
		iterTwo.next();
		iterTwo.next(); // 3
		
		iter.next();
		iter.next();
		iter.next(); // 3
		
		//iter.remove();		//take something out concurrently
		
		//numbers.addFirst(0);	//or put something in the beginning, throwing the
								//iterators off
	}
}