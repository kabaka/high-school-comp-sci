public class HexBoard extends CharMatrix
{
  public HexBoard(int rows, int cols)
  {
    super(rows, cols, '.');
  }

  public boolean isBlack(int r, int c)
  {
    return charAt(r, c) == 'B';
  }

  public boolean isWhite(int r, int c)
  {
    return charAt(r, c) == 'W';
  }

  public  boolean isGray(int r, int c)
  {
    return charAt(r, c) == '.';
  }

  public void setBlack(int r, int c)
  {
    setCharAt(r, c, 'B');
  }

  public void setWhite(int r, int c)
  {
    setCharAt(r, c, 'W');
  }

  public void setGray(int r, int c)
  {
    setCharAt(r, c, '.');
  }

  /**
   *  Returns true if there is a contiguous chain of black stones
   *  that starts in col 0 and ends in the last column of the board;
   *  otherwise returns false.
   */
  public boolean blackHasWon()
  {
  	int across = 0;

	for(int i = 0; i < numCols(); i++)
	{
		for(int j = 0; j < numRows(); j++)
		{
			if(isBlack(i, j))
				across++;
			else
				break;
		}
		if(across == numCols())
			return true;
	}
	return false;
  }

  /**
   *  Fills the contiguous area that contains r,c with gray color.
   *  Does nothing if r, c is out of bounds or is not black.
   */
  public void areaFill(int r, int c)
  {
  	//System.out.println("DEBUG: " + r + ", " + c);
    if(!isInBounds(r, c) || !isBlack(r, c))
    	return;
    // up left and down right
    // and up down left right

    if(isBlack(r, c))
    	setGray(r, c);

    if(isInBounds(r - 1, c - 1) && isBlack(r - 1, c - 1)) // up left
    	areaFill(r - 1, c - 1);
    if(isInBounds(r - 1, c) && isBlack(r - 1, c)) // up
    	areaFill(r - 1, c);
    if(isInBounds(r, c - 1) && isBlack(r, c - 1)) // left
    	areaFill(r, c - 1);
    if(isInBounds(r, c + 1) && isBlack(r, c + 1)) // right
    	areaFill(r, c + 1);
    if(isInBounds(r + 1, c + 1) && isBlack(r + 1, c + 1)) //right down
    	areaFill(r + 1, c + 1);
    if(isInBounds(r + 1, c) && isBlack(r + 1, c)) // down
    	areaFill(r + 1, c);
  }

  public String toString()
  {
    String s = "";

    for (int r = 0; r < numRows(); r++)
    {
      for (int c = 0; c < numCols(); c++)
      {
        if (isBlack(r, c))
          s += 'B';
        else if (isWhite(r, c))
          s += 'W';
        else if (isGray(r, c))
          s += 'X';
        else
          s += '.';
      }
      s += '\n';
    }
    return s;
  }

  //****************************************************************

  private boolean isInBounds(int row, int col)
  {
	return (row < numRows() && row >= 0 && col < numCols() && col >= 0);
  }
}
