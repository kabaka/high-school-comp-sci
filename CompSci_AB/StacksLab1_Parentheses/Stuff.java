import java.util.Stack;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Stuff
{
	public static void main(String[] args)
	{
		String input = "";
		Scanner in = new Scanner(System.in);
		
		printBanner();
		
		while(!input.toLowerCase().trim().equals("exit"))
		{
			try
			{
				System.out.print("Input expression: ");
				input = in.nextLine().trim();
				if(!input.toLowerCase().equals("clear") && !input.toLowerCase().equals("clr"))
					if(!isInfix(input))
						evalPostfix(input);
					else
						evalPostfix(infixToPostfix(input));
				else
				{
					for(int i = 0; i < 30; i ++)
						System.out.println();
					printBanner();
				}
			}
			catch(Exception e)
			{
				if(!input.equals("exit"))
					System.out.println("Error (L053R): Incompetent User");
			}
		}
		
	}
	
	private static void evalPostfix(String expression)
	{
		StringTokenizer tok = new StringTokenizer(expression);
		Stack<Double> operands = new Stack<Double>();
		String temp = "";
		
		while(tok.hasMoreTokens())
		{
			temp = tok.nextToken();
			if(Character.isDigit(temp.charAt(0)))
				operands.push(Double.valueOf(temp));
			else
			{
				double first = operands.pop();
				double second = operands.pop();
				if(temp.equals("+"))
					operands.push(second + first);
				else if(temp.equals("*"))
					operands.push(second * first);
				else if(temp.equals("/"))
					operands.push(second / first);
				else if(temp.equals("-"))
					operands.push(second - first);
			}
			
			System.out.println(operands.toString());
		}
	}
	
	private static String infixToPostfix(String expression)
	{
		System.out.print("Converting to postfix...");
		
		Stack<String> operators = new Stack<String>();
		String postfixExpression = "";
		
		for(int i = 0; i < expression.length(); i++)
		{
			char current = expression.charAt(i);
			
			if(current == ' ' ) continue;
			
			else if(current == '(')
				operators.push(current + "");
				
			else if(current == ')')
			{
				while(!operators.peek().equals("("))
					postfixExpression += " " + operators.pop();
				operators.pop();
			}
			
			else if(!Character.isDigit(current))	//if we have an operator
			{
				while(!operators.empty())
					if(!operators.peek().equals("("))
						postfixExpression += " " + operators.pop();
					else
						break;
				
				operators.push(current + "");
			}
			
			else							//or an operand
				postfixExpression += " " + current;
				
			//System.out.println(operators.toString() + "\t" + postfixExpression);
		}
		
		while(!operators.empty())
			postfixExpression += " " + operators.pop();
		
		
		System.out.println(postfixExpression);
		return postfixExpression;
	}
	
	private static boolean isInfix(String expression)
	{
		return (Character.isDigit(expression.charAt(expression.length() - 1)) ||
				(expression.charAt(expression.length() - 1)) == ')');
	}
	
	private static void printBanner()
	{
		System.out.println("**************************************");
		System.out.println("*Infix / Postfix Expression Evaluator*");
		System.out.println("**************************************");
		System.out.println("        [Type exit to exit]");
		System.out.println();
	}
}