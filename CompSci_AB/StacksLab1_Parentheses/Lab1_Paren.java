import java.util.Stack;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Lab1_Paren
{
	static Stack<Character> stk;
	public static void main(String[] args)
	{
		stk = new Stack<Character>();
		String input = "";
		Scanner in = new Scanner(System.in);	
		while(!input.toLowerCase().equals("exit"))
		{
			System.out.print("Enter parenthetically notated expression: ");
			input = in.nextLine();
			
			if(testForCorrectness(input))
				System.out.println("Parentheses are correct");
			else
				System.out.println("Parentheses are incorrect");
		}
	}
	
	private static boolean testForCorrectness(String expression)
	{
		StringTokenizer
		int countOpen = 0, countClose = 0;
		for(int i = 0; i < expression.length(); i++)
			stk.push(expression.charAt(i));
		while(!stk.empty())
		{
			char cur = stk.pop();
			if(cur == '(') countOpen++;
			else if(cur == ')') countClose++;
		}
		
		if(countOpen == countClose)
			return true;
		
		return false;
	}
}	