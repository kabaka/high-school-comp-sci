// Implements an iterator for a SinglyLinkedList.

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedListIterator implements Iterator<Object>
{
  private ListNode2 nextNode;
  private ListNode2 previousNode;

  // Constructor
  public DoublyLinkedListIterator(ListNode2 head)
  {
    nextNode = head;
    previousNode = null;
  }

  public boolean hasNext()
  {
    return nextNode != null;
  }

  public Object next()
  {
    if (nextNode == null)
      throw new NoSuchElementException();
    
    previousNode = nextNode;

    Object value = nextNode.getValue();
    nextNode = nextNode.getNext();
    return value;
  }

  public boolean hasPrevious()
  {
    return previousNode != null;
  }

  public Object previous()
  {
    if (previousNode == null)
      throw new NoSuchElementException();
      
    nextNode = previousNode;

    Object value = previousNode.getValue();
    previousNode = previousNode.getNext();
    return value;
  }

  // Not implemented.
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}
