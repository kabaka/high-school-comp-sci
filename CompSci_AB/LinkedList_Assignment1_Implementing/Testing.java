 import java.util.Scanner;
public class Testing
{
	private static DoublyLinkedList newList;
	private static int stopCount = 0;

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);

		String[] str = new String[3];
		str[0] = "1";
		str[1] = "2";
		str[2] = "3";

		newList = new DoublyLinkedList(str);

		System.out.println(newList);

		System.out.print(".add(Test) - ");
		System.out.println(newList.add("Test"));
		System.out.print(".add(Test 2) - ");
		System.out.println(newList.add("Test 2"));
		System.out.print(".add(Test 3) - ");
		System.out.println(newList.add("Test 3"));
		System.out.println(".add(2, Test 5)");
		newList.add(2,"Test 5");

		System.out.print(".get(2) - ");
		System.out.println(newList.get(2));

		System.out.println(newList);

		System.out.print(".get(1) - ");
		System.out.println((String)(newList.get(1)));
		System.out.print(".get(4) - ");
		System.out.println((String)(newList.get(4)));

		System.out.print(".remove(Test) - ");
		System.out.println(newList.remove("Test"));

		System.out.println(newList);

		System.out.print(".indexOf(Test 2) - ");
		System.out.println(newList.indexOf("Test 2"));
		System.out.println(".add(4, added at)");
		newList.add(4, "added at");
		System.out.println(".set(newList.indexOf(Test 2), Test 2.1)");
		newList.set(newList.indexOf("Test 2"), "Test 2.1");

		System.out.println(newList);

		System.out.print(".remove(5) - ");
		System.out.println(newList.remove(5));

		System.out.println(newList);

		System.out.print(".contains(Test 3) - ");
		System.out.println(newList.contains("Test 3"));
		System.out.print(".contains(Test 1) - ");
		System.out.println(newList.contains("Test 1"));

		newList.printBackwards();

		System.out.print("Press Enter to exit...");
		in.nextLine();
	}

	private static void stop(){try{Thread.sleep(10000);} catch(Exception ex){}stopCount++;System.out.println(stopCount);}
}