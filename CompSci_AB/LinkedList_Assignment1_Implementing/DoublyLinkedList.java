// Implements a singly-linked list.

import java.util.Iterator;

public class DoublyLinkedList implements Iterable<Object>
{
  private ListNode2 head;
  private ListNode2 tail;
  private ListNode2 currentNode;
  private int nodeCount;
  private int currentPosition;

  // Constructor: creates an empty list
  public DoublyLinkedList()
  {
	head = null;
	tail = null;
	currentNode = null;
	nodeCount = 0;
	currentPosition = 0;
  }

  // Constructor: creates a list that contains
  // all elements from the array values, in the same order
  public DoublyLinkedList(Object[] values)
  {
	tail = null;
	for (Object value : values) // for each value to insert
	{
	  ListNode2 node = new ListNode2(value, null, null);
	  ListNode2 prev = null;
	  if (head == null)
		head = node;
	  else
	  {
		tail.setNext(node);
		prev = tail;
	  }
	  tail = node;    // update tail
	  tail.setPrevious(prev);


	  if(currentNode == null)
	  {
	  	currentNode = head;
	  	currentPosition = 0;
	  }
	}

	nodeCount = values.length;
  }

  // Returns true if this list is empty; otherwise returns false.
  public boolean isEmpty()
  {
	return nodeCount == 0;
  }

  // Returns the number of elements in this list.
  public int size()
  {
	return nodeCount;
  }

  // Returns true if this list contains an element equal to obj;
  // otherwise returns false.
  public boolean contains(Object obj)
  {
  	ListNode2 myNode = head;
	for(Object ob : this)
	{
		if(obj.equals(ob)) return true;
		myNode = myNode.getNext();
	}
	return false;
  }

  // Returns the index of the first element in equal to obj;
  // if not found, retunrs -1.
  public int indexOf(Object obj)
  {
  	ListNode2 myNode = head;
	for(int i = 0; i < nodeCount; i++)
	{
		if(obj.equals(myNode.getValue())) return i;
		myNode = myNode.getNext();
	}
	return -1;
  }

  // Adds obj to this collection.  Returns true if successful;
  // otherwise returns false.
  public boolean add(Object obj)
  {
	if(head == null)
	{
  		head = new ListNode2(obj, null, null);
  		tail = head;  //update tail
  		head.setNext(tail);
  		currentNode = head;
  		currentPosition = 0;
	}
	else
	{
		ListNode2 newNode = new ListNode2(obj, tail, null);
		tail.setNext(newNode);
		tail = newNode;
	}

	nodeCount++;

	return true;
  }

  // Removes the first element that is equal to obj, if any.
  // Returns true if successful; otherwise returns false.
  public boolean remove(Object obj)
  {
	if(head == null)
		return false;
	else
	{
		/********DEBUG**********
		System.out.println("Here it is! " + this.toString());
		System.out.println("nodeCount: " + nodeCount);
		/********DEBUG**********/

		for(int j = 0; j < nodeCount; j++)
		{
			/********DEBUG**********
			System.out.println(currentNode + "; j: " + j);
			try{Thread.sleep(1000);} catch(Exception ex){}
			/********DEBUG**********/

			/********DEBUG**********
			System.out.println("pos: " + currentPosition);
			/********DEBUG**********/

			if(currentNode.getValue().equals(obj))
			{
				/********DEBUG**********
				System.out.println("remove at j: " + j);
				try{Thread.sleep(1000);} catch(Exception ex){}
				/********DEBUG**********/

				removeCurrent();
				return true;
			}

			if(currentNode.getNext() == null)
			{
				currentNode = head;
				currentPosition = 0;
			}
			else
			{
				currentNode = currentNode.getNext();
				currentPosition++;
			}
		}

		return false;
	}
  }

  //remove node at currentPosition
  private void removeCurrent()
  {
  		ListNode2 temp = new ListNode2(null, currentNode.getPrevious(), currentNode.getNext());

		if(currentNode.getPrevious() == null)
		{
			head = temp.getNext();
			currentNode = head.getNext();
		}
		else
		{
			temp.getPrevious().setNext(temp.getNext());
			currentNode = temp.getNext();
			//currentPosition++;
		}

		if(temp.getNext() != null)
			temp.getNext().setPrevious(temp);

		temp.setNext(null);
		temp.setPrevious(null);
		nodeCount--;
  }

  // Returns the i-th element.
  public Object get(int i)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount || i < 0) throw new IndexOutOfBoundsException();

  	moveTo(i);

	return currentNode.getValue();
  }

  // Replaces the i-th element with obj and returns the old value.
  public Object set(int i, Object obj)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount || i < 0) throw new IndexOutOfBoundsException();

  	moveTo(i);

	Object oldObj = currentNode.getValue();
	currentNode.setValue(obj);
	return oldObj;
  }

  // Inserts obj to become the i-th element. Increments the size
  // of the list by one.
  public void add(int i, Object obj)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount || i < 0) throw new IndexOutOfBoundsException();

  	moveTo(i);

  	/*System.out.println("cp: " + currentPosition);
  	System.out.println("prev: " + currentNode.getPrevious().getValue());
	System.out.println("next: " + currentNode.getNext().getValue());*/
  	ListNode2 temp = new ListNode2(obj, currentNode, currentNode.getNext());
  	temp.getNext().setPrevious(temp);

	//currentNode.getNext().setPrevious(temp);
  	currentNode.setNext(temp);


  	nodeCount++;
  }

  // Removes the i-th element and returns its value.
  // Decrements the size of the list by one.
  public Object remove(int i)
  {
	if(head == null || i >= nodeCount)
		throw new IndexOutOfBoundsException();
	else
	{


		//System.out.println(currentNode.getPrevious().getValue());
  		moveTo(i);
  		Object val = currentNode.getValue();

		ListNode2 temp = new ListNode2(null, currentNode.getPrevious(), currentNode.getNext());

		if(currentNode.getPrevious() == null)
		{
			head = temp.getNext();
			currentNode = head.getNext();
		}
		else
		{
			temp.getPrevious().setNext(temp.getNext());
			currentNode = temp.getNext();
			//currentPosition++;
		}


		temp.setNext(null);
		temp.setPrevious(null);
		nodeCount--;
		return val;
	}
  }

  // Returns a string representation of this list.
  public String toString()
  {
	String asString = "[";
	ListNode2 myNode = head;

	/*for(int i = 0; i < nodeCount; i++)
	{
		if(i > 0)
			asString += ", ";
		asString += myNode.getValue().toString();
		myNode = myNode.getNext();
	}*/
	boolean first = true;

	for(Object ob : this)
	{
		if(!first)
			asString += ", ";
		else
			first = false;

		asString += myNode.getValue().toString();
		myNode = myNode.getNext();
	}
	asString += "]";
	return asString;
  }

  // Returns an iterator for this collection.
  public Iterator<Object> iterator()
  {
	return new DoublyLinkedListIterator(head);
  }

  private void moveTo(int index)
  {
  	//make sure we haven't gone off the edge...
  	if(index  >= nodeCount || index  < 0) throw new IndexOutOfBoundsException();

  	//if(currentPosition == index) return;

  	while(currentPosition != index)
	{
		/********DEBUG**********
		System.out.println("index: " + index + "; currentPosition: " + currentPosition);
		/********DEBUG**********/

		if(index > currentPosition)
		{
			currentNode = currentNode.getNext();
			currentPosition++;
			/********DEBUG**********
			System.out.println("1 index: " + index + "; currentPosition: " + currentPosition);
			/********DEBUG**********/
		}
		else
		{
			currentNode = currentNode.getPrevious();
			currentPosition--;
			/********DEBUG**********
			System.out.println("2 index: " + index + "; currentPosition: " + currentPosition);
			/********DEBUG**********/
		}

		if(currentNode == null)
			currentNode = head;

		/********DEBUG**********
		System.out.println("currentPosition: " + currentPosition + "; value: " + currentNode.getValue());
		/********DEBUG**********/
	}
  }

    //Print the list backwards
  public void printBackwards()
  {
  	if(nodeCount == 0)
  		throw new IndexOutOfBoundsException();

  	ListNode2 curr = tail;

  	while(curr != null)
  	{
  		System.out.println(curr.getValue());
  		curr = curr.getPrevious();
  	}
  }

  //print the list
  public void printList()
  {
  	for(Object ob : this)
  		System.out.println(ob);
  }

  public ListNode2 find(Object obj)
  {
  	ListNode2 node = head;
  	while(node != null)
		if(node.getValue().equals(obj))
			return node;
		else
			node = node.getNext();
	return null;
  }

  public void clear()
  {
  	head = new ListNode2(null, null, null);
  	tail = head;
  }
}