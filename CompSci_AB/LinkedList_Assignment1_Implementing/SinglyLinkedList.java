// Implements a singly-linked list.

import java.util.Iterator;

public class SinglyLinkedList implements Iterable<Object>
{
  private ListNode head;
  private ListNode tail;
  private int nodeCount;

  // Constructor: creates an empty list
  public SinglyLinkedList()
  {
	head = null;
	tail = null;
	nodeCount = 0;
  }

  // Constructor: creates a list that contains
  // all elements from the array values, in the same order
  public SinglyLinkedList(Object[] values)
  {
	tail = null;
	for (Object value : values) // for each value to insert
	{
	  ListNode node = new ListNode(value, null);
	  if (head == null)
		head = node;
	  else
		tail.setNext(node);
	  tail = node;    // update tail
	}

	nodeCount = values.length;
  }

  // Returns true if this list is empty; otherwise returns false.
  public boolean isEmpty()
  {
	return nodeCount == 0;
  }

  // Returns the number of elements in this list.
  public int size()
  {
	return nodeCount;
  }

  // Returns true if this list contains an element equal to obj;
  // otherwise returns false.
  public boolean contains(Object obj)
  {
  	ListNode myNode = head;
	for(Object ob : this)
	{
		if(obj.equals(ob)) return true;
		myNode = myNode.getNext();
	}
	return false;
  }

  // Returns the index of the first element in equal to obj;
  // if not found, retunrs -1.
  public int indexOf(Object obj)
  {
  	ListNode myNode = head;
	for(int i = 0; i < nodeCount; i++)
	{
		if(obj.equals(myNode.getValue())) return i;
		myNode = myNode.getNext();
	}
	return -1;
  }

  // Adds obj to this collection.  Returns true if successful;
  // otherwise returns false.
  public boolean add(Object obj)
  {
	if(head == null)
	{
  		head = new ListNode(obj, null);
  		tail = head.getNext();  //update tail
	}
	else
	{
		ListNode newNode = new ListNode(obj, null);
		tail.setNext(newNode);
		tail = newNode;
	}

	nodeCount++;

	return true;
  }

  // Removes the first element that is equal to obj, if any.
  // Returns true if successful; otherwise returns false.
  public boolean remove(Object obj)
  {
	if(head == null)
		return false;
	else
	{
		ListNode node = head;
		ListNode last = head;

		while(node.getNext() != null)
		{
			if(node.getValue().equals(obj))
				break;
			last = node;
			node = node.getNext();
		}

		if(!node.getValue().equals(obj)) return false;

		ListNode temp = new ListNode(null, null);
		temp.setNext(node.getNext());

		last.setNext(temp.getNext());

		temp.setNext(null);
		nodeCount--;
		return true;
	}
  }


  // Returns the i-th element.
  public Object get(int i)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount) throw new IndexOutOfBoundsException();

  	ListNode myNode = head;
	for(int j = 0; j < i; j++)
		myNode = myNode.getNext();

	return myNode.getValue();
  }

  // Replaces the i-th element with obj and returns the old value.
  public Object set(int i, Object obj)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount) throw new IndexOutOfBoundsException();

  	ListNode myNode = head;
	for(int j = 0; j < i; j++)
		myNode = myNode.getNext();

	myNode.setValue(obj);

	tail = myNode.getNext();
	return true;
  }

  // Inserts obj to become the i-th element. Increments the size
  // of the list by one.
  public void add(int i, Object obj)
  {
  	//make sure we haven't gone off the edge...
  	if(i >= nodeCount) throw new IndexOutOfBoundsException();

  	ListNode temp = new ListNode(obj, null);
  	ListNode pos = head;

  	for(int j = 0; j < i; j++)
  		pos = pos.getNext();

  	temp.setNext(pos.getNext());

  	pos.setNext(temp);

  	nodeCount++;
  }

  // Removes the i-th element and returns its value.
  // Decrements the size of the list by one.
  public Object remove(int i)
  {
	if(head == null)
		return false;
	else
	{
		ListNode node = head;
		ListNode last = head;

		for(int j = 0; j < i; j++)
		{
			last = node;
			node = node.getNext();
		}

		//if(!node.getValue().equals(obj)) return false;

		ListNode temp = new ListNode(null, null);
		temp.setNext(node.getNext());

		last.setNext(temp.getNext());

		temp.setNext(null);
		nodeCount--;
		return true;
	}
  }

  // Returns a string representation of this list.
  public String toString()
  {
	String asString = "[";
	ListNode myNode = head;

	for(int i = 0; i < nodeCount; i++)
	{
		if(i > 0)
			asString += ", ";
		asString += myNode.getValue().toString();
		myNode = myNode.getNext();
	}
	asString += "]";

	return asString;
  }

  // Returns an iterator for this collection.
  public Iterator<Object> iterator()
  {
	return new SinglyLinkedListIterator(head);
  }

  //Print the list backwards
  public void printBackwards()
  {
  	if(nodeCount == 0)
  		throw new IndexOutOfBoundsException();

  	if(head.getNext() != null)
  		printBackwards(head.getNext());
  	System.out.println(head.getValue());
  }

  //The recursive element of the above method
  private void printBackwards(ListNode node)
  {
  	if(node.getNext() != null)
  		printBackwards(node.getNext());
  	System.out.println(node.getValue());
  }

  //print the list
  public void printList()
  {
  	for(Object ob : this)
  		System.out.println(ob);
  }

  public ListNode find(Object obj)
  {
  	ListNode node = head;
  	while(node.getNext() != null)
		if(node.getValue().equals(obj))
			return node;
	return null;
  }

  public void clear()
  {
  	head = new ListNode(null, null);
  	tail = head;
  }
}
