public class location
{
	private int x;
	private int y;
	private int dir;
	
	public location()
	{
		x = 0;
		y = 0;
		dir = 1;
	}
	
	public void setLocation(int xSet, int ySet, int dirSet)
	{
		x = xSet;
		y = ySet;
		dir = dirSet;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public int getDir()
	{
		return dir;
	}
	
}