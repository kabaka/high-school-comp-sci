import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/*
    this is the Controller component
*/

class Maze extends JFrame implements ActionListener
{

    private MazeView view;
    private MazeModel model;
    private JButton solveButton, stepButton, pauseButton, finishButton;
    private Timer timer;

    Maze()
    {
        super("Maze");

        // build the buttons
        JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        solveButton = new JButton("Solve");
        solveButton.addActionListener(this);
        controlPanel.add(solveButton);
        
        stepButton = new JButton("Step");
        stepButton.addActionListener(this);
        controlPanel.add(stepButton);
        
        pauseButton = new JButton("Pause");
        pauseButton.addActionListener(this);
        controlPanel.add(pauseButton);
        
        finishButton = new JButton("Finish");
        finishButton.addActionListener(this);
        controlPanel.add(finishButton);

        // build the view
        view = new MazeView();
        view.setBackground(Color.white);

        // put buttons, view together
        Container c = getContentPane();
        c.add(controlPanel, BorderLayout.NORTH);
        c.add(view, BorderLayout.CENTER);

        // construct the timer and the listener to step the maze
        timer = new Timer(0, new ActionListener()
                          {
                              public void actionPerformed(ActionEvent e)
                              {
                                  model.stepMaze();
                                  if(!model.notSolved())
                                  {
                                  	timer.stop();
		                            solveButton.setEnabled(false);
		                            finishButton.setEnabled(false);
						            pauseButton.setEnabled(false);
						            stepButton.setEnabled(false);
                                  }
                              }
                          }
                         );

        // build the model
        model = new MazeModel(view);
    }

    public void run()
    {
        timer.setCoalesce(true);
        timer.start();
    }

    // when user clicks on solve, the timer is started
    // each tick advances the maze solution.
    public void actionPerformed(ActionEvent e)
    {
        JButton b = (JButton)e.getSource();
        if ( b == solveButton )
        {
            this.run();
            solveButton.setEnabled(false);
            pauseButton.setEnabled(true);
        }
        
        else if ( b == stepButton )
        {
            this.run();
            //solveButton.setEnabled(false);
        }
        else if ( b == pauseButton )
        {
            timer.stop();
            solveButton.setEnabled(true);
            pauseButton.setEnabled(false);
        }
        else if ( b == finishButton )
        {
            solveButton.setEnabled(false);
            while(model.notSolved())
            	model.stepMaze();
            pauseButton.setEnabled(false);
            stepButton.setEnabled(false);
            
        }
    }

    public static void main(String[] args)
    {
        Maze conway = new Maze();
        conway.addWindowListener(new WindowAdapter()
                                 {
                                     public void windowClosing(WindowEvent e)
                                     {
                                         System.exit(0);
                                     }
                                 }
                                );
        //conway.setSize(570, 640);
        conway.setSize(1140, 1280);
        conway.show();
    }
}
