/**
 * @(#)SomeStuff.java
 * A short program to illustrate recursion and
 *
 * @author Kyle Johnson
 * @version 1.00 2006/11/7
 */
 import java.util.Scanner;
public class SomeStuff {


    /**
     * @param args the command line arguments
     */

    static Scanner in;
    static String mode = "main menu";

    public static void main(String[] args)
    {
    	try
    	{
	    	in = new Scanner(System.in);
	    	String inputText = "";
			boolean exit = false;

			while(!exit)
			{
		        System.out.println("Some Recursive Stuff...");
		        System.out.println("Choose:");
		        System.out.println("1 - Triangle");
		        System.out.println("2 - Prime Number");
		        System.out.println("Q - Quit");

		        inputText = prompt("Choice");

		        if(inputText.toLowerCase().equals("q"))
		        	exit = true;
		        else
		        {
			        Integer choice = Integer.valueOf(inputText);
			        if(choice == 1)
			        {
			        	mode = "triangle";
				        System.out.println("Answer: " + triangle(Double.valueOf(prompt("Find Triangle of"))));
			        }
			        else if(choice == 2)
			        {
			        	mode = "prime numbers";
						System.out.println("Answer: " + primeNumber(Double.valueOf(prompt("Test for Prime"))));
			        }
		        }
			}
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    }

    /* get input from user with a formatted prompt
     *
     * text: text of the prompt shown to user
     */
    private static String prompt(String text)
    {
    	System.out.print("[RecursiveStuff(" + mode + "): " + text + "]: ");
    	return in.nextLine();
    }

    /* triangle
     */
     private static double triangle(double number)
     {
     	if(number == 1)
     		return 1;
     	return number + triangle(number - 1);
     }

     /* prime numbers
      */
	private static boolean primeNumber(double number)
	{
		if(number == 1)
			return true;
		return primeNumber(number, number - 1);
	}

	private static boolean primeNumber(double number, double number2)
	{
		if(number2 == 1)
			return true;
		else if(number % number2 == 0)
			return false;
		return primeNumber(number, number2 - 1);
	}
}

