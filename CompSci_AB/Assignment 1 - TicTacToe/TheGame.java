/*TicTacToe game to illustrate the use of
 *2-d arrays.
 *Started: 8-30-06
 *Completed: 
 */
 import java.util.Scanner;
public class TheGame
{
	static int[][] theBoard = new int[3][3]; // 0 = empty, 1 = O, 2 = X
	static boolean playerMove = true; // true = O, false = X
	
	public static void main(String [] args)
	{
		resetBoard();
		
		System.out.println("Welcome to TicTacToe! Want to play? .. Good.");
		System.out.println("When prompted, type the row then column of your move");
		System.out.println("( i.e. Move for X > 2 3  will put an X on the right");
		System.out.println("  center position of the board )");
		while(doMove());
	}
	
	public static void resetBoard()
	{
		for(int i = 0; i < theBoard.length; i++)
			for(int j = 0; j < theBoard[0].length; j++)
				theBoard[i][j] = 0;
	}
	
	public static boolean doMove()
	{
		Scanner input = new Scanner(System.in);
		
		if(checkForWin())
		{
			char winner = 'O';
			if(playerMove) winner = 'X';
			System.out.println("The winner is " + winner + "!!");
			System.out.print("Play Again? [y]> ");
			String answer = input.nextLine().trim().toLowerCase();
			if(answer.equals("y") || answer.equals("")) 
			{
				resetBoard();
				return true;
			}
			else return false;
		}
		else
		if(checkForDraw())
		{
			System.out.println("The winner is.. nobody.. again..");
			System.out.print("Play Again? [y]> ");
			String answer = input.nextLine().trim().toLowerCase();
			if(answer.equals("y") || answer.equals("")) 
			{
				resetBoard();
				return true;
			}
			else return false;
		}
		System.out.println("Type Q to quit, R to restart");
		
		printBoard();
		
		if(!playerMove) System.out.print("Move for X > ");
		else System.out.print("Move for O > ");
		
		String temp = input.nextLine().trim();
		if(temp.toLowerCase().trim().equals("q")) return false;
		else if(temp.toLowerCase().trim().equals("r"))
		{
			resetBoard();
			return true;
		}
		
		Scanner process = new Scanner(temp).useDelimiter(" ");
		int row = 0, col = 0;
		try
		{
			row = process.nextInt() - 1;
			col = process.nextInt() - 1;
		}
		catch(Exception ex)
		{
			System.out.println("Invalid input!");
			System.out.println("Must be in format:\n");
			System.out.println("Move for X > 2 3  will put an X on the right");
			System.out.println("center position of the board");
			return true;
		}
		
		if(row > 2) row = 2;
		else if(row < 0) row = 0;
		if(col > 2) col = 2;
		else if(col < 0) col = 0;
		
		if(theBoard[row][col] == 0)
		{
			if(playerMove) theBoard[row][col] = 1;
			else theBoard[row][col] = 2;
			playerMove = !playerMove;
		}
		else System.out.println("That position is TAKEN! Pick another...");
		return true;
	}
	
	public static boolean checkForWin()
	{
		if(theBoard[0][1] > 0)
		{
			if(theBoard[0][0] == theBoard[0][1] && theBoard[0][1] == theBoard[0][2]) return true;
		}
		if(theBoard[1][1] > 0)
		{
			if(theBoard[1][0] == theBoard[1][1] && theBoard[1][1] == theBoard[1][2]) return true;
		}
		if(theBoard[2][1] > 0)
		{
			if(theBoard[2][0] == theBoard[2][1] && theBoard[2][1] == theBoard[2][2]) return true;
		}
		
		if(theBoard[1][0] > 0)
		{
			if(theBoard[0][0] == theBoard[1][0] && theBoard[1][0] == theBoard[2][0]) return true;
		}
		if(theBoard[1][1] > 0)
		{
			if(theBoard[0][1] == theBoard[1][1] && theBoard[1][1] == theBoard[2][1]) return true;
		}
		if(theBoard[1][2] > 0)
		{
			if(theBoard[0][2] == theBoard[1][2] && theBoard[1][2] == theBoard[2][2]) return true;
		}
		
		if(theBoard[1][1] > 0)
		{
			if(theBoard[0][0] == theBoard[1][1] && theBoard[1][1] == theBoard[2][2]) return true;
			else if(theBoard[0][2] == theBoard[1][1] && theBoard[1][1] == theBoard[2][0]) return true;
		}

		return false;
	}
	
	public static boolean checkForDraw()
	{
		for(int i = 0; i < theBoard.length; i++)
			for(int j = 0; j < theBoard[0].length; j++)
				if(theBoard[i][j] == 0) return false;
		return true;
	}
	
	public static void printBoard()
	{
		for(int i = 0; i < theBoard.length; i++)
		{
			for(int j = 0; j < theBoard[0].length; j++)
			{
				if(theBoard[i][j] == 0) System.out.print(" � ");
				else if(theBoard[i][j] == 1) System.out.print(" O ");
				else System.out.print(" X ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	
}