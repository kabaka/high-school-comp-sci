/**
 * @(#)Person.java
 * Representation of a person with a first and last name
 *
 * @author Kyle Johnson
 * @version 1.00 2007/1/3
 */


public class Person implements Comparable
{
	String firstName;
	String lastName;

    public Person()
    {
    	firstName = "";
    	lastName = "";
    }

    public Person(String last)
    {
    	lastName = last.trim();
    }

    public Person(String first, String last)
    {
    	firstName = first.trim();
    	lastName = last.trim();
    }

    public String getFirst()
    {
    	return firstName;
    }

    public String getLast()
    {
    	return lastName;
    }

    public void setFirstName(String first)
    {
    	firstName = first.trim();
    }

    public void setLastName(String last)
    {
    	lastName = last.trim();
    }

    public void setName(String first, String last)
    {
    	firstName = first.trim();
    	lastName = last.trim();
    }

    public int compareTo(Object compare)
    {
    	int result = ((Person)compare).getLast().compareTo(getLast());
		if(result != 0)
			return result;

		return ((Person)compare).getFirst().compareTo(getFirst());
    }

	public String toString()
	{
		return firstName.trim() + " " + lastName.trim();
	}

}