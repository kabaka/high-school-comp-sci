/**
 * @(#)Movie.java
 *
 *
 * @author Kyle Johnson
 * @version 1.00 2007/1/3
 */
 import java.util.LinkedList;

public class Movie implements Comparable
{
	String movieName;
	LinkedList<Person> actors;
	LinkedList<Person> directors;
	int year;


    public Movie()
    {
    	actors = new LinkedList<Person>();
    	movieName = "";
    	directors = new LinkedList<Person>();
    	year = 0;
    }

    public Movie(String name, LinkedList actorsList, LinkedList directorList, int _year)
    {
    	movieName = name.trim();
    	actors = actorsList;
    	directors = directorList;
    	year = _year;
    }

    public String getTitle()
    {
    	return movieName;
    }

    public int getYear()
    {
    	return year;
    }

    public LinkedList getDirectors()
    {
    	return directors;
    }

    public int compareTo(Object compare)
    {
		return ((Movie)compare).getTitle().compareTo(getTitle());
    }

    public String toString()
    {
    	return year + " " + movieName + " " + actors.toString() + " " + directors.toString();
    }

}