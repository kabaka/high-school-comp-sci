/**
 * @(#)MovieDatabaseDriver.java
 *
 *
 * @author Kyle Johnson
 * @version 1.00 2007/1/4
 */
 import java.util.Scanner;

public class MovieDatabaseDriver
{

    public static void main(String [] args)
    {
    	Scanner kb = new Scanner(System.in);

    	try
    	{
	    	MovieDatabase db = new MovieDatabase();
	    	db.loadDatabase();

	    	db.sortMovies();
	    	db.sortActors();

	    	db.printActors();
	    	System.out.println();
	    	System.out.println();
	    	db.printMovies();
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	System.out.println("Press any key to continue...");
    	kb.nextLine();
    }


}