/**
 * @(#)MovieDatabase.java
 *
 *
 * @author Kyle Johnson
 * @version 1.00 2007/1/3
 */
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.Scanner;
 import java.util.LinkedList;
 import java.util.StringTokenizer;
 import java.util.TreeSet;
 import java.io.File;
 import java.util.Stack;

public class MovieDatabase
{

	ArrayList<Movie> movies;
	TreeSet<Person> actors;

    public MovieDatabase()
    {
    	movies = new ArrayList<Movie>();
    	actors = new TreeSet<Person>();
    }

    public void loadDatabase()
    {
    	Scanner fiReader;

    	try
    	{
    		fiReader = new Scanner(new File("movies.txt"));
	    	int lineNum = 1;

	    	while(fiReader.hasNextLine())
	    	{
	    		String line = fiReader.nextLine();
	    		String lineFirst = line.substring(0, line.indexOf("  "));
	    		line = line.substring(line.indexOf("  "));

	    		try
	    		{
		    		StringTokenizer tok = new StringTokenizer(line);

		    		int year = Integer.valueOf(lineFirst.substring(0,4));

		    		String movieName = lineFirst.substring(lineFirst.indexOf(" "));

		    		String temp = tok.nextToken().trim();
		    		LinkedList<Person> actorsTemp = new LinkedList<Person>();

		    		StringTokenizer actorTok = new StringTokenizer(line.substring(line.indexOf(temp)), ",");
		    		temp = actorTok.nextToken().trim();

		    		while(!temp.startsWith("Dir:"))
		    		{
		    			if(temp.contains("Dir: "))
		    				temp = temp.substring(0, temp.indexOf("Dir: ")).trim();

		    			Person actor = new Person(temp.substring(0, temp.indexOf(" ")).trim(),
		    				temp.substring(temp.indexOf(" ") + 1).replaceAll(",", "").trim());

		    			actorsTemp.add(actor);
		    			actors.add(actor);

						if(!actorTok.hasMoreTokens())
							break;
		    			temp = actorTok.nextToken().trim();
		    		}

		    		String director = line.substring(line.indexOf("Dir: ") + 4);

		    		StringTokenizer dirTok = new StringTokenizer(director, ",");
		    		LinkedList<Person> dirTemp = new LinkedList<Person>();

		    		while(dirTok.hasMoreTokens())
		    		{
		    			String strTemp = dirTok.nextToken().trim();
		    			dirTemp.add(new Person(strTemp.substring(0, strTemp.indexOf(" ")), strTemp.substring(strTemp.indexOf(" "))));
		    		}

		    		movies.add(new Movie(movieName, actorsTemp, dirTemp, year));
	    		}
	    		catch(Exception ex)
	    		{
	    			System.err.println("ERROR: Database file is not formatted correctly!");
	    			System.err.println("Line number " + lineNum);
	    			System.err.println(line);

	    			ex.printStackTrace();

	    			Scanner kb = new Scanner(System.in);
	    			kb.nextLine();
	    		}

	    		lineNum++;
	    	}

	    	fiReader.close();
    	}
    	catch(java.io.FileNotFoundException ex)
    	{
    		System.err.println("movies.txt needs to be located in the same directory");
    		System.err.println("  as the program!");
    	}
    }

    public void sortMovies()
    {
    	Collections.sort(movies);
    	Collections.reverse(movies);
    }

    public void sortActors()
    {
    	Object[] objs = actors.toArray();
    	ArrayList temp = new ArrayList();

    	for(int i = 0; i < objs.length; i++)
    		temp.add(objs[i]);

    	Collections.sort(temp);
    	Collections.reverse(temp);

    	actors = new TreeSet();

    	for(Object ob : temp)
    		actors.add((Person)ob);
    }

    public void printActors()
    {
    	System.out.println("There are " + actors.size() + " actors in the list:");
     	//System.out.println(actors.toString());

     	Stack actrsPrnt = new Stack();
     	Object[] actrsObjs = actors.toArray();

     	for(Object obj : actrsObjs)
     		actrsPrnt.push(obj);

     	while(!actrsPrnt.empty())
     		System.out.println(((Person)(actrsPrnt.pop())).toString());

    }

    public void printMovies()
    {
    	for(Movie mv : movies)
    		System.out.println(mv.toString().trim());
    }
}