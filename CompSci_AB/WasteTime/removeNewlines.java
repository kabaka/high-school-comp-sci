/**
 * @(#)removeNewlines.java
 *
 *
 * @author Kyle Johnson
 * @version 1.00 2006/11/9
 */
 import java.io.*;
 import java.util.StringTokenizer;
 import java.util.Scanner;
public class removeNewlines
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
   	{
	   	Scanner input = new Scanner(System.in);
   		try
   		{
	       	System.out.print("Enter path to sourcecode to make unmaintainable: ");

	       	String fileContents = "";

			BufferedReader reader = new BufferedReader(new FileReader(new File(input.nextLine())));

			String line = reader.readLine();
				System.out.println(line);
			while(line != null)
			{
				System.out.println(line);
				fileContents += line + " ";
				line = reader.readLine();
			}

			reader.close();
			System.out.println("DEBUG: CLOSED FILE");

			String finalContents = "";

			for(int i = 0; i < fileContents.length() - 2; i++)
			{
				if(fileContents.charAt(i) == ' ' && fileContents.charAt(i + 1) == ' ')
				{
					finalContents += fileContents.substring(0,i+1);
					fileContents = fileContents.substring(i+1);
				}
				else
				{
					finalContents += fileContents.substring(0,i);
					fileContents = fileContents.substring(i);
				}

			}
			finalContents = finalContents.replaceAll("\t","");

			/*StringTokenizer tok = new StringTokenizer(fileContents, " ");


			while(tok.hasMoreTokens())
			{
				finalContents += tok.nextToken() + " ";
			}*/

			System.out.println(finalContents);
   		}
   		catch(Exception ex)
   		{
   			ex.printStackTrace();
   		}
		input.nextLine();
    }
}
