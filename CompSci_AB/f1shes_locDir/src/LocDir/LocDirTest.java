import java.awt.Color;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.util.Random;

public class LocDirTest
{
  public static void main(String[] args)
  {
    LocationDisplay display = new LocationDisplay(20, 20);

    createFrame(display, 20, 20);
    
    //createSquare(display, 0, 0, 19);
    
    LocDir ld1 = new LocDir(new Location(3, 7), Direction.SOUTH);
    
    randomFour(display);

  ////////////////  Start Location/Direction Code here ////////////
  ///////////////   remove the sample code            ////////////
/*
    try
    { Thread.sleep(500);
    }
    catch(InterruptedException ex){}

    Location loc = new Location(5, 10);
    display.setCellColor(loc, Color.green);
    System.out.println(loc);

    try
    { Thread.sleep(500);
    }
    catch(InterruptedException ex){}

    loc = new Location(10, 5);
    display.setCellColor(loc, Color.yellow);
    System.out.println(loc);

    try
    { Thread.sleep(500);
    }
    catch(InterruptedException ex){}

    loc = new Location(5, 5);
    Direction dir = Direction.WEST;
    display.setCellColor(loc, Color.blue);
    display.setCellDir(loc, dir);
    System.out.println(loc + " " + dir);

    try
    { Thread.sleep(500);
    }
    catch(InterruptedException ex){}

    loc = new Location(10, 10);
    dir = dir.toRight();
    display.setCellColor(loc, Color.red);
    display.setCellDir(loc, dir);
    System.out.println(loc + " " + dir);

    try
    { Thread.sleep(1000);
    }
    catch(InterruptedException ex){}

    display.clearCellColor(loc);
    display.clearCellDir(new Location(5,5));
*/
  }
  
  private static void createSquare(LocationDisplay disp, int x, int y, int length)
  {
    Location loc;
    for(int i = x; i <= x + length; i++)
	for(int j = y; j <= y + length; j++)
	{
	    if(i == x || i == x + length || j == y || j == y + length)
	    {
		loc = new Location(i,j);
		disp.setCellColor(loc, Color.blue);
		System.out.println(loc);
	    }
	}
  }

  private static void randomFour(LocationDisplay disp)
  {
      
  }

  private static void createFrame(JPanel display, int xCoord, int yCoord)
  {
    int width = (int)Math.round((display).getSize().getWidth());
    int height = (int)Math.round((display).getSize().getHeight());

    JFrame frame = new JFrame();
    frame.setVisible(true);

    width = width + frame.getInsets().right + frame.getInsets().left;
    height = width + frame.getInsets().top + frame.getInsets().bottom;

    frame.setSize(width,height);
    frame.setLocation(xCoord, yCoord);

    frame.getContentPane().add(display);
    frame.setVisible(true);
  }

}