/**
 * @(#)btNode.java
 *
 * btNode Class
 * Node used in a Binary Tree
 *
 * @author Kyle Johnson
 * @version 1.00 2006/12/5
 */
public class btNode<E extends Comparable>
{
	private btNode<E> leftBranch = null;
	private btNode<E> rightBranch = null;
	private E value = null;
	private boolean delete = false;

	/**
	 * Method btNode
	 *
	 * Creates a new Binary Tree node with no value and no children
	 */
	public btNode()
	{
	}

	/**
	 * Method btNode
	 *
	 * Creates a Binary Tree node with a specified value
	 */
	public btNode(E newValue)
	{
		value = newValue;
	}

	/**
	 * Method btNode
	 *
	 * Creates a Binary Tree node with a specified value and children
	 */
	public btNode(E newValue, btNode<E> newRightBranch, btNode<E> newLeftBranch)
	{
		value = newValue;
		rightBranch = newRightBranch;
		leftBranch = newLeftBranch;
	}

	/**
	 * Method setValue
	 *
	 * Set the value of this node to newValue
	 */
	public void setValue(E newValue)
	{
		value = newValue;
	}

	/**
	 * Method setRight
	 *
	 * Set the right branch in this node to newRight
	 */
	public void setRight(btNode<E> newRight)
	{
		rightBranch = newRight;
	}

	/**
	 * Method setLeft
	 *
	 * Set the left branch of this node to newLeft
	 */
	public void setLeft(btNode<E> newLeft)
	{
		leftBranch = newLeft;
	}

	/**
	 * Method getRight
	 *
	 *
	 * @return rightBranch
	 *
	 */
	public btNode<E> getRight()
	{
		return rightBranch;
	}

	/**
	 * Method getLeft
	 *
	 *
	 * @return leftBranch
	 *
	 */
	public btNode<E> getLeft()
	{
		return leftBranch;
	}

	/**
	 * Method getValue
	 *
	 *
	 * @return value
	 *
	 */
	public E getValue()
	{
		return value;
	}

	public boolean insertNode(E value)
	{
		if(getValue() != null)
		{
			if(getValue().compareTo(value) > 0)
			{
				if(getLeft() == null)
				{
					setLeft(new btNode<E>(value));
					return true;
				}
				else
					getLeft().insertNode(value);
			}
			else if(getValue().compareTo(value) < 0)
			{
				if(getRight() == null)
				{
					setRight(new btNode<E>(value));
					return true;
				}
				else
					getRight().insertNode(value);
			}
			else
				return false;
		}
		else
		{
			setValue(value);
			return true;
		}
		return true;
	}

	public E findNode(E target)
	{
		if(getValue() != null)
		{
			if(getValue().compareTo(target) > 0)
			{
				if(getLeft() == null)
					return null;
				else
					return getLeft().findNode(target);
			}
			else if(getValue().compareTo(target) < 0)
			{
				if(getRight() == null)
					return null;
				else
					return getRight().findNode(target);
			}
			else
				return target;
		}
		else
			return null;
	}

	public btNode getNode(E target)
	{
		if(getValue() != null)
		{
			if(getValue().compareTo(target) > 0)
			{
				if(getLeft() == null)
					return null;
				else
					return getLeft().getNode(target);
			}
			else if(getValue().compareTo(target) < 0)
			{
				if(getRight() == null)
					return null;
				else
					return getRight().getNode(target);
			}
			else
				return this;
		}
		else
			return null;
	}

	public void printPreOrder()
	{
		System.out.println(this.getValue());
		if(getLeft() != null)
		{
			getLeft().printInorder();
		}
		if(getRight() != null)
		{
			getRight().printInorder();
		}
	}

	public void printPostOrder()
	{
		if(getLeft() != null)
		{
			getLeft().printInorder();
		}
		if(getRight() != null)
		{
			getRight().printInorder();
		}
		System.out.println(this.getValue());
	}

	public void printInorder()
	{
		if(getLeft() != null)
		{
			getLeft().printInorder();
		}
			System.out.println(this.getValue());
		if(getRight() != null)
		{
			getRight().printInorder();
		}
	}

	public E findMax()
	{
		return ((getFarRight(this)).getValue());
	}

	public E findMin()
	{
		return ((getFarLeft(this)).getValue());
	}

	public boolean isLeaf(E target)
	{
		if(getValue() != null)
		{
			if(getValue().compareTo(target) > 0)
			{
				if(getLeft() == null)
					return false;
				else
					return getLeft().isLeaf(target);
			}
			else if(getValue().compareTo(target) < 0)
			{
				if(getRight() == null)
					return false;
				else
					return getRight().isLeaf(target);
			}
			else
				if(getRight() == null && getLeft() == null)
					return true;
				else
					return false;
		}
		else
			if(getRight() == null && getLeft() == null)
				return true;
			else
				return false;
	}

	private boolean isLeaf(btNode node)
	{
		if(node.getRight() == null && node.getLeft() == null)
			return true;
		return false;
	}

	public int leafCount()
	{
		int current = 0;

		if(getLeft() != null)
		{
			current += getLeft().leafCount();
		}
		if(getRight() != null)
		{
			current += getRight().leafCount();
		}

		if(getRight() == null && getLeft() == null)
			current++;

		return current;
	}

	public int sumNodes()
	{
		int current = 0;

		if(!(getValue() instanceof Number))
			throw new IllegalStateException("tree must be populated with only numbers");

		if(getLeft() != null)
		{
			current += getLeft().sumNodes();
		}
		if(getRight() != null)
		{
			current += getRight().sumNodes();
		}

		current += ((Number)getValue()).doubleValue();
		return current;
	}

	private btNode getParent(btNode target, btNode previous)
	{
		if(getValue() != null)
		{
			if(getValue().compareTo(target.getValue()) > 0)
			{
				if(getLeft() == null)
					return null;
				else
					return getLeft().getParent(target, this);
			}
			else if(getValue().compareTo(target.getValue()) < 0)
			{
				if(getRight() == null)
					return null;
				else
					return getRight().getParent(target, this);
			}
			else
				return previous;
		}
		else
			return null;
	}


	private btNode<E> getFarRight(btNode start)
	{
		if(start.getRight() != null)
			return start.getFarRight(start.getRight());
		return start;
	}

	private btNode<E> getFarLeft(btNode start)
	{
		if(start.getLeft() != null)
			return start.getFarLeft(start.getLeft());
		return start;
	}

	public btNode delete(E value)
	{
		btNode targetNode = getNode(value);

		if(targetNode == (null))
			return null;

		btNode parentOfTarget = getParent(targetNode, this);

		System.err.print("Deleting " + targetNode.getValue() + ", child of ");
		System.err.println(parentOfTarget.getValue());

		if(isLeaf(targetNode))
		{
			System.err.println("Deleting Leaf");

			if(parentOfTarget.getRight() != null &&
			   parentOfTarget.getRight().equals(targetNode))
				parentOfTarget.setRight(null);
			else
				parentOfTarget.setLeft(null);

			return null;
		}
		return(delete(targetNode, parentOfTarget));
	}

	private btNode delete(btNode target, btNode parent)
	{
		System.err.println("entered delete method 2");
		System.err.println("parent: " + parent.getValue());

		btNode far;
		if(delete) // get leftmost of right
			far = getFarLeft(target.getRight());
		else // get rightmost of left
			far = getFarRight(target.getLeft());

		delete = !delete;

		System.err.println("far: " + far.getValue());

		/*if(target.getLeft() != far)
			far.setLeft(parent.getLeft());
		if(target.getRight() != far)
			far.setRight(parent.getRight());*/

		/*System.err.println("farR: " + far.getRight().getValue());
		System.err.println("farL: " + far.getLeft().getValue());*/

		btNode farParent = getParent(far, this);

		System.err.println("farParent: " + farParent.getValue());

		/*if(farParent.getRight() != null && farParent.getRight().equals(far))
			farParent.setRight(null);
		else
			farParent.setLeft(null);*/

		if(parent != null)
		{
			if(parent.getRight() == target)
				parent.setRight(far);
			else if(parent.getLeft() == target)
				parent.setLeft(far);
		}
		/*else if(farParent == target)
			farParent.removeBranch(target);*/

		//if(farParent.hasChild())
		far.setLeft(target.getLeft());
		far.setRight(target.getRight());

		return far;
	}

	private boolean hasChild(btNode child)
	{
		return (getLeft() == child || getRight() == child);
	}

	private void removeBranch(btNode remove)
	{
		if(getRight() == remove)
			setRight(null);
		else
			setLeft(null);
	}

}
