/**
 * @(#)BinaryTree.java
 *
 * BinaryTree Class
 * Class for the data structure of a Binary Tree
 *
 * @author Kyle Johnson
 * @version 1.00 2006/12/5
 */
public class BinaryTree<E extends Comparable>
{
	private btNode<E> rootNode = null;
	private int nodeCount;


	/**
	 * Method BinaryTree
	 *
	 *
	 */
	public BinaryTree()
	{
		rootNode = new btNode<E>();
		nodeCount = 0;
	}

	/**
	 * Method BinaryTree
	 *
	 *
	 */
	public BinaryTree(btNode newRoot)
	{
		rootNode = newRoot;
		nodeCount = 0;
	}

	/**
	 * Method retrieve
	 *
	 *
	 * @return value at specified ...?
	 *
	 */
	public E retrieve(E obj)
	{
		return rootNode.findNode(obj);
	}

	/**
	 * Method insert
	 *
	 * Insert a given value into the tree
	 */
	public void insert(E obj)
	{
		if(rootNode.insertNode(obj))
			nodeCount++;
	}

	/**
	 * Method printPreOrder
	 *
	 *
	 */
	public void printPreOrder()
	{
		rootNode.printPreOrder();
	}

	/**
	 * Method printPostOrder
	 *
	 *
	 */
	public void printPostOrder()
	{
		rootNode.printPostOrder();
	}

	/**
	 * Method printInorder
	 *
	 *
	 */
	public void printInorder()
	{
		rootNode.printInorder();
	}

	public boolean isLeaf(E value)
	{
		return rootNode.isLeaf(value);
	}

	public int leafCount()
	{
		return rootNode.leafCount();
	}

	public double sumNodes()
	{
		return rootNode.sumNodes();
	}

	public int nodeCount()
	{
		return nodeCount;
	}

	public E findMax()
	{
		return rootNode.findMax();
	}

	public E findMin()
	{
		return rootNode.findMin();
	}

	public boolean delete(E value)
	{
		btNode node = rootNode.delete(value);
		if(node == null)
			return false;
		if(value.equals(rootNode.getValue()))
			rootNode = node;
		nodeCount--;
		return true;
	}
}
