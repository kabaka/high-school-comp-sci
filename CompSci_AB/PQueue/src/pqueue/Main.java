/*
 * Main.java
 *
 * Created on February 8, 2007, 9:25 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package pqueue;

 import java.util.PriorityQueue;
/**
 *
 * @author Johnsky
 */
public class Main
{
    
    /** Creates a new instance of Main - BAD, DON'T DO THIS!! */
    private Main()
    {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
	PriorityQueue<String> que = new PriorityQueue<String>();
	for(int i = 1; i <= 5 ; i++)
	    que.add("String" + i);
	que.add("String" + 0);
	
	while(!que.isEmpty())
	    System.out.println(que.remove());
	
	que.remove();
    }
    
}
